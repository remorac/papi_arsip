<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@uploads', dirname(dirname(__DIR__)) . '/uploads');
Yii::setAlias('@bundles', dirname(dirname(__DIR__)) . '/bundles');


$external_path_file = Yii::getAlias('@frontend/../UPLOADS.txt');
if (file_exists($external_path_file)) {
    $external_path = file_get_contents($external_path_file);
    if ($external_path) {
        Yii::setAlias('@uploads', $external_path);
    }
}

$external_path_file = Yii::getAlias('@frontend/../BUNDLES.txt');
if (file_exists($external_path_file)) {
    $external_path = file_get_contents($external_path_file);
    if ($external_path) {
        Yii::setAlias('@bundles', $external_path);
    }
}