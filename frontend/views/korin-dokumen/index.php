<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinDokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumen KORIN';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-dokumen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dokumen KORIN', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'korin_id',
            [
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin&KorinSearch[no_surat]='.$data->korin->no_surat);
                    }
                },
            ],
            //'dokumen_upload_id',
            [
                'label' => 'No. Dokumen',
                'format' => 'raw',
                'value' => function($data){
                    if($data->dokumenUpload){
                        return Html::a($data->dokumenUpload->no_dokumen.' ('.$data->dokumenUpload->dokumenJenis->nama.')', 
                            '?r=/dokumen-upload/view&id='.$data->dokumenUpload->id);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
