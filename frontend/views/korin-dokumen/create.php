<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinDokumen */

$this->title = 'Create Dokumen KORIN';
$this->params['breadcrumbs'][] = ['label' => 'Dokumen KORIN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-dokumen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
