<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinDokumen */

$this->title = 'Update Dokumen KORIN: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen KORIN', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-dokumen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
