<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinDokumen */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen KORIN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-dokumen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'korin.no_surat',
            [
                'attribute' => 'korin.no_surat',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => Html::a(
                    $model->korin ? $model->korin->no_surat : '', 
                    $model->korin ? '?r=/korin/view&id=' . $model->korin->id : ''
                    )
            ],
            //'dokumenUpload.pdf_filename',
            [
                'attribute' => 'dokumenUpload.no_dokumen',
                'label' => 'No. Dokumen',
                'format' => 'raw',
                'value' => Html::a(
                    $model->dokumenUpload ? $model->dokumenUpload->no_dokumen.' ('.$model->dokumenUpload->dokumenJenis->nama.')' : '', 
                    $model->dokumenUpload ? '?r=/dokumen-upload/view&id=' . $model->dokumenUpload->id : ''
                    )
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
