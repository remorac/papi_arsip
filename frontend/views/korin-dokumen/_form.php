<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use frontend\models\Korin;
use frontend\models\DokumenUpload;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinDokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-dokumen-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'id' => 'korin-dokumen'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'korin_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Korin::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <?= $form->field($model, 'dokumen_upload_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(DokumenUpload::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
