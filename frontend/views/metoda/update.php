<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Metoda */

$this->title = 'Update Metoda: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Metoda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metoda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
