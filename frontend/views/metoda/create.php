<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Metoda */

$this->title = 'Create Metoda';
$this->params['breadcrumbs'][] = ['label' => 'Metoda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metoda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
