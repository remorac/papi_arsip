<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */

$this->title = 'Update Gas Circulation: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-circulation-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
