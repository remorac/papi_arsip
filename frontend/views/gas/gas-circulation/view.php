<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */

$this->title = $model->no_reference;
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <div class="row">
    <div class="col-md-4">
    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view'],
        'template' => "<tr><th width=37%>{label}</th><td>{value}</td></tr>",
        'attributes' => [
            'id',
            'no_reference',
            [
                'attribute'=>'io',
                'value'=> $model->io == '1' ? 'IN' : 'OUT'
            ],
            'date',
            'gasPic.name:raw:Procurement PIC',
            'gasUserUnit.name:raw:User Unit',
            'gasUserPic.name:raw:User PIC',
            'gasVendorUnit.name:raw:Vendor Unit',
            'gasVendorPic.name:raw:Vendor PIC',
            // 'created_at:datetime',
            // 'updated_at:datetime',
            // 'createdBy.username:ntext:Created By',
            // 'updatedBy.username:ntext:Updated By',
        ],
    ]) ?>

    <big class="text-primary">Jumlah Tabung : <b><?= \frontend\models\GasCirculationTube::find()->where("gas_circulation_id = '".$model->id."'")->sum("count") ?></b></big>

    </div>

    <div class="col-md-8">
    <div class="" style="padding-top:0px; margin-bottom: 10px">
        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-inline'],
        ]); ?>

        <?= $form->field($modelDetail, 'gas_circulation_id')->hiddenInput()->label(false) ?>
        
        <?= $form->field($modelDetail, 'gas_tube_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\frontend\models\GasTube::find()->all(), 'id', 'DropdownDisplayItem'),
            'options' => ['placeholder' => 'pilih tabung ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false) ?>
        <?= $form->field($modelDetail, 'count')->textInput()->label('&nbsp; &nbsp; Quantity') ?>
        
        <div class="form-group" style="margin-top: -10px; padding-left: 20px">
            <?= Html::submitButton($modelDetail->isNewRecord ? 'Tambah Tabung' : 'Update', ['class' => $modelDetail->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        
        <?php ActiveForm::end(); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'gas/gas-circulation-tube',
                'template' => '<div class="act-danger">{delete}</div>',
            ],

            // 'id',
            // 'gas_circulation_id',
            // 'gas_tube_id',
            'gasTube.nomor:text:Tabung',
            'gasTube.gasType.name:text:Isi',
            'gasTube.gasOwner.name:text:Owner',
            'count',
        ],
    ]); ?>
    
    </div>
    </div>
</div>
