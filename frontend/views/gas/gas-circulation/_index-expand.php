<?php

use frontend\models\GasOwner;
use frontend\models\GasType;
use frontend\models\GasTube;
use frontend\models\GasCirculationTube;

?>

<div class="col-md-11 col-md-offset-1">

<table class="table table-condensed table-bordered">
	<tr>
		<th>No</th>
		<th>Isi</th>
		<th>Tabung</th>
		<th>Owner</th>
		<th>Jumlah</th>
	</tr>
<?php
	$i = 0;
	foreach ($model->gasCirculationTubes as $row) {
		$i++;
		echo "<tr>
			<td width=25px>".$i."</td>
			<td>".$row->gasTube->gasType->name."</td>
			<td>".$row->gasTube->nomor."</td>
			<td>".$row->gasTube->gasOwner->name."</td>
			<td>".$row->count."</td>
		</tr>";
	}
?>
</table>
</div>