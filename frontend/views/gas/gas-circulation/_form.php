<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

use frontend\models\GasPic;
use frontend\models\GasVendorUnit;
use frontend\models\GasVendorPic;
use frontend\models\GasUserUnit;
use frontend\models\GasUserPic;
use frontend\models\GasOwner;
use frontend\models\GasType;
use frontend\models\GasTube;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-circulation-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>
    
    <?= $form->field($model, 'no_reference')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'io')->dropDownList(['1'=>'IN', '2'=>'OUT'],['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'date')
        ->widget(DatePicker::classname(), [    
            // 'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd',
            ],
            'options' => [
                'value' => date("Y-m-d"),
            ],
        ]); 
    ?>

    <?= $form->field($model, 'gas_vendor_unit_id')->dropDownList(ArrayHelper::map(GasVendorUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    <?= $form->field($model, 'gas_vendor_pic_id')->dropDownList(ArrayHelper::map(GasVendorPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    <?= $form->field($model, 'gas_user_unit_id')->dropDownList(ArrayHelper::map(GasUserUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    <?= $form->field($model, 'gas_user_pic_id')->dropDownList(ArrayHelper::map(GasUserPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    <?= $form->field($model, 'gas_pic_id')->dropDownList(ArrayHelper::map(GasPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <br>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
