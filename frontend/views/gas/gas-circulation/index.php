<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasCirculationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gas Circulations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-index box box-body box-primary">

    <?php
      $gridColumns = [
          [
              'class' => 'kartik\grid\ExpandRowColumn',
              'expandIcon'=>'<span class="glyphicon glyphicon-plus"></span>',
              'collapseIcon'=>'<span class="glyphicon glyphicon-minus"></span>',
              'value' => function ($model, $key, $index, $column) {
                      return GridView::ROW_COLLAPSED;
              },
              'detail'=>function ($model, $key, $index, $column) {
                      return Yii::$app->controller->renderPartial('_index-expand.php', ['model'=>$model]);
              },
              'detailOptions'=>[
                  'class'=> 'kv-state-enable',
              ],
          ],

          [
              'class' => 'yii\grid\ActionColumn',
              'headerOptions'=>['class'=>'opsi'],
              'template' => '
                  <div class="btn btn-xs btn-default act-success">{view}</div>
                  <div class="btn btn-xs btn-default act-warning">{update}</div>
                  <div class="btn btn-xs btn-default act-danger">{delete}</div>
              ',
          ],
          ['class' => 'yii\grid\SerialColumn'],

          // ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
          [
              'attribute'=>'io',
              'format'=>'raw',
              'headerOptions'=>['class'=>'opsi-sm'],
              'value'=> function($data) {
                  return $data->io == '1' ? "IN" : "OUT";
              }
          ],
          'no_reference:text:No Referensi',
          [
              'attribute'=>'date',
              'format'=>'raw',
              'headerOptions'=>['class'=>'opsi'],
          ],
          // 'time',
          'gasVendorUnit.name:text:Vendor',
          'gasVendorPic.name:text:Vendor PIC',
          'gasUserUnit.name:text:User',
          'gasUserPic.name:text:User PIC',
          'gasPic.name:text:Procurement PIC',
          // 'created_at',
          // 'updated_at',
          // 'created_by',
          // 'updated_by',
      ];

      $fullExportMenu = ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumns,
          'target' => ExportMenu::TARGET_SELF,
          'fontAwesome' => true,
          'pjaxContainerId' => 'kv-pjax-container',
          'dropdownOptions' => [
              'label' => 'Export',
              'class' => 'btn btn-default',
              'itemsBefore' => [
                  '<li class="dropdown-header">Export All Data</li>',
              ],
          ],
      ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        // 'floatHeader'=>true,
        // 'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'columns' => $gridColumns,
        'toolbar' => [
            
                
                    Html::a('<i class="glyphicon glyphicon-plus"></i> &nbsp;Create', ['create'], [
                        'class' => 'btn btn-success',
                        'title' => 'Refresh',
                    ]),
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> &nbsp;Refresh', ['index'], [
                        'class' => 'btn btn-default',
                        'title' => 'Refresh',
                    ]),
            
            $fullExportMenu,
            '{toggleData}',
        ],
        'export' => ['label' => '&nbsp;Export&nbsp;'],
        // 'export' => false,
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        'exportConfig' => [
            GridView::EXCEL => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
            GridView::CSV => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
            // GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],

        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
    ]); ?>

</div>
