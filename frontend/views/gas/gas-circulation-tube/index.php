<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasCirculationTubeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search & Export';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-tube-index box box-body box-primary">

    <?php
      $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
          'attribute' => 'gas_circulation.id',
          'label' => 'No Transaksi',
          'value' => function($data) {
            return $data->gasCirculation->id;
          }
        ],
        'gasCirculation.gasVendorUnit.name:text:Vendor',
        [
          'attribute' => 'gas_user_unit.name',
          'label' => 'Area',
          'value' => function($data) {
            return $data->gasCirculation->gasUserUnit->name;
          }
        ],
        [
          'attribute' => 'gas_circulation.date',
          'label' => 'Date',
          'value' => function($data) {
            return $data->gasCirculation->date;
          }
        ],
        [
          'attribute' => 'gas_circulation.io',
          'label' => 'In/Out',
          'value' => function($data) {
            return $data->gasCirculation->io == 1 ? "In" : "Out";
          },
          'filter' => ['' => '[all]', '1' => 'In', '2' => 'Out'],
        ],
        'gas_tube_id:text:ID Tabung',
        [
          'attribute' => 'gas_tube.nomor',
          'label' => 'Nomor',
          'value' => function($data) {
            return $data->gasTube->nomor;
          }
        ],
        'gasTube.gasOwner.name:text:Owner',
        'gasTube.gasType.name:text:Type',
        'count',
        'created_at:datetime',
        // 'updated_at',
        // 'created_by',
        // 'updated_by',

        // ['class' => 'yii\grid\ActionColumn'],
      ];

      $fullExportMenu = ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumns,
          'target' => ExportMenu::TARGET_SELF,
          'fontAwesome' => true,
          'pjaxContainerId' => 'kv-pjax-container',
          'dropdownOptions' => [
              'label' => 'Export',
              'class' => 'btn btn-default',
              'itemsBefore' => [
                  '<li class="dropdown-header">Export All Data</li>',
              ],
          ],
      ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        // 'floatHeader'=>true,
        // 'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'columns' => $gridColumns,
        'toolbar' => [
            [
                // 'content'=>
                //     Html::a('<i class="glyphicon glyphicon-repeat"></i> &nbsp;Refresh', ['weekly/refresh'], [
                //         'class' => 'btn btn-default',
                //         'title' => 'Refresh',
                //     ]),
            ],
            $fullExportMenu,
            '{toggleData}',
        ],
        'export' => ['label' => '&nbsp;Export&nbsp;'],
        // 'export' => false,
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        'exportConfig' => [
            GridView::EXCEL => ['filename' => 'GAS_'.date('Y-m-d')],
            GridView::CSV => ['filename' => 'GAS_'.date('Y-m-d')],
            // GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],

        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
    ]); ?>

</div>
