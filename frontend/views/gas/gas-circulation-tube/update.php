<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculationTube */

$this->title = 'Update Gas Circulation Tube: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulation Tubes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-circulation-tube-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
