<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculationTube */

$this->title = 'Create Gas Circulation Tube';
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulation Tubes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-tube-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
