<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculationTube */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-circulation-tube-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gas_circulation_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'gas_tube_id')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
