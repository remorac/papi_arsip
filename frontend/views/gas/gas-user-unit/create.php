<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasUserUnit */

$this->title = 'Create User Unit';
$this->params['breadcrumbs'][] = ['label' => 'User Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-user-unit-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
