<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasType */

$this->title = 'Create Gas Type';
$this->params['breadcrumbs'][] = ['label' => 'Gas Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-type-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
