<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Gas Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-type-view box box-body box-success">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:ntext:Created By',
            'updatedBy.username:ntext:Updated By',
        ],
    ]) ?>

</div>
