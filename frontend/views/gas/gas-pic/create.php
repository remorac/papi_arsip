<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasPic */

$this->title = 'Create Procurement PIC';
$this->params['breadcrumbs'][] = ['label' => 'PICs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-pic-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
