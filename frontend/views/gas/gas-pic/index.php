<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasPicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Procurement PIC';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-pic-index box box-body box-primary">

    <p>
        <?= Html::a('Create PIC', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'contentOptions'=>['class'=>'serial-column']],
            ['class' => 'yii\grid\ActionColumn', 'contentOptions'=>['class'=>'opsi-sm']],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            'name',

        ],
    ]); ?>

</div>
