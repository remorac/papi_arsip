<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use miloschuman\highcharts\Highcharts;

use frontend\models\GasCirculation;
use frontend\models\GasCirculationTube;
use frontend\models\GasVendorUnit;
use frontend\models\GasUserUnit;
use frontend\models\GasTube;
use frontend\models\GasOwner;
use frontend\models\GasType;

	$all_o2 = GasTube::find()->joinWith(['gasType'])->where("gas_owner_id = '1' and gas_type.name = 'Oxygen'")->sum('count');
	$all_co2 = GasTube::find()->joinWith(['gasType'])->where("gas_owner_id = '1' and gas_type.name = 'CO2'")->sum('count');
	$all_lpg = GasTube::find()->joinWith(['gasType'])->where("gas_owner_id = '1' and gas_type.name = 'LPG'")->sum('count');
	$all_n2 = GasTube::find()->joinWith(['gasType'])->where("gas_owner_id = '1' and gas_type.name = 'N2'")->sum('count');

	$all_o2 = empty($all_o2) ? 0 : $all_o2 ;
	$all_co2 = empty($all_co2) ? 0 : $all_co2 ;
	$all_lpg = empty($all_lpg) ? 0 : $all_lpg ;
	$all_n2 = empty($all_n2) ? 0 : $all_n2 ;

	$o2  = "";
	$co2 = "";
	$lpg = "";
	$n2  = "";
	$arr_name = "";

	$c_date 	= GasCirculation::find()->groupBy("date")->count();

	$vendors 	= GasVendorUnit::find()->orderBy("name")->all();
	$areas 		= GasUserUnit::find()->orderBy("name")->all();
	$owners 	= GasOwner::find()->all();
	$types 		= GasType::find()->all();
	foreach ($areas as $area) {

		$arr_name.= "'".$area->name."', ";

		$c_o2 	= GasCirculationTube::find()
					->joinWith(["gasCirculation"])
					->joinWith(["gasTube"])
					->joinWith(["gasType"])
			 		->where("gas_user_unit_id = '".$area->id."' and gas_type.name = 'Oxygen'")
			 		->groupBy("gas_circulation_tube.id")
			 		->count();
		$c_co2 	= GasCirculationTube::find()
					->joinWith(["gasCirculation"])
					->joinWith(["gasTube"])
					->joinWith(["gasType"])
			 		->where("gas_user_unit_id = '".$area->id."' and gas_type.name = 'CO2'")
			 		->groupBy("gas_circulation_tube.id")
			 		->count();
		$c_lpg 	= GasCirculationTube::find()
					->joinWith(["gasCirculation"])
					->joinWith(["gasTube"])
					->joinWith(["gasType"])
			 		->where("gas_user_unit_id = '".$area->id."' and gas_type.name = 'LPG'")
			 		->groupBy("gas_circulation_tube.id")
			 		->count();
		$c_n2 	= GasCirculationTube::find()
					->joinWith(["gasCirculation"])
					->joinWith(["gasTube"])
					->joinWith(["gasType"])
			 		->where("gas_user_unit_id = '".$area->id."' and gas_type.name = 'N2'")
			 		->groupBy("gas_circulation_tube.id")
			 		->count();

		$o2.= "{";
		$o2.= "y : ".number_format($c_o2/$c_date, 2).", ";
		$o2.= "url : '?r=highlight/list&barang_jasa_id=1&activity_id='";
		$o2.= "}, ";

		$co2.= "{";
		$co2.= "y : ".number_format($c_co2/$c_date).", ";
		$co2.= "url : '?r=highlight/list&barang_jasa_id=1&activity_id='";
		$co2.= "}, ";

		$lpg.= "{";
		$lpg.= "y : ".number_format($c_lpg/$c_date, 2).", ";
		$lpg.= "url : '?r=highlight/list&barang_jasa_id=1&activity_id='";
		$lpg.= "}, ";

		$n2.= "{";
		$n2.= "y : ".number_format($c_n2/$c_date, 2).", ";
		$n2.= "url : '?r=highlight/list&barang_jasa_id=1&activity_id='";
		$n2.= "}, ";

	}

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasCirculationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gas Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-report-index">

	<div class="row">

		<div class="col-md-12" style="margin-bottom:10px" id="count">
	    <div class="col-md-12 paper">
	    	<div class="col-xs-12"><br><h4>Jumlah Tabung Milik PTSP</h4></div>
	    	<div class="col-md-3">
	          <div class="small-box bg-aqua paper">
	            <div class="inner">
	              <h3 style="color:white; font-weight:100"><?=$all_o2?><sup style="font-size: 20px"></sup></h3>
	              <p>Oxygen</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fire-extinguisher"></i>
	            </div>
	            <p class="small-box-footer"> </p>
	            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
	          </div>
	        </div>

	    	<div class="col-md-3">
	          <div class="small-box bg-aqua paper">
	            <div class="inner">
	              <h3 style="color:white; font-weight:100"><?=$all_co2?><sup style="font-size: 20px"></sup></h3>
	              <p>Carbon Dioxide</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fire-extinguisher"></i>
	            </div>
	            <p class="small-box-footer"> </p>
	            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
	          </div>
	        </div>

	    	<div class="col-md-3">
	          <div class="small-box bg-aqua paper">
	            <div class="inner">
	              <h3 style="color:white; font-weight:100"><?=$all_lpg?><sup style="font-size: 20px"></sup></h3>
	              <p>Liquified Petroleum Gas</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fire-extinguisher"></i>
	            </div>
	            <p class="small-box-footer"> </p>
	            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
	          </div>
	        </div>

	    	<div class="col-md-3">
	          <div class="small-box bg-aqua paper">
	            <div class="inner">
	              <h3 style="color:white; font-weight:100"><?=$all_n2?><sup style="font-size: 20px"></sup></h3>
	              <p>Nitrogen</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fire-extinguisher"></i>
	            </div>
	            <p class="small-box-footer"> </p>
	            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
	          </div>
	        </div>
	    </div>
	    </div>

	    <div class="col-md-12" style="margin-bottom:10px" id="average">
		    <div id="" class="paper col-md-12" style="padding-top:0px;padding-bottom: 10px; height:466px !important; overflow:hidden">
		    	<div class="col-xs-12"><br><h4>Rata-rata Pemakaian Tabung Gas Per Hari <span class="pull-right">
		    		<select class="form-control" id="avg_filter" name="avg_filter">
		    			<option value="0">ALL</option>
		    			<option value="O2">O2</option>
		    			<option value="CO2">CO2</option>
		    			<option value="LPG">LPG</option>
		    			<option value="N2">N2</option>
		    		</select>
		    	</span></h4></div>

		    	<div id="chart0" class="col-xs-12">
		    	<?php
					echo Highcharts::widget();
		    	?>
		    	</div>
		    	<div id="chartO2" class="col-xs-12">
		    	<?php
					echo Highcharts::widget();
		    	?>
		    	</div>
		    	<div id="chartCO2" class="col-xs-12">
		    	<?php
					echo Highcharts::widget();
		    	?>
		    	</div>
		    	<div id="chartLPG" class="col-xs-12">
		    	<?php
					echo Highcharts::widget();
		    	?>
		    	</div>
		    	<div id="chartN2" class="col-xs-12">
		    	<?php
					echo Highcharts::widget();
		    	?>
		    	</div>
		    </div>
	    </div>

	    <div class="col-md-12" style="margin-bottom:10px" id="distribution">
		    <div id="" class="paper col-md-12" style="padding-top:0px;padding-bottom: 25px;">
		    	<div class="col-xs-12"><br><h4>Sebaran Tabung</h4></div>

		    	<div class="col-xs-12">
	    			<table class="table table-condensed table-bordered table-striped">
	    				<tr>
	    					<th rowspan="2" style="text-align:center; vertical-align:middle !important; width:200px">A R E A</th>
	    					<th rowspan="2" style="text-align:center; vertical-align:middle !important">Total</th>
	    					<th colspan="5">O2</th>
	    					<th colspan="5">CO2</th>
	    					<th colspan="5">LPG</th>
	    					<th colspan="5">N2</th>
	    				</tr>
	    				<tr>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    				</tr>
	    				<?php
			    		foreach ($areas as $area) {
		    				?>
			    			<tr>
			    				<td><?= $area->name ?></td>
			    				<th>
										<div class="panel panel-success" style="padding:0; margin:0;">
										    <div class="panel-heading pointer" style="padding:0 2px; margin:0;" data-toggle="collapse" data-parent="" href="#<?= $area->id ?>">
										        <b><?= $area->tubesUsedCount ?></b>
										    </div>
										    <div id="<?= $area->id ?>" class="panel-collapse collapse">
										    <div class="panel-body" style="padding:0 2px; margin:0">
													<?php
													 foreach ($area->tubesUsed as $tubeUsed) {
														 echo $tubeUsed->gasTube->nomor."<br>";
													 }
												 ?>
										    </div>
										    </div>
										</div>

									</th>
			    				<td><?= $area->tubesUsedCountO2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountO2($owner->id) == 0 ? "" : $area->tubesUsedCountO2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountCO2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountCO2($owner->id) == 0 ? "" : $area->tubesUsedCountCO2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountLPG() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountLPG($owner->id) == 0 ? "" : $area->tubesUsedCountLPG($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountN2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountN2($owner->id) == 0 ? "" : $area->tubesUsedCountN2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    			</tr>
		    				<?php
		    			}
			    		?>
			    		<tr>
			    			<th style="text-align:center;">TOTAL</th>
			    			<th>
			    				<?=
			    					GasCirculationTube::find()
			    						->joinWith(['gasCirculation'])
			    						->where("
			    							gas_user_unit_id != '' and
							                gas_circulation.`date` >
							                    (select max(`date`) from gas_circulation a, gas_circulation_tube b where
							                        a.id = b.gas_circulation_id and
							                        a.io = '1' and
							                        b.gas_tube_id = gas_circulation_tube.gas_tube_id
							                    )
			    							")
			    						->sum("gas_circulation_tube.count");
			    				?>
			    			</th>
			    			<?php
			    				foreach ($types as $type) {
			    					echo "<th>".$type->tubesUsedCountUser()."</th>";
			    					foreach ($owners as $owner) {
		    							$c = $type->tubesUsedCountUser($owner->id) == 0 ? "" : $type->tubesUsedCountUser($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
			    				}
	    					?>
			    		</tr>
	    			</table>
		    	</div>

		    	<div class="col-xs-12">
	    			<table class="table table-condensed table-bordered table-striped">
	    				<tr>
	    					<th rowspan="2" style="text-align:center; vertical-align:middle !important; width:200px">V E N D O R</th>
	    					<th rowspan="2" style="text-align:center; vertical-align:middle !important">Total</th>
	    					<th colspan="5">O2</th>
	    					<th colspan="5">CO2</th>
	    					<th colspan="5">LPG</th>
	    					<th colspan="5">N2</th>
	    				</tr>
	    				<tr>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    					<th>Jumlah</th>
	    					<?php
	    						foreach ($owners as $owner) {
	    							echo "<th>$owner->initial</th>";
	    						}
	    					?>
	    				</tr>
	    				<?php
			    		foreach ($vendors as $area) {
		    				?>
			    			<tr>
			    				<td><?= $area->name ?></td>
			    				<th><?= $area->tubesUsedCount ?></th>
			    				<td><?= $area->tubesUsedCountO2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountO2($owner->id) == 0 ? "" : $area->tubesUsedCountO2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountCO2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountCO2($owner->id) == 0 ? "" : $area->tubesUsedCountCO2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountLPG() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountLPG($owner->id) == 0 ? "" : $area->tubesUsedCountLPG($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    				<td><?= $area->tubesUsedCountN2() ?></td>
		    					<?php
		    						foreach ($owners as $owner) {
		    							$c = $area->tubesUsedCountN2($owner->id) == 0 ? "" : $area->tubesUsedCountN2($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
		    					?>
			    			</tr>
		    				<?php
		    			}
			    		?>
			    		<tr>
			    			<th style="text-align:center;">TOTAL</th>
			    			<th>
			    				<?=
			    					GasCirculationTube::find()
			    						->joinWith(['gasCirculation'])
			    						->where("
			    							gas_vendor_unit_id != '' and
							                gas_circulation.`date` >
							                    (select max(`date`) from gas_circulation a, gas_circulation_tube b where
							                        a.id = b.gas_circulation_id and
							                        a.io = '1' and
							                        b.gas_tube_id = gas_circulation_tube.gas_tube_id
							                    )
			    							")
			    						->sum("gas_circulation_tube.count");
			    				?>
			    			</th>
			    			<?php
			    				foreach ($types as $type) {
			    					echo "<th>".$type->tubesUsedCountVendor()."</th>";
			    					foreach ($owners as $owner) {
		    							$c = $type->tubesUsedCountVendor($owner->id) == 0 ? "" : $type->tubesUsedCountVendor($owner->id);
		    							echo "<td>".$c."</td>";
		    						}
			    				}
	    					?>
			    		</tr>
	    			</table>
		    	</div>

		    </div>


	    </div>

    </div>

</div>

<?php
	$this->registerJs("
		$(function() {
		  	$('#chart0').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [
	         			".$arr_name."
	         		]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Tabung' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'O2',
		    			color: 'teal',
		      			data: [
		      				".$o2."
		      			],
		    		},
		    		{
		    			name: 'CO2',
		    			color: 'indianred',
		      			data: [
		      				".$co2."
		      			],
		    		},
		    		{
		    			name: 'LPG',
		    			color: 'orange',
		      			data: [
		      				".$lpg."
		      			],
		    		},
		    		{
		    			name: 'N2',
		    			color: 'blue',
		      			data: [
		      				".$n2."
		      			],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>


<?php
	$this->registerJs("
		$(function() {
		  	$('#chartO2').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [
	         			".$arr_name."
	         		]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Tabung' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'O2',
		    			color: 'teal',
		      			data: [
		      				".$o2."
		      			],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>


<?php
	$this->registerJs("
		$(function() {
		  	$('#chartCO2').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [
	         			".$arr_name."
	         		]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Tabung' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'CO2',
		    			color: 'indianred',
		      			data: [
		      				".$co2."
		      			],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>


<?php
	$this->registerJs("
		$(function() {
		  	$('#chartLPG').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [
	         			".$arr_name."
	         		]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Tabung' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'LPG',
		    			color: 'orange',
		      			data: [
		      				".$lpg."
		      			],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>


<?php
	$this->registerJs("
		$(function() {
		  	$('#chartN2').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [
	         			".$arr_name."
	         		]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Tabung' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'N2',
		    			color: 'blue',
		      			data: [
		      				".$n2."
		      			],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>

<?php
	$this->registerJs("

		$('#avg_filter').change(function() {
			$('#chart0').hide();
			$('#chartO2').hide();
			$('#chartCO2').hide();
			$('#chartLPG').hide();
			$('#chartN2').hide();
			if (this.value == '0') $('#chart0').show();
			if (this.value == 'O2') $('#chartO2').show();
			if (this.value == 'CO2') $('#chartCO2').show();
			if (this.value == 'LPG') $('#chartLPG').show();
			if (this.value == 'N2') $('#chartN2').show();
		});
	", \yii\web\VIEW::POS_READY);
?>
