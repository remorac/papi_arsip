<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasUserPic */

$this->title = 'Create User PIC';
$this->params['breadcrumbs'][] = ['label' => 'User PICs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-user-pic-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
