<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasUserPicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User PICs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-user-pic-index box box-body box-primary">

    <p>
        <?= Html::a('Create User PIC', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
