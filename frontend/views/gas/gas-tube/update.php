<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTube */

$this->title = 'Update Tabung Gas: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-tube-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
