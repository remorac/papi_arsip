<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasTubeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tabung Gas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-index box box-body box-primary">

    <p>
        <?= Html::a('Create Tabung Gas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            'gasType.name:text:Type',
            'gasOwner.name:text:Owner',
            'nomor',
            'count',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
