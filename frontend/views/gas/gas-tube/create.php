<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasTube */

$this->title = 'Create Tabung Gas';
$this->params['breadcrumbs'][] = ['label' => 'Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
