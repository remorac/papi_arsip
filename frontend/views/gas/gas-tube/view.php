<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTube */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-view box box-body box-success">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'gasType.name:text:Type',
            'gasOwner.name:text:Owner',
            'nomor',
            'count',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:ntext:Created By',
            'updatedBy.username:ntext:Updated By',
        ],
    ]) ?>

</div>
