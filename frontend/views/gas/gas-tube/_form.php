<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTube */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-tube-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gas_type_id')
            ->label('Type')
            ->dropDownList(ArrayHelper::map(\frontend\models\GasType::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'gas_owner_id')
            ->label('Owner')
            ->dropDownList(ArrayHelper::map(\frontend\models\GasOwner::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'nomor')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'count')->textInput(['maxlength' => 11]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
