<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasVendorUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendor Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-vendor-unit-index box box-body box-primary">

    <p>
        <?= Html::a('Create Vendor Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
