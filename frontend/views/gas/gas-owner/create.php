<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasOwner */

$this->title = 'Create Gas Owner';
$this->params['breadcrumbs'][] = ['label' => 'Gas Owners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-owner-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
