<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasOwner */

$this->title = 'Update Gas Owner: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Gas Owners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-owner-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
