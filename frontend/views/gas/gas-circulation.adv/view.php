<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */

$this->title = $model->no_reference;
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'options'=> ['class' => 'table  detail-view paper'],
                'template' => "<tr><th width=40%>{label}</th><td>{value}</td></tr>",
                'attributes' => [
                    'id',
                    'io',
                    'date',
                    'no_reference',
                    'gasVendorUnit.name:raw:Vendor Unit',
                    'gasVendorPic.name:raw:Vendor PIC',
                    'gasUserUnit.name:raw:User Unit',
                    'gasUserPic.name:raw:User PIC',
                    'gasPic.name:raw:Procurement PIC',
                    'created_at:datetime',
                    'updated_at:datetime',
                    'createdBy.username:ntext:Created By',
                    'updatedBy.username:ntext:Updated By',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <th width="50px" style="text-align:center">No</th>
                    <th style="text-align:left">Jenis</th>
                    <th style="text-align:left">Owner</th>
                    <th style="text-align:left">Tabung</th>
                    <th style="text-align:right">Qty</th>
                </tr>
                <?php
                    $i = 0;
                    foreach ($model->gasCirculationTubes as $tube) {
                        $i++;
                ?>
                <tr>
                    <td style="text-align:center"><?=$i?></td>
                    <td style="text-align:left"><?=$tube->gasTube->gasType->name?></td>
                    <td style="text-align:left"><?=$tube->gasTube->gasOwner->name?></td>
                    <td style="text-align:left"><?=$tube->gasTube->nomor?></td>
                    <td style="text-align:right"><?=$tube->count?></td>
                </tr>
                <?php 
                    }
                ?>
            </table>
        </div>
    </div>

</div>
