<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasCirculationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gas Circulations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gas Circulation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],    
        // 'floatHeader'=>true,
        // 'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'expandIcon'=>'<span class="glyphicon glyphicon-plus"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-minus"></span>',
                'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('_index-expand.php', ['model'=>$model]);
                },                                
                'detailOptions'=>[
                    'class'=> 'kv-state-enable',
                ],
            ],

            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            [
                'attribute'=>'io',
                'format'=>'raw',
                'value'=> function($data) {
                    return $data->io == '1' ? "IN" : "OUT";
                }
            ],
            'date',
            // 'time',
            'gasVendorUnit.name:text:Vendor',
            'gasVendorPic.name:text:Vendor PIC',
            'gasUserUnit.name:text:User',
            'gasUserPic.name:text:User PIC',
            'gasPic.name:text:PIC',
            'no_reference',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'toolbar' => [
            [
                'content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> &nbsp;Refresh', ['weekly/refresh'], [
                        'class' => 'btn btn-default', 
                        'title' => 'Refresh',
                    ]),
            ],
            '{export}',
            '{toggleData}',
        ],
        // 'export' => ['label' => '&nbsp;Export&nbsp;'],
        'export' => false,
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        'exportConfig' => [
            GridView::EXCEL => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
            GridView::CSV => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
            // GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],

        ],
        'panel'=>[
            'type'=>GridView::TYPE_INFO,
            'heading'=> false,
            'before'=>'<div class="btn">{summary}</div>',
        ],
    ]); ?>

</div>
