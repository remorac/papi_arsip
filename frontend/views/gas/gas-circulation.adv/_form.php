<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

use frontend\models\GasPic;
use frontend\models\GasVendorUnit;
use frontend\models\GasVendorPic;
use frontend\models\GasUserUnit;
use frontend\models\GasUserPic;
use frontend\models\GasOwner;
use frontend\models\GasType;
use frontend\models\GasTube;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-circulation-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-sm-12">
        
            <table class="table table-bordered paper">
                <tr>
                    <td width="100"><?= $form->field($model, 'io')->radioList(['1'=>'IN', '2'=>'OUT'])->label(false) ?></td>
                    
                    <?php if ($model->isNewRecord) { ?>
                    <td width="100"><?= $form->field($model, 'fromto')->radioList(['User'=>'User', 'Vendor'=>'Vendor'])->label(false) ?></td>
                    <?php } ?>

                    <td width="150">
                    <?php 
                        echo $form->field($model, 'date')
                        ->widget(DatePicker::classname(), [    
                            'type' => DatePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format'=>'yyyy-mm-dd',
                            ],
                            'options' => [
                                'value' => date("Y-m-d"),
                            ],
                        ]); 
                    ?>
                    </td>
                    <td width="150"><?= $form->field($model, 'no_reference')->textInput(['maxlength' => 200]) ?></td>
                    <td width="150"><?= $form->field($model, 'gas_pic_id')->dropDownList(ArrayHelper::map(GasPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?></td>
                    
                    <?php if ($model->isNewRecord) { ?>
                    <td class="vendor" style="background: rgb(225, 255, 182); margin: 0 !important;display: none">
                        <?= $form->field($model, 'gas_vendor_unit_id')->dropDownList(ArrayHelper::map(GasVendorUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                    </td>
                    <td width="150" class="vendor" style="background: rgb(225, 255, 182); margin: 0 !important;display: none">
                        <?= $form->field($model, 'gas_vendor_pic_id')->dropDownList(ArrayHelper::map(GasVendorPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                    </td>
                    <td class="area" style="background: rgb(182, 255, 242); margin: 0 !important;display: none">
                        <?= $form->field($model, 'gas_user_unit_id')->dropDownList(ArrayHelper::map(GasUserUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                    </td>
                    <td width="150" class="area" style="background: rgb(182, 255, 242); margin: 0 !important;display: none">
                        <?= $form->field($model, 'gas_user_pic_id')->dropDownList(ArrayHelper::map(GasUserPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                    </td>
                    <?php } else { ?>
                        <?php if (!empty($model->gas_vendor_unit_id)) { ?>
                            <td class="vendor" style="background: rgb(225, 255, 182); margin: 0 !important;">
                                <?= $form->field($model, 'gas_vendor_unit_id')->dropDownList(ArrayHelper::map(GasVendorUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                            </td>
                            <td width="150" class="vendor" style="background: rgb(225, 255, 182); margin: 0 !important;">
                                <?= $form->field($model, 'gas_vendor_pic_id')->dropDownList(ArrayHelper::map(GasVendorPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                            </td>
                        <?php } ?>
                        <?php if (!empty($model->gas_user_unit_id)) { ?>
                            <td class="area" style="background: rgb(182, 255, 242); margin: 0 !important;">
                                <?= $form->field($model, 'gas_user_unit_id')->dropDownList(ArrayHelper::map(GasUserUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                            </td>
                            <td width="150" class="area" style="background: rgb(182, 255, 242); margin: 0 !important;">
                                <?= $form->field($model, 'gas_user_pic_id')->dropDownList(ArrayHelper::map(GasUserPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
                            </td>
                        <?php } ?>
                    <?php } ?>
                    <td class="filler" <?= !$model->isNewRecord ? "style='display:none'" : ""; ?>></td>
                </tr>
            </table>

        </div>

        <?php
            $i = 0;
            $owners = GasOwner::find()->all();
            foreach ($owners as $owner) {
                ?>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <?=$owner->name?>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php 
                            $types = GasType::find()->orderBy("name desc")->all();
                            foreach ($types as $type) {
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b><?=$type->name?></b> <span id="subtotal" class="pull-right badge" style="background: #ccc; color:green">0</span>
                                    </div>
                                    <div class="panel-body" style="padding: 0 !important">
                                    <table width="100%" class="table table-condensed" style="margin: 0 !important">
                                    <?php
                                        $tubes = GasTube::find()->where("gas_owner_id = '".$owner->id."' and gas_type_id = '".$type->id."'")->all();
                                        foreach ($tubes as $tube) {
                                            $i++;
                                            ?>
                                            <tr>
                                            <td><input type="checkbox" id="tabung[<?=$i?>]" name="tabung[<?=$i?>]" value="<?=$tube->id?>"> <span id="tabung[<?=$i?>]">&nbsp;<?=$tube->nomor?></span></td>
                                            <td width="50%"><input <?= ($tube->count > 1) ? 'type="text" value=""' : 'type ="hidden" value="1"'; ?> id="jumlah[<?=$i?>]" name="jumlah[<?=$i?>]" style="width: 100% !important; text-align:right"></td>
                                            </tr>  
                                            <?php                                            
                                        }
                                    ?>  
                                    </table>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
        
        <div class="col-sm-12">
            <div class="form-group">
                    <hr>
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php //cek
    if ($model->isNewRecord) {
        $this->registerJs('

            $(document).on("click", "#gascirculation-fromto input[type=radio]", function(){
                if(this.value == "User") {
                    $(".area").show();
                    $(".vendor").hide();
                    $(".filler").hide();
                    $("#gascirculation-gas_vendor_unit_id").val("");
                    $("#gascirculation-gas_vendor_pic_id").val("");
                } else {
                    $(".area").hide();
                    $(".vendor").show();
                    $(".filler").hide();
                    $("#gascirculation-gas_user_unit_id").val("");
                    $("#gascirculation-gas_user_pic_id").val("");
                }
            });
            ', \yii\web\VIEW::POS_READY
        ); 
    }
?>
