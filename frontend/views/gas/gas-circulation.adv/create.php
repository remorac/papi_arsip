<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasCirculation */

$this->title = 'Create Gas Circulation';
$this->params['breadcrumbs'][] = ['label' => 'Gas Circulations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-circulation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelTubes' => $modelTubes,
    ]) ?>

</div>
