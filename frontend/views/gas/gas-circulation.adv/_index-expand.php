<?php

use frontend\models\GasOwner;
use frontend\models\GasType;
use frontend\models\GasTube;
use frontend\models\GasCirculationTube;

?>

<div class="col-md-11 col-md-offset-1">

<?php 
	$owners = GasOwner::find()->all();
	foreach ($owners as $owner) {
		?>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?=$owner->name?>
					</div>
					<div class="panel-body">
						<?php 
                            $types = GasType::find()->orderBy("name desc")->all();
                            foreach ($types as $type) {
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b><?=$type->name?></b> <span id="subtotal" class="pull-right badge" style="background: #ccc; color:green">0</span>
                                    </div>
                                    <div class="panel-body" style="padding: 0 !important">
                                    <table width="100%" class="table table-condensed" style="margin: 0 !important">
                                    <?php 
										$i = 0;
                                        $tubes = GasTube::find()->where("gas_owner_id = '".$owner->id."' and gas_type_id = '".$type->id."'")->all();
                                        foreach ($tubes as $tube) {
                                        	if (GasCirculationTube::find()->where("gas_tube_id = '".$tube->id."'")->one()) {
                                        	if ($tube->id = GasCirculationTube::find()->where("gas_tube_id = '".$tube->id."'")->one()->id) {
	                                            $i++;
	                                            ?>
	                                            <tr>
	                                            <td width="20px"><?=$i?>.</td>
	                                            <td><span id="tabung[<?=$i?>]">&nbsp;<?=$tube->nomor?></span></td>
	                                            <td align="right" width="50%"><?= $tube->count > 1 ? $tube->count : ""; ?></td>
	                                            </tr>  
	                                            <?php     
                                            }    
                                            }                                   
                                        }
                                    ?>  
                                    </table>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
					</div>
				</div>
			</div>
		<?php
	}

?>

<table class="table table-condensed table-bordered">
	<tr>
		<th>No</th>
		<th>Isi</th>
		<th>Tabung</th>
		<th>Owner</th>
		<th>Jumlah</th>
	</tr>
<?php
	$i = 0;
	foreach ($model->gasCirculationTubes as $row) {
		$i++;
		echo "<tr>
			<td width=25px>".$i."</td>
			<td>".$row->gasTube->gasType->name."</td>
			<td>".$row->gasTube->nomor."</td>
			<td>".$row->gasTube->gasOwner->name."</td>
			<td>".$row->count."</td>
		</tr>";
	}
?>
</table>
</div>