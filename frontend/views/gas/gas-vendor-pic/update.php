<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasVendorPic */

$this->title = 'Update Vendor PIC: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendor PICs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-vendor-pic-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
