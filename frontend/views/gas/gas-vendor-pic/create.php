<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasVendorPic */

$this->title = 'Create Vendor PIC';
$this->params['breadcrumbs'][] = ['label' => 'Vendor PICs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-vendor-pic-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
