<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use wbraganca\dynamicform\DynamicFormWidget;

use frontend\models\GasTube;
use frontend\models\GasPic;
use frontend\models\GasVendorUnit;
use frontend\models\GasVendorPic;
use frontend\models\GasUserUnit;
use frontend\models\GasUserPic;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTubeCirculation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-tube-circulation-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'io')->radioList(['1'=>'IN', '2'=>'OUT'])->label('IN/OUT') ?>
    
    <?= $form->field($model, 'fromto')->radioList(['User'=>'User', 'Vendor'=>'Vendor'])->label('From/To') ?>

    <?= $form->field($model, 'date')->textInput(['value'=> date('Y-m-d')]) ?>

    <div id="vendor" style="display: none">
    <?= $form->field($model, 'gas_vendor_unit_id')
        ->dropDownList(ArrayHelper::map(GasVendorUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'gas_vendor_pic_id')
        ->dropDownList(ArrayHelper::map(GasVendorPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    </div>

    <div id="user" style="display: none">
    <?= $form->field($model, 'gas_user_unit_id')
        ->dropDownList(ArrayHelper::map(GasUserUnit::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'gas_user_pic_id')
        ->dropDownList(ArrayHelper::map(GasUserPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>
    </div>
<!-- 
    <?= $form->field($model, 'time')->textInput() ?>
 -->
<!-- 
    <?= $form->field($model, 'gas_tube_id')
        ->label('Tube No')
        ->dropDownList(ArrayHelper::map(GasTube::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>
 -->
    <?= $form->field($model, 'gas_pic_id')
        ->dropDownList(ArrayHelper::map(GasPic::find()->all(), 'id', 'name'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'no_reference')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'gas_tube_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(GasTube::find()->all(), 'id', 'DropdownDisplayItem'), 
        // 'maintainOrder' => true,
        'options' => ['placeholder' => '- select -', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'count')->textInput(['maxlength' => 11]) ?>
    
    


    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <br>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php //cek
    $this->registerJs('

        $(document).on("click", "#gastubecirculation-fromto input[type=radio]", function(){
            if(this.value == "User") {
                $("#user").show();
                $("#vendor").hide();
                $("#gastubecirculation-gas_vendor_unit_id").val("");
                $("#gastubecirculation-gas_vendor_pic_id").val("");
            } else {
                $("#user").hide();
                $("#vendor").show();
                $("#gastubecirculation-gas_user_unit_id").val("");
                $("#gastubecirculation-gas_user_pic_id").val("");
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>
