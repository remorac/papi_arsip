<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GasTubeCirculationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sirkulasi Tabung Gas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-circulation-index box box-body box-primary">

    <p>
        <?= Html::a('Create Sirkulasi Tabung Gas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'id', 'headerOptions'=>['class'=>'opsi-sm']],
            [
                'attribute'=>'io',
                'format'=>'raw',
                'value'=> function($data) {
                    return $data->io == '1' ? "IN" : "OUT";
                }
            ],
            'date',
            'time',
            'gasVendorUnit.name:text:Vendor',
            'gasVendorPic.name:text:Vendor PIC',
            'gasUserUnit.name:text:User',
            'gasUserPic.name:text:User PIC',
            'gasPic.name:text:PIC',
            [
                'attribute'=>'gasTube.nomor',
                'label'=>'Tabung',
                'format'=>'raw',
                'value'=>function($data) {
                    return "<b>".$data->gasTube->nomor."</b> <br> (".
                            $data->gasTube->gasType->name." - ".
                            $data->gasTube->gasOwner->name.")"; 
                }
            ],
            'count',
            'no_reference',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
