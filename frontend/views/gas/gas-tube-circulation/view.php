<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTubeCirculation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sirkulasi Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-circulation-view box box-body box-success">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'io',
            'date',
            'time',
            'no_reference',
            'gas_vendor_unit_id',
            'gas_vendor_pic_id',
            'gas_user_unit_id',
            'gas_user_pic_id',
            'gas_pic_id',
            'gas_tube_id',
            'count',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:ntext:Created By',
            'updatedBy.username:ntext:Updated By',
        ],
    ]) ?>

</div>
