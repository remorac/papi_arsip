<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTubeCirculationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gas-tube-circulation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'io') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'time') ?>

    <?= $form->field($model, 'gas_tube_id') ?>

    <?php // echo $form->field($model, 'gas_vendor_unit_id') ?>

    <?php // echo $form->field($model, 'gas_vendor_pic_id') ?>

    <?php // echo $form->field($model, 'gas_user_unit_id') ?>

    <?php // echo $form->field($model, 'gas_user_pic_id') ?>

    <?php // echo $form->field($model, 'gas_pic_id') ?>

    <?php // echo $form->field($model, 'no_reference') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
