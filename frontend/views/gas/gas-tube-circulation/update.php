<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GasTubeCirculation */

$this->title = 'Update Sirkulasi Tabung Gas: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sirkulasi Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gas-tube-circulation-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
