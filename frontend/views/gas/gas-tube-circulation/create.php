<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GasTubeCirculation */

$this->title = 'Create Sirkulasi Tabung Gas';
$this->params['breadcrumbs'][] = ['label' => 'Sirkulasi Tabung Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gas-tube-circulation-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
