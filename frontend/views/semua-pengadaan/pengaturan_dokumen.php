<?php 

use yii\helpers\Html;
use yii\grid\GridView;

use frontend\models\ActivityDokumen;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanDokumenNotrequired,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        [
            'attribute' => 'dokumenJenis.nama',
            'label' => 'Jenis Dokumen',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenJenis->nama;
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'Activity',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                	if (ActivityDokumen::find()->where("dokumen_jenis_id = '".$data->dokumen_jenis_id."'")->one()->activity->nama)
                    return ActivityDokumen::find()->where("dokumen_jenis_id = '".$data->dokumen_jenis_id."'")->one()->activity->nama;
                }
            },
        ],
    ],
]); ?>
