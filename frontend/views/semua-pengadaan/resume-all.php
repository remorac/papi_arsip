<table>    
    <tr>
        <td><b>Pengadaan</b></td>
        <td><b>Purchase Order</b></td>
        <td><b>Dokuman</b></td>
        <td><b>Tahun</b></td>
    </tr>

    <?php
        $models = \frontend\models\Pengadaan::find()->limit(3)->all();
        foreach ($models as $model) {
            foreach ($model->pengadaanActivities as $pengadaanActivity) {
                foreach ($pengadaanActivity->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
    ?>
    
                    <tr>
                        <td><?= $model->nama ?></td>
                        <td><?= $model->groupedPo ?></td>
                        <td><?= $pengadaanActivityDokumen->dokumenUpload->pdf_filename ?></td>
                        <td><?= substr($pengadaanActivityDokumen->dokumenUpload->tgl_dokumen, 0, 4) ?></td>
                    </tr>

    <?php           
                }
            }
    ?>

    <?php
            foreach ($model->pengadaanFileUncategorizeds as $pengadaanFileUncategorized) {
    ?>
    
                <tr>
                    <td><?= $model->nama ?></td>
                    <td><?= $model->groupedPo ?></td>
                    <td><?= $pengadaanFileUncategorized->filename ?></td>
                    <td></td>
                </tr>

    <?php
            }
        }
    ?>

</table>