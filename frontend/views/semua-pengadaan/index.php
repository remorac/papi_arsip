<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use frontend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Arsip Procurement';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="afis-index box box-primary box-body">

    <?php 
        
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:70px'],
                'value' => function($data) {
                    $opt = '';
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=semua-pengadaan/view&id=".$data->id."' data-pjax=0> <span class='text-primary'><i class='glyphicon glyphicon-eye-open'></i> View Detail</span></a>";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=worksheet-purchaser/view&id=".$data->id."' data-pjax=0> <span class='text-muted'><i class='glyphicon glyphicon-pencil'></i> Edit</span></a>";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=semua-pengadaan/download-spreadsheet&id=".$data->id."' data-pjax=0> <span class='text-success'><i class='glyphicon glyphicon-file'></i>Resume</span></a>";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='".Url::to(['create-zip', 'id' => $data->id])."' data-method='post'> <span class='text-warning'><i class='fa fa-file-zip-o'></i> Create ZIP</span></a>";
                    
                    if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    for ($i=0; $i < 100; $i++) {
                        if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.part-'.$i.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.part-'.$i.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    }
                    return $opt;
                }
            ],
            // [
            //   'attribute' => 'id',
            //   'contentOptions'=>['class'=>'style="width:50px"'],
            // ],
            [
                'attribute' => 'barang_jasa_id',
                'label' => 'BRG/JSA',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 90px;'],
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode;
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'barang_jasa_id', [
                    '1'=>'BRG',
                    '2'=>'JSA',
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 70px;'],
                'value' => function($data){
                    if($data->barangJasa){
                        return str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama:raw:Nama Pengadaan',
            [
                'attribute' => 'groupedKpp',
                'label' => 'KPP',
                'value' => function ($data) {
                    return $data->groupedKpp;
                }
            ],
            [
                'attribute' => 'groupedPo',
                'label' => 'PO',
                'value' => function ($data) {
                    return $data->groupedPo;
                }
            ],
            [
                'attribute' => 'groupedDocs',
                'label' => 'PO Linked',
                'value' => function ($data) {
                    return substr($data->groupedDocs, 0, 50).' ...';
                }
            ],
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
            ],
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return $data->purchaserPersonelProcurement->personel->singkatan;
                    }
                },
            ],
            [
                'attribute' => 'groupedAccess',
                'label' => 'Access Group',
                'value' => function ($data) {
                    return $data->groupedAccess;
                }
            ],
            [
                // 'attribute' => 'groupedLocator',
                'label' => 'Locator',
                'value' => function ($data) {
                    return $data->groupedLocator;
                }
            ],
            // 'level_approval_id',
            // 'nilai_oe',
            // 'start_plan',
            // 'requirement_date',
            // 'delivery_time:datetime',
            // 'po_reference',
            // 'documents_verification',
            // 'isi_disposisi',
            // 'subgroup',
            // 'kontrak_butuh',
            // 'pr_ada',
            // 'total_harga_sebelum_diskon',
            // 'total_harga_setelah_diskon',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            /*[
                'attribute'=>'created_by',
                'label'=>'Created By',
                'format' => 'raw',
                'value'=>function($data) { return "<small>".str_replace('.', ' ', $data->createdBy->username)."</small>"; },
                'filter' => Html::activeDropDownList($searchModel, 'created_by', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],*//*
            [
                'attribute'=>'updated_by',
                'label'=>'Updated By',
                'format' => 'raw',
                'value'=>function($data) { return "<small>".str_replace('.', ' ', $data->updatedBy->username)."</small>"; },
                'filter' => Html::activeDropDownList($searchModel, 'updated_by', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],*/

            /*[
                'attribute' => '',
                'label' => 'Verification',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:100px'],
                'value' => function($data) {
                    // $opt = "<div class='btn-group' role='group'>";
                    $opt = "";
                    $a_verified = $data->verified_by_arsiparis == 1 ? "btn-success" : "btn-danger";
                    $p_verified = $data->verified_by_purchaser == 1 ? "btn-success" : "btn-danger";
                    $m_verified = $data->verified_by_manager   == 1 ? "btn-success" : "btn-danger";
                    $opt.= "<a class='btn btn-xs $a_verified' href='?r=semua-pengadaan/verified-by-arsiparis&id=".$data->id."' data-pjax=1>A</a>&nbsp;";
                    $opt.= "<a class='btn btn-xs $p_verified' href='?r=semua-pengadaan/verified-by-purchaser&id=".$data->id."' data-pjax=1>P</a>&nbsp;";
                    $opt.= "<a class='btn btn-xs $m_verified' href='?r=semua-pengadaan/verified-by-manager&id=".$data->id."' data-pjax=1>M</a>";
                    // $opt.="</div>";
                    return $opt;
                }
            ],*/
            /*[
                'attribute' => 'verified_by_arsiparis',
                'label' => 'Verifikasi Arsiparis',
                'format' => 'raw',
                'headerOptions'=>['style'=>'width:50px', 'title' => 'Arsiparis Verification'],
                'value' => function($data) {
                    return $data->verified_by_arsiparis == 0
                        ? "<small class='text-danger'>unverified</small>"
                        : "<small class='text-success'>".str_replace('.', ' ', is_object($data->verifiedByArsiparis) ? $data->verifiedByArsiparis->username : '')."</small>";
                },
                'filter' => Html::activeDropDownList($searchModel, 'verified_by_arsiparis', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            [
                'attribute' => 'verified_by_purchaser',
                'label' => 'Verifikasi Purchaser',
                'format' => 'raw',
                'headerOptions'=>['style'=>'width:50px', 'title' => 'Purchaser Verification'],
                'value' => function($data) {
                    return $data->verified_by_purchaser == 0
                        ? "<small class='text-danger'>unverified</small>"
                        : "<small class='text-success'>".str_replace('.', ' ', is_object($data->verifiedByPurchaser) ? $data->verifiedByPurchaser->username : '')."</small>";
                },
                'filter' => Html::activeDropDownList($searchModel, 'verified_by_purchaser', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            [
                'attribute' => 'verified_by_manager',
                'label' => 'Verifikasi Manager',
                'format' => 'raw',
                'headerOptions'=>['style'=>'width:50px', 'title' => 'Manager Verification'],
                'value' => function($data) {
                    return $data->verified_by_manager == 0
                        ? "<small class='text-danger'>unverified</small>"
                        : "<small class='text-success'>".str_replace('.', ' ', is_object($data->verifiedByManager) ? $data->verifiedByManager->username : '')."</small>";
                },
                'filter' => Html::activeDropDownList($searchModel, 'verified_by_manager', [
                    '0'=>'unverified',
                    '4'=>'piery.togap',
                    '6'=>'hamdi.ayussa',
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],*/
            // 'tmp_old_id',

        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'filename' => 'Afis',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            $exportMenu,
            Html::a('<i class="fa fa-tags"></i> ' . 'Locator', ['/locator-procurement/index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            Html::a('<i class="fa fa-file"></i> ' . 'List of All Files', ['download-spreadsheet-all'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => 'grid'],
        ],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>
