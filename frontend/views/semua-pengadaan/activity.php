<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\models\PengadaanPrItem;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

//temporary handling error for setting planned duration
/*if ($model->nilai_oe == null) $model->nilai_oe = 0;

if(is_array($alerts)){
    foreach($alerts as $alert){
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => $alert['class'],
            ],
            'body' => $alert['body'],
        ]);   
    }
}*/
?>

<div id="activity-jadwal">
<?= $this->render('activity_jadwal', [
        'model' => $model,
        'alerts' => $alerts,
    ]) ?>
</div> <!-- end of worksheet -->


<!-- modal window for activity schedule --> 
<form class="form-horizontal" id="pengadaan-activity_form">
<div class="modal fade" id="modalPengadaanActivity">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Start Date</label>
                    <div class="col-sm-9">
                        <?=DatePicker::widget([
                            'id' => 'pengadaan-activity_start_date',
                            'name' => 'pengadaan-activity_start_date',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">End Date</label>
                    <div class="col-sm-9">
                        <?=DatePicker::widget([
                            'id' => 'pengadaan-activity_end_date',
                            'name' => 'pengadaan-activity_end_date',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                    </div>
                    <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                </div>
                <input type="hidden" id="pengadaan-activity_pengadaan_id" name="pengadaan-activity_pengadaan_id" value="<?=$model->id?>">
                <input type="hidden" id="pengadaan-activity_activity_id" name="pengadaan-activity_activity_id">
                <input type="hidden" id="pengadaan-activity_id" name="pengadaan-activity_id">
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" id="btn_activity_date" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>



<!-- modal window for start plan --> 
<form class="form-horizontal" id="pengadaan-startplan_form">
<div class="modal fade" id="modalPengadaanStartPlan">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Set Start Plan</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <?=DatePicker::widget([
                            'id' => 'pengadaan-start_plan',
                            'name' => 'pengadaan-start_plan',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                    </div>
                </div>
                <input type="hidden" id="pengadaan-startplan_pengadaan_id" name="pengadaan-startplan_pengadaan_id" value="<?=$model->id?>">
           </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" id="btn_startplan_date" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

