<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'barang_jasa_id') ?>

    <?= $form->field($model, 'metoda_id') ?>

    <?php // echo $form->field($model, 'purchaser_personel_procurement_id') ?>

    <?php // echo $form->field($model, 'level_approval_id') ?>

    <?php // echo $form->field($model, 'nilai_oe') ?>

    <?php // echo $form->field($model, 'start_plan') ?>

    <?php // echo $form->field($model, 'requirement_date') ?>

    <?php // echo $form->field($model, 'delivery_time') ?>

    <?php // echo $form->field($model, 'po_reference') ?>

    <?php // echo $form->field($model, 'documents_verification') ?>

    <?php // echo $form->field($model, 'isi_disposisi') ?>

    <?php // echo $form->field($model, 'subgroup') ?>

    <?php // echo $form->field($model, 'kontrak_butuh') ?>

    <?php // echo $form->field($model, 'pr_ada') ?>

    <?php // echo $form->field($model, 'total_harga_sebelum_diskon') ?>

    <?php // echo $form->field($model, 'total_harga_setelah_diskon') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'tmp_old_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
