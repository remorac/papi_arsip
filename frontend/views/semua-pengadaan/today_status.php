<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanIssue,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        [
            'attribute' => 'pengadaan_activity_id',
            'label' => 'Aktifitas',
            'format' => 'raw',
            'headerOptions' => ['width' => '150px'],
            'value' => function($data){
                if($data){
                    return $data->pengadaanActivity->activity->nama;
                }
            },
        ],
        [
            'attribute' => 'tanggal',
            'label' => 'Tanggal',
            'format' => 'raw',
            'headerOptions' => ['width' => '150px'],
            'value' => function($data){
                if($data){
                    return $data->tanggal;
                }
            },
        ],
        [
            'attribute' => 'keterangan',
            'label' => 'Keterangan',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                	return nl2br($data->keterangan);
                }
            },
        ],
    ],
]); ?>
