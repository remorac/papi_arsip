<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

//use frontend\models\SapPo;

$i = 0;
$models = $dataProviderPengadaanPo->getModels();

?>

<?= GridView::widget([
    'tableOptions' => ['class' => 'table'],
    'dataProvider' => $dataProviderPengadaanPo,
    'filterModel' => $searchModelPengadaanPo,
    'columns' => [
       [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style' => 'width:25px'],
        ],
       [
           'attribute' => 'purchasing_document',
           'format' => 'raw',
           'value' => function($model) {
                return $this->render('purchase_order_detail',['model' => $model]);
           }
       ],
    ],
   // 'itemOptions' => ['class' => 'item'],
   // 'itemView' => function ($model, $key, $index, $widget) {
   //     return $this->render('purchase_order_detail',['model' => $model]);
   // },
]) ?>

<!-- modal window for Harga Sebelum Diskon --> 
<form class="form-horizontal" id="hsd_adjusted_form">
<div class="modal fade" id="modalHSD">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="modal-title" id="item"></span>
            </div>
            <div class="modal-body" style="background:#fafafa">
                Harga Sebelum Diskon 
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" id="harga-sebelum-diskon_harga" name="harga-sebelum-diskon_harga" class="form-control text-right">
                        <input type="hidden" id="harga-sebelum-diskon_id" name="harga-sebelum-diskon_id">
                        <input type="hidden" id="harga-sebelum-diskon_po" name="harga-sebelum-diskon_po">
                        <input type="hidden" id="harga-sebelum-diskon_item_po" name="harga-sebelum-diskon_item_po">
                        <span class="text-muted">Click <code>Save Change</code> or press <code>Enter</code> to save, or click <code>Close</code> to cancel.</span>
                    </div>
                </div>
           </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_hsd_adjusted">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>


<!-- modal window for Update Contract Reference --> 
<form class="form-horizontal" id="contract_ref_form">
<div class="modal fade" id="modalContractRef">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="modal-title" id="no_po"></span>
            </div>
            <div class="modal-body" style="background:#fafafa">
                Contract Reference 
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" id="contract-reference_contract_reference" name="contract-reference_contract_reference" class="form-control">
                        <input type="hidden" id="contract-reference_id" name="contract-reference_id">
                    </div>
                </div>
           </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_update_contract" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<?php //form activity schedule
    $this->registerJs('
        function showModalHargaSebelumDiskon(id, harga, item, item_po, po) {
            $("#modalHSD").modal("show");
            $("#modalHSD #item").html(item);
            $("#harga-sebelum-diskon_id").val(id);
            $("#harga-sebelum-diskon_harga").val(harga);
            $("#harga-sebelum-diskon_item_po").val(item_po);
            $("#harga-sebelum-diskon_po").val(po);
        }
    ', \yii\web\VIEW::POS_END);

    $this->registerJs('
        function showModalContractRef(id, contract_reference, no_po) {
            $("#modalContractRef").modal("show");
            $("#modalContractRef #no_po").html("PO: <b>"+no_po+"</b>");
            $("#contract-reference_id").val(id);
            $("#contract-reference_contract_reference").val(contract_reference);
        }
    ', \yii\web\VIEW::POS_END);

    $this->registerJs('
        $(document).on("click", "#btn_hsd_adjusted", function(){
            data = $("#hsd_adjusted_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/hsdadjusted",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#purchase-order_list", timeout: 10000});
                }
            });
        });
    ', \yii\web\VIEW::POS_READY);

    $this->registerJs('
        $(document).on("click", "#btn_update_contract", function(){
            data = $("#contract_ref_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/update-contractref",
                data: "op=updateContract&pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#purchase-order_list", timeout: 10000});
                }
            });
        });
    ', \yii\web\VIEW::POS_READY);


    $this->registerJs('
        $(document).on("keypress", "#harga-sebelum-diskon_harga", function(e){
            if (e.which === 13) {
                e.preventDefault();
                data = $("#hsd_adjusted_form").serialize();
                $.ajax({
                    url: "?r=worksheet-purchaser/hsdadjusted",
                    data: "op=save&"+data,
                    cache: "",
                    success:function(r) {
                        $(".modal").modal("hide");
                        $.pjax.reload({container: "#purchase-order_list", timeout: 10000});
                    }
                });
            }
        });
    ', \yii\web\VIEW::POS_READY);
?>