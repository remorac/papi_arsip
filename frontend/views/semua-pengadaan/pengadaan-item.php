<?php 

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        
        'nama',
        'quantity',
        'satuan',
        [
            'attribute' => 'keterangan',
            'label' => 'Keterangan',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                	return nl2br($data->keterangan);
                }
            },
        ],
    ],
]); ?>
