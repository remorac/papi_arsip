<?php 
    if ($dv == 1) {
        ?>
            <div class='alert alert-success'>
                <button type='button' id='btn_unverify' class='btn btn-default pull-right'><i class='glyphicon glyphicon-remove'></i>&nbsp; Unverify </button>
                <b>Status: <br> <big class='text-default'>VERIFIED</big></b>
            </div>
        <?php
    } else {
        ?>
            <div class='alert alert-danger'>
                <button type='button' id='btn_verify' class='btn btn-default pull-right'><i class='glyphicon glyphicon-ok'></i>&nbsp; Verify </button>
                <b>Status: <br> <big class='text-default'>NOT VERIFIED</big></b>
            </div>
        <?php
    }
?>