<table>
    <tr>
        <td><b>Pengadaan</b></td>
        <td colspan="2"><?= $model->nama ?></td>
    </tr>
    <tr>
        <td><b>Purchase Order</b></td>
        <td colspan="2"><?= $model->groupedPo ?></td>
    </tr>
    <tr>
        <td><b>Box Procurement</b></td>
        <td colspan="2"><?= $model->groupedLocator ?></td>
    </tr>
    <tr>
        <td><b>Box Legal</b></td>
        <td colspan="2"><?= $model->groupedLegalLocator ?></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2"></td>
    </tr>

    
    <tr>
        <td></td>
        <td><b>Dokuman</b></td>
        <td><b>Tahun</b></td>
    </tr>

    <?php
        foreach ($model->pengadaanActivities as $pengadaanActivity) {
            foreach ($pengadaanActivity->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
    ?>
    
    <tr>
        <td></td>
        <td><?= $pengadaanActivityDokumen->dokumenUpload->pdf_filename ?></td>
        <td><?= substr($pengadaanActivityDokumen->dokumenUpload->tgl_dokumen, 0, 4) ?></td>
    </tr>

    <?php           
            }
        }
    ?>

    <?php
        foreach ($model->pengadaanFileUncategorizeds as $pengadaanFileUncategorized) {
    ?>
    
    <tr>
        <td></td>
        <td><?= $pengadaanFileUncategorized->filename ?></td>
        <td></td>
    </tr>

    <?php
        }
    ?>

</table>