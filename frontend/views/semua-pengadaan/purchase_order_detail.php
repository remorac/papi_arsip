<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

//use frontend\models\SapPo;


?>

<div class="panel panel-default" style="margin-bottom:0px">
    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#purchase_order_list" href="#tab<?=$model->purchasing_document?>">
        <div class="panel-title">
            <span class="text-success"><?=$model->purchasing_document?></span>
        </div>
        <?php 
            $vendor         = "";
            $document_date  = "";
            if (($model->sapPo)) {
                $vendor        = $model->sapPo[0]->{"Vendor/supplying plant"} != "" ? $model->sapPo[0]->{"Vendor/supplying plant"} : "";
                $document_date = $model->sapPo[0]->{"Document Date"} != "" ? $model->sapPo[0]->{"Document Date"} : "";
            }
            echo $vendor
                ."<br> Doc Date : ".$document_date;
        ?>
    </div>
    <div id="tab<?=$model->purchasing_document?>" class="panel-collapse collapse">
    <div class="panel-body">
        <?php 
            /*$provider = new \yii\data\ActiveDataProvider([
                'query' => \frontend\models\SapPo::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);*/
            $searchModel = new \frontend\models\SapPoSearch();
            $queryParams = Yii::$app->request->queryParams;
        
            if (!isset($queryParams['SapPoSearch'])) $queryParams['SapPoSearch'] = array();
            $queryParams['SapPoSearch'] = array_merge($queryParams['SapPoSearch'], 
                ['Purchasing Document' => $model->purchasing_document,]
            );
            $provider = $searchModel->search($queryParams);
            $provider->pagination->defaultPageSize = 10;
        ?>

        
        <?= GridView::widget([
            'dataProvider' => $provider,
            'filterModel' => null,
            'tableOptions' => ['class' => 'table table-hover paper'],
            'columns' => [
                [   
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['class' => 'serial-column'],
                    'contentOptions' => ['class' => 'serial-column'],
                ],
                'Item',
                'Material',
                'Short Text',
                'Order Quantity:integer:Qty',
                'Net price:integer:Harga PO (satuan)',
            ],
        ]); ?>

    </div>
    </div>
</div>
