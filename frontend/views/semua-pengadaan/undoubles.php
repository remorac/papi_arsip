<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Check Korin Double';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <span class="callout callout-warning col-sm-12"> <i class="fa fa-exclamation-triangle"></i> &nbsp; This page is made to help us to find duplicated korin data due to database merging, and delete it. The operation CAN NOT BE UNDONE, so please choose carefully. </span></p>

<?php 
	$query = new \yii\db\Query;
    $query->select("*")
        ->from("00_doubles")
        ->where("ordering <= '338'")
        ->orderBy("ordering, id");
    $command = $query->createCommand();
    $data = $command->queryAll();

    echo "
    	<table class='table table-condensed table-bordered table-striped'>
    		<tr>
                <th>options</th>
    			<th>ordering</th>
    			<th>id</th>
                <th>no_surat</th>
    			<th>perihal</th>
    			<th>updated_at</th>
                <th>c_asg</th>
    			<th>c_pgd</th>
                <th>c_doc</th>
    		</tr>
    ";
    $i = 0;
    foreach ($data as $row) {
    	$i++;
        $url = \yii\helpers\Url::to(['korin/delete', 'id'=>$row['id'], 'double'=>'1']);
    	echo "
    		<tr>
                <td width='100'>
                    <a target='_blank' href='?r=korin/view&id=$row[id]' class='btn btn-sm btn-default'>view</a> &nbsp; 
                    <a href='$url' style='color:red' 
                        data-confirm='Are you sure you want to delete this item? \nWARNING : This operation CAN NOT BE UNDONE, please choose carefully.' 
                        data-method='post'>
                        delete
                    </a>
                </td>
    			<td>$row[ordering]</td>
    			<td>$row[id]</td>
                <td>$row[no_surat]</td>
    			<td>$row[perihal]</td>
    			<td>$row[updated_at]</td>
                <td>$row[c_asg]</td>
    			<td>$row[c_pgd]</td>
                <td>$row[c_doc]</td>
    		</tr>
        ";
    }
    echo "</table>";
?>

</div>
