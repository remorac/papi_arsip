<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-area-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
