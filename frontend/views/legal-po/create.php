<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\LegalPo */

$this->title = 'Create Legal Po';
$this->params['breadcrumbs'][] = ['label' => 'Legal Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="legal-po-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form') ?>
    </div>

</div>
