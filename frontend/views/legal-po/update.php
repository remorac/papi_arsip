<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\LegalPo */

$this->title = 'Update Legal Po: ' . $model->No_Kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Legal Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->No_Kontrak, 'url' => ['view', 'id' => $model->No_Kontrak]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="legal-po-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
