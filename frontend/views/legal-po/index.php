<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LegalPoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contract - PO';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="legal-po-index box box-primary box-body">

    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'Kode_Klasifikasi',
            'Purchase_Order',
            'No_Kontrak',
            'Isi_Berkas',
            'Tahun',
            'Vendor',
            'Box',
            'Lokasi',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Legal Pos',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);

        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            /*[
                'contentOptions' => ['class' => 'action-column nowrap text-left'],
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
                    },
                    'update' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
                    },
                    'delete' => function ($url) {
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                            'data-method' => 'post', 
                            'data-confirm' => 'Are you sure you want to delete this item?']);
                    },
                ],
            ],*/
            [
                'attribute' => 'Kode_Klasifikasi',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Purchase_Order',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'No_Kontrak',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Isi_Berkas',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Tahun',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Vendor',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Box',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'Lokasi',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Import', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>