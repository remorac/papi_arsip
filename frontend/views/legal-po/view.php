<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\LegalPo */

$this->title = $model->No_Kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Legal Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="legal-po-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->No_Kontrak], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->No_Kontrak], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                    'Kode_Klasifikasi:ntext',
                'Nama_Berkas:ntext',
                'Purchase_Order:ntext',
                'No_Kontrak:ntext',
                'Isi_Berkas:ntext',
                'Tahun:ntext',
                'Jumlah_Berkas:ntext',
                'Jumlah:ntext',
                'Asli_Copy:ntext',
                'Retensi_Masa_Simpan:ntext',
                'Retensi_Permanen_Musnah:ntext',
                'No_Boks_Lokasi:ntext',
            ],
        ]) ?>
    </div>
</div>
