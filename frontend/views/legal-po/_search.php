<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LegalPoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="legal-po-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Kode_Klasifikasi') ?>

    <?= $form->field($model, 'Nama_Berkas') ?>

    <?= $form->field($model, 'Purchase_Order') ?>

    <?= $form->field($model, 'No_Kontrak') ?>

    <?= $form->field($model, 'Isi_Berkas') ?>

    <?php // echo $form->field($model, 'Tahun') ?>

    <?php // echo $form->field($model, 'Jumlah_Berkas') ?>

    <?php // echo $form->field($model, 'Jumlah') ?>

    <?php // echo $form->field($model, 'Asli_Copy') ?>

    <?php // echo $form->field($model, 'Retensi_Masa_Simpan') ?>

    <?php // echo $form->field($model, 'Retensi_Permanen_Musnah') ?>

    <?php // echo $form->field($model, 'No_Boks_Lokasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
