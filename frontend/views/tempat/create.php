<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Tempat */

$this->title = Yii::t('app', 'Create Tempat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tempat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tempat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
