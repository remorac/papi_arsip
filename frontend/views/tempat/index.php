<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TempatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tempat');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tempat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tempat'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions'=> ['style' => 'width:4em'],
            ],
            //'id',
            
            [
                'attribute' => 'id',
                'headerOptions'=> ['style' => 'width:4em'],
                'format' => 'integer',
            ],
            'nama:ntext',
            //'created_at',
            //'updated_at',
            //'created_by',
            // 'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=> ['style' => 'width:5em'],
            ],
        ],
    ]); ?>

</div>
