<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranJenisSub */

$this->title = 'Create Penomoran Jenis Sub';
$this->params['breadcrumbs'][] = ['label' => 'Penomoran Jenis Subs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penomoran-jenis-sub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
