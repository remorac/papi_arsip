<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranJenisSub */

$this->title = 'Update Penomoran Jenis Sub: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Penomoran Jenis Subs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penomoran-jenis-sub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
