<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-vendor-index box box-body box-primary">

    <p>
        <?= Html::a('Create Ppl Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
