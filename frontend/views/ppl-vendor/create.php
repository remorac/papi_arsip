<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplVendor */

$this->title = 'Create Ppl Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-vendor-create box box-body box-success">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
