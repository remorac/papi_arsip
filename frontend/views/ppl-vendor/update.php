<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplVendor */

$this->title = 'Update Ppl Vendor: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-vendor-update box box-body box-warning">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
