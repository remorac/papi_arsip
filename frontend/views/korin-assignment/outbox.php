<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Outbox Senior Manager';
$this->params['breadcrumbs'][] = "Senior Manager";
$this->params['breadcrumbs'][] = "Outbox";
?>
<div class="korin-assignment-index box box-body box-primary">

    <div class="btn-group">
        <?= Html::a('Inbox', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Outbox', '#', ['class' => 'btn btn-info active']) ?>
    </div>
    <p></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=korin-assignment/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-primary'></i></a>";
                    if (!$data->pengadaanKorins) {
                        $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=korin-assignment/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-warning'></i></a> &nbsp;";
                        $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['korin-assignment/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                    }
                    $opt.="</div>";
                    return $opt;
                }
            ],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            [
                'attribute' => 'korin.no_surat',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin/view&id=' . $data->korin->id, ['data-pjax'=>'0']);
                    }
                },
            ],
            'tanggal_assignment',
            //'mgr_personel_id',
            [
                'attribute' => 'personelProcurement.personel.nama',
                'label' => 'Assigned to',
                'format' => 'raw',
                'value' => function($data){
                    if($data->personelProcurement){
                        return Html::a($data->personelProcurement->personel->nama, '?r=personel/view&id=' . $data->personelProcurement->id);
                    }
                },
            ],
            [
                'attribute' => 'korin.perihal',
                'label' => 'Perihal',
                'format' => 'raw',
                'value' => function($data) {
                    foreach ($data->pengadaanKorins as $pengadaanKorin) {
                        return $pengadaanKorin->korin->perihal."<br>";
                    }
                }
            ],
            'keterangan:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            /*[
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 70px;']
            ],*/
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
