<?php 

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinVendor,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => 'vendor.vendor',
            'label' => 'Kode',
            'format' => 'raw',
            'value' => function($data){
                if($data->sapVendor->Vendor){
                    return $data->sapVendor->Vendor;
                }
            },
        ],
        [
            'attribute' => '{name of vendor}',
            'label' => 'Nama',
            'format' => 'raw',
            'value' => function($data){
                if($data->sapVendor->{'Name of vendor'}){
                    return $data->sapVendor->{'Name of vendor'};
                }
            },
        ],
    ],
]); ?>