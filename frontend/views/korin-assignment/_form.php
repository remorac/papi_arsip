<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;
use frontend\models\Korin;
use frontend\models\Personel;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-assignment-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'korin_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Korin::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <?= $form->field($model, 'tanggal_assignment')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?= $form->field($model, 'manager_personel_procurement_id')
                ->dropDownList(ArrayHelper::map(PersonelProcurement::find()
                        ->where(['=', 'jabatan', 'Manager'])   
                        ->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
