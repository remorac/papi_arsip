<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */

$this->title = 'Disposisi: ' . ' ' . $modelKorin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Senior Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="korin-assignment-update box box-body box-success">

    <?= $this->render('_formAssign', [
        'model' => $model,
        'modelKorin' => $modelKorin,

        'searchModelKorinDokumen' => $searchModelKorinDokumen,
        'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

        'searchModelKorinVendor' => $searchModelKorinVendor,
        'dataProviderKorinVendor' => $dataProviderKorinVendor,
        
        'searchModelKorinItem' => $searchModelKorinItem,
        'dataProviderKorinItem' => $dataProviderKorinItem,

        'searchModelKorinPrItem' => $searchModelKorinPrItem,
        'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
    ]) ?>

</div>
