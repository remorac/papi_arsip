<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use yii\widgets\DetailView;
use kartik\select2\Select2;
use frontend\models\Korin;
use frontend\models\PersonelProcurement;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-assignment-form">

    <div class="row">
        <div class="col-md-3">
            <?php $form = ActiveForm::begin([
                /*'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-3 text-right\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-offset-3 col-md-9\">{error}</div>",
                ],*/
            ]); ?>

            <?= $form->field($model, 'korin_id')->textInput(['maxlength' => 100, 'readonly' => 'readonly']) ?>

            <?= $form->field($model, 'tanggal_assignment')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>

            <?= $form->field($model, 'manager_personel_procurement_id')
                ->dropDownList(ArrayHelper::map(PersonelProcurement::find()
                        ->where(['=', 'jabatan', 'Manager'])
                        ->orWhere(['=', 'id', '4'])
                        ->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

            <?= $form->field($model, 'keterangan')->textarea(['rows' => 2]) ?>

            <div class="form-group">
                <!-- <div class="col-md-offset-3 col-md-9">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div> -->
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


        <div class="col-md-9">
            <div class="nav-tabs-custom paper">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#korin" aria-controls="home" role="tab" data-toggle="tab">KORIN</a></li>
                    <li role="presentation"><a href="#document" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
                    <!-- <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab" data-toggle="tab">Bidder List</a></li> -->
                    <!-- <li role="presentation"><a href="#itemKorin" aria-controls="messages" role="tab" data-toggle="tab">Item KORIN PP</a></li> -->
                    <!-- <li role="presentation"><a href="#itemPr" aria-controls="settings" role="tab" data-toggle="tab">Item PR</a></li> -->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="korin">
                        <?= DetailView::widget([
                            'model' => $modelKorin,
                            'options'=> ['class' => 'table detail-view  paper'],
                            'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
                            'attributes' => [
                                'id',
                                'no_surat',
                                [
                                    'attribute' => 'korinJenis.nama',
                                    'label' => 'Jenis KORIN',
                                    'format' => 'raw',
                                    'value' => Html::a(
                                        $modelKorin->korinJenis ? $modelKorin->korinJenis->nama : '', 
                                        // $modelKorin->korinJenis ? '?r=/korin-jenis/view&id=' . $modelKorin->korinJenis->id : ''
                                        '#'
                                        )
                                ],
                                [
                                    'attribute' => 'barangJasa.nama',
                                    'label' => 'Barang/Jasa',
                                ],
                                'pic_initial',
                                'tgl_terima',
                                'tanggal_surat',
                                // 'ringkasan_isi:ntext',
                                'perihal',
                                'tembusan',
                                'yg_menandatangani',
                                'lampiran',
                                'isi_disposisi:ntext',
                                // 'kebutuhan_disposisi',
                                'requirement_date',
                                'revisi_referensi',
                            ],
                        ]) ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="document">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'document_list']); ?>
                            <?= $this->render('dokumen', [
                                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="vendor">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'vendor_list']); ?>
                            <?= $this->render('vendor', [
                                'searchModelKorinVendor' => $searchModelKorinVendor,
                                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemKorin">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemKorin_list']); ?>
                            <?= $this->render('itemKorin', [
                                'searchModelKorinItem' => $searchModelKorinItem,
                                'dataProviderKorinItem' => $dataProviderKorinItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemPr">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemPr_list']); ?>
                            <?= $this->render('itemPr', [
                                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>

            </div>
        </div> <!-- end of col-md-12 -->
    </div>

</div>



<?php 
    if ($modelKorin->barang_jasa_id == 1)
        $this->registerJs('$("#korinassignment-manager_personel_procurement_id").val("2")', \yii\web\VIEW::POS_READY); 
    if ($modelKorin->barang_jasa_id == 2) {
        if (Yii::$app->user->id == 14)
            $this->registerJs('$("#korinassignment-manager_personel_procurement_id").val("4")', \yii\web\VIEW::POS_READY);
        else 
            $this->registerJs('$("#korinassignment-manager_personel_procurement_id").val("13")', \yii\web\VIEW::POS_READY);
    }
    $this->registerJs('$("#korinassignment-keterangan").val("Untuk diproses.")', \yii\web\VIEW::POS_READY); 
?>
