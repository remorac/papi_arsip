<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */

$this->title = 'Update Disposisi: ' . ' ' . $model->korin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Disposisi SM', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->korin->no_surat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-assignment-update box box-body box-warning">

    <?= $this->render('_formAssign', [
        'model' => $model,
        'modelKorin' => $modelKorin,

        'searchModelKorinDokumen' => $searchModelKorinDokumen,
        'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

        'searchModelKorinVendor' => $searchModelKorinVendor,
        'dataProviderKorinVendor' => $dataProviderKorinVendor,
        
        'searchModelKorinItem' => $searchModelKorinItem,
        'dataProviderKorinItem' => $dataProviderKorinItem,

        'searchModelKorinPrItem' => $searchModelKorinPrItem,
        'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
    ]) ?>

</div>
