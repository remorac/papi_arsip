<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */

$this->title = $model->korin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Disposisi SM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-assignment-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-3">
            <?= DetailView::widget([
                'model' => $model,
                'options'=> ['class' => 'table  detail-view paper'],
                'attributes' => [
                    'id',
                    //'korin_id',
                    [
                        'label' => 'No. KORIN',
                        'format' => 'raw',
                        'value' => Html::a(
                            $model->korin ? $model->korin->no_surat : '', 
                            $model->korin ? '?r=/korin/view&id=' . $model->korin->id : ''
                            )
                    ],
                    'tanggal_assignment',
                    [
                        'label' => 'Assigned to',
                        'format' => 'raw',
                        'value' => Html::a(
                            $model->personelProcurement ? $model->personelProcurement->personel->nama : '', 
                            $model->personelProcurement ? '?r=/personel/view&id=' . $model->personelProcurement->id : ''
                            )
                    ],
                    'keterangan:ntext',
                    'created_at',
                    'updated_at',
                    [
                        'label' => 'Created By',
                        'format' => 'raw',
                        'value' => Html::a(
                            $model->createdBy ? $model->createdBy->username : '', 
                            // $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                            '#'
                            )
                    ],
                    [
                        'label' => 'Updated By',
                        'format' => 'raw',
                        'value' => Html::a(
                            $model->updatedBy ? $model->updatedBy->username : '', 
                            // $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                            '#'
                            )
                    ],
                ],
            ]) ?>
        </div>


        <div class="col-md-9">
            <div class="nav-tabs-custom paper">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#korin" aria-controls="home" role="tab" data-toggle="tab">KORIN</a></li>
                    <li role="presentation"><a href="#document" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
                    <!-- <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab" data-toggle="tab">Bidder List</a></li> -->
                    <!-- <li role="presentation"><a href="#itemKorin" aria-controls="messages" role="tab" data-toggle="tab">Item KORIN PP</a></li> -->
                    <!-- <li role="presentation"><a href="#itemPr" aria-controls="settings" role="tab" data-toggle="tab">Item PR</a></li> -->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="korin">
                        <?= DetailView::widget([
                            'model' => $modelKorin,
                            'options'=> ['class' => 'table detail-view  paper'],
                            'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
                            'attributes' => [
                                'id',
                                'no_surat',
                                [
                                    'attribute' => 'korinJenis.nama',
                                    'label' => 'Jenis KORIN',
                                    'format' => 'raw',
                                    'value' => Html::a(
                                        $modelKorin->korinJenis ? $modelKorin->korinJenis->nama : '', 
                                        $modelKorin->korinJenis ? '?r=/korin-jenis/view&id=' . $modelKorin->korinJenis->id : ''
                                        )
                                ],
                                [
                                    'attribute' => 'barangJasa.nama',
                                    'label' => 'Barang/Jasa',
                                ],
                                'pic_initial',
                                'tgl_terima',
                                'tanggal_surat',
                                // 'ringkasan_isi:ntext',
                                'perihal',
                                'tembusan',
                                'yg_menandatangani',
                                'lampiran',
                                'isi_disposisi:ntext',
                                // 'kebutuhan_disposisi',
                                'requirement_date',
                                'revisi_referensi',
                            ],
                        ]) ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="document">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'document_list']); ?>
                            <?= $this->render('dokumen', [
                                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="vendor">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'vendor_list']); ?>
                            <?= $this->render('vendor', [
                                'searchModelKorinVendor' => $searchModelKorinVendor,
                                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemKorin">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemKorin_list']); ?>
                            <?= $this->render('itemKorin', [
                                'searchModelKorinItem' => $searchModelKorinItem,
                                'dataProviderKorinItem' => $dataProviderKorinItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemPr">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemPr_list']); ?>
                            <?= $this->render('itemPr', [
                                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>

            </div>
        </div> <!-- end of col-md-12 -->
    </div>

</div>
