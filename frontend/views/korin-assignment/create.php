<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinAssignment */

$this->title = 'Create Disposisi';
$this->params['breadcrumbs'][] = ['label' => 'Disposisi SM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-assignment-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
