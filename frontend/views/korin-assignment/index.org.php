<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disposisi SM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-assignment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disposisi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            //'korin_id',
            [
                'attribute' => 'korin.no_surat',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin&KorinSearch[no_surat]=' . $data->korin->no_surat);
                    }
                },
            ],
            'tanggal_assignment',
            //'mgr_personel_id',
            [
                'attribute' => 'mgrPersonel.nama',
                'label' => 'Asssigned to',
                'format' => 'raw',
                'value' => function($data){
                    if($data->mgrPersonel){
                        return Html::a($data->mgrPersonel->nama, '?r=personel/view&id=' . $data->mgrPersonel->id);
                    }
                },
            ],
            'keterangan:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
        ],
    ]); ?>

</div>
