<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inbox Senior Manager';
$this->params['breadcrumbs'][] = "Senior Manager";
$this->params['breadcrumbs'][] = "Inbox";
?>
<div class="korin-assignment-index box box-body box-primary">

    <div class="btn-group">
        <?= Html::a('Inbox', '#', ['class' => 'btn btn-info active']) ?>
        <?= Html::a('Outbox', ['outbox'], ['class' => 'btn btn-default']) ?>
    </div>
    <p></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['class'=>'opsi'],
                'template' => '{assign}',
                'buttons' => [
                    'assign' => function ($url, $model) {
                        /*if ($model->tgl_terima > '2016-02-12') {
                            if (Yii::$app->user->id == '14')
                                return Html::a('<span class="glyphicon glyphicon-hand-right text-success"></span> Disposisi', $url, [
                                        'title' => Yii::t('app', 'Assign'),
                                        'class' => 'btn btn-xs btn-default'
                                ]);
                            else
                                return "<button class='btn btn-xs btn-danger' disabled><span class='glyphicon glyphicon-hand-right text-white'></span> Disposisi </button>";
                        } else {
                            if (Yii::$app->user->id == '3')
                                return Html::a('<span class="glyphicon glyphicon-hand-right text-success"></span> Disposisi', $url, [
                                        'title' => Yii::t('app', 'Assign'),
                                        'class' => 'btn btn-xs btn-default'
                                ]);
                            else
                                return "<button class='btn btn-xs btn-danger' disabled><span class='glyphicon glyphicon-hand-right text-white'></span> Disposisi </button>";
                        }*/   
                        return Html::a('<span class="glyphicon glyphicon-hand-right text-success"></span> Disposisi', $url, [
                                'title' => Yii::t('app', 'Assign'),
                                'class' => 'btn btn-xs btn-default'
                        ]);                     
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'assign') {
                        $url = "?r=korin-assignment/assign&id=".$model->id; // your own url generation logic
                        return $url;
                    }
                }     
            ],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            //'no_surat',
            [
                'attribute' => 'no_surat',
                'label' => 'No. Surat',
                'format' => 'raw',
                'value' => function($data){
                    if($data->no_surat){
                        return Html::a($data->no_surat, '?r=korin/view&id=' . $data->id, ['data-pjax'=>'0']);
                    }
                },
            ],
            //'korin_jenis_id',
            [
                'attribute' => 'korinJenis.nama',
                'label' => 'Jenis KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korinJenis){
                        return Html::a($data->korinJenis->nama, '?r=korin-assignment&KorinUnassignedSearch[korinJenis.nama]=' . $data->korinJenis->nama);
                    }
                },
            ],
            //'barang_jasa_id',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?r=korin-assignment&KorinUnassignedSearch[barangJasa.nama]=' . $data->barangJasa->nama);
                    }
                },
            ],
            //'revisi_referensi',
            'pic_initial',
            // 'tgl_terima',
            [
                'attribute' => 'tgl_terima',
                'contentOptions'=>['style'=>'width: 100px;']
            ],
            // 'tanggal_surat',
            [
                'attribute' => 'tanggal_surat',
                'contentOptions'=>['style'=>'width: 100px;']
            ],
            // 'ringkasan_isi:ntext',
            'perihal',
            // 'tembusan',
            // 'yg_menandatangani',
            // 'lampiran',
            // 'isi_disposisi:ntext',
            // 'kebutuhan_disposisi',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
