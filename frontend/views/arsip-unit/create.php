<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ArsipUnit */

$this->title = 'Create Arsip Unit';
$this->params['breadcrumbs'][] = ['label' => 'Arsip Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-unit-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
