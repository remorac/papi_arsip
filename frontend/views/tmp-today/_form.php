<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpToday */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmp-today-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'brg_jsa')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nama_pengadaan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'monitoring_aktifitas')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rencana_pengadaan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'realisasi_pengadaan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'issue_aktifitas')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
