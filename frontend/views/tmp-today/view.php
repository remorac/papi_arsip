<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpToday */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tmp Todays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-today-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'brg_jsa:ntext',
            'nama_pengadaan:ntext',
            'monitoring_aktifitas:ntext',
            'rencana_pengadaan:ntext',
            'realisasi_pengadaan:ntext',
            'issue_aktifitas:ntext',
            'keterangan:ntext',
        ],
    ]) ?>

</div>
