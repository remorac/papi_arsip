<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpToday */

$this->title = 'Update Tmp Today: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tmp Todays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tmp-today-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
