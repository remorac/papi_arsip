<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpTodaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmp-today-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'brg_jsa') ?>

    <?= $form->field($model, 'nama_pengadaan') ?>

    <?= $form->field($model, 'monitoring_aktifitas') ?>

    <?= $form->field($model, 'rencana_pengadaan') ?>

    <?php // echo $form->field($model, 'realisasi_pengadaan') ?>

    <?php // echo $form->field($model, 'issue_aktifitas') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
