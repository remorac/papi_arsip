<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TmpToday */

$this->title = 'Create Tmp Today';
$this->params['breadcrumbs'][] = ['label' => 'Tmp Todays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-today-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
