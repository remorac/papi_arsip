<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TmpTodaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tmp Todays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-today-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tmp Today', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'brg_jsa:ntext',
            'nama_pengadaan:ntext',
            'monitoring_aktifitas:ntext',
            'rencana_pengadaan:ntext',
            // 'realisasi_pengadaan:ntext',
            // 'issue_aktifitas:ntext',
            // 'keterangan:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
