<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Posisi */

$this->title = 'Create Posisi';
$this->params['breadcrumbs'][] = ['label' => 'Posisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
