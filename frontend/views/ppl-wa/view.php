<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplWa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Was', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-wa-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pplVendor.name:text:Vendor',
            'pplBuyer.name:text:Buyer',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => '<a target="_blank" href="'.Url::base().'/ppl/'.$model->file.'">'.$model->file.'</a>',
            ],
            'locator',
        ],
    ]) ?>

</div>
