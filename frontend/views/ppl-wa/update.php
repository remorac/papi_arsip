<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplWa */

$this->title = 'Update Ppl Wa: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Was', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-wa-update box box-body box-warning">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
