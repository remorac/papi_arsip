<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplWaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'WA';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-wa-index box box-body box-primary">

    <p>
        <?= Html::a('Create WA', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['update', 'id' => $model->id]).'" class="btn btn-xs btn-default option-update"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],

            // 'id',
            [
                'attribute' => 'ppl_vendor_id',
                'value'     => 'pplVendor.name',
                'filter'    => Select2::widget([
                    'model' => $searchModel, 
                    'attribute' => 'ppl_vendor_id', 
                    'data' => ArrayHelper::map(\frontend\models\PplVendor::find()->all(), 'id', 'name'),                    
                    'options' => ['placeholder' => '[all]'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) 
            ],
            [
                'attribute' => 'ppl_buyer_id',
                'value'     => 'pplBuyer.name',
                'filter'    => Select2::widget([
                    'model' => $searchModel, 
                    'attribute' => 'ppl_buyer_id', 
                    'data' => ArrayHelper::map(\frontend\models\PplBuyer::find()->all(), 'id', 'name'),                    
                    'options' => ['placeholder' => '[all]'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) 
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function($model) {
                    return '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $model->file]).'">'.$model->file.'</a>';
                }
            ],
            'locator',
        ],
    ]); ?>

</div>
