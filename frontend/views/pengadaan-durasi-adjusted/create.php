<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanDurasiAdjusted */

$this->title = 'Create Pengadaan - Adjustment Durasi';
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - Adjustment Durasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-durasi-adjusted-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
