<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanDurasiAdjusted */

$this->title = 'Update Pengadaan - Adjustment Durasi: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - Adjustment Durasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-durasi-adjusted-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
