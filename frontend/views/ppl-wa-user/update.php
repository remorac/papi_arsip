<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplWaUser */

$this->title = 'Update Ppl Wa User: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Wa Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-wa-user-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
