<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplWaUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Wa Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-wa-user-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Wa User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['update', 'id' => $model->id]).'" class="btn btn-xs btn-default option-update"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],

            // 'id',
            [
                'attribute' => 'ppl_user_id',
                'value'     => 'pplUser.name',
                'filter'    => Select2::widget([
                    'model' => $searchModel, 
                    'attribute' => 'ppl_user_id', 
                    'data' => ArrayHelper::map(\frontend\models\PplUser::find()->all(), 'id', 'name'),                    
                    'options' => ['placeholder' => '[all]'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) 
            ],
            [
                'attribute' => 'ppl_buyer_id',
                'value'     => 'pplBuyer.name',
                'filter'    => Select2::widget([
                    'model' => $searchModel, 
                    'attribute' => 'ppl_buyer_id', 
                    'data' => ArrayHelper::map(\frontend\models\PplBuyer::find()->all(), 'id', 'name'),                    
                    'options' => ['placeholder' => '[all]'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) 
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function($model) {
                    return '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $model->file]).'">'.$model->file.'</a>';
                }
            ],
            'locator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
