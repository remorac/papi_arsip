<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplWaUser */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Wa Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-wa-user-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pplUser.name:text:User',
            'pplBuyer.name:text:Buyer',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => '<a target="_blank" href="'.Url::base().'/ppl/'.$model->file.'">'.$model->file.'</a>',
            ],
            'locator',
        ],
    ]) ?>

</div>
