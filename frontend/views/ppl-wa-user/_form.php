<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplWaUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-wa-user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'ppl_user_id')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'select vendor...'],
        'data' => ArrayHelper::map(\frontend\models\PplUser::find()->all(), 'id', 'name'),
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'ppl_buyer_id')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'select buyer...'],
        'data' => ArrayHelper::map(\frontend\models\PplBuyer::find()->all(), 'id', 'name'),
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'uploadedFile')->widget(FileInput::classname(), [
        'options' => ['placeholder' => 'browse document...'],
        'pluginOptions' => [
            'showPreview' => false,
            'showUpload' => false,
        ],
    ]) ?>

    <?= $form->field($model, 'locator')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
