<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplWaUser */

$this->title = 'Create Ppl Wa User';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Wa Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-wa-user box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
