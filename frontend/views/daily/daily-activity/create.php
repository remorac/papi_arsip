<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyActivity */

$this->title = 'Create Daily Activity';
$this->params['breadcrumbs'][] = ['label' => 'Daily Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-activity-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
