<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyActivity */

$this->title = 'Update Daily Activity: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-activity-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
