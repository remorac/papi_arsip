<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyPurchaserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Purchasers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-purchaser-index box box-body box-primary">

    <p>
        <?= Html::a('Create Daily Purchaser', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'user_id',
            'daily_category_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
