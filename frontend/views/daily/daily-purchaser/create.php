<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyPurchaser */

$this->title = 'Create Daily Purchaser';
$this->params['breadcrumbs'][] = ['label' => 'Daily Purchasers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-purchaser-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
