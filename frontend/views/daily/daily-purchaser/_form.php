<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\models\User;
use frontend\models\DailyCategory;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyPurchaser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-purchaser-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username'), ['prompt'=>'- select -']) ?>
	
	<?= $form->field($model, 'daily_category_id')->dropDownList(ArrayHelper::map(DailyCategory::find()->all(), 'id', 'name'), ['prompt'=>'- select -']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
