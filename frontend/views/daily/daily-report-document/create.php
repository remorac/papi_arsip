<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReportDocument */

$this->title = 'Create Daily Report Document';
$this->params['breadcrumbs'][] = ['label' => 'Daily Report Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-document-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
