<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyReportDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Report Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-document-index box box-body box-primary">

    <p>
        <?= Html::a('Create Daily Report Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'daily_report_id',
            'daily_document_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
