<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReportDocument */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Report Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-document-view box box-body box-success">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'daily_report_id',
            'daily_document_id',
        ],
    ]) ?>

</div>
