<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReportDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-report-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'daily_report_id')->textInput() ?>

    <?= $form->field($model, 'daily_document_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
