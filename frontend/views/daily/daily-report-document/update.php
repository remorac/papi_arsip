<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReportDocument */

$this->title = 'Update Daily Report Document: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Report Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-report-document-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
