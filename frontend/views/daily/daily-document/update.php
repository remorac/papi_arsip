<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyDocument */

$this->title = 'Update Daily Document: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-document-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
