<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-document-index box box-body box-primary">

    <p>
        <?= Html::a('Create Daily Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
