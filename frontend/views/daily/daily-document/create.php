<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyDocument */

$this->title = 'Create Daily Document';
$this->params['breadcrumbs'][] = ['label' => 'Daily Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-document-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
