<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Methods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-method-index box box-body box-primary">

    <p>
        <?= Html::a('Create Daily Method', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'duration',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
