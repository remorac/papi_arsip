<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyMethod */

$this->title = 'Update Daily Method: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-method-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
