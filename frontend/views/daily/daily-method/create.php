<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyMethod */

$this->title = 'Create Daily Method';
$this->params['breadcrumbs'][] = ['label' => 'Daily Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-method-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
