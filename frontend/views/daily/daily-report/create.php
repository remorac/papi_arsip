<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReport */

$this->title = 'Create Daily Report';
$this->params['breadcrumbs'][] = ['label' => 'Daily Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
