<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use frontend\models\DailyPurchaser;
use frontend\models\DailyMethod;
use frontend\models\DailyActivity;
use frontend\models\DailyDocument;
use frontend\models\DailyReportDocument;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-report-form">

    <?php $form = ActiveForm::begin([
        /*'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "
                <div class=\"col-md-2 text-right\">{label}</div>\n
                <div class=\"col-md-10\">{input}</div>\n
                <div class=\"col-md-offset-2 col-md-10\">{error}</div>
            ",
        ],*/
    ]); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">

                        <div class="col-md-12">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>

                        <div class="col-md-4">

                            <?= $form->field($model, 'user_initial')->textInput(['maxlength' => 255]) ?>

                            <?php //echo $form->field($model, 'daily_purchaser_id')->dropDownList(ArrayHelper::map(DailyPurchaser::find()->all(), 'id', 'name'), ['prompt'=>'- select -']) ?>
                            
                            <?= $form->field($model, 'daily_method_id')->dropDownList(ArrayHelper::map(DailyMethod::find()->all(), 'id', 'name'), ['prompt'=>'- select -']) ?>

                        </div>

                        <div class="col-md-4" style="border-left:1px dashed silver">

                            <?= $form->field($model, 'is_need_contract')->dropDownList(
                                ['Ya' => 'Ya', 'Tidak' => 'Tidak'], 
                                ['prompt'=>'- select -']) ?>
                        
                            <?php 
                                /*echo $form->field($model, 'plan_start')->widget(DatePicker::classname(), [
                                    'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                                ]);*/ 
                            ?>

                            <?php 
                                echo $form->field($model, 'real_start')->widget(DatePicker::classname(), [
                                    'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                                ]); 
                            ?>

                        </div>

                        <div class="col-md-4" style="border-left:1px dashed silver">

                            <?php 
                                echo $form->field($model, 'plan_finish')->widget(DatePicker::classname(), [
                                    'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                                ]); 
                            ?>

                            <?php 
                                echo $form->field($model, 'real_finish')->widget(DatePicker::classname(), [
                                    'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                                ]); 
                            ?>
                        </div>

                        <div class="col-md-12">
                            <?= $form->field($model, 'activity_issue')->textarea(['rows' => 5]) ?>
                        </div>

                    </div>
                </div>

                <div class="col-md-3" style="border-left:1px dashed silver">
                    <?= $form->field($model, 'am_daily_activity_id')->dropDownList(
                            ArrayHelper::map(DailyActivity::find()->all(), 'id', 'name'), 
                            ['prompt'=>'- select -']) ?>

                    <?php 
                        echo $form->field($model, 'am_target')->widget(DatePicker::classname(), [
                            'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                        ]); 
                    ?>

                    <?= $form->field($model, 'pm_daily_activity_id')->dropDownList(
                            ArrayHelper::map(DailyActivity::find()->all(), 'id', 'name'), 
                            ['prompt'=>'- select -']) ?>

                    <?php 
                        echo $form->field($model, 'pm_start')->widget(DatePicker::classname(), [
                            'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                        ]); 
                    ?>

                    <?php 
                        echo $form->field($model, 'pm_finish')->widget(DatePicker::classname(), [
                            'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd'],
                        ]); 
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-3" style="border-left:1px dashed silver;">
            <label class="control-label">Uncompleted Documents</label>
            <div class="col-md-12 paper" style="height:350px; overflow:auto; padding:0; border:1px solid silver">

                <table class="table table-supercondensed table-hover paper" style="margin:0">
                <?php
                    $modelDocuments = DailyDocument::find()->all();

                    foreach ($modelDocuments as $modelDocument) {
                        $checked    = "";
                        $color      = "gray";
                        foreach ($model->dailyReportDocuments as $dailyReportDocument) {
                            if ($dailyReportDocument->daily_document_id == $modelDocument->id) {
                                $checked    = "checked";
                                $color      = "red";
                                break;
                            }
                        }
                        echo "
                            <tr id='tr".$modelDocument->id."' style='color:".$color."'>
                                <td>
                                    <input type='checkbox' name='c_rep_doc[]' 
                                        id='cb".$modelDocument->id."' 
                                        value='".$modelDocument->id."' 
                                        ".$checked."
                                        onchange='javascript:setRowColor(this.checked, this.value)'>
                                </td>
                                <td style='vertical-align:middle; cursor:arrow'
                                    onclick=\"javascript:$('#cb".$modelDocument->id."').click()\">
                                    ".$modelDocument->name."
                                </td>
                            </tr>
                        ";
                    }
                ?>
                </table>
            </div>
        </div>
    
        <div class="form-group">
            <div class="col-md-12">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs('
        function setRowColor(checked, value) {
            if(checked) {
                $("tr#tr"+value).css("color", "red");
            } else {
                $("tr#tr"+value).css("color", "gray");
            }         
        }
    ', \yii\web\VIEW::POS_END);
?>