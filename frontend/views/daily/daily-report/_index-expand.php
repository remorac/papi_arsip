<?php
	use yii\helpers\Html;
?>

<div class="col-md-11 col-md-offset-1">
	<table class="table table-condensed table-bordered">
		<tr>
			<th>Keterangan</th>
		</tr>
		<tr>
			<td><?= nl2br($model->activity_issue) ?></td>
		</tr>
	</table>
	<table class="table table-condensed table-bordered">
		<tr>
			<th>Uncompleted Documents</th>
		</tr>
		<tr>
			<td>
				<?php if (!empty($model->dailyReportDocuments)) { ?>
                    <ul class="list-group">
                    <?php
                        foreach ($model->dailyReportDocuments as $dailyReportDocument) {
                            echo "
                                <li class='list-group-item' style='padding:5px 10px'>
                                        ".$dailyReportDocument->dailyDocument->name."
                                </li>
                            ";
                        }
                    ?>
                    </ul>
                <?php } else { ?>
                    <big class="text-muted text-center col-md-12">no uncompleted documents.</big>
                <?php } ?>
			</td>
		</tr>
	</table>

	<?= Html::a('No Changes? Update This!', ['no-changes', 'id'=>$model->id], [
									'title' => 'no-changes',
									'class' => 'btn btn-primary pull-right',
									// 'data' => [
									// 		'confirm' => 'Are you sure there\'s no more changes to this item? This button will set LAST UPDATED to today.',
									// 		'method' => 'post',
									// 	],
									]) ?>
</div>
