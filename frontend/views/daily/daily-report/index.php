<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use frontend\models\DailyPurchaser;
use frontend\models\DailyActivity;
use frontend\models\DailyMethod;
use frontend\models\DailyCategory;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-index box box-body box-primary">

    <?php
        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=daily/daily-report/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success' title='view'></i></a>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=daily/daily-report/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary' title='update'></i></a>";
                    $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['daily/daily-report/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                    $opt.= "</div>";
                    return $opt;
                }
            ],

            // 'id',
            [
                'attribute'=>'dailyCategory.id',
                'label'=>'BRG/JSA',
                'value'=>function($data) {
                    return $data->dailyCategory->name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'dailyCategory.id',
                    ArrayHelper::map(DailyCategory::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]',
                    ]
                )
            ],
            'name:ntext',
            'user_initial',
            [
                'attribute'=>'daily_purchaser_id',
                'label'=>'Purchaser',
                'format'=>'raw',
                'value'=>'dailyPurchaser.name',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'daily_purchaser_id',
                    ArrayHelper::map(DailyPurchaser::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]',
                    ]
                )
            ],
            [
                'attribute'=>'daily_method_id',
                'label'=>'Metode',
                'format'=>'raw',
                'value'=>'dailyMethod.name',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'daily_method_id',
                    ArrayHelper::map(DailyMethod::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]',
                    ]
                )
            ],
            // 'is_need_contract',
            // 'plan_start',
            // 'real_start',
            // 'plan_finish',
            /*[
                'attribute'=>'',
                'label'=>'Rencana Schedule Pengadaan',
                'format'=>'raw',
                'value'=>function($data) {
                    return $data->real_start." - ".$data->plan_finish;
                }
            ],
            'real_finish',
            [
                'attribute'=>'am_daily_activity_id',
                'label'=>'Aktivitas Pagi',
                'format'=>'raw',
                'value'=>'amDailyActivity.name',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'pm_daily_activity_id',
                    ArrayHelper::map(DailyActivity::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]',
                    ]
                )
            ],
            'am_target:text:Target Selesai Aktv. Pagi',*/
            [
                'attribute'=>'pm_daily_activity_id',
                'label'=>'Current Activity',
                'format'=>'raw',
                'value'=>'pmDailyActivity.name',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'pm_daily_activity_id',
                    ArrayHelper::map(DailyActivity::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]',
                    ]
                )
            ],
            // 'pm_start',
            // 'pm_finish',
            /*[
                'attribute'=>'',
                'label'=>'Rencana Schedule Aktv. Sore',
                'format'=>'raw',
                'value'=>function($data) {
                    return $data->pm_start." - ".$data->pm_finish;
                }
            ],*/
            // 'activity_issue:ntext',
            [
                'attribute'=>'',
                'format'=>'raw',
                'label'=>'Status Dokumen',
                'value'=>function($data) {
                    if (!empty($data->dailyReportDocuments)) {
                        return "<span class='text-danger'>Belum Lengkap</span>";
                    } else {
                        return "<span class='text-success'>Lengkap</span>";
                    }
                }
            ],
            [
                'attribute'=>'updated_at',
                'format'=>'raw',
                'label'=>'Last Updated',
                'value'=>function($data) {
                    return date('Y-m-d', $data->updated_at) < date('Y-m-d') ?
                        "<span class='text-danger'>".date('Y-m-d', $data->updated_at)."</span>" :
                        "<span class='text-success'>".date('Y-m-d', $data->updated_at)."</span>";
                }
            ],
            [
                'class' =>'kartik\grid\ExpandRowColumn',
                'expandIcon'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-chevron-down"></span>',
                'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('_index-expand.php', ['model'=>$model]);
                },
                'detailOptions'=>[
                    'class'=> 'kv-state-enable',
                ],
            ],
        ];

        $fullExportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true,
            'pjaxContainerId' => 'kv-pjax-container',
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
        ]);
    ?>

    <?= GridView::widget(['dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'floatHeader'=>true,
        'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => 'kv-pjax-container'],
        ],
        'columns' => $gridColumns,
        'toolbar' => [
        //    '{export}',
            Html::a('Create Daily Report', ['create'], ['class' => 'btn btn-success']),
            Html::a('Highlight', ['highlight'], ['class' => 'btn btn-info']),
            $fullExportMenu,
            '{toggleData}',
        ],
        //'export' => ['label' => '&nbsp;Export&nbsp;'],
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        //'exportConfig' => [
        //    GridView::EXCEL => ['filename' => 'Semua_Pengadaan_'.date('Y-m-d')],
        //    GridView::CSV => ['filename' => 'Semua_Pengadaan_'.date('Y-m-d')],
        //    GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
        //],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
    ]); ?>

</div>
