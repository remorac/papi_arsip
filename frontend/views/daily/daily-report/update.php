<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReport */

$this->title = 'Update Daily Report: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-report-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
