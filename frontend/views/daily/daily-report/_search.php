<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'user_initial') ?>

    <?= $form->field($model, 'daily_purchaser_id') ?>

    <?php // echo $form->field($model, 'activity_issue') ?>

    <?php // echo $form->field($model, 'is_need_contract') ?>

    <?php // echo $form->field($model, 'am_daily_activity_id') ?>

    <?php // echo $form->field($model, 'am_target') ?>

    <?php // echo $form->field($model, 'pm_daily_activity_id') ?>

    <?php // echo $form->field($model, 'pm_start') ?>

    <?php // echo $form->field($model, 'pm_finish') ?>

    <?php // echo $form->field($model, 'plan_start') ?>

    <?php // echo $form->field($model, 'plan_finish') ?>

    <?php // echo $form->field($model, 'real_start') ?>

    <?php // echo $form->field($model, 'real_finish') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
