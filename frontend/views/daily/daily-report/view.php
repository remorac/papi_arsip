<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DailyReport */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daily Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-report-view box box-body box-success">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
    <div class="col-md-8">
        <?= DetailView::widget([
            'model' => $model,
            'options'=> ['class' => 'table detail-view paper'],
            'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
            'attributes' => [
                'id',
                'name:ntext',
                'user_initial',
                'dailyMethod.name:text:Metoda',
                'dailyPurchaser.name:text:Purchaser',
                'dailyPurchaser.dailyCategory.name:text:Kategori',
                'is_need_contract',
                // 'plan_start',
                'real_start',
                'plan_finish',
                'real_finish',
                'amDailyActivity.name:text:Aktivitas Pagi',
                'am_target',
                'pmDailyActivity.name:text:Aktivitas Sore',
                'pm_start',
                'pm_finish',
                'activity_issue:ntext',
                'created_at:timestamp',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
    </div>
    <div class="col-md-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="panel-title">Uncompleted Documents</div>
            </div>
            <div class='panel-body' style="min-height:638px;">
                <?php if (!empty($model->dailyReportDocuments)) { ?>
                    <ul class="list-group">
                    <?php            
                        foreach ($model->dailyReportDocuments as $dailyReportDocument) {
                            echo "
                                <li class='list-group-item' style='padding:5px 10px'>
                                        ".$dailyReportDocument->dailyDocument->name."
                                </li>
                            ";
                        }
                    ?> 
                    </ul>
                <?php } else { ?>
                    <h1 class="text-muted text-center">no uncompleted documents.</h1>
                <?php } ?>
            </div>
        </div>
    </div>

</div>
