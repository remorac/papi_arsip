<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use frontend\models\DailyReport;
use frontend\models\DailyReportDocument;
use frontend\models\DailyPurchaser;
use frontend\models\DailyActivity;
use frontend\models\DailyMethod;
use frontend\models\DailyCategory;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Reports';
$this->params['breadcrumbs'][] = $this->title;

$brg_total = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['daily_category_id' => '1'])->count();
$jsa_total = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['daily_category_id' => '2'])->count();

?>
<div class="daily-report-index box box-body box-primary">

  <p>
    <?= Html::a('Detail', ['index'], ['class' => 'btn btn-info']) ?>
  </p>

  <div class="nav-tabs-custom paper">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tabBarang" aria-controls="tabBarang" role="tab" data-toggle="tab">BARANG &nbsp; <span class=" badge pull-right"><?= $brg_total ?></span></a></li>
        <li role="presentation"><a href="#tabJasa" aria-controls="tabJasa" role="tab" data-toggle="tab">JASA &nbsp; <span class="badge pull-right"><?= $jsa_total ?></span></a></li>
    </ul>
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="tabBarang">

      <div class="row">

        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title">Resume Per Aktivitas</div>
            </div>
            <div class="panel-body" id="brg_parent">

              <?php
                $dailyActivities = DailyActivity::find()->asArray()->all();
                foreach ($dailyActivities as $dailyActivity) {
                  $act_count = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['pm_daily_activity_id' => $dailyActivity['id'], 'daily_category_id' => '1'])->count();
                  if ($act_count > 0) {
              ?>
                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#brg_parent" href="#brg<?=$dailyActivity['id']?>" style="padding: 7px 10px !important">
                        <b><?=$dailyActivity['name']?> <span class="pull-right btn-xs btn-default"><?= $act_count ?></span></b>
                    </div>
                    <div id="brg<?=$dailyActivity['id']?>" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                          <?php
                            $actDailyReports = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['pm_daily_activity_id' => $dailyActivity['id'], 'daily_category_id' => '1'])->all();
                            foreach ($actDailyReports as $actDailyReport) {
                            echo "<table class='table table-condensed'><tr><td colspan=2><b>".$actDailyReport->name."</b></td></tr>"
                                ."<tr><td width='100px'>User</td><td>".$actDailyReport->user_initial."</td></tr>"
                                ."<tr><td>Purchaser</td><td>".$actDailyReport->dailyPurchaser->name."</td></tr>"
                                ."<tr><td>Keterangan</td><td>".nl2br($actDailyReport->activity_issue)."</td></tr>"
                                ."<tr><td>Last Updated</td><td>".Yii::$app->formatter->asRelativeTime($actDailyReport->updated_at)."</td></tr>"
                                ."</table>";
                            }
                          ?>
                        </div>
                    </div>
                </div>
              <?php
                  }
                }
              ?>

            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title">Resume Per Purchaser</div>
            </div>
            <div class="panel-body" id="prc_parent">
              <?php
                $dailyPurchasers = DailyPurchaser::find()->where(['daily_category_id'=>'1'])->asArray()->all();
                foreach ($dailyPurchasers as $dailyPurchaser) {

                  $act_count = DailyReport::find()->where(['daily_purchaser_id'=>$dailyPurchaser['id']])->count();

                  $unc_count = DailyReportDocument::find()
                                ->joinWith(['dailyReport'])
                                ->where(['daily_purchaser_id'=>$dailyPurchaser['id']])
                                ->andWhere('daily_report_id is not null')
                                ->groupBy('daily_report_id')
                                ->count();

                  $cmp_count = $act_count - $unc_count;

                  $notUpdatedCount  = DailyReport::find()->where(['daily_purchaser_id'=>$dailyPurchaser['id']])->andWhere("FROM_UNIXTIME(updated_at, '%Y%m%d') != FROM_UNIXTIME(UNIX_TIMESTAMP(), '%Y%m%d') ")->count();
                  if ($notUpdatedCount == 0) { $updateStatus = "<i>completely updated</i>"; }
                  elseif ($notUpdatedCount == $act_count) { $updateStatus = "<i class='text-danger'>not updated yet</i>";}
                  else { $updateStatus = "<i class='text-warning'>partially updated</i>";}

                  if ($act_count > 0) {
              ?>
                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#prc_parent" href="#prc<?=$dailyPurchaser['id']?>" style="padding: 7px 10px !important">
                        <b><?=$dailyPurchaser['name']?></b> - <?=$updateStatus?> <span class="pull-right btn-xs btn-default"><?= $unc_count ?> belum lengkap, <?= $cmp_count ?> lengkap, total <b><?= $act_count ?></b></span>
                    </div>
                    <div id="prc<?=$dailyPurchaser['id']?>" class="panel-collapse collapse">
                      <div class="panel-body" style="background:#fafafa">
                        <?php
                          $actDailyReports = DailyReport::find()->where(['daily_purchaser_id' => $dailyPurchaser['id']])->all();
                          foreach ($actDailyReports as $actDailyReport) {
                            $statusDocuments = count($actDailyReport->dailyReportDocuments) == 0 ? "Lengkap" : "Belum Lengkap";
                            echo "<table class='table table-condensed'><tr><td colspan=2><b>".$actDailyReport->name."</b></td></tr>"
                              ."<tr><td width='100px'>User</td><td>".$actDailyReport->user_initial."</td></tr>"
                              ."<tr><td>Current Activity</td><td>".$actDailyReport->pmDailyActivity->name."</td></tr>"
                              ."<tr><td>Status Dokumen</td><td>".$statusDocuments."</td></tr>"
                              ."<tr><td>Keterangan</td><td>".nl2br($actDailyReport->activity_issue)."</td></tr>"
                              ."<tr><td>Last Updated</td><td>".Yii::$app->formatter->asRelativeTime($actDailyReport->updated_at)."</td></tr>"
                              ."</table>";
                          }
                        ?>
                      </div>
                    </div>
                </div>
              <?php
                  }
                }
              ?>
            </div>
          </div>
        </div>

      </div> <!-- end row -->

  </div>

  <div role="tabpanel" class="tab-pane" id="tabJasa">

    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">Resume Per Aktivitas</div>
          </div>
          <div class="panel-body" id="jsa_parent">

            <?php
              $dailyActivities = DailyActivity::find()->asArray()->all();
              foreach ($dailyActivities as $dailyActivity) {
                $act_count = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['pm_daily_activity_id' => $dailyActivity['id'], 'daily_category_id' => '2'])->count();
                if ($act_count > 0) {
            ?>
              <div class="panel panel-success" style="margin-bottom:5px">
                  <div class="panel-heading pointer" data-toggle="collapse" data-parent="#jsa_parent" href="#jsa<?=$dailyActivity['id']?>" style="padding: 7px 10px !important">
                      <b><?=$dailyActivity['name']?> <span class="pull-right btn-xs btn-default"><?= $act_count ?></span></b>
                  </div>
                  <div id="jsa<?=$dailyActivity['id']?>" class="panel-collapse collapse">
                    <div class="panel-body" style="background:#fafafa">
                      <?php
                        $actDailyReports = DailyReport::find()->joinWith(['dailyPurchaser'])->where(['pm_daily_activity_id' => $dailyActivity['id'], 'daily_category_id' => '2'])->all();
                        foreach ($actDailyReports as $actDailyReport) {
                        echo "<table class='table table-condensed'><tr><td colspan=2><b>".$actDailyReport->name."</b></td></tr>"
                            ."<tr><td width='100px'>User</td><td>".$actDailyReport->user_initial."</td></tr>"
                            ."<tr><td>Purchaser</td><td>".$actDailyReport->dailyPurchaser->name."</td></tr>"
                            ."<tr><td>Keterangan</td><td>".nl2br($actDailyReport->activity_issue)."</td></tr>"
                            ."<tr><td>Last Updated</td><td>".Yii::$app->formatter->asRelativeTime($actDailyReport->updated_at)."</td></tr>"
                            ."</table>";
                        }
                      ?>
                    </div>
                  </div>
              </div>
            <?php
                }
              }
            ?>

          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">Resume Per Purchaser</div>
          </div>
          <div class="panel-body" id="prc_parent">
            <?php
              $dailyPurchasers = DailyPurchaser::find()->where(['daily_category_id'=>'2'])->asArray()->all();
              foreach ($dailyPurchasers as $dailyPurchaser) {

                $act_count = DailyReport::find()->where(['daily_purchaser_id'=>$dailyPurchaser['id']])->count();

                $unc_count = DailyReportDocument::find()
                              ->joinWith(['dailyReport'])
                              ->where(['daily_purchaser_id'=>$dailyPurchaser['id']])
                              ->andWhere('daily_report_id is not null')
                              ->groupBy('daily_report_id')
                              ->count();

                $cmp_count = $act_count - $unc_count;

                $notUpdatedCount  = DailyReport::find()->where(['daily_purchaser_id'=>$dailyPurchaser['id']])->andWhere("FROM_UNIXTIME(updated_at, '%Y%m%d') != FROM_UNIXTIME(UNIX_TIMESTAMP(), '%Y%m%d') ")->count();
                if ($notUpdatedCount == 0) { $updateStatus = "<i>completely updated</i>"; }
                elseif ($notUpdatedCount == $act_count) { $updateStatus = "<i class='text-danger'>not updated yet</i>";}
                else { $updateStatus = "<i class='text-warning'>partially updated</i>";}

                if ($act_count > 0) {
                ?>
                <div class="panel panel-success" style="margin-bottom:5px">
                  <div class="panel-heading pointer" data-toggle="collapse" data-parent="#prc_parent" href="#prc<?=$dailyPurchaser['id']?>" style="padding: 7px 10px !important">
                      <b><?=$dailyPurchaser['name']?></b> - <?=$updateStatus?> <span class="pull-right btn-xs btn-default"><?= $unc_count ?> belum lengkap, <?= $cmp_count ?> lengkap, total <b><?= $act_count ?></b></span>
                  </div>
                  <div id="prc<?=$dailyPurchaser['id']?>" class="panel-collapse collapse">
                    <div class="panel-body" style="background:#fafafa">
                      <?php
                        $actDailyReports = DailyReport::find()->where(['daily_purchaser_id' => $dailyPurchaser['id']])->all();
                        foreach ($actDailyReports as $actDailyReport) {
                          $statusDocuments = count($actDailyReport->dailyReportDocuments) == 0 ? "Lengkap" : "Belum Lengkap";
                          echo "<table class='table table-condensed'><tr><td colspan=2><b>".$actDailyReport->name."</b></td></tr>"
                            ."<tr><td width='100px'>User</td><td>".$actDailyReport->user_initial."</td></tr>"
                            ."<tr><td>Current Activity</td><td>".$actDailyReport->pmDailyActivity->name."</td></tr>"
                            ."<tr><td>Status Dokumen</td><td>".$statusDocuments."</td></tr>"
                            ."<tr><td>Keterangan</td><td>".nl2br($actDailyReport->activity_issue)."</td></tr>"
                            ."<tr><td>Last Updated</td><td>".Yii::$app->formatter->asRelativeTime($actDailyReport->updated_at)."</td></tr>"
                            ."</table>";
                        }
                      ?>
                    </div>
                  </div>
              </div>
            <?php
                }
              }
            ?>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
