<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DailyCategory */

$this->title = 'Create Daily Category';
$this->params['breadcrumbs'][] = ['label' => 'Daily Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-category-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
