<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DailyCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-category-index box box-body box-primary">

    <p>
        <?= Html::a('Create Daily Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
