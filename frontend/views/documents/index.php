<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

use frontend\models\BarangJasa;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            [
                'label' => '',
                'format' => 'raw',
                'headerOptions' => ['class' => 'opsi-xs'],
                'value' => function($data){
                    if($data->id){
                        return Html::a('<i class="glyphicon glyphicon-file text-primary">', '?r=documents/view&id='.$data->id, [
                            'title' => 'detail',
                            'class' => 'btn btn-xs btn-default',
                        ]);
                    }
                },
            ],
            //'kode',
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode, 4, "0", STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => 'barangJasa',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barang_jasa_id]=' . $data->barang_jasa_id.'&r=pengadaan');
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'barang_jasa_id', ArrayHelper::map(BarangJasa::find()->asArray()->where("id!='4'")->all(), 'id', 'nama'),['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            [
                'attribute' => 'documents_verification',
                'format' => 'raw',
                'value' => function($data){
                    return $data->documents_verification == 1 ? 
                    "<font color='green'><b>VERIFIED</b></font>" : 
                    "<font color='red'><b>UNVERIFIED</b></font>";
                },
                'filter' => Html::activeDropDownList($searchModel, 'documents_verification', ['0'=>'UNVERIFIED','1'=>'VERIFIED'],['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
