<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use frontend\models\Pengadaan;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\UnitPosisi;
use frontend\models\KorinDokumenSearch;
use frontend\models\PengadaanActivityDokumenSearch;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-view box box-body box-info">

    <div class="row">
        <div class="col-md-6">
            <?= Html::button('Print', [ 'class' => 'btn btn-primary', 'onclick' => 'window.print();' ]); ?>
        </div>
        <div class="col-md-6 text-right">
            <?php
                $next = Pengadaan::find()
                        ->where('id >'.$model->id)
                        ->orderBy('id')
                        ->limit(1)
                        ->all();
                $prev = Pengadaan::find()
                        ->where('id <'.$model->id)
                        ->orderBy(['id'=> SORT_DESC])
                        ->limit(1)
                        ->all();
            ?>
            <?php if ($prev) echo Html::a('Prev', '?r=documents/view&id='.$prev[0]->id, [ 'class' => 'btn btn-default']); ?>
            <?php if ($next) echo Html::a('Next', '?r=documents/view&id='.$next[0]->id, [ 'class' => 'btn btn-default']); ?>
        </div>
    </div>
    <p></p>
    
	<div class="paper" style="padding:20px !important">
		<div class="printable">
			<h1><?=$model->nama?></h1>
			<table class="vtop" width="100%">
				<tr>
				<td width="100%">
					<table class="vtop">
						<tr>
							<td>KODE</td>
							<td>&nbsp;:&nbsp;</td>
							<td><?=$model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT)?></td>
						</tr>
						<tr>
							<td>JENIS (BARANG/JASA)</td>
							<td>&nbsp;:&nbsp;</td>
							<td><?=$model->barangJasa->nama?></td>
						</tr>
						<tr>
							<td>UNIT USER</td>
							<td>&nbsp;:&nbsp;</td>
							<td>
								<?php 
									$pic = "";
									$pengadaanKorin = PengadaanKorin::find()->where(['pengadaan_id' => $model->id])->all(); 
									if ($pengadaanKorin) {
										foreach ($pengadaanKorin as $row) {
											$korin = Korin::find()->where(['=', 'id', $row->korin_id])->one();
											if ($korin) {
												$arr_korin_id = explode('/', $korin->no_surat);
												$kodeSurat = $arr_korin_id[3];
												$unitPosisi = UnitPosisi::find()->where(['=', 'kode_surat', $arr_korin_id[3]])->one();
												if (!$unitPosisi) $unitPosisi = UnitPosisi::find()->where(['=', 'kode_surat', $arr_korin_id[4]])->one();
												if ($unitPosisi) echo $unitPosisi->nama."<br>"; else echo $arr_korin_id[3]."<br>";
												$pic .= $korin->pic_initial."<br>";
											}
										}
									}
								?>
							</td>
						</tr>
						<tr>
							<td>PIC USER</td>
							<td>&nbsp;:&nbsp;</td>
							<td><?=$pic?></td>
						</tr>
						<tr>
							<td>PURCHASER </td>
							<td>&nbsp;:&nbsp;</td>
							<td><?=$model->purchaserPersonelProcurement->personel->nama?></td>
						</tr>
						<tr>
							<td>METODA</td>
							<td>&nbsp;:&nbsp;</td>
							<td><?=$model->metoda->nama?></td>
						</tr>
					</table>
				</td>
				<td>
					<table class="vtop">
						
					</table>
				</td>
				</tr>
			</table>

			<br>
			<div class="panel panel-success">
				<div class="panel-heading"><div class="panel-title">KORIN PP Documents</div></div>
				<div class="panel-body">   
					<?php
						foreach ($model->pengadaanKorins as $row) {
							echo "<big>".$row->korin->no_surat."</big>";
							$searchModelKorinDokumen      	= new KorinDokumenSearch();
					        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $row->korin_id]]);
					        $dataProviderKorinDokumen     	= $searchModelKorinDokumen->search(Yii::$app->request->queryParams);

					        echo GridView::widget([
							    'dataProvider' => $dataProviderKorinDokumen,
							    'filterModel' => null,
							    'summary' => '',
							    'tableOptions' => ['class' => 'table  table-hover paper'],
							    'columns' => [
							        [   
							            'class' => 'yii\grid\SerialColumn',
							            'headerOptions' => ['class' => 'serial-column'],
							            'contentOptions' => ['class' => 'serial-column'],
							        ],
							        [
							        	'attribute'=>'dokumen_jenis_id',
							        	'format'=>'raw',
							        	'label'=>'Jenis',
							        	'value'=>function($data) {return $data->dokumenUpload->dokumenJenis->nama;}
							        ],
							        [
							        	'attribute'=>'dokumen_upload_id',
							        	'format'=>'raw',
							        	'label'=>'File',
							        	'value'=>function($data) {return "<a target='_blank' href='".Url::base()."/document/".$data->dokumenUpload->pdf_filename."'>".$data->dokumenUpload->pdf_filename."</a>";}
							        ],
							        [
							        	'attribute'=>'dokumenUpload.no_dokumen',
							        	'format'=>'raw',
							        	'label'=>'No. Dokumen',
							        	'value'=>function($data) {return $data->dokumenUpload->no_dokumen;}
							        ],
							        [
							        	'attribute'=>'dokumenUpload.tgl_dokumen',
							        	'format'=>'raw',
							        	'label'=>'Tgl. Dokumen',
							        	'value'=>function($data) {return $data->dokumenUpload->tgl_dokumen;}
							        ],
							    ],
							]); 
						}
						
        			?>
				</div>
			</div>
			   

			<div class="panel panel-success">
				<div class="panel-heading"><div class="panel-title">Activity Documents</div></div>
				<div class="panel-body">        
					<?php
						foreach ($model->pengadaanActivities as $row) {
							echo "<big>".$row->activity->nama."</big>";
							$searchModelPengadaanActivityDokumen   = new PengadaanActivityDokumenSearch();
					        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanActivityDokumenSearch' => ['pengadaan_activity_id' => $row->id]]);
					        $dataProviderPengadaanActivityDokumen     	= $searchModelPengadaanActivityDokumen->search(Yii::$app->request->queryParams);

					        echo GridView::widget([
							    'dataProvider' => $dataProviderPengadaanActivityDokumen,
							    'filterModel' => null,
							    'summary' => '',
							    'tableOptions' => ['class' => 'table  table-hover paper'],
							    'columns' => [
							        [   
							            'class' => 'yii\grid\SerialColumn',
							            'headerOptions' => ['class' => 'serial-column'],
							            'contentOptions' => ['class' => 'serial-column'],
							        ],
							        [
							        	'attribute'=>'dokumen_jenis_id',
							        	'format'=>'raw',
							        	'label'=>'Jenis',
							        	'value'=>function($data) {return $data->dokumenUpload->dokumenJenis->nama;}
							        ],
							        [
							        	'attribute'=>'dokumen_upload_id',
							        	'format'=>'raw',
							        	'label'=>'File',
							        	'value'=>function($data) {return "<a target='_blank' href='".Url::base()."/document/".$data->dokumenUpload->pdf_filename."'>".$data->dokumenUpload->pdf_filename."</a>";}
							        ],
							        [
							        	'attribute'=>'dokumenUpload.no_dokumen',
							        	'format'=>'raw',
							        	'label'=>'No. Dokumen',
							        	'value'=>function($data) {return $data->dokumenUpload->no_dokumen;}
							        ],
							        [
							        	'attribute'=>'dokumenUpload.tgl_dokumen',
							        	'format'=>'raw',
							        	'label'=>'Tgl. Dokumen',
							        	'value'=>function($data) {return $data->dokumenUpload->tgl_dokumen;}
							        ],
							    ],
							]); 
						}
						
        			?>
				</div>
			</div>

		</div>
	</div>
</div>


<?php 
    if ($prev) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 37:
                    window.location="?r=documents/view&id="+'.$prev[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault();
        });
        ', \yii\web\VIEW::POS_READY
    ); 

    if ($next) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 39:
                    window.location="?r=documents/view&id="+'.$next[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault(); 
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

