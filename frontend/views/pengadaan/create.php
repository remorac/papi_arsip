<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$title_ext = $is_centerled ? ' Centerled' : '';
$this->title = 'Create Pengadaan'.$title_ext;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'is_centerled' => $is_centerled,
    ]) ?>

</div>
