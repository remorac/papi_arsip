<?php 

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinDokumen,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => 'dokumenJenis.nama',
            'label' => 'Jenis Dokumen',
            'format' => 'raw',
            'value' => function($data){
                if($data->dokumenUpload->dokumenJenis){
                    return $data->dokumenUpload->dokumenJenis->nama;
                }
            },
        ],
        [
            'attribute' => 'dokumenUpload.pdf_filename',
            'label' => 'File',
            'format' => 'raw',
            'value' => function($data){
                if($data->dokumenUpload){
                    return Html::a('Open', 'document/'.$data->dokumenUpload->pdf_filename, ['target'=>'blank', 'data-pjax'=>0, 'class'=>'btn btn-xs btn-default']);
                }
            },
        ],
        [
            'attribute' => 'dokumenUpload.no_dokumen',
            'label' => 'No. Dokumen',
            'format' => 'raw',
            'value' => function($data){
                if($data->dokumenUpload){
                    return Html::a($data->dokumenUpload->no_dokumen, '?r=dokumen-upload/view&id='.$data->dokumenUpload->id, ['target'=>'blank', 'data-pjax'=>0]);
                }
            },
        ],
        [
            'attribute' => 'dokumenUpload.tgl_dokumen',
            'label' => 'Tgl. Dokumen',
            'format' => 'raw',
            'value' => function($data){
                if($data->dokumenUpload){
                    return $data->dokumenUpload->tgl_dokumen;
                }
            },
        ],
    ],
]); ?>
