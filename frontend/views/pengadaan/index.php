<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\PengadaanKorin;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inbox Manager';
$this->params['breadcrumbs'][] = "Manager";
$this->params['breadcrumbs'][] = "Inbox";
?>
<div class="korin-assignment-index box box-body box-primary">

    
    <div class="btn-group">
        <?= Html::a('Inbox', '#', ['class' => 'btn btn-info active']) ?>
        <?= Html::a('Outbox', ['outbox'], ['class' => 'btn btn-default']) ?>
    </div>
    <?= Html::a('Create Centerled Proc', ['create', 'is_centerled' => 1], ['class' => 'btn btn-default']) ?>
    <?= Html::a('Create Pengadaan', ['create'], ['class' => 'btn btn-default']) ?>
    <p></p>
    
    <?php $toOutbox = false; ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    if ($data->status == 0) {
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/assign&id=".$data->id."' data-pjax=0 title='Assign'><i class='glyphicon glyphicon-hand-right text-success'></i></a>";
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/merge&id=".$data->id."' data-pjax=0 title='Merge'><i class='glyphicon glyphicon-log-in text-primary'></i></a>";
                    } elseif ($data->status == 1) {
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/assign&id=".$data->id."' data-pjax=0 title='Assign'><i class='glyphicon glyphicon-hand-right text-success'></i></a>";
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/merge&id=".$data->id."' data-pjax=0 title='Merge'><i class='glyphicon glyphicon-log-in text-primary'></i></a>";
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/done&id=".$data->id."' data-pjax=0 title='Mark as Done'><i class='glyphicon glyphicon-arrow-down text-warning'></i></a>";
                    } elseif ($data->status == 2) {
                        $opt .= "<a class='btn btn-xs btn-default' href='?r=pengadaan/undone&id=".$data->id."' data-pjax=0 title='Mark as Undone'><i class='glyphicon glyphicon-arrow-up text-warning'></i></a>";
                    }
                    $opt.="</div>";
                    return $opt;
                }
            ],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            //'korin_id',
            [
                'attribute' => 'korin.no_surat',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin/view&id=' . $data->korin->id);
                    }
                },
            ],
            'korin.perihal',
            'tanggal_assignment',
            //'mgr_personel_id',
            [
                'attribute' => 'personelProcurement.personel.nama',
                'label' => 'Assigned to',
                'format' => 'raw',
                'value' => function($data){
                    if($data->personelProcurement){
                        //return Html::a($data->personelProcurement->personel->nama, '?r=personel/view&id=' . $data->personelProcurement->id);
                        return Html::a($data->personelProcurement->personel->nama, '#');
                    }
                },
            ],
            'keterangan:ntext',
            // [
            //     'attribute' => 'status',
            //     'label' => 'Status',
            //     'format' => 'raw',
            //     'value' => function($data){
            //         if($data->status == '0') return "0 - Belum Didisposisi"; 
            //         if($data->status == '1') return "1 - Sudah Didisposisi";
            //         if($data->status == '2') return "2 - Selesai";
            //     },
            // ],
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
                'attribute' => 'jumlahPengadaanKorin',
                'label' => 'Assigned',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 70px;'],
                'value' => function($data){
                    return $data->jumlahPengadaanKorin > 0 ? '<span class="glyphicon glyphicon-ok"></span> ('.$data->jumlahPengadaanKorin.'x)' : '<span class="text-danger">0</span>' ;
                },
            ],

            /*[
                'attribute' => 'dispositionTime',
                'label' => 'Disposition Time',
                'format' => 'raw',
                'value' => function($data) {
                    $return = "";
                    foreach ($data->pengadaanKorins as $pengadaanKorin) {
                        $return = $pengadaanKorin->updated_at;
                    }
                    return $return;
                }
            ],*/

/*
            [
                'attribute' => 'pengadaanKorins.id',
                'label' => 'Assigned',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 70px;'],
                'value' => function($data){
                    if($data->pengadaanKorins){
                        $count_pengadaanKorin = PengadaanKorin::find()->where("korin_id = '".$data->korin_id."'")->count();
                        return '<span class="glyphicon glyphicon-ok"></span> ('.$count_pengadaanKorin.'x)' ;
                    } else { return ''; }
                },
            ],
*/
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
