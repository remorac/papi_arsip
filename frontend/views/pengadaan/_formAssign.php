<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use frontend\models\Metoda;
use frontend\models\PersonelProcurement;
use frontend\models\Pengadaan;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanKorin;
use frontend\models\BarangJasa;
use frontend\models\LevelApproval;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-form">

    <div class="row">
        <div class="col-md-4">
            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-md-4 text-right\">{label}</div>\n<div class=\"col-md-8\">{input}</div>\n<div class=\"col-md-offset-4 col-md-8\">{error}</div>",
                ],
            ]); ?>

            <?= $form->field($model, 'nama')->textarea(['maxlength' => 200, 'rows' => 2]) ?>

            <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>

            <?= $form->field($model, 'metoda_id')->dropDownList(ArrayHelper::map(Metoda::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>

            <?= $form->field($model, 'purchaser_personel_procurement_id')->dropDownList(ArrayHelper::map(PersonelProcurement::find()->where("jabatan='Purchaser'")->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>
            
            <?= $form->field($model, 'level_approval_id')->dropDownList(ArrayHelper::map(LevelApproval::find()->orderBy("id desc")->all(), 'id', 'approver')) ?>

            <?php // echo $form->field($model, 'nilai_oe')->textInput() ?>
            <!-- 
            <?= $form->field($model, 'start_plan')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>
            -->
            <?= $form->field($model, 'requirement_date')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>
                
        <!-- 
            <?= $form->field($model, 'delivery_time')->textInput() ?>

            <?= $form->field($model, 'po_reference')->textInput() ?>
        -->

            <?= $form->field($model, 'isi_disposisi')->textarea(['maxlength' => 200, 'rows' => 1, 'value' => 'Untuk diproses.']) ?>

            <?= $form->field($model, 'subgroup')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Search Subgroup ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['sap-subgroup/list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                    'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                ],
            ]);?>

            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php //echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div class="col-md-6">
            <div class="nav-tabs-custom paper">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#korin" aria-controls="home" role="tab" data-toggle="tab">KORIN</a></li>
                    <li role="presentation"><a href="#document" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
                    <!-- <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab" data-toggle="tab">Bidder List</a></li> -->
                    <!-- <li role="presentation"><a href="#itemKorin" aria-controls="messages" role="tab" data-toggle="tab">Item KORIN PP</a></li> -->
                    <!-- <li role="presentation"><a href="#itemPr" aria-controls="settings" role="tab" data-toggle="tab">Item PR</a></li> -->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="korin">
                        <?= DetailView::widget([
                            'model' => $modelKorin,
                            'options'=> ['class' => 'table  detail-view paper'],
                            'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
                            'attributes' => [
                                //'id',
                                'no_surat',
                                [
                                    'attribute' => 'korinJenis.nama',
                                    'label' => 'Jenis KORIN',
                                    'format' => 'raw',
                                    'value' => Html::a(
                                        $modelKorin->korinJenis ? $modelKorin->korinJenis->nama : '', 
                                        $modelKorin->korinJenis ? '?r=/korin-jenis/view&id=' . $modelKorin->korinJenis->id : ''
                                        )
                                ],
                                [
                                    'attribute' => 'barangJasa.nama',
                                    'label' => 'Barang/Jasa',
                                ],
                                'pic_initial',
                                'tgl_terima',
                                'tanggal_surat',
                                // 'ringkasan_isi:ntext',
                                'perihal',
                                'tembusan',
                                'yg_menandatangani',
                                'lampiran',
                                'isi_disposisi:ntext',
                                // 'kebutuhan_disposisi',
                                'requirement_date',
                                'revisi_referensi',
                            ],
                        ]) ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="document">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'document_list']); ?>
                            <?= $this->render('dokumen', [
                                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="vendor">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'vendor_list']); ?>
                            <?= $this->render('vendor', [
                                'searchModelKorinVendor' => $searchModelKorinVendor,
                                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemKorin">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemKorin_list']); ?>
                            <?= $this->render('itemKorin', [
                                'searchModelKorinItem' => $searchModelKorinItem,
                                'dataProviderKorinItem' => $dataProviderKorinItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="itemPr">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemPr_list']); ?>
                            <?= $this->render('itemPr', [
                                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
                            ]) ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div> <!-- end of col -->

            <?php 
                $pengadaanKorin = PengadaanKorin::find()->where("korin_id = '".$modelKorin->id."'")->all();
                if (is_array($pengadaanKorin)) {
            ?>
            <br>
            <div class="paper col-sm-12">
                <h3>Pengadaan Terkait</h3>
                <ul>
                    <?php foreach ($pengadaanKorin as $row) {
                        echo "<li><a target='_blank' href='?r=highlight/view&id=".$row->pengadaan_id."'><big>"
                            .$row->pengadaan->barangJasa->kode.str_pad($row->pengadaan->kode,5,"0",STR_PAD_LEFT)
                            ." - ".$row->pengadaan->nama
                            ."</big></a></li>";
                    }?>
                </ul>
                <br>
            </div>
            <?php } ?>
        </div>

        <div class="col-md-2">
            <b>WORKLOAD PURCHASER</b>
            <table class="table  paper">
                <?php
                    $purchaser = PersonelProcurement::find()->where("jabatan = 'Purchaser'")->all();
                    foreach ($purchaser as $row) {
                        $workload   = Pengadaan::find()->where("purchaser_personel_procurement_id = '".$row->id."'")->count();
                        $poDone     = PengadaanActivity::find()->joinWith('pengadaan')
                            ->where("purchaser_personel_procurement_id = '".$row->id."' and
                                    activity_id = '11' and (end_date is not null and end_date<>'0000-00-00')")->count();
                        $inprogress = $workload - $poDone;
                        echo    "<tr><th>".$row->personel->singkatan.
                                "</th><td>
                                    <b>".$inprogress."</b> In Progress <br>".
                                    $poDone." PO Done <br>".
                                    $workload." Total <br>".
                                "</td></tr>";
                    }
                ?>
            </table>
        </div>
    </div>

</div>



<?php 
    $this->registerJs('$("#pengadaan-nama").val("'.$modelKorin->perihal.'")', \yii\web\VIEW::POS_READY);
    $this->registerJs('$("#pengadaan-barang_jasa_id").val("'.$modelKorin->barang_jasa_id.'")', \yii\web\VIEW::POS_READY); 
    if ($modelKorin->barang_jasa_id == 1 || $modelKorin->barang_jasa_id == 2)  
    $this->registerJs('$("#pengadaan-barang_jasa_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
    //$this->registerJs('$("#pengadaan-nilai_oe").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
?>