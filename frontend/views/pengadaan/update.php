<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = 'Update Pengadaan: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
