<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = 'Disposisi';
$this->params['breadcrumbs'][] = ['label' => 'Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-create box box-body box-success">

    <?= $this->render('_formAssign', [
        'model' => $model,
        'modelKorin' => $modelKorin,

        'searchModelKorinDokumen' => $searchModelKorinDokumen,
        'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

        'searchModelKorinVendor' => $searchModelKorinVendor,
        'dataProviderKorinVendor' => $dataProviderKorinVendor,
        
        'searchModelKorinItem' => $searchModelKorinItem,
        'dataProviderKorinItem' => $dataProviderKorinItem,

        'searchModelKorinPrItem' => $searchModelKorinPrItem,
        'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
    ]) ?>

</div>
