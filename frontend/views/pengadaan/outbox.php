<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Outbox';
$this->params['breadcrumbs'][] = "Manager";
$this->params['breadcrumbs'][] = "Outbox";
?>
<div class="pengadaan-outbox box box-body box-primary">

    <div class="btn-group">
        <?= Html::a('Inbox', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Outbox', '#', ['class' => 'btn btn-info active']) ?>
    </div>
    <p></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=pengadaan/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-primary'></i></a>";
                    // if (!$data->pengadaanDurasiAdjusteds) {
                        $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=pengadaan/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-warning'></i></a> &nbsp;";
                        $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['pengadaan/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                    // }
                    $opt.="</div>";
                    return $opt;
                }
            ],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 100px;'],
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            [
            	'attribute' => '',
            	'label' => 'No. KORIN PP',
            	'format' => 'raw',
            	'value' => function($data) {
            		foreach ($data->pengadaanKorins as $pengadaanKorin) {
            			return "<a target='_blank' href='?r=korin/view&id=".$pengadaanKorin->korin->id."'>".$pengadaanKorin->korin->no_surat."</a><br>";
            		}
            	}
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?r=pengadaan/outbox&PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama);
                    }
                },
            ],
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=pengadaan/outbox&PengadaanSearch[metoda.nama]=' . $data->metoda->nama);
                    }
                },
            ],
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            /*[
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 70px;']
            ],*/
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
