<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use frontend\models\Metoda;
use frontend\models\PersonelProcurement;
use frontend\models\BarangJasa;
use frontend\models\LevelApproval;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */
/* @var $form yii\widgets\ActiveForm */

if (!isset($is_centerled)) $is_centerled = 0;
?>

<div class="pengadaan-form box box-body">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'is_centerled', ['value' => $is_centerled]) ?>

    <?= $form->field($model, 'nama')->textarea(['maxlength' => 200, 'rows' => 2]) ?>

    <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'metoda_id')->dropDownList(ArrayHelper::map(Metoda::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>

    <?= $form->field($model, 'purchaser_personel_procurement_id')->dropDownList(ArrayHelper::map(PersonelProcurement::find()->where("jabatan='Purchaser'")->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>
            
    <?= $form->field($model, 'level_approval_id')->dropDownList(ArrayHelper::map(LevelApproval::find()->orderBy("id desc")->all(), 'id', 'approver')) ?>
    
    <?= $form->field($model, 'isi_disposisi')->textarea(['rows' => 1]) ?>

    <?php // echo $form->field($model, 'nilai_oe')->textInput() ?>
    <!-- 
    <?= $form->field($model, 'start_plan')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>
    -->
    <?= $form->field($model, 'requirement_date')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?= $form->field($model, 'subgroup')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Search Subgroup ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['sap-subgroup/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
        ],
    ]);?>
                
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs('$("#pengadaan-barang_jasa_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
?>