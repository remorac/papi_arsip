<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = 'Gabungkan '.$model->korin->no_surat.' ke Pengadaan';
$this->params['breadcrumbs'][] = ['label' => 'Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-create box box-body box-success">

    <?= $this->render('_formMerge', [
        'model' => $model,
    ]) ?>

</div>
