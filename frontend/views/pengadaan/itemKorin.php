<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        'nama',
        'quantity',
        'satuan',
        'keterangan',
    ],
]); ?>