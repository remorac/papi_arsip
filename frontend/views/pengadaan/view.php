<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\Pengadaan;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\SapSubgroup;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

//temporary handling error for setting planned duration
if ($model->nilai_oe==null) $model->nilai_oe = 0;

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-view box box-body box-info">

    <p>
        <?= Html::a('More Detail', ['highlight/view','id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
    <div class="col-md-6">
    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'template' => "<tr><th width='138px'>{label}</th><td>{value}</td></tr>",
        'attributes' => [
            //'id',
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'value' => $model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT)
            ],
            'nama',
            //'barang_jasa_id',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => Html::a(
                    $model->metoda ? $model->metoda->nama : '', 
                    //$model->metoda ? '?r=/metoda/view&id=' . $model->metoda->id : ''
                    '#'
                    )
            ],
            //'level_approval_id',
            [
                'attribute' => 'levelApproval.approver',
                'label' => 'Level Approval',
                'format' => 'raw',
                'value' => Html::a(
                    $model->levelApproval ? $model->levelApproval->approver : '', 
                    //$model->levelApproval ? '?r=/level-approval/view&id=' . $model->levelApproval->id : ''
                    '#'
                    )
            ],
            'nilai_oe:integer',
            [
                'attribute' => 'purchaserPersonelProcurement.personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => Html::a(
                    $model->purchaserPersonelProcurement ? $model->purchaserPersonelProcurement->personel->nama : '', 
                    //$model->purchaserPersonelProcurement ? '?r=/personel/view&id=' . $model->purchaserPersonelProcurement->id : ''
                    '#'
                    )
            ],
            'isi_disposisi:ntext',
            'requirement_date:date',
            'start_plan:date',
            'po_reference',
            'delivery_time',
            [
                'attribute' => 'subgroup',
                'label' => 'Subgroup',
                'format' => 'raw',
                'value' => is_object($model->subgroup) ? $model->subgroup." - ".SapSubgroup::find()->where("`Sub Material Group SGG`='".$model->subgroup."'")->one()->{"Sub Material Group Desc SGG"} : "",
            ],
            'is_centerled',
            /*'created_at',
            'updated_at',
            'created_by',
            'updated_by',*/
        ],
    ]) ?>

    </div>
    
    <div class="col-md-6">
    
    <!-- <big>KORIN Permintaan Pengadaan</big> -->
    <table class="table  paper" style="margin-bottom:0 !important">
        <tr style="background:#eee;">
            <th>No. KPP</th>
            <th>Unit</th>
            <th>Person In Charge</th>
        </tr>
        <?php 
            $pic = "";
            $pengadaanKorin = PengadaanKorin::find()->where(['pengadaan_id' => $model->id])->all(); 
            if ($pengadaanKorin) {
                foreach ($pengadaanKorin as $row) {
                    $korin = Korin::find()->where(['=', 'id', $row->korin_id])->one();
                    if ($korin) {
                        
                        $pic = $korin->pic_initial;
                        ?>
                            <tr>
                                <td><?=Html::a($korin->no_surat, '?r=korin/view&id='.$korin->id)?></td>
                                <td>(to be repaired)</td>
                                <td><?=$pic?></td>
                            </tr>
                        <?php
                    }
                }
            }
        ?>
    </table>

    </div>
    </div>
               
</div>

