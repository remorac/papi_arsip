<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disposisi Manager';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('New Disposition', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama.'&r=pengadaan');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonel){
                        return Html::a($data->purchaserPersonel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
        ],
    ]); ?>

</div>
