<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Legal */

$this->title = 'Update Legal: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Legals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="legal-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
