<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Legal */

$this->title = 'Upload Contract';
$this->params['breadcrumbs'][] = ['label' => 'Legals', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="legal-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
