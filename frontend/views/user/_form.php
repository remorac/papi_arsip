<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = \kartik\widgets\ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'username')
    ->textInput(['maxlength' => 72])
    ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 60]) ?>

    <?php
        echo $form->field($model, 'personel_id')->widget(Select2::classname(), [
            'data' => $personelSelect2Items,
            'options' => [
                'placeholder' => 'Pilih Personel...'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ->label('Personel');
    ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model,'status')->dropDownList(array("10"=>"Active", "0"=>"Inactive"), array('empty'=>'Select Value')); ?>


    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php \kartik\widgets\ActiveForm::end(); ?>

</div>
