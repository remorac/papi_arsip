<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view box box-body box-info">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            'username',
            // 'authAssignment.item_name:text:Role',
            [
                'attribute' => 'personel.nama',
                'label' => 'Personel',
                'format' => 'raw',
                'value' => Html::a(
                    $model->personel ? $model->personel->nama : '', 
                    $model->personel ? '?r=/personel/view&id=' . $model->personel->id : ''
                    )
            ],
            'email:email',
            [
                'attribute' => 'statusNama',
                'label' => 'Status',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'createdBy.username',
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->createdBy ? $model->createdBy->username : '', 
                    $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    )
            ],
            [
                //'attribute' => 'updatedBy.username',
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->updatedBy ? $model->updatedBy->username : '', 
                    $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    )
            ],
        ],
    ]) ?>

</div>
