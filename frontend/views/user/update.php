<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$this->title = Yii::t('app', 'Update {modelClass} : ', [
    'modelClass' => 'User',
]) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
        // 'authAssignmentModel' => $authAssignmentModel,
        'personelSelect2Items' => $personelSelect2Items,
        // 'authItemSelect2Items' => $authItemSelect2Items,
    ]) ?>

</div>
