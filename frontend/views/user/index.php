<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-body box-primary">

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:150px'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=admin/assignment/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-lock text-default'></i></a>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=user/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=user/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                    $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['user/delete', 'id' => $data->id], [
                        'title' => 'delete',
                        'class' => 'btn btn-xs btn-default',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                    $opt.="</div>";
                    return $opt;
                }
            ],

            'username',
            //'password_hash',
            //'role_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Personel',
                'format' => 'raw',
                'value' => function($data){
                    if($data->personel){
                        return Html::a($data->personel->nama, '?r=user&UserSearch[personel.nama]=' . $data->personel->nama);
                    }
                },
            ],
            /*[
                'attribute' => 'authAssignment.item_name',
                'label' => 'Role',
                'format' => 'raw',
                'value' => function($data){
                    if($data->authAssignment){
                        return Html::a($data->authAssignment->item_name, '?r=user&UserSearch[authAssignment.item_name]=' . $data->authAssignment->item_name);
                    }
                },
            ],*/
            'email:email',
            //'status',
            // 'auth_key',
            // 'created_at',
            // 'updated_at',
            // 'password_reset_token',
            //[
            //    'attribute' => 'statusNama',
            //    'label' => 'Status',
            //],
            [
                'attribute' => 'statusNama',
                'label' => 'Status',
                'format' => 'raw',
                'value' => function($data){
                    if($data->statusNama){
                        return Html::a($data->statusNama, '?UserSearch[statusNama]=' . $data->statusNama.'&r=user');
                    }
                },
            ],
        ],
    ]); ?>

</div>
