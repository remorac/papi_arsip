<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$this->title = Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
        // 'authAssignmentModel' => $authAssignmentModel,
        'personelSelect2Items' => $personelSelect2Items,
        // 'authItemSelect2Items' => $authItemSelect2Items,
    ]) ?>

</div>
