<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PosisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grouping PR Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Group Selected Items', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $model,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',
            'singkatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>    

</div>
