<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorinDocument */

$this->title = 'Create Ppl Korin Document';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korin Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-korin-document-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
