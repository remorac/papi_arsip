<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplKorinDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Korin Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-korin-document-index box box-body box-primary">

    <p>
        <?= Html::a('Create Ppl Korin Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ppl_korin_id',
            'file',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
