<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorinDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-korin-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ppl_korin_id')->textInput() ?>

    <?= $form->field($model, 'file')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
