<?php
ini_set('memory_limit','256M');
use yii\helpers\Html;
use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;

/* @var $this yii\web\View */

$this->title = 'PERFORMA PENGADAAN';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index">
    <p>
        <?= Html::button('Print', [ 'class' => 'btn btn-primary', 'onclick' => 'window.print();' ]); ?>
    </p>

    <div class="paper col-sm-12">
        <div class="printable">
        <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

        <table class="table table-bordered table-condensed" width="100%">
            <tr>
                <th>ACTIVITY</th>
                <th>Quantity</th>
                <th>Early</th>
                <th>On Schedule</th>
                <th>Delay</th>
                <th>Not Comply</th>
                <th>Comply</th>
            </tr>
            <?php 
                $i=1;
                $total_quantity     = 0;
                $total_early        = 0;
                $total_onSchedule   = 0;
                $total_delay        = 0;
                $total_notcomply    = 0;
                $total_comply       = 0;

                $te_quantity     = 0;
                $te_early        = 0;
                $te_onSchedule   = 0;
                $te_delay        = 0;
                $te_notcomply    = 0;
                $te_comply       = 0;

                $activity = Activity::find()->where("id>=2 and id<=10")->all();
                foreach ($activity as $row) {
                    $i++;

                    //init vars 
                    $no = 0;
                    $std_selesai    = "0000-00-00";
                    $std_mulai[$no] = "0000-00-00";
                    $real_selesai   = "0000-00-00";
                    $real_mulai     = "0000-00-00";
                    $early          = 0;
                    $onSchedule     = 0;
                    $delay          = 0;
                    $notcomply      = 0;
                    $comply         = 0;

                    //loop pengadaan
                    $pengadaanActivity = PengadaanActivity::find()->where(['activity_id' => $row->id])->all();
                    foreach ($pengadaanActivity as $row1) {
                        $no++;
                        //get durasi adjusted
                        $pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()
                                ->where(['=', 'activity_id', $row1->activity_id])
                                ->andWhere(['=','pengadaan_id', $row1->pengadaan_id])
                                ->one();
                        $std_durasi = (is_object($pengadaanDurasiAdjusted)) ? $pengadaanDurasiAdjusted->durasi : 0;
                        
                        //real date
                        $dStart = new DateTime($row1->start_date);
                        $dEnd   = new DateTime($row1->end_date);
                        if ($row1->end_date == "0000-00-00") $dEnd = new DateTime(date('Y-m-d'));
                        $dDiff  = $dStart->diff($dEnd);
                        $real_mulai     = $row1->start_date;
                        $real_selesai   = $row1->end_date;
                        $real_durasi    = $dDiff->days+1;
                        $saving         = $std_durasi - $real_durasi;
                        if ($saving >=0) $comply++; else $notcomply++;

                        //set standard date
                        $std_mulai[1] = isset($real_mulai) ? $real_mulai : date('Y-m-d');
                        if ($no > 1) $std_mulai[$no] = $std_selesai;
                        $date = new DateTime($std_mulai[$no]);
                        if ($no > 1) $date->add(new DateInterval('P1D'));
                        $std_mulai[$no] = $date->format('Y-m-d');

                        //adjust start date
                        while (HariLibur::checkHoliday($std_mulai[$no])) {
                            $date->add(new DateInterval('P1D'));                         
                            $std_mulai[$no] = $date->format('Y-m-d');
                        }
                        $durasi_adder = $std_durasi > 1 ? $std_durasi-1 : 0; 
                        $date->add(new DateInterval('P'.($durasi_adder).'D'));                          
                        $std_selesai = $date->format('Y-m-d');

                        //set end date 
                        $adder = HariLibur::countHoliday($std_mulai[$no], $std_selesai);
                        $date->add(new DateInterval('P'.($adder).'D'));                         
                        $std_selesai = $date->format('Y-m-d');

                        //adjust end date
                        while (HariLibur::checkHoliday($std_selesai)) {
                            $date->add(new DateInterval('P1D'));                         
                            $std_selesai = $date->format('Y-m-d');
                        }

                        //set status
                        if ($real_selesai == $std_selesai) $onSchedule++;
                        if ($real_selesai > $std_selesai) $delay++;
                        if ($real_selesai < $std_selesai) $early++;
                    }
                    ?>
                    <tr>
                        <td><?=$row->nama?></td>
                        <td><?=PengadaanActivity::find()
                            ->where(['activity_id' => $i])
                            ->count();?></td>
                        <td><?=$early?></td>
                        <td><?=$onSchedule?></td>
                        <td><?=$delay?></td>
                        <td><?=$notcomply?></td>
                        <td><?=$comply?></td>
                    </tr>
                    <?php
                    //set totals
                    $total_early        += $early;
                    $total_onSchedule   += $onSchedule;
                    $total_delay        += $delay;
                    $total_notcomply    += $notcomply;
                    $total_comply       += $comply;
                    $total_quantity     += $notcomply+$comply;

                    if ($row->id != 4) {
                        $te_early       += $early;
                        $te_onSchedule  += $onSchedule;
                        $te_delay       += $delay;
                        $te_notcomply   += $notcomply;
                        $te_comply      += $comply;
                        $te_quantity    += $notcomply+$comply;
                    }
                }
            ?>

            <tr>
                <th>TOTAL</th>
                <th><?=$total_quantity?></th>
                <th><?=$total_early?></th>
                <th><?=$total_onSchedule?></th>
                <th><?=$total_delay?></th>
                <th><?=$total_notcomply?></th>
                <th><?=$total_comply?></th>
            </tr>
            <tr>
                <th>TOTAL (exclude Evaluasi Teknis)</th>
                <th><?=$te_quantity?></th>
                <th><?=$te_early?></th>
                <th><?=$te_onSchedule?></th>
                <th><?=$te_delay?></th>
                <th><?=$te_notcomply?></th>
                <th><?=$te_comply?></th>
            </tr>
        </table>
        </div>
    </div>
</div>
&nbsp;
<?php /*
    echo "<h4>Memory Usage";
    echo "<br><small><b>".number_format((memory_get_peak_usage(true)/1024/1024),'2')." MiB | </b>allocated by system";
    echo "<br><b>".number_format((memory_get_peak_usage(false)/1024/1024),'2')." MiB | </b>used by script</small></h4>";
*/?>