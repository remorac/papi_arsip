<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'PCT', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-view">

    <h1>PCT <small><i class="glyphicon glyphicon-chevron-right"></i> <?= Html::encode($this->title) ?></small></h1>

    <p>
        <?= Html::button('Print', [ 'class' => 'btn btn-primary', 'onclick' => 'window.print();' ]); ?>
    </p>

    <div style="padding:10px;box-shadow:0 0 1px rgba(0,0,0,0.5)">
    <div class="printable">
        <center><h1>Highlight</h1></center>
        <table width="100%">
        <tr>
        <td width="50%">
            <table>
                <tr>
                    <td>JENIS (BARANG/JASA)</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->barangJasa?></td>
                </tr>
                <tr>
                    <td>UNIT USER</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>
                        <?php 
                            $pengadaanKorin = PengadaanKorin::find()->where(['=', 'pengadaan_id', $model->id])->one(); 
                            if (is_object($pengadaanKorin)) {
                                $korin = Korin::find()->where(['=', 'id', $pengadaanKorin->korin_id])->all();
                                if (is_object($korin)) {
                                    $arr_korin_id = explode('/', $korin->no_surat);
                                    $kodeSurat = $arr_korin_id[3];
                                    echo $kodeSurat;
                                }
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>PIC USER</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td>
            <table class="vtop">
                <tr>
                    <td style="white-space:nowrap">NAMA PENGADAAN </td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->nama?></td></tr>
                <tr>
                    <td>PURCHASER </td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->purchaserPersonel->nama?></td></tr>
            </table>
        </td>
        </tr>
        </table>
        <br>
        <table class="table table-bordered ">
            <tr style="background:lightblue;">
                <th rowspan="2">NO</th>
                <th rowspan="2">ACTIVITY</th>
                <th colspan="3">PCT Planning</th>
                <th colspan="3">PCT Real</th>
                <th colspan="2">Durasi</th>
                <th>Schedule</th>
            </tr>
             <tr style="background:lightblue">
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Comply/Not</th>
                <th>Saving</th>
                <th>E/O/D</th>
            </tr>
            <?php
                $rec_metodaActivity = MetodaActivity::find()->where(['metoda_id' => $model->metoda_id])->all();
                $no=0;
                foreach ($rec_metodaActivity as $row) {
                    $no++;
                    ?>
                    <tr>
                        <td><?=$no?></td>
                        <td><?=Activity::find()->where(['=', 'id', $row->activity_id])->one()->nama?></td>
                        <td></td>
                        <td></td>
                        <td>
                            <?php
                                $durasiStandar = DurasiStandar::find()->where(['=', 'activity_id', $row->activity_id])
                                        ->andWhere(['=','level_approval_id', 
                                                LevelApproval::find()->where(['<=','nilai_min',$model->nilai_oe])
                                                        ->andWhere(['>=','nilai_max',$model->nilai_oe])->one()->id
                                        ])->one();
                                if(is_object($durasiStandar)){ echo $durasiStandar->durasi;}
                            ?>
                        </td>
                        <!-- <td>
                            <?php
                                $pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()->where(['=', 'activity_id', $row->activity_id])
                                        ->andWhere(['=','pengadaan_id', $model->id])
                                        ->one();
                                if(is_object($pengadaanDurasiAdjusted)){ echo $pengadaanDurasiAdjusted->durasi;}
                            ?>
                        </td> -->
                        <?php
                        $pengadaanActivity = PengadaanActivity::find()->where(['=', 'activity_id', $row->activity_id])
                                ->andWhere(['=','pengadaan_id',$model->id])
                                ->one();
                        if(is_object($pengadaanActivity)){
                            $dStart = new DateTime($pengadaanActivity->start_date);
                            $dEnd   = new DateTime($pengadaanActivity->end_date);
                            if ($pengadaanActivity->end_date == "0000-00-00") $dEnd = $dStart;
                            $dDiff  = $dStart->diff($dEnd); $days = $dDiff->days == 0 ? 1 : $dDiff->days;
                            echo "<td>".$pengadaanActivity->start_date."</td>";
                            echo "<td>".$pengadaanActivity->end_date."</td>";
                            echo "<td>".$days."</td>";
                            echo "<td></td>
                                <td></td>
                                <td></td>";
                        } else {
                            echo "<td colspan=6>tidak ada data (aktifitas ini tidak/belum dilaksanakan)</td>";
                        }
                        ?>
                    </tr>
                    <?php
                }
            ?>
        </table>
        
        <div class="row">
        <?php
            $rec_metodaActivity = MetodaActivity::find()->where(['metoda_id' => $model->metoda_id])->all();
            foreach ($rec_metodaActivity as $row) {
                $pengadaanActivity = PengadaanActivity::find()->where(['=', 'activity_id', $row->activity_id])
                            ->andWhere(['=','pengadaan_id',$model->id])
                            ->one();
                if(is_object($pengadaanActivity)){
                    if (!empty($pengadaanActivity->keterangan)) {
                        echo "<ul>";
                        echo "<li><b>".strtoupper(Activity::find()->where(['=', 'id', $row->activity_id])->one()->nama)."</b><br>";
                        echo "".nl2br($pengadaanActivity->keterangan)."</li>";
                        echo "</ul>";
                    }
                }
            }
        ?>
        </div>

    </div>
    </div>

</div>
