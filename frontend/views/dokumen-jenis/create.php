<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DokumenJenis */

$this->title = 'Create Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-jenis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
