<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan - Activity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengadaan - Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pengadaan_id',
            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaan){
                        return Html::a($data->pengadaan->nama, '?r=pengadaan&PengadaanSearch[nama]=' . $data->pengadaan->nama);
                    }
                },
            ],
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activity){
                        return Html::a($data->activity->nama, '?r=activity&ActivitySearch[nama]=' . $data->activity->nama);
                    }
                },
            ],
            'start_date:date',
            'end_date:date',
            // 'keterangan:ntext',
            // 'kendala:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
