<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Afis */

$this->title = 'Rename : ' . $filename;
$this->params['breadcrumbs'][] = ['label' => 'Warehouse', 'url' => ['index']];
// $this->params['breadcrumbs'][] = 'Rename';
?>
<div class="afis-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <div class="afis-form">

		    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

		    <?= Html::hiddenInput('id', $id) ?>

		    <div class="form-group">
				<label class="control-label col-sm-3" for="filename">New Filename</label>
				<div class="col-sm-6">
					<?= Html::textInput('filename', $filename, ['class' => 'form-control']) ?>
				</div>
			</div>
		    
		    <div class="form-panel col-sm-12">
		        <div class="row">
		    	    <div class="col-sm-6 col-sm-offset-3">
		    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i>  Rename', ['class' => 'btn btn-primary']) ?>
		            </div>
			    </div>
		    </div>

		    <?php ActiveForm::end(); ?>

		</div>
    </div>

</div>
