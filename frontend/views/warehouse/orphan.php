<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AfisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unsaved Files ('.count($orphans).')';
$this->params['breadcrumbs'][] = ['label' => 'Warehouse', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="afis-index box box-primary box-body">
    <?php if (count($orphans) > 0) { ?><p class="text-success">Try patching or populating again to save these file.</p><?php } ?>
    <div class="panel panel-default panel-body"> 
        <table class="table">
        <?php foreach ($orphans as $orphan) { ?>            
                <tr><td>
            <?= Html::a('Open File', ['download', 'filename' => $orphan['filename']], [
                'class' => 'btn btn-xs btn-success',
                'target' => '_blank',
            ]) ?> 
            <?= Html::a('Rename', ['rename', 'filename' => $orphan['filename']], ['class' => 'btn btn-xs btn-warning']) ?> 
            <?= Html::a('Delete', ['remove', 'filename' => $orphan['filename']], [
                'class' => 'btn btn-xs btn-danger',
                'data-confirm' => 'Are you sure you want to delete this item?',
                'data-method' => 'post',
            ]) ?> 
            &nbsp;
            <span class="text-primary"><?= $orphan['filename'] ?> &nbsp; </span>
                </td></tr>                
        <?php } ?>
        </table>
    </div>

</div>