<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Warehouse */

$this->title = 'Upload GR';
$this->params['breadcrumbs'][] = ['label' => 'Warehouses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warehouse-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
