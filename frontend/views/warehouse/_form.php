<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model frontend\models\Warehouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="warehouse-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <?php echo $form->field($model, 'pdfFiles[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'application/pdf',
            'multiple' => true,
        ],
        'pluginOptions' => [
            // 'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'allowedFileExtensions' => ['pdf'],
            'maxFileCount'=>100,
        ],
    ])->hint('Format: pdf. File dapat diupload beberapa sekaligus menggunakan <code>Ctrl+klik</code>.'); ?>
    
    <div class="form-panel col-sm-12">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> Upload', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
