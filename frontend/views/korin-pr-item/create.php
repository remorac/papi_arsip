<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinPrItem */

$this->title = 'Create KORIN PP - Item PR';
$this->params['breadcrumbs'][] = ['label' => 'KORIN PP - Item PR', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-pr-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
