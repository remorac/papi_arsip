<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\SapPr;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinPrItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KORIN PP - Item PR', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-pr-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'korin_id',
            [
                'attribute' => 'korin_id',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => Html::a(
                    $model->korin ? $model->korin->no_surat : '', 
                    $model->korin ? '?r=/korin/view&id=' . $model->korin->id : ''
                    )
            ],
            'purchase_requisition',
            //'item_of_requisition',
            [
                'attribute' => 'item_of_requisition',
                'label' => 'Item of Requisition',
                'format' => 'raw',
                'value' => $model->item_of_requisition.']'
                    .' <i>'.$model->sapPr->{'Short Text'}
                    .' ('.$model->sapPr->{'Quantity Requested'}." ".$model->sapPr->{'Unit of Measure'}.")</i>"
            ],
            //'korin_item_id',
            [
                'attribute' => 'korin_item_id',
                'label' => 'Item KORIN PP',
                'format' => 'raw',
                'value' => Html::a(
                    $model->korinItem ? $model->korinItem->nama : '', 
                    $model->korinItem ? '?r=/korin-item/view&id=' . $model->korinItem->id : ''
                    )
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
