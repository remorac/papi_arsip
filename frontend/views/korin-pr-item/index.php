<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\SapPr;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinPrItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KORIN PP - Item PR';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-pr-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create KORIN PP - Item PR', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'korin_id',
            [
                'label' => 'No. KORIN PP',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin/view&id='.$data->korin->id);
                    }
                },
            ],
            'purchase_requisition',
            //'item_of_requisition',
            [
                'label' => 'Item of Requisition',
                'format' => 'raw',
                'value' => function($data){
                    if($data->sapPr){
                        return $data->item_of_requisition.']'
                    .' <i>'.$data->sapPr->{'Short Text'}
                    .' ('.$data->sapPr->{'Quantity Requested'}." ".$data->sapPr->{'Unit of Measure'}.")</i>";
                    }
                },
            ],
            //'korin_item_id',
            [
                'label' => 'Item KORIN PP',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korinItem){
                        return Html::a($data->korinItem->nama, '?r=korin-item/view&id='.$data->korinItem->id);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
