<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\models\Activity;
use frontend\models\Periode;

/* @var $this yii\web\View */
/* @var $model frontend\models\PeriodeActivity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="periode-activity-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'periode_id')->dropDownList(ArrayHelper::map(Periode::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

    <?= $form->field($model, 'activity_id')->dropDownList(ArrayHelper::map(Activity::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
