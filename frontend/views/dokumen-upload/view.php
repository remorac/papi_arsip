<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\select2\Select2;
use yii\web\JsExpression;

use frontend\models\KorinDokumen;
use frontend\models\PengadaanActivityDokumen;

/* @var $this yii\web\View */
/* @var $model frontend\models\DokumenUpload */

$this->title = $model->no_dokumen;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-view box box-body box-info">

    <?php if (Yii::$app->user->can('/dokumen-upload/*')) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Hubungkan ke Korin Masuk', 'javascript:linkKorin('.$model->id.')', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Hubungkan ke Korin Keluar', 'javascript:linkKorinKeluar('.$model->id.')', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Hubungkan ke Aktifitas Pengadaan', 'javascript:linkPengadaan('.$model->id.')', ['class' => 'btn btn-default']) ?>
    </p>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            'no_dokumen',
            //'dokumen_jenis_id',
            [
                'attribute' => 'dokumenJenis.nama',
                'label' => 'Jenis Dokumen',
                'format' => 'raw',
                'value' => Html::a(
                    $model->dokumenJenis ? $model->dokumenJenis->nama : '', 
                    $model->dokumenJenis ? '?r=/dokumen-jenis/view&id=' . $model->dokumenJenis->id : ''
                    )
            ],
            //'pdf_filename:ntext',
            [
                'attribute' => 'pdf_filename:ntext',
                'label' => 'File',
                'format' => 'raw',
                'value' => Html::a($model->pdf_filename, Url::to(['dokumen-upload/download', 'filename' => $model->pdf_filename]), ['target'=>'_blank'])
            ],
            'tgl_dokumen',
            [
                'attribute' => 'status_disposisi',
                'value' => $model->status_disposisi == 1 ? "Lengkap" : "Belum Lengkap"
            ],
            'keterangan:ntext',
            'created_at',
            'updated_at',
            [
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->createdBy ? $model->createdBy->username : '', 
                    // $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    '#'
                    )
            ],
            [
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->updatedBy ? $model->updatedBy->username : '', 
                    // $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    '#'
                    )
            ],

        ],
    ]) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Links</div>
        </div>
        <div class="panel-body">
            <?php 
                $korinDokumen               = KorinDokumen::find()->where("dokumen_upload_id='".$model->id."'")->all();
                $pengadaanActivityDokumen   = PengadaanActivityDokumen::find()->where("dokumen_upload_id='".$model->id."'")->all();

                if (($korinDokumen)) {
                    echo "<b>KORIN:</b> <br>";
                    foreach ($korinDokumen as $row) {
                        echo "<a target='_blank' href='?r=korin/view&id=".$row->korin->id."'>".$row->korin->no_surat."</a><br>";
                    }
                    //echo "<br>";
                }
                if (($pengadaanActivityDokumen)) {
                    echo "<b>PENGADAAN:</b> <br>";
                    foreach ($pengadaanActivityDokumen as $row) {
                        echo "<a target='_blank' href='?r=semua-pengadaan/view&id=".$row->pengadaanActivity->pengadaan->id."'>".$row->pengadaanActivity->pengadaan->nama." - ".$row->pengadaanActivity->activity->nama."</a><br>";
                    }
                    //echo "<br>";
                }
            ?>
        </div>
    </div>

</div>



<form id="formLinkKorin" action="?r=dokumen-upload/linkkorin" method="post">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
<input type="hidden" name="linkKorin_id" value="<?=$model->id?>">
<div class="modal fade" id="modalLinkKorin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hubungkan ke Korin Masuk</h4>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <p>
                    Dokumen ini dapat dihubungkan dengan data yang telah dientrikan pada menu <a href="?r=korin" target="_blank">Korin Masuk</a>.
                    Silahkan pilih data KORIN MASUK yang merupakan dokumen ini atau yang melampirkan dokumen ini.
                </p>
                <?php
                    $url = Url::to(['korin/list']);
                    echo Select2::widget([
                        'id' => 'korin_id',
                        'name' => 'korin_id',
                        'options' => [
                            'placeholder' => 'select Korin Masuk ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                        ],
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<form id="formLinkKorinKeluar" action="?r=dokumen-upload/linkkorinkeluar" method="post">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
<input type="hidden" name="linkKorinKeluar_id" value="<?=$model->id?>">
<div class="modal fade" id="modalLinkKorinKeluar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hubungkan ke Korin Keluar</h4>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <p>
                    Dokumen ini dapat dihubungkan dengan data yang telah dientrikan pada menu <a href="?r=korin-keluar" target="_blank">Korin Keluar</a>.
                    Silahkan pilih data KORIN KELUAR yang merupakan dokumen ini atau yang melampirkan dokumen ini.
                </p>
                <?php
                    $url = Url::to(['korin-keluar/list']);
                    echo Select2::widget([
                        'id' => 'korin_keluar_id',
                        'name' => 'korin_keluar_id',
                        'options' => [
                            'placeholder' => 'select Korin Keluar ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                        ],
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>


<form id="formLinkPengadaan" action="?r=dokumen-upload/linkpengadaan" method="post">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
<input type="hidden" name="linkPengadaan_id" value="<?=$model->id?>">
<div class="modal fade" id="modalLinkPengadaan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hubungkan ke Pengadaan</h4>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <p>
                    Dokumen ini dapat dihubungkan dengan data Aktifitas Pengadaan yang telah dientrikan pada <a href="?r=worksheet-purchaser" target="_blank">Worksheet Purchaser</a>.
                    Silahkan pilih data Pengadaan dan Aktifitas yang terkait dengan dokumen ini.
                </p>
                <?php
                    $url = Url::to(['pengadaan-activity/list']);
                    echo Select2::widget([
                        'id' => 'pengadaan-activity_id',
                        'name' => 'pengadaan-activity_id',
                        'options' => [
                            'placeholder' => 'select Pengadaan & Aktifitas ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                        ],
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>


<?php 
    //showModal
    $this->registerJs('
        function linkKorin(id) {
            $("#modalLinkKorin").modal("show");
        }
        function linkKorinKeluar(id) {
            $("#modalLinkKorinKeluar").modal("show");
        }
        function linkPengadaan(id) {
            $("#modalLinkPengadaan").modal("show");
        }
        ', \yii\web\VIEW::POS_END
    ); 
?>