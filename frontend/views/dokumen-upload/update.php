<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DokumenUpload */

$this->title = 'Update Dokumen Upload: ' . ' ' . $model->no_dokumen;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_dokumen, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dokumen-upload-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
