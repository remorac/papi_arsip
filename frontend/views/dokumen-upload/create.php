<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DokumenUpload */

$this->title = 'Upload Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Upload Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
