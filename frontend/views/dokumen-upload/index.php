<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DokumenUploadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Dokumen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-upload-index box box-body box-primary">
    
    <p>
        <?= Html::a('Upload Dokumen', ['create'], ['class' => 'btn btn-success']) ?>
        <?php 
        if (Yii::$app->user->id == "10") {
            echo Html::a('Download Semua TOR', ['download-tor'], ['class' => 'btn btn-default']);
        }
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
        
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=dokumen-upload/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=dokumen-upload/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                    $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['dokumen-upload/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                    $opt.="</div>";
                    return $opt;
                }
            ],
            
            [
                'attribute' => 'status_disposisi',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:25px'],
                'value' => function($data) {
                    if ($data->status_disposisi == 1) {
                        return "<i class='glyphicon glyphicon-paperclip text-success' title='disposisi lengkap'></i>";   
                    } else {
                        return "<i class='glyphicon glyphicon-paperclip text-danger' title='disposisi belum lengkap'></i>";
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'status_disposisi', ['1'=>'Disposisi Lengkap', '0'=>'Disposisi Belum Lengkap'], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            'no_dokumen',
            //'dokumen_jenis_id',
            [
                'attribute' => 'dokumenJenis.nama',
                'label' => 'Jenis Dokumen',
                'format' => 'raw',
                'value' => function($data){
                    if($data->dokumenJenis){
                        //return Html::a($data->dokumenJenis->nama, '?r=dokumen-jenis&dokumenJenisSearch[nama]=' . $data->dokumenJenis->nama);
                        return Html::a($data->dokumenJenis->nama, '#');
                    }
                },
            ],
            //'pdf_filename:ntext',
            [
                'attribute' => 'pdf_filename',
                'label' => 'File',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pdf_filename){
                        return Html::a($data->pdf_filename, Url::to(['dokumen-upload/download', 'filename' => $data->pdf_filename]), ['target'=>'blank']);
                    }
                },
            ],
            [
                'attribute' => 'tgl_dokumen',
                'contentOptions'=>['style'=>'width: 120px;'],
                'format' => 'raw',
                'filter' => kartik\widgets\DatePicker::widget([
                    'attribute' => 'tgl_dokumen',
                    'model' => $searchModel,
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ])
            ],
            // 'keterangan',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>

</div>
