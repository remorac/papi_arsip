<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use frontend\models\DokumenJenis;

/* @var $this yii\web\View */
/* @var $model frontend\models\DokumenUpload */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-upload-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'dokumen')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showPreview' => false,
            /*'showCaption' => true,
            'showRemove' => true,*/
            'showUpload' => false
        ]
    ]); ?>

    <!-- <?= $form->field($model, 'no_dokumen')->textInput(['maxlength' => 100]) ?> -->

    <div id="dokumenunique" class="col-md-10 col-md-offset-2" style="padding-left: 5px"></div>    
    <?= $form->field($model, 'no_dokumen')->textInput(['maxlength' => 100, 'onchange' => 'javascript:dokumenUnique(this.value)']) ?>

    <?= $form->field($model, 'dokumen_jenis_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(DokumenJenis::find()->all(), 'id', 'nama'),
        'options' => ['placeholder' => '-- select --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'tgl_dokumen')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?= $form->field($model, 'status_disposisi')->dropDownList(['1' => 'Lengkap','0' => 'Belum Lengkap']) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php //autodetect file serial
    $this->registerJs('
        $(document).on("change", "#dokumenupload-dokumen", function(){
            filename = $("#dokumenupload-dokumen").val();
            arr_file = filename.split("_");
            last_arr = arr_file.pop();
            arr_numb = last_arr.split(".");
            arr_numb.pop();
            serialno = arr_numb.join(".");
            $("#dokumenupload-no_dokumen").val(serialno);
            dokumenUnique(serialno);
        });

        $(document).on("click", ".fileinput-remove", function(){
            $("#dokumenupload-no_dokumen").val("");
            dokumenUnique("");
        });
        ', \yii\web\VIEW::POS_READY
    );

    $this->registerJs('
        function dokumenUnique(serialno) {
            $.get("index.php?r=dokumen-upload/dokumen-unique&no_dokumen="+serialno, function(data) {
                $("#dokumenunique").html(data);
            });
        }
        ', \yii\web\VIEW::POS_END
    ); 
?>
