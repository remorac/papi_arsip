<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

dmstr\web\AdminLteAsset::register($this);
frontend\assets\AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition layout-top-nav fixed skin-blue">
<?php $this->beginBody() ?>

<div class="wrapper">

<header class="main-header">

    <?php NavBar::begin([
        // 'brandLabel' => Html::img('@web/img/ukom-logo-sm.png', ['style' => 'height:40px; margin-top:-10px']),
        'brandLabel' => '<big><span style="color:#fff">Files for Auditors</span></big>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
            'style' => 'box-shadow: 0 0 5px rgba(0,0,0,0.2)',
        ],
    ]);
    $menuItems = [
        // ['label' => 'Paket Try Out', 'url' => ['/package/index']],
        // ['label' => 'Tutor Area', 'url' => ['/author/index']],
    ];
    // $menuItems = mdm\admin\components\Helper::filter($menuItems);
    
    if (!Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Profile', 'url' => ['/profile/index']];
    }
    // $menuItems[] = ['label' => 'Berita Appskep', 'url' => ['/news/index']];
    // $menuItems[] = ['label' => 'Testimonial', 'url' => ['/testimonial/index']];
    // $menuItems[] = ['label' => 'Team', 'url' => ['/site/about']];

    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Masuk', 'url' => ['/user/security/login']];
        // $menuItems[] = ['label' => 'Pendaftaran', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest];
    } else {
        /*$menuItems[] = ['label' => Yii::$app->user->identity->complete_name, 'url' => ['#'], 'items' => [
            ['label' => 'Ubah Password', 'url' => ['/site/changepassword']],
            '<li>'
                    . Html::beginForm(['/user/security/logout'], 'post')
                    . Html::submitButton(
                        'Logout',
                        ['class' => 'btn btn-link', 'style' => 'margin-left:7px']
                    )
                    . Html::endForm()
                    . '</li>',
        ]];*/
        $menuItems[] = ['label' => Yii::$app->user->identity->username, 'url' => ['#'], 'items' => [
            // ['label' => 'Ubah Password', 'url' => ['/site/changepassword']],
            ['label' => 'Logout',
                'url' => ['site/logout'],
                'linkOptions' => ['data-method' => 'POST'],
            ],
        ]];
        /*$menuItems[] = '<li>'
                    . Html::beginForm(['/user/security/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout', 'style' => 'color:white']
                    )
                    . Html::endForm()
                    . '</li>';*/
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
</header>

    <?= $this->render(
        'auditor-content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>