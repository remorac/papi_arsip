<aside class="main-sidebar">
    
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/luffy.png" class="img-circlee" alt="User Image"; style="border-radius: 10px; box-shadow:0 0 1px green" />
            </div>
            <div class="pull-left info">
                <div style="margin-bottom:10px"><?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username ?></div>
                <a href="#"><i class="envelope text-success"></i> <?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->email ?></a>
            </div>
        </div>

        <div class="text-center" style="
            background: #444; 
            border: none !important;
            border-radius: 4px; 
            border: 1px solid #eee; 
            margin:5px 10px 10px 10px;
            padding: 2px; 
            font-size: 5px;
        ">
            <!-- <i class="ellipsis-h text-primary"></i> -->
        </div>

        <?php
            $menuItems = [
                ['label' => ': :  '.\Yii::$app->user->identity->username.' : :', 'options' => ['class' => 'header text-center bold']],

                [
                    'label' => 'TOOLS',
                    'icon' => 'wrench',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        // ['label' => 'Masschive', 'icon' => 'target', 'url' => ['/masschive/index']],
                        ['label' => 'Auditor Page', 'icon' => 'target', 'url' => ['/auditor/index']],
                        ['label' => 'Import & Search PO', 'icon' => 'book', 'url' => ['/tmp-list-po/index']],
                        // ['label' => 'Advanced Search', 'icon' => 'search', 'url' => ['/semua-pengadaan/advanced-search']],
                    ],
                ],
                [
                    'label' => 'PROCUREMENT',
                    'icon' => 'shopping-cart',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => 'PENGADAAN PO',
                            'icon' => 'pencil-square',
                            // 'url' => '#',
                            'options' => ['class' => 'treeview'],
                            'items' => [
                                ['label' => 'SEMUA PENGADAAN', 'icon' => 'file', 'url' => ['/semua-pengadaan/index']],
                                // ['label' => 'Import Data SAP', 'icon' => 'glyphicon glyphicon-import', 'url' => ['/import/index'],],
                                ['label' => 'Upload Dokumen', 'icon' => 'upload', 'url' => ['/dokumen-upload/index'],],
                                ['label' => 'Korin Masuk', 'icon' => 'file', 'url' => ['/korin/index']],
                                ['label' => 'Korin Keluar', 'icon' => 'file-o', 'url' => ['/korin-keluar/index']],
                                ['label' => 'Disposisi Senior Manager', 'icon' => 'file-text', 'url' => ['/korin-assignment/index']],
                                ['label' => 'Disposisi Manager', 'icon' => 'file-text-o', 'url' => ['/pengadaan/index']],
                                ['label' => 'Worksheet Purchaser', 'icon' => 'tasks', 'url' => ['/worksheet-purchaser/index']],
                                // ['label' => 'Perpanjangan Delivery', 'icon' => 'truck', 'url' => ['/perpanjangan-delivery/index']],                                
                                ['label' => 'Locator', 'icon' => 'tags', 'url' => ['/locator-procurement/index']],                                
                                ['label' => 'Access Group', 'icon' => 'lock', 'url' => ['/access-group/index']],                                
                                ['label' => 'User Access Group', 'icon' => 'lock', 'url' => ['/user-access-group/index']],                                
                            ],
                        ],
                        [
                            'label' => 'PPL & CONTAINER',
                            'icon' => 'phone-square',
                            // 'url' => '#',
                            'options' => ['class' => 'treeview'],
                            'items' => [
                                ['label' => 'MONITORING', 'icon' => 'circle', 'url' => ['/ppl-detail/index'],],
                                ['label' => 'KORIN PPL', 'icon' => 'circle', 'url' => ['/ppl-korin/index'],],
                                ['label' => 'WA - VENDOR', 'icon' => 'circle', 'url' => ['/ppl-wa/index'],],
                                ['label' => 'WA - USER', 'icon' => 'circle', 'url' => ['/ppl-wa-user/index'],],
                                ['label' => 'CONTAINER', 'icon' => 'circle', 'url' => ['/ppl-container/index'],],
                                ['label' => 'GAS', 'icon' => 'circle', 'url' => ['/ppl-gas/index'],],
                                ['label' => 'Buyer', 'icon' => 'circle', 'url' => ['/ppl-buyer/index'],],
                                ['label' => 'Vendor', 'icon' => 'circle', 'url' => ['/ppl-vendor/index'],],
                                ['label' => 'Area', 'icon' => 'circle', 'url' => ['/ppl-area/index'],],
                                ['label' => 'User', 'icon' => 'circle', 'url' => ['/ppl-user/index'],],
                                // ['label' => 'Korin Document', 'icon' => 'circle', 'url' => ['/ppl-korin-document/index'],],
                            ],
                        ],
                        ['label' => 'Uncategorized', 'icon' => 'folder', 'url' => ['/procurement-folder/index'],],
                        ['label' => 'Penomoran Dokumen', 'icon' => 'tags', 'url' => ['/penomoran-register/index'],],
                        // ['label' => 'Overview', 'icon' => 'circle', 'url' => ['/site/overview'],],
                        [
                            'label' => 'unused',
                            'icon' => 'ban',
                            'url' => '#',
                            'options' => ['class' => 'treeview'],
                            'items' => [
                                [
                                    'label' => 'TABUNG GAS',
                                    'icon' => 'fire-extinguisher',
                                    'url' => '#',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'REPORT', 'icon' => 'circle', 'url' => ['/gas/gas-report/index'],],
                                        ['label' => 'CIRCULATION', 'icon' => 'circle', 'url' => ['/gas/gas-circulation/index'],],
                                        ['label' => 'SEARCH & EXPORT', 'icon' => 'circle', 'url' => ['/gas/gas-circulation-tube/index'],],
                                        ['label' => 'Tube', 'icon' => 'circle', 'url' => ['/gas/gas-tube/index'],],
                                        ['label' => 'Type', 'icon' => 'circle', 'url' => ['/gas/gas-type/index'],],
                                        ['label' => 'Owner', 'icon' => 'circle', 'url' => ['/gas/gas-owner/index'],],
                                        ['label' => 'Person In Charge', 'icon' => 'circle', 'url' => ['/gas/gas-pic/index'],],
                                        ['label' => 'User Unit', 'icon' => 'circle', 'url' => ['/gas/gas-user-unit/index'],],
                                        ['label' => 'User PIC', 'icon' => 'circle', 'url' => ['/gas/gas-user-pic/index'],],
                                        ['label' => 'Vendor Unit', 'icon' => 'circle', 'url' => ['/gas/gas-vendor-unit/index'],],
                                        ['label' => 'Vendor PIC', 'icon' => 'circle', 'url' => ['/gas/gas-vendor-pic/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'DAILY',
                                    'icon' => 'calendar',
                                    'url' => '#',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'REPORT', 'icon' => 'circle', 'url' => ['/daily/daily-report/index'],],
                                        ['label' => 'Activity', 'icon' => 'circle', 'url' => ['/daily/daily-activity/index'],],
                                        ['label' => 'Category', 'icon' => 'circle', 'url' => ['/daily/daily-category/index'],],
                                        ['label' => 'Method', 'icon' => 'circle', 'url' => ['/daily/daily-method/index'],],
                                        ['label' => 'Purchaser', 'icon' => 'circle', 'url' => ['/daily/daily-purchaser/index'],],
                                        ['label' => 'Document', 'icon' => 'circle', 'url' => ['/daily/daily-document/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Reports',
                                    'icon' => 'bar-chart',
                                    'url' => '#',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        // ['label' => 'Semua Pengadaan', 'icon' => 'circle', 'url' => ['/semua-pengadaan/index'],],
                                        ['label' => 'Today Status', 'icon' => 'circle', 'url' => ['/weekly/index'],],
                                        ['label' => 'Procurement Cycle Time', 'icon' => 'circle', 'url' => ['/pct/index'],],
                                        ['label' => 'Highlight', 'icon' => 'circle', 'url' => ['/highlight/index'],],
                                        ['label' => 'Document', 'icon' => 'circle', 'url' => ['/documents/index'],],
                                        ['label' => 'Price Comparison', 'icon' => 'circle', 'url' => ['/price-comparison/index'],],
                                        ['label' => 'Performa Pengadaan', 'icon' => 'circle', 'url' => ['/performa-pengadaan/index'],],
                                        ['label' => 'KPI Biaya', 'icon' => 'circle', 'url' => ['/kpi-biaya-report/index'],],
                                        ['label' => 'KPI Durasi', 'icon' => 'circle', 'url' => ['/kpi-durasi-report/index'],],
                                        ['label' => 'KPI Biaya - Kuartal', 'icon' => 'circle', 'url' => ['/kpi-biaya-kuartal-report/index'],],
                                        ['label' => 'KPI Durasi - Kuartal', 'icon' => 'circle', 'url' => ['/kpi-durasi-kuartal-report/index'],],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                ['label' => 'AFIS', 'icon' => 'money', 'url' => ['/afis/index'],],
                ['label' => 'LEGAL', 'icon' => 'balance-scale', 'url' => ['/legal/index'],],
                ['label' => 'WAREHOUSE', 'icon' => 'cubes', 'url' => ['/warehouse/index'],],
                ['label' => 'LOGISTIK', 'icon' => 'truck', 'url' => ['/logistic/index'],],
                ['label' => 'TECHNICAL ADMINISTRATION', 'icon' => 'folder', 'url' => ['/technical-administration-folder/index'],],
                ['label' => 'HRGA', 'icon' => 'folder', 'url' => ['/hrga-folder/index'],],
                /*[
                    'label' => 'Other Units',
                    'icon' => 'sitemap',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        ['label' => 'HRGA', 'icon' => 'users', 'url' => ['/arsip-file-hrga/index'],],
                        ['label' => 'UTILITY', 'icon' => 'industry', 'url' => ['/arsip-file-utility/index'],],
                        ['label' => 'TECHNICAL ADM', 'icon' => 'map', 'url' => ['/arsip-file-tech-adm/index'],],
                        ['label' => 'Categories', 'icon' => 'gear', 'url' => ['/arsip-file-category/index'],],
                    ],
                ],*/
                
                /*[
                    'label' => 'Delivery',
                    'icon' => 'truck',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Reports', 'icon' => 'circle', 'url' => ['/delivery/index'],],
                        ['label' => 'Import ME2M', 'icon' => 'circle', 'url' => ['/delivery/import'],],
                        ['label' => 'Worksheet', 'icon' => 'circle', 'url' => ['/delivery/worksheet'],],
                    ],
                ],*/
                [
                    'label' => 'Access Control',
                    'icon' => 'lock',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        ['label' => 'Users', 'icon' => 'street-view', 'url' => ['/user/index']],
                        ['label' => 'Assignment', 'icon' => 'circle', 'url' => ['/admin/assignment'],],
                        ['label' => 'Role', 'icon' => 'circle', 'url' => ['/admin/role'],],
                        ['label' => 'Permission', 'icon' => 'circle', 'url' => ['/admin/permission'],],
                        ['label' => 'Route', 'icon' => 'circle', 'url' => ['/admin/route'],],
                        ['label' => 'Rule', 'icon' => 'circle', 'url' => ['/admin/rule'],],
                        // ['label' => 'about yii2-admin', 'icon' => 'circle', 'url' => ['/admin/default'],],
                    ],
                ],
                /*[
                    'label' => 'Master',
                    'icon' => 'th',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Tempat', 'icon' => 'circle', 'url' => ['/tempat/index'],],
                        ['label' => 'Hari Libur', 'icon' => 'circle', 'url' => ['/hari-libur/index'],],
                    ],
                ],*/
                
                // ['label' => 'Event', 'icon' => 'calendar', 'url' => ['/event/index']],
                // ['label' => 'Check Pengadaan Double', 'icon' => 'book', 'url' => ['/semua-pengadaan/undoubler']],
                // ['label' => 'Check Korin Double', 'icon' => 'book', 'url' => ['/semua-pengadaan/korin-undoubler']],
            ];

            $menuItems = mdm\admin\components\Helper::filter($menuItems);
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
