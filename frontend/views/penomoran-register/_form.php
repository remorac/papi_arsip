<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

use frontend\models\PenomoranJenis;
use frontend\models\PenomoranJenisSub;
use frontend\models\Unit;

/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranRegister */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penomoran-register-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>
    
    <div style="display:<?php echo $model->isNewRecord ? 'block' : 'none'; ?>">
        <?= $form->field($model, 'penomoran_jenis_id')->dropDownList(ArrayHelper::map(PenomoranJenis::find()->all(), 'id', 'DropdownDisplayItem'), ['style'=>'font-size:20px;height:auto']) ?>
    </div>
    
    <?= $form->field($model, 'penomoran_jenis_sub_id')->dropDownList(ArrayHelper::map(PenomoranJenisSub::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'-']) ?>
    
    <div id="manual_num" class="form-group" style="display:none">
        Belum ada data tersimpan pada tahun ini untuk <b><span id="jenis_dokumen">jenis dokumen yang dipilih</span></b>. Isi nomor urut pada field berikut atau kosongkan untuk memulai dari "001".
        <input type="text" class="form-control" id="num_setter" name="num_setter">
    </div>

    <div id="div_tujuan" style="display:none"> 
        <?= $form->field($model, 'tujuan')->dropDownList(ArrayHelper::map(Unit::find()->all(), 'nama', 'DropdownDisplayItem'),['prompt'=>'-']) ?>
    </div>

    <?= $form->field($model, 'perihal')->textarea(['rows' => 2]) ?>

    <div class="form-group" style="display:<?php echo $model->isNewRecord ? 'block' : 'none'; ?>">
        <div class="col-md-offset-2 col-md-10">
            <input type="checkbox" id="todayswitch" name="todayswitch" checked="checked" onclick="setTanggal(this.checked)"> <b>Dokumen Hari Ini</b> 
        </div>
    </div>

    <div id="boxtgl">
    <?php 
        echo $form->field($model, 'tanggal_dokumen')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd',
            ],
            'options' => [
                'value' => date("Y-m-d"),
            ],
        ]); 
    ?>
    </div>
    
    <br>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php //cek
    $this->registerJs('
        
        setTanggal(true);

        $("#penomoranregister-penomoran_jenis_sub_id").html("");
        $.ajax({
            url: "?r=penomoran-register/subjenis",
            data: "penomoran_jenis_id="+$("#penomoranregister-penomoran_jenis_id").val(),
            cache: "",
            success:function(r) {
                $("#penomoranregister-penomoran_jenis_sub_id").html(r);
                $("#penomoranregister-penomoran_jenis_id").val()==2 ? $("#div_tujuan").show() : $("#div_tujuan").hide();
            }
        });

        $(document).on("change", "#penomoranregister-penomoran_jenis_id", function(){
            $("#penomoranregister-penomoran_jenis_sub_id").val("");
            $("#penomoranregister-penomoran_jenis_sub_id").html("");
            $.ajax({
                url: "?r=penomoran-register/subjenis",
                data: "penomoran_jenis_id="+$(this).val(),
                cache: "",
                success:function(r) {
                    $("#penomoranregister-penomoran_jenis_sub_id").html(r);
                    $("#penomoranregister-penomoran_jenis_id").val()==2 ? $("#div_tujuan").show() : $("#div_tujuan").hide();
                    $("#penomoranregister-tujuan").val("");
                }
            });
        });
        
        // JS below is used for enabling manual number at first used for each penomoran_jenis_id
        /*$.ajax({
            url: "?r=penomoran-register/chexist",
            data: "penomoran_jenis_id="+$("#penomoranregister-penomoran_jenis_id").val(),
            cache: "",
            success:function(r) {
                $("#jenis_dokumen").html($("#penomoranregister-penomoran_jenis_id option:selected").text());
                r == 0 ? $("#manual_num").show() : $("#manual_num").hide(); 
            }
        });*/
        /*$(document).on("change", "#penomoranregister-penomoran_jenis_id", function(){
            $("#num_setter").val("");
            $.ajax({
                url: "?r=penomoran-register/chexist",
                data: "penomoran_jenis_id="+$(this).val(),
                cache: "",
                success:function(r) {
                    $("#jenis_dokumen").html($("#penomoranregister-penomoran_jenis_id option:selected").text());
                    r == 0 ? $("#manual_num").show() : $("#manual_num").hide(); 
                }
            });
        });*/
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //cek
    $this->registerJs('

        function setTanggal(today) {
            if(today) {
                $("#boxtgl").hide();
                $("#penomoranregister-tanggal_dokumen").val("'.date('Y-m-d').'");
            } else {
                $("#boxtgl").show();
            }
        }

        ', \yii\web\VIEW::POS_END
    ); 
?>