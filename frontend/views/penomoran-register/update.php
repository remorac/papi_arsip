<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranRegister */

$this->title = 'Update Dokumen: ' . ' ' . str_pad($model->nomor_dokumen, 3, "0", STR_PAD_LEFT).$model->suffix;
$this->params['breadcrumbs'][] = ['label' => 'Penomoran Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => str_pad($model->nomor_dokumen, 3, "0", STR_PAD_LEFT).$model->suffix, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penomoran-register-update">

    <h1>
    	<?= Html::encode($this->title) ?>
        <?= Html::a('Back', '?r=penomoran-register/index&sort=-id', ['class' => 'btn btn-default pull-right']) ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
