<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranRegister */

$separator = ($model->sub_nomor == "") ? "" : ".";

$this->title = str_pad($model->nomor_dokumen, 3, "0", STR_PAD_LEFT).$separator.$model->sub_nomor.$model->suffix;
$this->params['breadcrumbs'][] = ['label' => 'Penomoran Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="penomoran-register-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= "&nbsp;&nbsp;<b>".Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'text-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])."</b>" ?>
        <?= Html::a('Back', '?r=penomoran-register/index&sort=-id', ['class' => 'btn btn-default pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
        'attributes' => [
            //'id',
            [
                'attribute'=>'penomoran_jenis_id',
                'format'=>'raw',
                'value'=> $model->penomoranJenis->nama
            ],
            [
                'attribute'=>'penomoran_jenis_sub_id',
                'format'=>'raw',
                'value'=> is_object($model->penomoranJenisSub) ? $model->penomoranJenisSub->nama : '-',
            ],
            [
                'attribute'=>'',
                'format'=>'raw',
                'label'=>'Nomor Dokumen',
                'value'=> str_pad($model->nomor_dokumen, 3, "0", STR_PAD_LEFT)
                    .$separator
                    .$model->sub_nomor
                    .$model->suffix
            ],
            [
                'attribute'=>'tujuan',
                'format'=>'raw',
                'visible'=>  $model->penomoran_jenis_id == "2" ? true : false
            ],
            'perihal:ntext',
            'tanggal_dokumen',
            'created_at',
            'updated_at',
            [
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->createdBy ? $model->createdBy->username : '', 
                    // $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    '#'
                    )
            ],
            [
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->updatedBy ? $model->updatedBy->username : '', 
                    // $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    '#'
                    )
            ],
        ],
    ]) ?>

</div>
