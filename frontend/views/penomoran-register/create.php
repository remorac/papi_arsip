<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranRegister */

$this->title = 'New Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Penomoran Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penomoran-register-create">

    <h1>
    	<?= Html::encode($this->title) ?>
        <?= Html::a('Back', '?r=penomoran-register/index&sort=-id', ['class' => 'btn btn-default pull-right']) ?>
    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
