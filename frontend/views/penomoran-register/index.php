<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use frontend\models\PenomoranJenis;
use frontend\models\PenomoranJenisSub;
use frontend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PenomoranRegisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penomoran Dokumen';
$this->params['breadcrumbs'][] = $this->title;

$dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);
?>
<div class="penomoran-register-index box box-primary box-body">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        $gridColumns = [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=penomoran-register/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=penomoran-register/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                    /*$opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['penomoran-register/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);*/
                    $opt.= "</div>";
                    return $opt;
                }
            ],
            [
                'attribute'=>'penomoranJenis.nama',
                'label'=>'Jenis Dokumen',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'penomoran_jenis_id', 
                    ArrayHelper::map(PenomoranJenis::find()->asArray()->all(), 'id', 'nama'),
                    [
                        'class'=>'form-control',
                        'prompt' => '[ ALL ]', 
                        'onchange'=>'javascript:$("#penomoranregistersearch-penomoran_jenis_sub_id").val("");'
                    ]
                ),
            ],
            [
                'attribute'=>'penomoranJenisSub.nama',
                'label'=>'Sub Jenis',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'penomoran_jenis_sub_id', 
                    !isset($searchModel->penomoran_jenis_id) || $searchModel->penomoran_jenis_id == ""  
                        ? ArrayHelper::map(PenomoranJenisSub::find()->asArray()->all(), 'id', 'nama')
                        : ArrayHelper::map(PenomoranJenisSub::find()->where(['penomoran_jenis_id' => $searchModel->penomoran_jenis_id])->asArray()->all(), 'id', 'nama'),
                    ['class'=>'form-control','prompt' => '[ ALL ]']
                ),
            ],
            [
                'attribute'=>'nomor_dokumen',
                'format'=>'raw',
                'value'=>function($data) {
                    $separator = ($data->sub_nomor == "") ? "" : ".";
                    return str_pad($data->nomor_dokumen, 3, "0", STR_PAD_LEFT)
                        .$separator
                        .$data->sub_nomor
                        .$data->suffix;
                }
            ],
            /*[
                'attribute'=>'tujuan',
                'label'=>'Tujuan (KORIN)'
            ],*/
            'perihal:ntext',
            'tanggal_dokumen',
            'updated_at',
            [
                'attribute'=>'updated_by',
                'format'=>'raw',
                'value'=>function($data) {
                    return $data->updatedBy->username;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'updated_by', 
                    ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username'),
                    ['class'=>'form-control','prompt' => '[ ALL ]']
                ),
            ],
        ];

        $fullExportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true,
            'pjaxContainerId' => 'kv-pjax-container',
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
        ]);
    ?>

    <?= GridView::widget(['dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        // 'floatHeader'=>true,
        // 'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => 'kv-pjax-container'],
        ],
        'columns' => $gridColumns,
        'toolbar' => [
            Html::a('New Dokumen', ['create'], ['class' => 'btn btn-success']),
            $fullExportMenu,
            '{toggleData}',
        ],
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
    ]); ?>

</div>
