<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Unit;
use frontend\models\Posisi;

/* @var $this yii\web\View */
/* @var $model frontend\models\UnitPosisi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-posisi-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map(Unit::find()->all(), 'id', 'DropdownDisplayItem')) ?>
    
    <?= $form->field($model, 'posisi_id')->dropDownList(ArrayHelper::map(Posisi::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'nama')->textInput() ?>

    <?= $form->field($model, 'kode_surat')->textInput() ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
