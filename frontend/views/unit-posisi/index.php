<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UnitPosisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kode Surat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-posisi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kode Surat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'unit_id',
            [
                'attribute' => 'unit.nama',
                'label' => 'Unit Kerja',
                'format' => 'raw',
                'value' => function($data){
                    if($data->unit){
                        return Html::a($data->unit->nama, '?r=unit&UnitSearch[nama]=' . $data->unit->nama);
                    }
                },
            ],
            [
                'attribute' => 'posisi.nama',
                'label' => 'Posisi',
                'format' => 'raw',
                'value' => function($data){
                    if($data->posisi){
                        return Html::a($data->posisi->nama, '?r=posisi&PosisiSearch[nama]=' . $data->posisi->nama);
                    }
                },
            ],
            'nama',
            'kode_surat',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
