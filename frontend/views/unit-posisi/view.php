<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\UnitPosisi */

$this->title = $model->kode_surat;
$this->params['breadcrumbs'][] = ['label' => 'Kode Surat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-posisi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'unit_id',
            [
                'attribute' => 'unit.nama',
                'label' => 'Unit Kerja',
                'format' => 'raw',
                'value' => Html::a(
                    $model->unit ? $model->unit->nama : '', 
                    $model->unit ? '?r=/unit/view&id=' . $model->unit->id : ''
                    )
            ],
            //'posisi_id',
            [
                'attribute' => 'posisi.nama',
                'label' => 'Posisi',
                'format' => 'raw',
                'value' => Html::a(
                    $model->posisi ? $model->posisi->nama : '', 
                    $model->posisi ? '?r=/posisi/view&id=' . $model->posisi->id : ''
                    )
            ],
            'nama',
            'kode_surat',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
