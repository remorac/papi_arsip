<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\UnitPosisi */

$this->title = 'Update Kode Surat: ' . ' ' . $model->kode_surat;
$this->params['breadcrumbs'][] = ['label' => 'Kode Surat', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_surat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unit-posisi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
