<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\UnitPosisi */

$this->title = 'Create Kode Surat';
$this->params['breadcrumbs'][] = ['label' => 'Kode Surat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-posisi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
