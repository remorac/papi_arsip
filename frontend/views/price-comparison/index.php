<?php

use frontend\models\Pengadaan;
use frontend\models\PengadaanPrItem;
use frontend\models\HargaSebelumDiskon;
use frontend\models\PengadaanPo;
use frontend\models\SapPo;

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */

$this->title = 'Price Comparison';
$this->params['breadcrumbs'][] = $this->title;

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
		select
			pengadaan.id,
			pengadaan.nama,
			purchase_requisition,
			item_of_requisition,
			pengadaan_po.purchasing_document,
			item,
			sap_pr.Material as material_pr,
			sap_pr.`Short Text` as desc_pr,
			sap_po.Material as material_po,
			sap_po.`Short Text` as desc_po,
			oe_adjusted as oe,
			harga_sebelum_diskon.harga as bd,
			sap_po.`net price` / sap_po.`price unit` as ad,
			sap_pr.`Quantity Requested` as qty_pr,
			sap_po.`Order Quantity` as qty_po
		from
			pengadaan,
			pengadaan_pr_item,
			pengadaan_po,
			sap_pr,
			sap_po,
			harga_sebelum_diskon
		where
			pengadaan.id = pengadaan_pr_item.pengadaan_id
			and pengadaan.id = pengadaan_po.pengadaan_id
			and pengadaan_pr_item.purchase_requisition = sap_pr.`Purchase Requisition`
			and pengadaan_pr_item.item_of_requisition = sap_pr.`Item of Requisition`
			and pengadaan_po.purchasing_document = sap_po.`Purchasing Document`
			and sap_pr.Material = sap_po.Material
			and harga_sebelum_diskon.purchasing_document = sap_po.`Purchasing Document`
			and harga_sebelum_diskon.purchasing_document = pengadaan_po.purchasing_document
			and harga_sebelum_diskon.item_po = sap_po.Item
			and pengadaan_pr_item.closed = '0'
		");

$result = $command->queryAll();

?>

<p>
    Here's the price comparison of Owner Estimate, Before Discount, and After Discount
</p>	

<div class="paper col-sm-12" style="padding-top:10px; padding-bottom:10px;">
<div id="chart">
	<?php  
		echo Highcharts::widget();
	?>
</div>
</div>

<?php 
	
	$total_OE = 0;
	$total_BD = 0;
	$total_AD = 0;
	foreach ($result as $row) {
		$total_OE+= $row['oe'] * $row['qty_po'];
		$total_BD+= $row['bd'] * $row['qty_po'];
		$total_AD+= $row['ad'] * $row['qty_po'];
	}

	$this->registerJs("
		$(function() {
		  	$('#chart').highcharts({
		    	chart: {
		      		type: 'column',
		      		style: {
			            fontFamily: 'Roboto'
			        }
		    	},
		    	title: {text: ''},
		    	xAxis: {
	         		'categories': [
		         		'OWNER ESTIMATE', 
		         		'BEFORE DISCOUNT', 
		         		'AFTER DISCOUNT'
		         	],
		    	},
	      		yAxis: {
	         		title: { 'text': 'Total Price' },
	         		labels: {
				        formatter: function () {
				            return Highcharts.numberFormat(this.value, 0,'.',',');
				        }
				    }
	      		},
			    /*plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		}
		    	},*/

		    	series: [
		    		{
		    			name: 'Price',
		      			data: [
		      				".$total_OE.",
		        			".$total_BD.",
		        			".$total_AD.",
		      			],
		      			showInLegend : false,
		    		},
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>


<!-- <div class="row">
<div class="col-sm-12" style="margin-top:40px;">
<div class="paper">
<?php 
	
	echo "<table class='table  table-hover'>
		<tr>
			<th class='text-right'>PR</th>
			<th class='text-right'>PR item</th>
			<th class='text-right'>PR qty</th>
			<th class='text-right'>PO</th>
			<th class='text-right'>PO item</th>
			<th class='text-right'>PO qty</th>
			<th class='text-right'>Mat</th>
			<th class='text-right'>OE 1</th>
			<th class='text-right'>OE all</th>
			<th class='text-right'>BD 1</th>
			<th class='text-right'>BD all</th>
			<th class='text-right'>AD 1</th>
			<th class='text-right'>AD all</th>
		</tr>";

	$total_OE = 0;
	$total_BD = 0;
	$total_AD = 0;
	foreach ($result as $row) {
		$total_OE+= $row['oe'] * $row['qty_po'];
		$total_BD+= $row['bd'] * $row['qty_po'];
		$total_AD+= $row['ad'] * $row['qty_po'];
		echo "<tr>
			<td class='text-right'>".$row['purchase_requisition']."</td>
			<td class='text-right'>".$row['item_of_requisition']."</td>
			<td class='text-right'>".$row['qty_pr']."</td>
			<td class='text-right'>".$row['purchasing_document']."</td>
			<td class='text-right'>".$row['item']."</td>
			<td class='text-right'>".$row['qty_po']."</td>
			<td class='text-right'>".$row['material_po']."</td>
			<td class='text-right'>".number_format($row['oe'], 2)."</td>
			<td class='text-right'>".number_format($row['oe'] * $row['qty_po'], 2)."</td>
			<td class='text-right'>".number_format($row['bd'], 2)."</td>
			<td class='text-right'>".number_format($row['bd'] * $row['qty_po'], 2)."</td>
			<td class='text-right'>".number_format($row['ad'], 2)."</td>
			<td class='text-right'>".number_format($row['ad'] * $row['qty_po'], 2)."</td>
		</tr>";
	}
	echo "</table>";
?>
</div>
</div>
</div> -->