<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\ActivityGroup;

/* @var $this yii\web\View */
/* @var $model frontend\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'singkatan')->textInput(['maxlength' => 15]) ?>

    <?php echo '';// $form->field($model, 'activity_group_id')->textInput(['maxlength' => 10]) ?>

    <?=
    $form->field($model, 'activity_group_id')
         ->dropDownList(
                ArrayHelper::map(ActivityGroup::find()->all(), 'id', 'DropdownDisplayItem')
                )
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
