<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Activity');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Activity'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions'=> ['style' => 'width:4em'],
            ],
            //'id',
            
            [
                'attribute' => 'id',
                'headerOptions'=> ['style' => 'width:4em'],
                'format' => 'integer',
            ],
            
            'nama',
            'singkatan',
            //'activity_group_id',
            [
                'attribute' => 'activity_group.nama',
                'label' => 'Group',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activityGroup){
                        return Html::a($data->activityGroup->nama, '?ActivitySearch[activity_group.nama]=' . $data->activityGroup->nama.'&r=activity');
                    }
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=> ['style' => 'width:5em'],
            ],
        ],
    ]); ?>

</div>
