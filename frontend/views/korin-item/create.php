<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinItem */

$this->title = 'Create KORIN PP - Item';
$this->params['breadcrumbs'][] = ['label' => 'KORIN PP - Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
