<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KORIN PP - Item';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create KORIN PP - Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'korin_id',
            [
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin/view&id='.$data->korin->id);
                    }
                },
            ],
            'nama',
            'quantity',
            'satuan',
            'keterangan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
