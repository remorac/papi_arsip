<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinItem */

$this->title = $model->korin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'KORIN PP - Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'korin.no_surat',
            [
                'attribute' => 'korin.no_surat',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => Html::a(
                    $model->korin ? $model->korin->no_surat : '', 
                    $model->korin ? '?r=/korin/view&id=' . $model->korin->id : ''
                    )
            ],
            'nama',
            'quantity',
            'satuan',
            'keterangan',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
