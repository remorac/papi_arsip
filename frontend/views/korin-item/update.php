<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinItem */

$this->title = 'Update KORIN PP - Item: ' . ' ' . $model->korin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'KORIN PP - Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->korin->no_surat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
