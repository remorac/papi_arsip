<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'barang') ?>

    <?= $form->field($model, 'metoda_id') ?>

    <?= $form->field($model, 'purchaser_personel_id') ?>

    <?php // echo $form->field($model, 'start_plan') ?>

    <?php // echo $form->field($model, 'requirement_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
