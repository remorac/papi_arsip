<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\SapPr;

$dataProviderPengadaanPrItem->pagination = false;
?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanPrItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_item-pr_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],

        'purchase_requisition',
        [
            'attribute' =>'item_of_requisition',
            'label' => 'Item PR',
        ],
        [
            'attribute' => '',
            'label' => 'Material / Service',
            'value' => function($data) {
                return $data->sapPr ? $data->sapPr->{"Material"} : "";
            }
        ],
        [
            'attribute' => '',
            'label' => 'Description',
            'value' => function($data) {
                return $data->sapPr ? $data->sapPr->{"Short Text"} : "";
            }
        ],
        [
            'attribute' => '',
            'label' => 'Quantity Requested',
            'headerOptions' => ['style' => 'text-align:right'],
            'contentOptions' => ['style' => 'text-align:right'],
            'value' => function($data) {
                return $data->sapPr ? $data->sapPr->{"Quantity Requested"} : "";
            }
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'Price per Unit (OE)',
            'headerOptions' => ['style' => 'text-align:right'],
            'contentOptions' => ['style' => 'text-align:right'],
            'value' => function($data) {
                return $data->sapPr ? number_format($data->sapPr->{"Valuation Price"} / $data->sapPr->{"Price Unit"}) : "";
            }
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'OE Adjusted',
            'headerOptions' => ['style' => 'text-align:right'],
            'contentOptions' => ['style' => 'text-align:right;white-space:nowrap'],
            'value' => function($data) {
                $color = "";
                $matdesc = "";
                if (is_object($data->sapPr)) {
                    $color = $data->oe_adjusted == ($data->sapPr->{"Valuation Price"} / $data->sapPr->{"Price Unit"})
                    ? "color:crimson" : "";
                    $matdesc = $data->sapPr->Material."</b><br>".$data->sapPr->{"Short Text"};
                }
                return "<span style='$color'>"
                    .number_format($data->oe_adjusted)
                    ."<span>"
                    ."&nbsp; 
                    <button type='button' class='btn btn-default btn-xs' 
                    	onclick='showModalOe(
                    		\"".$data->id."\",
                    		\"".$data->oe_adjusted."\",
                    		\"<b>".$matdesc."\"
                    	)'>
                    	<i class='glyphicon glyphicon-pencil text-primary'></i>
                    </button>"
                    ;
            }
        ],
        [
            'attribute' => 'closed',
            'format' => 'raw',
            'label' => 'Status',
            'headerOptions' => ['width'=> '120px'],
            'value' => function($data) {
                $active_close       = "btn-default";
                $active_close_label = "close";
                $active_contd       = "btn-default";
                $active_contd_label = "continue";
                if ($data->closed == 0 ) {
                    $active_contd       = "btn-success active disabled"; 
                    $active_contd_label = "continued"; 
                } else { 
                    $active_close       = "btn-danger active disabled";
                    $active_close_label = "closed"; 
                }
                return  "<div class='btn-group' style='display:flex'>".
                        "<button id='".$data->id."' class='$active_close btn_close_item btn btn-xs'>$active_close_label</button>".
                        "<button id='".$data->id."' class='$active_contd btn_continue_item btn btn-xs'>$active_contd_label</button>".
                        "</div>";
            }
        ]
    ],
]); ?>



<!-- modal window for OE --> 
<form class="form-horizontal" id="oe_adjusted_form">
<div class="modal fade" id="modalOeAdjusted">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="modal-title" id="item"></span>
            </div>
            <div class="modal-body" style="background:#fafafa">
            	OE Adjusted
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" id="pengadaan-pr-item_oe_adjusted" name="pengadaan-pr-item_oe_adjusted" class="form-control text-right">
                        <input type="hidden" id="pengadaan-pr-item_id" name="pengadaan-pr-item_id">
                    </div>
                </div>
           </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_oe_adjusted" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<?php //form activity schedule
    $this->registerJs('
        function showModalOe(id, oe_adjusted, item) {
            $("#modalOeAdjusted").modal("show");
            $("#modalOeAdjusted #item").html(item);
            $("#pengadaan-pr-item_id").val(id);
            $("#pengadaan-pr-item_oe_adjusted").val(oe_adjusted);
        }
    ', \yii\web\VIEW::POS_END);

    $this->registerJs('
        $(document).on("click", "#btn_oe_adjusted", function(){
            data = $("#oe_adjusted_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/oeadjusted",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#item-pr_list", timeout: 10000});
                }
            });
        });
    ', \yii\web\VIEW::POS_READY);
?>