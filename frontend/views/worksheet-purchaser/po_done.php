<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Worksheet Purchaser';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

    <div class="btn-group">
        <?= Html::a('Inbox', '?r=worksheet-purchaser/index', ['class' => 'btn btn-default']) ?>
        <?= Html::a('In Progress', '?r=worksheet-purchaser/inprogress', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Closed', '#', ['class' => 'btn btn-info active']) ?>
    </div>
    <p></p>
    
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-striped table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
    
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi-sm'],
                'value' => function($data) {
                    $opt = "";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=worksheet-purchaser/view&id=".$data->id."' data-pjax=0> <span class='text-primary'><i class='glyphicon glyphicon-eye-open'></i> View Detail</span></a>";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=worksheet-purchaser/create-zip&id=".$data->id."' data-pjax=0> <span class='text-warning'><i class='fa fa-file-zip-o'></i> Create ZIP</span></a>";
                    
                    if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    for ($i=0; $i < 100; $i++) {
                        if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.part-'.$i.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.part-'.$i.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    }
                    return $opt;
                }
            ],
    
            /*[
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],*/
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => '',
                'label' => 'No. KPP',
                'value' => function($data) {
                    if ($data->pengadaanKorins) {
                        foreach ($data->pengadaanKorins as $pengadaanKorin) {
                            return $pengadaanKorin->korin->no_surat;
                        }
                    }
                }
            ],
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama.'&r=worksheet-purchaser');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
