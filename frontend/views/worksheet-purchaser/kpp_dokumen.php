<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\KorinDokumen;
use frontend\models\KorinDokumenSearch;

$searchModelKorinDokumen    = new \frontend\models\KorinDokumenSearch();
$queryParamsKorinDokumen    = Yii::$app->request->queryParams;

if (!isset($queryParamsKorinDokumen['KorinDokumenSearch'])) $queryParamsKorinDokumen['KorinDokumenSearch'] = array();
$queryParamsKorinDokumen['KorinDokumenSearch'] = array_merge($queryParamsKorinDokumen['KorinDokumenSearch'], 
    ['korin_id' => $korin_id,]
);
$dataProviderKorinDokumen   = $searchModelKorinDokumen->search($queryParamsKorinDokumen);

$dataProviderKorinDokumen->pagination->pageSize = 10;

?>    

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinDokumen,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        [
            'attribute' => '',
            'label' => 'Jenis',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->dokumenJenis->nama;
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'File',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return Html::a($data->dokumenUpload->pdf_filename, Url::to(['dokumen-upload/download', 'filename' => $data->dokumenUpload->pdf_filename]), ['target'=>'blank', 'data-pjax'=>0]);
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'Nomor',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return Html::a($data->dokumenUpload->no_dokumen, Url::to(['dokumen-upload/view', 'id' => $data->dokumenUpload->id]));
                }
            },
        ],
        [
            'attribute' => 'tgl_dokumen',
            'label' => 'Tgl',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->tgl_dokumen;
                }
            },
        ],
        /*[
            'header' => 'Check',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->fileExists ? '<b class="small text-success">OK (upload success)</b>' : '<b class="small text-danger">NOK (upload failed)</b>';
                }
            },
        ],*/
    ],
]); ?>            
