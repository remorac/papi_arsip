<?php 

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_pengadaan-item_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],
        'nama',
        'quantity',
        'satuan',
        [
            'attribute' => 'keterangan',
            'label' => 'Keterangan',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                	return nl2br($data->keterangan);
                }
            },
        ],
    ],
]); ?>
