<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanActivityDokumenSearch;

$searchModelPengadaanActivityDokumen    = new \frontend\models\PengadaanActivityDokumenSearch();
$queryParamsPengadaanActivityDokumen    = Yii::$app->request->queryParams;

if (!isset($queryParamsPengadaanActivityDokumen['PengadaanActivityDokumenSearch'])) $queryParamsPengadaanActivityDokumen['PengadaanActivityDokumenSearch'] = array();
$queryParamsPengadaanActivityDokumen['PengadaanActivityDokumenSearch'] = array_merge($queryParamsPengadaanActivityDokumen['PengadaanActivityDokumenSearch'], 
    ['pengadaan_activity_id' => $pengadaanActivity_id,]
);
$dataProviderPengadaanActivityDokumen   = $searchModelPengadaanActivityDokumen->search($queryParamsPengadaanActivityDokumen);

$dataProviderPengadaanActivityDokumen->pagination->pageSize = 10;

?>    

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanActivityDokumen,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi'],
            'value' => function($data) {
                return Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', 'javascript:removeLinkDocument("'.$data->id.'","'.$data->pengadaan_activity_id.'")', [
                    'title' => 'Remove Link',
                    'class' => 'btn btn-xs btn-default '
                ])
                ."&nbsp;".
                Html::a('<i class="fa fa-edit text-warning" ></i>', 'javascript:formRenameDocument("'.$data->dokumen_upload_id.'","'.$data->pengadaan_activity_id.'","'.$data->dokumenUpload->dokumenJenis->nama.'","'.$data->dokumenUpload->pdf_filename.'")', [
                    'title' => 'Rename',
                    'class' => 'btn btn-xs btn-default '
                ]);
            }
        ],
        [
            'attribute' => '',
            'label' => 'Jenis',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->dokumenJenis->nama;
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'File',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return Html::a($data->dokumenUpload->pdf_filename, Url::to(['dokumen-upload/download', 'filename' => $data->dokumenUpload->pdf_filename]), ['target'=>'blank', 'data-pjax'=>0]);
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'Nomor',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return Html::a($data->dokumenUpload->no_dokumen, Url::to(['dokumen-upload/view', 'id' => $data->dokumenUpload->id]));
                }
            },
        ],
        [
            'attribute' => '',
            'label' => 'Tanggal',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->tgl_dokumen;
                }
            },
        ],
        /*[
            'header' => 'Check',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->dokumenUpload->fileExists ? '<b class="small text-success">OK (upload success)</b>' : '<b class="small text-danger">NOK (upload failed)</b>';
                }
            },
        ],*/
    ],
]); ?>            
