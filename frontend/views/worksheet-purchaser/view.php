<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use yii\web\JsExpression;
use kartik\select2\Select2;

use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\DokumenJenis;
use frontend\models\ActivityDokumen;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;
use frontend\models\Tempat;
use frontend\models\Event;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

//temporary handling error for setting planned duration
if ($model->nilai_oe == null) $model->nilai_oe = 0;

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Worksheet Purchaser', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT);
?>
<div class="pengadaan-view box box-body box-info">

    <div class="nav-tabs-custom paper">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#pengadaan" aria-controls="home" role="tab" data-toggle="tab">Pengadaan</a></li>
            <li role="presentation"><a href="#korin-pp" aria-controls="profile" role="tab" data-toggle="tab">KORIN PP</a></li>
            <!-- <li role="presentation"><a href="#pengadaan-item" aria-controls="profile" role="tab" data-toggle="tab">Item Pengadaan</a></li> -->
            <li role="presentation"><a href="#item-pr" aria-controls="profile" role="tab" data-toggle="tab">Item PR</a></li>
            <li role="presentation"><a href="#purchase-order" aria-controls="messages" role="tab" data-toggle="tab">Purchase Order</a></li>
            <li role="presentation"><a href="#activity" aria-controls="settings" role="tab" data-toggle="tab">Procurement Cycle Time</a></li>
            <li role="presentation"><a href="#activity-detail" aria-controls="settings" role="tab" data-toggle="tab">Documents</a></li>
            <li role="presentation"><a href="#today-status" aria-controls="settings" role="tab" data-toggle="tab">Issue (Today Status)</a></li>
            <li role="presentation"><a href="#event" aria-controls="settings" role="tab" data-toggle="tab">Event</a></li>
            <li role="presentation"><a href="#verification" aria-controls="settings" role="tab" data-toggle="tab">Verifikasi</a></li>
            <li role="presentation"><a href="#access-group" aria-controls="settings" role="tab" data-toggle="tab">Access Group</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="pengadaan">
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Adjust Durasi Plan', ['adjust', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                    <!-- 
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                     -->
                    <a class='btn btn-default' href='?r=worksheet-purchaser/create-zip&id=<?= $model->id ?>' data-pjax=0> <span class='text-warning'><i class='fa fa-file-zip-o'></i> Create ZIP</span></a>
                    <?php 
                        if (file_exists(Yii::getAlias('@bundles/'.$model->nama.'.zip'))) echo '<a class="btn btn-default" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $model->nama.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a> ';
                        for ($i=0; $i < 100; $i++) {
                            if (file_exists(Yii::getAlias('@bundles/'.$model->nama.'.part-'.$i.'.zip'))) echo '<a class="btn btn-default" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $model->nama.'.part-'.$i.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP - '.$i.'</span></a> ';
                        }
                    ?>
                </p>    

                <?= DetailView::widget([
                    'model' => $model,
                    'options'=> ['class' => 'table detail-view  table-bordered'],
                    'template' => "<tr><th width='138px'>{label}</th><td>{value}</td></tr>",
                    'attributes' => [
                        //'id',
                        [
                            'attribute' => 'kode',
                            'label' => 'Kode',
                            'format' => 'raw',
                            'value' => $model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT)
                        ],
                        'nama',
                        //'barang_jasa_id',
                        [
                            'attribute' => 'barangJasa.nama',
                            'label' => 'Barang/Jasa',
                        ],
                        //'metoda_id',
                        [
                            'attribute' => 'metoda.nama',
                            'label' => 'Metoda',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->metoda ? $model->metoda->nama : '', 
                                $model->metoda ? '?r=/metoda/view&id=' . $model->metoda->id : ''
                                )
                        ],
                        //'level_approval_id',
                        [
                            'attribute' => 'levelApproval.approver',
                            'label' => 'Level Approval',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->levelApproval ? $model->levelApproval->approver : '', 
                                $model->levelApproval ? '?r=/level-approval/view&id=' . $model->levelApproval->id : ''
                                )
                        ],
                        'nilai_oe:decimal',
                        [
                            'attribute' => 'purchaserPersonelProcurement.personel.nama',
                            'label' => 'Purchaser',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->purchaserPersonelProcurement ? $model->purchaserPersonelProcurement->personel->nama : '', 
                                $model->purchaserPersonelProcurement ? '?r=/personel/view&id=' . $model->purchaserPersonelProcurement->id : ''
                                )
                        ],
                        'start_plan:date',
                        'requirement_date:date',
                        'po_reference',
                        'delivery_time',
                        'isi_disposisi',
                        'created_at',
                        'updated_at',
                        'createdBy.username:text:Created By',
                        'updatedBy.username:text:Updated By',
                        'kontrak_butuh',
                        'is_centerled',
                        // 'pr_ada',
                        // 'total_harga_sebelum_diskon:integer',
                        // 'total_harga_setelah_diskon:integer',
                    ],
                ]) ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="korin-pp">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#korin-pp_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Link KORIN PP</h4>
                    </div>
                    <div id="korin-pp_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-10">
                                    <?php
                                        $url = Url::to(['korin/list']);
                                        echo Select2::widget([
                                            'id' => 'korin_id',
                                            'name' => 'korin_id',
                                            'options' => [
                                                'placeholder' => 'select KORIN PP ...',
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'minimumInputLength' => 3,
                                                'ajax' => [
                                                    'url' => $url,
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                ],
                                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                                'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                            ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-2" style="padding-left:0">
                                    <button type="button" id="btn_korin-pp_create" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'korin-pp_list']); ?>
                    <?= $this->render('korin', [
                        'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
                        'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="pengadaan-item">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#pengadaan-item_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add Item Pengadaan</h4>
                    </div>
                    <div id="pengadaan-item_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form-horizontal" id="pengadaan-item_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pengadaan-item_nama" name="pengadaan-item_nama">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Quantity</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pengadaan-item_quantity" name="pengadaan-item_quantity">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Satuan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pengadaan-item_satuan" name="pengadaan-item_satuan">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Keterangan</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="pengadaan-item_keterangan" name="pengadaan-item_keterangan"></textarea>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_pengadaan-item_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengadaan-item_list']); ?>
                    <?= $this->render('pengadaan-item', [
                        'searchModelPengadaanItem' => $searchModelPengadaanItem,
                        'dataProviderPengadaanItem' => $dataProviderPengadaanItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="item-pr">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#item-pr_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add PR Item</h4>
                    </div>
                    <div id="item-pr_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form-horizontal" id="item-pr_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">No. PR</label>
                                            <div class="col-sm-9">
                                                <!-- <input type="text" name="itemPr-purchase_requisition" class="form-control" id="itemPr-purchase_requisition"> -->
                                                <?php
                                                    $url = Url::to(['worksheet-purchaser/listpr']);
                                                    echo Select2::widget([
                                                        'id' => 'itemPr-purchase_requisition',
                                                        'name' => 'itemPr-purchase_requisition',
                                                        'options' => [
                                                            'placeholder' => 'select No. PR ...',
                                                        ],
                                                        'pluginOptions' => [
                                                            'allowClear' => true,
                                                            'minimumInputLength' => 3,
                                                            'ajax' => [
                                                                'url' => $url,
                                                                'dataType' => 'json',
                                                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                            ],
                                                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                                        ],
                                                    ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <input type="checkbox" name="itemPr-select_all_item" id="itemPr-select_all_item" onchange="javascript:$('#itemPr-item_of_requisition').attr('readonly', this.checked)">
                                                &nbsp;ALL ITEMS
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Item PR</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemPr-item_of_requisition" class="form-control" id="itemPr-item_of_requisition">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_item-pr_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'item-pr_list']); ?>
                    <?= $this->render('item_pr', [
                        'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
                        'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="purchase-order">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#purchase-order_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add Purchase Order</h4>
                    </div>
                    <div id="purchase-order_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form-horizontal" id="po_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Purchase Order</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="po-purchasing_document" class="form-control" id="po-purchasing_document">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contract Reference</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="po-contract_reference" class="form-control" id="po-contract_reference">
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_purchase-order_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>false, 'id'=>'purchase-order_list']); ?>
                    <?= $this->render('purchase_order', [
                        'searchModelPengadaanPo' => $searchModelPengadaanPo,
                        'dataProviderPengadaanPo' => $dataProviderPengadaanPo,
                        'model' => $model,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="activity">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity_list']); ?>
                    <?= $this->render('activity', [
                        'model' => $model,
                        'alerts' => $alerts,
                    ]) ?>
                <?php Pjax::end(); ?>
                <?= Html::a('Export to Excel', ['/pct/export-excel', 'id' => $model->id], ['class' => 'btn btn-success']); ?>
            </div>

            <!-- activity detail -->
            <div role="tabpanel" class="tab-pane" id="activity-detail">

                <!-- <big>Dokumen Upload</big> -->
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity-detail_list']); ?>
                    <?= $this->render('activity_detail', [
                        'model' => $model,
                        'alerts' => $alerts,
                    ]) ?>
                <?php Pjax::end(); ?>

                <!-- <br>
                <big>Pengaturan Dokumen</big>
                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#activity-detail" href="#notreqdoc_form" style="padding: 7px 10px !important">
                        <span class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> &nbsp; Jenis Dokumen Yang Tidak Diperlukan</span>
                    </div>
                    <div id="notreqdoc_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-10">
                                    <?php
                                        echo Select2::widget([
                                            'id' => 'dokumen_jenis_id',
                                            'name' => 'dokumen_jenis_id',
                                            'data' => ArrayHelper::map(ActivityDokumen::find()->all(), 'id', 'DropdownDisplayItem'),
                                            'options' => [
                                                'placeholder' => 'select jenis dokumen ...',
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-2" style="padding-left:0">
                                    <button type="button" id="btn_notreqdoc_create" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                            <br>
                            <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengaturan-dokumen_list']); ?>
                                <?= $this->render('pengaturan_dokumen', [
                                    'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,
                                ]) ?>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#activity-detail" href="#verify_form" style="padding: 7px 10px !important">
                        <span class="panel-title"><i class="glyphicon glyphicon-ok-circle"></i> &nbsp; Verifikasi Dokumen </span>
                    </div>
                    <div id="verify_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="">
                                <?php $dv = Pengadaan::findOne($model->id)->documents_verification; ?>
                                <?php Pjax::begin(['timeout'=>10000, 'id'=>'documents_verification']); ?>
                                    <?php 
                                        echo $this->render('documents_verification', [
                                            'dv' => $dv
                                        ]);
                                    ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
            <!-- activity detail -->

            <div role="tabpanel" class="tab-pane" id="today-status">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#pengadaan-issue_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Issue (Today Status)</h4>
                    </div>
                    <div id="pengadaan-issue_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form-horizontal" id="pengadaan-issue_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Aktifitas</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="pengadaan-issue_pengadaan_activity_id" name="pengadaan-issue_pengadaan_activity_id">
                                                    <?php
                                                        foreach ($model->pengadaanActivities as $row) {
                                                            echo "<option value='".$row->id."'> ".$row->activity->nama." </option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tanggal</label>
                                            <div class="col-sm-9">
                                                <?=DatePicker::widget([
                                                    'id' => 'pengadaan-issue_tanggal',
                                                    'name' => 'pengadaan-issue_tanggal',
                                                    'value' => date('Y-m-d'),
                                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'format' => 'yyyy-mm-dd'
                                                    ]
                                                ]);?>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Keterangan</label>
                                            <div class="col-sm-9">
                                                <textarea rows="3" id="pengadaan-issue_keterangan" name="pengadaan-issue_keterangan" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_pengadaan-issue_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengadaan-issue_list']); ?>
                    <?= $this->render('today_status', [
                        'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="event">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#event_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add Event</h4>
                    </div>
                    <div id="event_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form-horizontal" id="event_form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Aktifitas</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="event_activity_id" name="event_activity_id">
                                                <option value=""></option>
                                                    <?php
                                                        $aktifitas = PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$model->id."' and durasi != 0")->all();
                                                        foreach ($aktifitas as $row) {
                                                            echo "<option value='".$row->activity->id."'> ".$row->activity->nama." </option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tanggal</label>
                                            <div class="col-sm-9">
                                                <?=DatePicker::widget([
                                                    'id' => 'event_tanggal',
                                                    'name' => 'event_tanggal',
                                                    'value' => date('Y-m-d'),
                                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'format' => 'yyyy-mm-dd'
                                                    ]
                                                ]);?>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Jam</label>
                                            <div class="col-sm-9">
                                                <?php 
                                                    echo TimePicker::widget(
                                                    [
                                                        'name' => 'event_jam',
                                                        'pluginOptions' => [
                                                            'showMeridian' => false, 
                                                        ],
                                                    ])
                                                ?>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tempat</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="event_tempat_id" name="event_tempat_id">
                                                <option value=""></option>
                                                <?php
                                                    $tempat = Tempat::find()->all();
                                                    foreach ($tempat as $row) {
                                                        echo "<option value='".$row->id."'> ".$row->nama." </option>";
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-9"><div class="help-block"></div></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_event_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'event_list']); ?>
                    <?= $this->render('event', [
                        'dataProviderEvent' => $dataProviderEvent,
                    ]) ?>
                <?php Pjax::end(); ?>

            </div>



            <div role="tabpanel" class="tab-pane" id="verification">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'verification_list']); ?>
                    <?= $this->render('verification', [
                        'model' => $model,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="access-group">
                <?php Pjax::begin(['timeout' => 10000, 'id' => 'access-group_list']); ?>
                    <?= $this->render('access-group', [
                        'model' => $model,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

        </div>
    </div>
</div>

<?php 
    //die(Yii::$app->request->get('goto'));
    /*if ($prev) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 37:
                    window.location="?r=worksheet-purchaser/view&id="+'.$prev[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault();
        });
        ', \yii\web\VIEW::POS_READY
    ); 

    if ($next) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 39:
                    window.location="?r=worksheet-purchaser/view&id="+'.$next[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault(); 
        });
        ', \yii\web\VIEW::POS_READY
    ); */

    // /die('var target = '. Yii::$app->request->get('goto') .';');
    $this->registerJs('
        var target = "'. Yii::$app->session->getFlash('goto') .'";
        $("#" + target).collapse("show");
        location.href = "#" + target;
        ', \yii\web\VIEW::POS_READY 
        );

    $this->registerCss('.alert-success {
        border-color: green;
    }'
    );

?>

<?php //KorinJS
    $this->registerJs('
        $("#btn_korin-pp_create").click(function() {
            id = $("#korin_id").val();
            $.ajax({
                url: "?r=worksheet-purchaser/korin",
                data: "op=create&pengadaan_id='.$model->id.'&korin_id="+id,
                cache: "",
                success:function(r) {
                    $("#select2-korin_id-container").html("<span class=\"select2-selection__placeholder\">select KORIN PP ...</span>");
                    $.pjax.reload({container: "#korin-pp_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_korin-pp_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/korin",
                    data: "op=delete&pengadaan_id='.$model->id.'&korin_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#korin-pp_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //PurchaseOrderJS
    $this->registerJs('
        $("#btn_purchase-order_create").click(function() {
            data = $("form#po_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/purchaseorder",
                data: "op=create&pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    //$("#select2-purchase-order-container").html("<span class=\"select2-selection__placeholder\">select PO ...</span>");
                    $("#po-purchasing_document").val("");
                    $("#po-contract_reference").val("");
                    $.pjax.reload({container: "#purchase-order_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_purchase-order_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/purchaseorder",
                    data: "op=delete&pengadaan_id='.$model->id.'&po_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#purchase-order_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //itemPrJS
    $this->registerJs('
        $("#btn_item-pr_create").click(function() {
            data = $("form#item-pr_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/itempr",
                data: "op=create&pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("#itemPr-item_of_requisition").val("");
                    if (document.getElementById("itemPr-select_all_item").checked) 
                    {
                        $("#itemPr-purchase_requisition").val("");
                        $("#select2-itemPr-purchase_requisition-container").html("<span class=\"select2-selection__placeholder\">select No. PR ...</span>");
                    }
                    $.pjax.reload({container: "#item-pr_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_item-pr_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/itempr",
                    data: "op=delete&pengadaan_id='.$model->id.'&pengadaan-pr-item_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#item-pr_list", timeout: 10000});
                    }
                });
            }
        });

        $(document).on("click", ".btn_close_item", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to close this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/itempr",
                    data: "op=close&pengadaan_id='.$model->id.'&pengadaan-pr-item_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#item-pr_list", timeout: 10000});
                    }
                });
            }
        });

        $(document).on("click", ".btn_continue_item", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to continue this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/itempr",
                    data: "op=continue&pengadaan_id='.$model->id.'&pengadaan-pr-item_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#item-pr_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //docNR-JS
    $this->registerJs('
        $("#btn_notreqdoc_create").click(function() {
            id = $("#dokumen_jenis_id").val();
            $.ajax({
                url: "?r=worksheet-purchaser/pengadaandokumennotrequired",
                data: "op=create&pengadaan_id='.$model->id.'&dokumen_jenis_id="+id,
                cache: "",
                success:function(r) {
                    $("#select2-dokumen_jenis_id-container").html("<span class=\"select2-selection__placeholder\">select jenis dokumen ...</span>");
                    $.pjax.reload({container: "#pengaturan-dokumen_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_notreqdoc_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/pengadaandokumennotrequired",
                    data: "op=delete&pengadaan_id='.$model->id.'&id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#pengaturan-dokumen_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //IssueJS
    $this->registerJs('
        $("#btn_pengadaan-issue_create").click(function() {
            data = $("form#pengadaan-issue_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/pengadaanissue",
                data: "sub=pengadaan-issue&op=create&pengadaan-issue_pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#pengadaan-issue_form").find("textarea").val("");
                    $.pjax.reload({container: "#pengadaan-issue_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_pengadaan-issue_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/pengadaanissue",
                    data: "sub=pengadaan-issue&op=delete&pengadaan-issue_pengadaan_id='.$model->id.'&pengadaan-issue_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#pengadaan-issue_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //EventJS
    $this->registerJs('
        $("#btn_event_create").click(function() {
            data = $("form#event_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/event",
                data: "sub=event&op=create&event_pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#event_form").find("select").val("");
                    $.pjax.reload({container: "#event_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_event_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/event",
                    data: "sub=event&op=delete&event_pengadaan_id='.$model->id.'&event_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#event_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //PengadaanItemJS
    $this->registerJs('
        $("#btn_pengadaan-item_create").click(function() {
            data = $("form#pengadaan-item_form").serialize();
            $.ajax({
                url: "?r=worksheet-purchaser/pengadaanitem",
                data: "op=create&pengadaan-item_pengadaan_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#pengadaan-item_form").find("input,textarea").val("");
                    $.pjax.reload({container: "#pengadaan-item_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_pengadaan-item_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/pengadaanitem",
                    data: "op=delete&pengadaan-item_pengadaan_id='.$model->id.'&pengadaan-item_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#pengadaan-item_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //PengadaanItemJS
    $this->registerJs('
        $(document).on("click", "#btn_verify", function(){
            if (confirm("Are you sure you the documents are completed?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/documentsverification",
                    data: "op=verify&pengadaan_id='.$model->id.'",
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#documents_verification", timeout: 10000});
                    }
                });
            }
        });

        $(document).on("click", "#btn_unverify", function(){
            if (confirm("Are you sure you to unverify documents completion?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/documentsverification",
                    data: "op=unverify&pengadaan_id='.$model->id.'",
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#documents_verification", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>