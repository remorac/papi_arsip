<?php
	use \yii\helpers\Url;
?>

<div class="row">

<?php 
	if ($model->verified_by_arsiparis == 0) {
		$text 	= "Verify";
		$label 	= "<h1 class='text-danger'><i class='fa fa-remove'></i> Unverified</h1>";
	} else {
		$text 	= "Unverify";
		$label 	= "<h1 class='text-success'><i class='fa fa-check'></i> Verified</h1>";
	}
?>

<div class="col-md-4">
	<div class="panel panel-success">
		<div class="panel-heading">
			<div class="panel-title">Arsiparis</div>
		</div>
		<div class="panel-body">
			<?=$label?>
		</div>
		<div class="panel-footer">
			<?php if (Yii::$app->user->can('/worksheet-purchaser/verified-by-arsiparis')) { ?>
			<a href="<?=Url::to(['worksheet-purchaser/verified-by-arsiparis', 'id'=>$model->id])?>"><?=$text?></a>
			<?php } ?>
			&nbsp;
		</div>
	</div>
</div>

<?php 
	if ($model->verified_by_purchaser == 0) {
		$text 	= "Verify";
		$label 	= "<h1 class='text-danger'><i class='fa fa-remove'></i> Unverified</h1>";
	} else {
		$text 	= "Unverify";
		$label 	= "<h1 class='text-success'><i class='fa fa-check'></i> Verified</h1>";
	}
?>

<div class="col-md-4">
	<div class="panel panel-success">
		<div class="panel-heading">
			<div class="panel-title">Purchaser</div>
		</div>
		<div class="panel-body">
			<?=$label?>
		</div>
		<div class="panel-footer"> 
			<?php if (Yii::$app->user->can('/worksheet-purchaser/verified-by-purchaser')) { ?>
			<a href="<?=Url::to(['worksheet-purchaser/verified-by-purchaser', 'id'=>$model->id])?>"><?=$text?></a>
			<?php } ?>
			&nbsp;
		</div>
	</div>
</div>

<?php 
	if ($model->verified_by_manager == 0) {
		$text 	= "Verify";
		$label 	= "<h1 class='text-danger'><i class='fa fa-remove'></i> Unverified</h1>";
	} else {
		$text 	= "Unverify";
		$label 	= "<h1 class='text-success'><i class='fa fa-check'></i> Verified</h1>";
	}
?>

<div class="col-md-4">
	<div class="panel panel-success">
		<div class="panel-heading">
			<div class="panel-title">Manager</div>
		</div>
		<div class="panel-body">
			<?=$label?>
		</div>
		<div class="panel-footer">
			<?php if (Yii::$app->user->can('/worksheet-purchaser/verified-by-manager')) { ?>
			<a href="<?=Url::to(['worksheet-purchaser/verified-by-manager', 'id'=>$model->id])?>"><?=$text?></a>
			<?php } ?>
			&nbsp;
		</div>
	</div>
</div>

</div>