<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use yii\web\JsExpression;
use kartik\select2\Select2;

use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\KorinDokumen;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

// batch upload form
?>

<?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

<?= $form->field($model, 'pdf_files[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'application/pdf',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => true,
            'allowedFileExtensions' => ['pdf'],
            'maxFileSize'=>3072000,
            'maxFileCount'=>20,
        ],
    ])->hint('Format: pdf. File dapat diupload maksimal 20 file, dengan cara <code>Ctrl+klik</code>.'); ?>

<?php ActiveForm::end(); ?>

<?php Pjax::begin(['timeout'=>10000, 'id'=>'pengadaan-file-uncategorized']); ?>
<table class="table table-condensed table-hover">
<?php 
    $i = 0;
    foreach ($model->pengadaanFileUncategorizeds as $pengadaanFileUncategorized) {
        echo '<tr>
            <td>'.++$i.'</td>
            <td><a target="_blank" data-pjax=0 href="'.Url::to(['dokumen-upload/download-uncategorized', 'filename' => $pengadaanFileUncategorized->filename]).'">'.$pengadaanFileUncategorized->filename.'</a></td>
            <td><a data-pjax=0 href="javascript:formRenameDocumentUncategorized(\''.$pengadaanFileUncategorized->id.'\', \''.$pengadaanFileUncategorized->filename.'\')"><i class="glyphicon glyphicon-edit text-warning"></i></a></td>
            <td><a data-method="post" data-confirm="Are you sure you want to delete this item?" data-pjax=0 href="'.Url::to(['delete-uncategorized', 'id' => $pengadaanFileUncategorized->id]).'"><i class="glyphicon glyphicon-trash text-danger"></i></a></td>
        <tr>';
    }
?>
</table>
<?php Pjax::end(); ?>

<?php

//temporary handling error for setting planned duration
if ($model->nilai_oe == null) $model->nilai_oe = 0;

//init vars
$no                 = 0;
$show_no            = 0;
$adder              = 0;
$std_selesai        = "0000-00-00"; 
$workdays_start     = "0000-00-00";
$workdays_end       = "0000-00-00";
$total_durasi_plan  = 0;
$total_durasi_real  = 0;
$total_saving       = 0;
$durasi_eva_tek     = 0;
$real_eva_tek       = 0;
$currentActivity    = "Tidak Ada";

//show KPP docs
foreach ($model->pengadaanKorins as $pengadaanKorin) {
?>
    <div class="panel panel-success" style="margin-bottom:5px">
        <div class="panel-heading pointer" data-toggle="collapse" data-parent="#activity-detail_list" href="#tab0" style="padding: 7px 10px !important">
            <span><span class="badge">00</span> &nbsp; <b>KPP</b>  <?= $pengadaanKorin->korin->no_surat ?></span>
        </div>
        <div id="tab0" class="panel-collapse collapse">
            <div class="panel-body" style="background:#fafafa">                    
                <div class="row">                    
                    <div class="col-sm-12">
                        <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity-dokumen_0']); ?>
                            <?php if (KorinDokumen::find()->where("korin_id ='".$pengadaanKorin->korin_id."'")->count() >= 1) { ?>
                                <?= $this->render('kpp_dokumen', [
                                        'korin_id' => $pengadaanKorin->korin_id,
                                ])?>  
                            <?php } ?>
                        <?php Pjax::end(); ?>                       
                    </div> 
                </div> 
            </div>
        </div>
    </div>

<?php }

//loop activity based on method
$rec_metodaActivity = MetodaActivity::find()->where(['metoda_id' => $model->metoda_id])->orderBy("urutan ASC")->all();
foreach ($rec_metodaActivity as $row) {
    $no++;

    //get durasi standar
    $durasiStandar = DurasiStandar::find()
            ->where(['=', 'activity_id', $row->activity_id])
            ->andWhere(['=','level_approval_id', 
                    LevelApproval::find()
                    ->where(['<=','nilai_min',$model->nilai_oe])
                    ->andWhere(['>=','nilai_max',$model->nilai_oe])
                    ->one()->id])
            ->one();
    $std_durasi = (is_object($durasiStandar)) ? $durasiStandar->durasi : 0;
    if ($row->activity_id==5) $durasi_eva_tek = $std_durasi;

    //get durasi adjusted
    $pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()
            ->where(['=', 'activity_id', $row->activity_id])
            ->andWhere(['=','pengadaan_id', $model->id])
            ->one();
    if (is_object($pengadaanDurasiAdjusted)) $std_durasi = $pengadaanDurasiAdjusted->durasi;
    $total_durasi_plan+=$std_durasi;
    if ($row->activity_id==5) $durasi_eva_tek = $std_durasi;

    //get real date
    $pengadaanActivity = PengadaanActivity::find()
            ->where(['=', 'activity_id', $row->activity_id])
            ->andWhere(['=','pengadaan_id',$model->id])
            ->one();
    if (is_object($pengadaanActivity)){
        $dStart = new DateTime($pengadaanActivity->start_date);
        $dEnd   = new DateTime($pengadaanActivity->end_date);
        if ($pengadaanActivity->end_date == "0000-00-00") $dEnd = new DateTime(date('Y-m-d'));
        $dDiff  = $dStart->diff($dEnd);

        $real_mulai     = $pengadaanActivity->start_date;
        $real_selesai   = $pengadaanActivity->end_date;
        if ($real_selesai=="0000-00-00") $real_selesai = date('Y-m-d');
        $real_durasi    = $dDiff->days+1;
        $saving         = $std_durasi - $real_durasi;
        $comply         = $saving >=0 ? "Comply" : "Not Comply";
        //set durasi real evaluasi teknis
        if (($model->metoda_id == 1 || $model->metoda_id == 2 || $model->metoda_id == 5 || $model->metoda_id == 6) && ($row->activity_id == 5 && $real_mulai != 0))
            $real_eva_tek = $real_durasi;
        //set current activity
        if ($std_durasi!=0 && $real_mulai!="0000-00-00" && $pengadaanActivity->end_date=="0000-00-00") 
                $currentActivity = Activity::find()->where(['=', 'id', $row->activity_id])->one()->nama;
    }

    //set standard date
    $std_mulai[1] = isset($real_mulai) ? $real_mulai : date('Y-m-d');
    if ($no > 1) $std_mulai[$no] = $std_selesai;
    $date = new DateTime($std_mulai[$no]);
    if ($no > 1) $date->add(new DateInterval('P1D'));
    $std_mulai[$no] = $date->format('Y-m-d');

    //adjust start date
    while (HariLibur::checkHoliday($std_mulai[$no])) {
        $date->add(new DateInterval('P1D'));                         
        $std_mulai[$no] = $date->format('Y-m-d');
    }
    $durasi_adder = $std_durasi > 1 ? $std_durasi-1 : 0; 
    $date->add(new DateInterval('P'.($durasi_adder).'D'));                          
    $std_selesai = $date->format('Y-m-d');

    //set end date 
    $adder = HariLibur::countHoliday($std_mulai[$no], $std_selesai);
    $date->add(new DateInterval('P'.($adder).'D'));                         
    $std_selesai = $date->format('Y-m-d');

    //adjust end date
    while (HariLibur::checkHoliday($std_selesai)) {
        $date->add(new DateInterval('P1D'));                         
        $std_selesai = $date->format('Y-m-d');
    }

    //set status
    if (is_object($pengadaanActivity)){
        if ($real_selesai == $std_selesai) $eod = "On Schedule";
        if ($real_selesai > $std_selesai) $eod = "Delay";
        if ($real_selesai < $std_selesai) $eod = "Early";
    }

    //set sum variable
    if (is_object($pengadaanActivity)){
        if ($workdays_start == 0) $workdays_start = $pengadaanActivity->start_date;
        if ($pengadaanActivity->end_date != "0000-00-00") $workdays_end = $pengadaanActivity->end_date;
    }
    
    //showing data
    if(is_object($pengadaanActivity) && $std_durasi!=0 && $real_mulai!="0000-00-00"){ $show_no++; ?>

        <div class="panel panel-success" style="margin-bottom:5px">
            <div class="panel-heading pointer" data-toggle="collapse" data-parent="#activity-detail_list" href="#tab<?=$row->activity->id?>" style="padding: 7px 10px !important">
                <span><span class="badge"><?= $show_no<10 ? '0'.$show_no : $show_no ?></span> &nbsp; <b><?=($row->activity->nama)?></b></span>
            </div>
            <div id="tab<?=$row->activity->id?>" class="panel-collapse collapse">
                <div class="panel-body" style="background:#fafafa">
                        
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <a href="javascript:formLinkDocument('<?=$pengadaanActivity->id?>', '<?=$pengadaanActivity->activity->nama?>')" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> &nbsp; Link Document</a>
                                <!-- <a href="#" class="btn btn-sm btn-default"><i class="fa fa-link"></i> &nbsp; Upload Document</a> -->
                            </p>
                        </div>

                        <div class="col-sm-12">
                            
                            <!-- <div id="activity-dokumen_<?=$pengadaanActivity->id?>"> -->
                                <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity-dokumen_'.$pengadaanActivity->id]); ?>
                                    <?php if (PengadaanActivityDokumen::find()->where("pengadaan_activity_id ='".$pengadaanActivity->id."'")->count() >= 1) { ?>
                                        <?= $this->render('activity_dokumen', [
                                                'pengadaanActivity_id' => $pengadaanActivity->id,
                                        ])?>  
                                    <?php } ?>
                                <?php Pjax::end(); ?>                       
                            <!-- </div>  -->
                            
                        </div> 
                    </div> <!-- end row -->
                                    
                </div>
            </div>
        </div>

    <?php } ?>

<?php
} //end of activity loop
?>

<!-- modal window for activity schedule --> 
<form class="form-horizontal" id="link_document_form">
<div class="modal fade" id="modalLinkDocument">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                            $url = \yii\helpers\Url::to(['dokumen-upload/list']);
                            echo Select2::widget([
                                'id' => 'pengadaan-activity-dokumen_dokumen_upload_id',
                                'name' => 'pengadaan-activity-dokumen_dokumen_upload_id',
                                'options' => [
                                    'placeholder' => 'select document ...',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                    'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                ],
                            ]);
                        ?>
                    </div>
                </div>
                <input type="hidden" id="pengadaan-activity-dokumen_pengadaan_activity_id" name="pengadaan-activity-dokumen_pengadaan_activity_id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_link_document btn btn-primary">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<!-- modal window for activity schedule --> 
<form class="form-horizontal" id="rename_document_form">
<div class="modal fade" id="modalRenameDocument">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="pengadaan-activity-dokumen_pdf_filename" name="pengadaan-activity-dokumen_pdf_filename">
                        <span class="small">Harap tidak mengubah atau menghilangkan ekstensi file (".pdf", ".docx", dll) saat melakukan rename file.</span>
                    </div>
                </div>
                <input type="hidden" id="pengadaan-activity-dokumen_dokumen_upload_id" name="pengadaan-activity-dokumen_dokumen_upload_id">
                <input type="hidden" id="pengadaan-activity-dokumen_pengadaan_activity_id" name="pengadaan-activity-dokumen_pengadaan_activity_id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_link_document btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn_rename_document btn btn-primary">Rename</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<!-- modal window for activity schedule --> 
<form class="form-horizontal" id="rename_document_form_uncategorized">
<div class="modal fade" id="modalRenameDocumentUncategorized">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="pengadaan_file_uncategorized_filename" name="pengadaan_file_uncategorized_filename">
                        <span class="small">Harap tidak mengubah atau menghilangkan ekstensi file (".pdf", ".docx", dll) saat melakukan rename file.</span>
                    </div>
                </div>
                <input type="hidden" id="pengadaan_file_uncategorized_id" name="pengadaan_file_uncategorized_id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_link_document_uncategorized btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn_rename_document_uncategorized btn btn-primary">Rename</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>

<?php //form 
    $this->registerJs('
        function formLinkDocument(id, activity_nama) {
            $("#modalLinkDocument").modal("show");
            $("#modalLinkDocument .modal-title").text(activity_nama);
            $("#modalLinkDocument .modal-title").prepend("<i class=\'glyphicon glyphicon-file\'></i> &nbsp;");
            $("#modalLinkDocument #pengadaan-activity-dokumen_pengadaan_activity_id").val(id);
        }

        function formRenameDocument(dokumen_upload_id, pengadaan_activity_id, dokumen_jenis, old_name) {
            $("#modalRenameDocument").modal("show");
            $("#modalRenameDocument .modal-title").text(dokumen_jenis);
            $("#modalRenameDocument .modal-title").prepend("<i class=\'fa fa-edit\'></i> &nbsp;");
            $("#modalRenameDocument #pengadaan-activity-dokumen_pengadaan_activity_id").val(pengadaan_activity_id);
            $("#modalRenameDocument #pengadaan-activity-dokumen_dokumen_upload_id").val(dokumen_upload_id);
            $("#modalRenameDocument #pengadaan-activity-dokumen_pdf_filename").val(old_name);
        }

        function formRenameDocumentUncategorized(pengadaan_file_uncategorized_id, old_name) {
            $("#modalRenameDocumentUncategorized").modal("show");
            $("#modalRenameDocumentUncategorized .modal-title").text("Rename");
            $("#modalRenameDocumentUncategorized .modal-title").prepend("<i class=\'fa fa-edit\'></i> &nbsp;");
            $("#modalRenameDocumentUncategorized #pengadaan_file_uncategorized_id").val(pengadaan_file_uncategorized_id);
            $("#modalRenameDocumentUncategorized #pengadaan_file_uncategorized_filename").val(old_name);
        }

        function removeLinkDocument(id, pengadaan_activity_id) {
            if (confirm("Are you sure you want to remove this document link?")) {
                $.ajax({
                    url: "?r=worksheet-purchaser/pengadaanactivitydokumen",
                    data: "op=delete&pengadaan-activity-dokumen_id="+id+"&pengadaan-activity-dokumen_pengadaan_activity_id="+pengadaan_activity_id,
                    cache: "",
                    success:function(r) {
                        //$("#activity-dokumen_"+pengadaan_activity_id).html(r);
                        $.pjax.reload({container: "#activity-dokumen_"+pengadaan_activity_id, timeout: 10000});
                    }
                });
            }
        }

        $(document).on("click", ".panel-heading", function(){
            $(this).css("margin-bottom","20px !important");
        });
    ', \yii\web\VIEW::POS_END);

    $this->registerJs('
        $(document).on("click", ".btn_link_document", function(){
            data = $("#link_document_form").serialize();
            pengadaan_activity_id = $("#modalLinkDocument #pengadaan-activity-dokumen_pengadaan_activity_id").val();
            $.ajax({
                url: "?r=worksheet-purchaser/pengadaanactivitydokumen",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    //$("#activity-dokumen_"+pengadaan_activity_id).html(r);
                    $.pjax.reload({container: "#activity-dokumen_"+pengadaan_activity_id, timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_rename_document", function(){
            data = $("#rename_document_form").serialize();
            pengadaan_activity_id = $("#modalRenameDocument #pengadaan-activity-dokumen_pengadaan_activity_id").val();
            $.ajax({
                url: "?r=worksheet-purchaser/rename-file",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#activity-dokumen_"+pengadaan_activity_id, timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_rename_document_uncategorized", function(){
            data = $("#rename_document_form_uncategorized").serialize();
            pengadaan_file_uncategorized_id = $("#modalRenameDocumentUncategorized #pengadaan_file_uncategorized_id").val();
            $.ajax({
                url: "?r=worksheet-purchaser/rename-file-uncategorized",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#pengadaan-file-uncategorized", timeout: 10000});
                }
            });
        });

        $(document).on("submit", "#rename_document_form", function(e){
            e.preventDefault();
            $(".btn_rename_document").click();
            return false;
        });

        $(document).on("submit", "#rename_document_form_uncategorized", function(e){
            e.preventDefault();
            $(".btn_rename_document_uncategorized").click();
            return false;
        });

        $(document).on("submit", "#link_document_form", function(e){
            e.preventDefault();
            $(".btn_link_document").click();
            return false;
        });
    ', \yii\web\VIEW::POS_READY);
?>