<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use frontend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Worksheet Purchaser';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

<?php
    $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:70px'],
                'value' => function($data) {
                    $opt = "";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=worksheet-purchaser/view&id=".$data->id."' data-pjax=0> <span class='text-primary'><i class='glyphicon glyphicon-eye-open'></i> View Detail</span></a>";
                    $opt.= "<a class='btn btn-block btn-xs btn-default' href='?r=worksheet-purchaser/create-zip&id=".$data->id."' data-pjax=0> <span class='text-warning'><i class='fa fa-file-zip-o'></i> Create ZIP</span></a>";
                    
                    if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    for ($i=0; $i < 100; $i++) {
                        if (file_exists(Yii::getAlias('@bundles/'.$data->nama.'.part-'.$i.'.zip'))) $opt.= '<a class="btn btn-block btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->nama.'.part-'.$i.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i> Download ZIP</span></a>';
                    }
                    return $opt;
                }
            ],
            [
              'attribute' => 'id',
              'contentOptions'=>['class'=>'style="width:50px"'],
            ],
            [
                'attribute' => 'barang_jasa_id',
                'label' => 'BRG/JSA',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 90px;'],
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode;
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'barang_jasa_id', [
                    '1'=>'BRG',
                    '2'=>'JSA',
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 70px;'],
                'value' => function($data){
                    if($data->barangJasa){
                        return str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => 'groupedKpp',
                'label' => 'KPP',
                'value' => function ($data) {
                    return $data->groupedKpp;
                }
            ],
            [
                'attribute' => 'groupedPo',
                'label' => 'PO',
                'value' => function ($data) {
                    return $data->groupedPo;
                }
            ],
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
            ],
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return $data->purchaserPersonelProcurement->personel->singkatan;
                    }
                },
            ],
            // 'level_approval_id',
            // 'nilai_oe',
            // 'start_plan',
            // 'requirement_date',
            // 'delivery_time:datetime',
            // 'po_reference',
            // 'documents_verification',
            // 'isi_disposisi',
            // 'subgroup',
            // 'kontrak_butuh',
            // 'pr_ada',
            // 'total_harga_sebelum_diskon',
            // 'total_harga_setelah_diskon',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            [
                'attribute'=>'created_by',
                'label'=>'Created By',
                'format' => 'raw',
                'value'=>function($data) { return "<small>".str_replace('.', ' ', $data->createdBy->username)."</small>"; },
                'filter' => Html::activeDropDownList($searchModel, 'created_by', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            [
                'attribute'=>'updated_by',
                'label'=>'Updated By',
                'format' => 'raw',
                'value'=>function($data) { return "<small>".str_replace('.', ' ', $data->updatedBy->username)."</small>"; },
                'filter' => Html::activeDropDownList($searchModel, 'updated_by', [
                    ArrayHelper::map(User::find()->all(), 'id', 'username')
                ], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
        ];

        $fullExportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true,
            'pjaxContainerId' => 'kv-pjax-container',
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
        ]);
    ?>

    <div class="btn-group">
        <?= Html::a('Inbox', '?r=worksheet-purchaser/index', ['class' => 'btn btn-default']) ?>
        <?= Html::a('In Progress', '#', ['class' => 'btn btn-info active']) ?>
        <?= Html::a('Closed', '?r=worksheet-purchaser/podone', ['class' => 'btn btn-default']) ?>
    </div>
    <div class="pull-right"><?= $fullExportMenu ?></div>
    <p></p>

    <?= GridView::widget(['dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        // 'floatHeader'=>true,
        // 'floatHeaderOptions'=>['scrollingTop'=>'0'],
        'pjax' => true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => 'kv-pjax-container'],
        ],
        'columns' => $gridColumns,
        'toolbar' => [
        //    '{export}',
            $fullExportMenu,
            '{toggleData}',
        ],
        //'export' => ['label' => '&nbsp;Export&nbsp;'],
        'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
        //'exportConfig' => [
        //    GridView::EXCEL => ['filename' => 'Semua_Pengadaan_'.date('Y-m-d')],
        //    GridView::CSV => ['filename' => 'Semua_Pengadaan_'.date('Y-m-d')],
        //    GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
        //],
        // 'panel'=>[
        //     'type'=>GridView::TYPE_DEFAULT,
        //     'heading'=> false,
        //     'before'=>'<div class="btn disabled">{summary}</div>',
        // ],
    ]); ?>

</div>