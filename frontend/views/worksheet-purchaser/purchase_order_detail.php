<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

//use frontend\models\SapPo;


?>

<div class="panel panel-default" style="margin-bottom:0px">
    <div class="panel-heading pointer" data-toggle="collapse" data-parent="#purchase_order_list" href="#tab<?=$model->purchasing_document?>">
        <div class="panel-title">
            <span class="text-success"><?=$model->purchasing_document?></span>
            <a id="<?=$model->id?>" class="btn_purchase-order_delete btn btn-sm btn-default pull-right">
                <i class="glyphicon glyphicon-trash pointer text-danger"></i>
                &nbsp;Remove
            </a>
        </div>
        <?php 
            $vendor         = "";
            $document_date  = "";
            if (($model->sapPo)) {
                $vendor        = $model->sapPo[0]->{"Vendor/supplying plant"} != "" ? $model->sapPo[0]->{"Vendor/supplying plant"} : "";
                $document_date = $model->sapPo[0]->{"Document Date"} != "" ? $model->sapPo[0]->{"Document Date"} : "";
            }
            echo $vendor
                ."<br> Doc Date : ".$document_date;
        ?>
    </div>
    <div id="tab<?=$model->purchasing_document?>" class="panel-collapse collapse">
    <div class="panel-body">
        <?php 
            /*$provider = new \yii\data\ActiveDataProvider([
                'query' => \frontend\models\SapPo::find(),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);*/
            $searchModel = new \frontend\models\SapPoSearch();
            $queryParams = Yii::$app->request->queryParams;
        
            if (!isset($queryParams['SapPoSearch'])) $queryParams['SapPoSearch'] = array();
            $queryParams['SapPoSearch'] = array_merge($queryParams['SapPoSearch'], 
                ['Purchasing Document' => $model->purchasing_document,]
            );
            $provider = $searchModel->search($queryParams);
            $provider->pagination->defaultPageSize = 10;
        ?>

        
        <?= GridView::widget([
            'dataProvider' => $provider,
            'filterModel' => null,
            'tableOptions' => ['class' => 'table table-hover paper'],
            'columns' => [
                [   
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['class' => 'serial-column'],
                    'contentOptions' => ['class' => 'serial-column'],
                ],
                'Item',
                'Material',
                'Short Text',
                'Order Quantity:integer:Qty',
                'Net price:integer:Harga PO (satuan)',
                [
                    'attribute'=>'', 'format'=>'raw', 'label'=>'Harga Sebelum Diskon',
                    'value'=> function($data) {
                        $bg         = "style='color:crimson'";
                        $hsd_harga  = $data->{"Net price"};
                        $hsd_id     = "";
                        
                        if ($data->hargaSebelumDiskon) {
                            $bg         = "";
                            $hsd_harga  = $data->hargaSebelumDiskon->harga;
                            $hsd_id     = $data->hargaSebelumDiskon->id;
                        }   
                        return "<span $bg>".number_format($hsd_harga)."</span>
                            <a class='pull-right' $bg 
                                href='javascript:showModalHargaSebelumDiskon(
                                        \"".$hsd_id."\",
                                        \"".$hsd_harga."\",
                                        \"<big>".Html::encode($data->Material)."</big><br>".Html::encode(addslashes($data->{"Short Text"}))."\",
                                        \"".$data->Item."\",
                                        \"".$data->{"Purchasing Document"}."\"
                                )'>
                                <i class='glyphicon glyphicon-pencil'></i>
                            </a>
                        ";
                    }
                ],
            ],
        ]); ?>

    </div>
    </div>
</div>
