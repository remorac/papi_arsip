<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Worksheet Purchaser';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

    <div class="btn-group">
        <?= Html::a('Inbox', '#', ['class' => 'btn btn-info active']) ?>
        <?= Html::a('In Progress', '?r=worksheet-purchaser/inprogress', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Closed', '?r=worksheet-purchaser/podone', ['class' => 'btn btn-default']) ?>
    </div>
    <p></p>
    
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
    
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi-sm'],
                'value' => function($data) {
                    return "<a class='btn btn-xs btn-default' href='?r=worksheet-purchaser/adjust&id=".$data->id."' data-pjax=0> <span class='text-success'><i class='glyphicon glyphicon-time'></i> Adjust</span></a>";
                }
            ],
    
            /*[
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],*/
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => '',
                'label' => 'No. KPP',
                'value' => function($data) {
                    if ($data->pengadaanKorins) {
                        foreach ($data->pengadaanKorins as $pengadaanKorin) {
                            return $pengadaanKorin->korin->no_surat;
                        }
                    }
                }
            ],
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama.'&r=worksheet-purchaser');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    
</div>
