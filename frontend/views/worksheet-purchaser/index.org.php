<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Worksheet Purchaser';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama.'&r=worksheet-purchaser');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 70px;'],
                'template' => '{adjust} &nbsp; {view}',
                'buttons' => [
                    'adjust' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-time"></span>', $url, [
                                    'title' => Yii::t('app', 'Adjust Durasi Plan'),
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'adjust') {
                        $url = "?r=worksheet-purchaser/adjust&id=".$model->id; // your own url generation logic
                        return $url;
                    }
                    if ($action === 'view') {
                        $url = "?r=worksheet-purchaser/view&id=".$model->id; // your own url generation logic
                        return $url;
                    }
                }    

            ],
        ],
    ]); ?>

</div>
