<?php
use \yii\helpers\Url;
use frontend\models\AccessGroup;
use frontend\models\PengadaanAccessGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$accessGroups = AccessGroup::find()->all();
$pengadaanAccessGroups = PengadaanAccessGroup::findAll(['pengadaan_id' => $model->id]);
?>

<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin() ?>
        <?php foreach ($accessGroups as $accessGroup) { ?>
            <?= Html::checkbox($accessGroup->id, ($pengadaanAccessGroup = PengadaanAccessGroup::findOne(['pengadaan_id' => $model->id, 'access_group_id' => $accessGroup->id])) !== null ? $pengadaanAccessGroup->access_group_id : null) . ' ' . $accessGroup->name . '<br>' ?> 
        <?php } ?>
        
        <br><?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>