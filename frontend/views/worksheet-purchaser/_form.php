<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use frontend\models\Metoda;
use frontend\models\PersonelProcurement;
use frontend\models\BarangJasa;
use frontend\models\LevelApproval;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'nama')->textarea(['maxlength' => 200, 'rows' => 3]) ?>
<!-- 
    <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'metoda_id')->dropDownList(ArrayHelper::map(Metoda::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'purchaser_personel_procurement_id')->dropDownList(ArrayHelper::map(PersonelProcurement::find()->where("jabatan='Purchaser'")->all(), 'id', 'DropdownDisplayItem')) ?>
 -->    
    <?= $form->field($model, 'level_approval_id')->dropDownList(ArrayHelper::map(LevelApproval::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'nilai_oe')->textInput() ?>
    <!-- 
    <?= $form->field($model, 'start_plan')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>
    -->
    <?= $form->field($model, 'requirement_date')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?= $form->field($model, 'delivery_time')->textInput() ?>

    <?= $form->field($model, 'po_reference')->textInput(['maxlength' => 20]) ?>
                
    <?= $form->field($model, 'kontrak_butuh')->dropDownList(['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt'=>'- select -']) ?>
    <!--
    <?= $form->field($model, 'pr_ada')->dropDownList(['Sudah' => 'Sudah', 'Belum' => 'Belum'], ['prompt'=>'- select -']) ?>
    <?= $form->field($model, 'total_harga_sebelum_diskon')->textInput() ?>
    <?= $form->field($model, 'total_harga_setelah_diskon')->textInput() ?>
     -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs('$("#pengadaan-barang_jasa_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
    $this->registerJs('$("#pengadaan-metoda_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
    $this->registerJs('$("#pengadaan-purchaser_personel_procurement_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
    $this->registerJs('$("#pengadaan-level_approval_id").attr("readonly","readonly")', \yii\web\VIEW::POS_READY); 
?>