<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\DurasiStandar;
use frontend\models\PengadaanDurasiAdjusted;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

$this->title = 'Adjust Durasi Plan';
$this->params['breadcrumbs'][] = ['label' => 'Purchaser', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-create box box-body box-success">

    <p class="lead"><?=$model->nama?></p>
    <table class="transparent" style="margin-bottom:10px">
    	<tr class="small">
    		<th>Metoda &nbsp;</th>
    		<td> : <?=$model->metoda->nama?></td>
    	</tr>
    	<tr class="small">
    		<th>Level Approval &nbsp;</th>
    		<td> : <?=$model->levelApproval->approver?></td>
    	</tr>
    </table>

    <?php $form = ActiveForm::begin(); ?>
	    <table class="table table-supercondensed table-hover vmiddle paper">
	    	<tr>
	    		<th width="50px" class="text-right">No</th>
	    		<th>Activity</th>
	    		<th width="120px" class="text-center">Durasi Standar</th>
	    		<th width="120px" class="text-center">Durasi Plan</th>
	    	</tr>
		    <?php 
		    	$no = 0;
		    	foreach ($modelMetodaActivity as $row) {
		    		$no++;
		    		$durasi = 0;
		    		if ($model->level_approval_id)
		    		$durasi = DurasiStandar::find()
		    				->where("level_approval_id = '".$model->level_approval_id."' and activity_id = '".$row->activity->id."'")
		    				->one()->durasi;
		    		if (is_object(PengadaanDurasiAdjusted::find()
		    				->where("pengadaan_id = '".$model->id."' and activity_id = '".$row->activity->id."'")->one())) 
		    			$adjustment = PengadaanDurasiAdjusted::find()
		    				->where("pengadaan_id = '".$model->id."' and activity_id = '".$row->activity->id."'")->one()->durasi;
		    		else $adjustment = $durasi;
		    		echo "<tr>
		    			<td align='right'>$no</td>
		    			<td>".$row->activity->nama."</td>
		    			<td class='text-center'>".$durasi."</td>
		    			<td class='text-center'>
		    				<input type='text' id='durasi[]' name='durasi[]' class='text-center' style='width:70px;' value='$adjustment'>
		    				<input type='hidden' id='activity_id[]' name='activity_id[]' value='".$row->activity->id."'>
		    			</td>
		    		</tr>";
		    	}
		    ?>
		    <tr><th></th><th></th><th></th><th class="text-center"><button type="submit" class="btn btn-success" style="width:70px">Save</button></th></tr>
	 	</table>
 	<?php ActiveForm::end(); ?>


</div>
