<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinKeluarDokumen */

$this->title = 'Create Korin Keluar Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Korin Keluar Dokumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-keluar-dokumen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
