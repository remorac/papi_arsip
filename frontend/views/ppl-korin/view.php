<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ppl-korin-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'options' => ['class' => 'table detail-view paper'],
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'date',
            'received_at',
            // 'paid_at',
            'value:integer',
            'released_value:integer',
            'locator',
        ],
    ]) ?>

</div>

    <div class="row">
        <div class="col-md-5">
        <div class="box">
        <div class="box-header">
            <h4 class="box-title">Documents</h4>
        </div>
        <div class="box-body">

            <?php $form = ActiveForm::begin([
                'action' => Url::to(['ppl-korin/document']),
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <?= Html::activeHiddenInput($modelDocument, 'ppl_korin_id', ['value' => $model->id])?>
            <?= $form->field($modelDocument, 'uploadedFile')->widget(FileInput::classname(), [
                'options' => ['placeholder' => 'browse document...'],
                'pluginOptions' => [
                    'showPreview' => false,
                ],
            ])->label(false); ?>


            <?php ActiveForm::end(); ?>

            <table class="table">
            <?php foreach ($model->pplKorinDocuments as $pplKorinDocument) { ?>
                <tr>
                    <td style="width:50px">
                        <a href="<?= Url::to(['delete-file', 'id' => $model->id, 'document_id' => $pplKorinDocument->id]) ?>" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                    <td>
                        <a target="_blank" href="<?= Url::to(['/dokumen-upload/download-ppl', 'filename' => $pplKorinDocument->file]) ?>">
                        <?= $pplKorinDocument->file ?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </table>

        </div>
        </div>
        </div>

        <div class="col-md-7">
        <div class="box">
        <div class="box-header">
            <h4 class="box-title">Pembayaran</h4>
        </div>
        <div class="box-body">

            <?php $form = ActiveForm::begin([
                'action' => Url::to(['ppl-korin/payment']),
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <?= Html::activeHiddenInput($modelBayar, 'ppl_korin_id', ['value' => $model->id])?>
            
            <div class="row">
            <div class="col-md-6" style="padding-right: 0">
            <?= $form->field($modelBayar, 'ppl_vendor_id')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'select vendor...'],
                'data' => ArrayHelper::map(\frontend\models\PplVendor::find()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(false) ?>

            </div>

            <div class="col-md-4" style="padding-right: 0">

            <?= $form->field($modelBayar, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'select date...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(false) ?>
            </div>
            <div class="col-md-2">
            <?= Html::submitButton('Save', ['class' => 'btn-block btn btn-primary']) ?>
            </div>
            </div>


            <?php ActiveForm::end(); ?>

            <table class="table">
            <?php foreach ($model->pplKorinBayars as $pplKorinBayar) { ?>
                <tr>
                    <td style="width:50px">
                        <a href="<?= Url::to(['delete-payment', 'id' => $model->id, 'payment_id' => $pplKorinBayar->id]) ?>" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                    <td><?= $pplKorinBayar->pplVendor->name ?></td>
                    <td><?= $pplKorinBayar->date ?></td>
                </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>

<!-- </div> -->
