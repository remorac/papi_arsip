<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-korin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        // 'options' => ['placeholder' => Yii::t('app', 'Starting Date')],
        // 'attribute2'=>'date',
        // 'type' => DatePicker::TYPE_RANGE,
        'pluginOptions' => [
            'autoclose' => true,
            // 'startView'=>'year',
            // 'minViewMode'=>'months',
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'received_at')->widget(DatePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>
<!-- 
    <?= $form->field($model, 'paid_at')->widget(DatePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>
 -->
    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'released_value')->textInput() ?>

    <?= $form->field($model, 'locator')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
