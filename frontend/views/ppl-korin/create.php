<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorin */

$this->title = 'Create Ppl Korin';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-korin-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
