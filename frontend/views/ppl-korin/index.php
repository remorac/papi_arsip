<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

use frontend\models\PplKorin;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplKorinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Korins';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ppl-korin-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Korin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['update', 'id' => $model->id]).'" class="btn btn-xs btn-default option-update"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],
            // 'id',
            'name',
            'date',
            'received_at',
            // 'paid_at',
            [
                'attribute' => 'value',     
                'headerOptions' => ['class' => 'text-right'],        
                'contentOptions' => ['class' => 'text-right'],  
                'footerOptions' => ['style' => 'background:#fafafa'],  
                'format' => 'raw',
                'value' => function($model) { return 'Rp '.Yii::$app->formatter->asDecimal($model->value); },
                // 'footer' => '<b><span>TOTAL :</span> <span class="pull-right">Rp '.Yii::$app->formatter->asDecimal(PplKorin::getTotal($dataProvider->models, 'value')).'</span></b>',
            ],
            [
                'attribute' => 'released_value',     
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'footerOptions' => ['style' => 'background:#fafafa'],  
                'format' => 'raw',
                'value' => function($model) { return 'Rp '.Yii::$app->formatter->asDecimal($model->released_value); },
                // 'footer' => '<b><span>TOTAL :</span> <span class="pull-right">Rp '.Yii::$app->formatter->asDecimal(PplKorin::getTotal($dataProvider->models, 'released_value')).'</span></b>',
            ],
            'locator',
            [
                'attribute' => '',
                'label' => 'Pembayaran',
                'format' => 'raw',
                'contentOptions' => ['style' => 'white-space:nowrap'],
                'value' => function($model) {
                    $docs = "";
                    foreach ($model->pplKorinBayars as $pplKorinBayar) {
                        $colorClass = $pplKorinBayar->date ? 'text-success' : 'text-danger';
                        $docs.= '<span class="'.$colorClass.'" title="'.$pplKorinBayar->date.'">'.$pplKorinBayar->pplVendor->name.'</span><br>';
                    }
                    return $docs;
                }
            ],
            [
                'attribute' => '',
                'label' => 'Dokumen',
                'format' => 'raw',
                'contentOptions' => ['style' => 'white-space:nowrap'],
                'value' => function($model) {
                    $docs = "";
                    foreach ($model->pplKorinDocuments as $pplKorinDocument) {
                        $docs.= '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $pplKorinDocument->file]).'">'.$pplKorinDocument->file.'</a><br>';
                    }
                    return $docs;
                }
            ],
        ],
    ]); ?>

    <table class="table">
        <tr>
            <th width="200px">
                Total Nilai Korin
            </th>
            <th>
                Rp <?= Yii::$app->formatter->asDecimal(\frontend\models\PplKorin::find()->sum('value')) ?>
            </th>
        </tr>
        <tr>
            <th>
                Total Nilai Release
            </th>
            <th>
                Rp <?= Yii::$app->formatter->asDecimal(\frontend\models\PplKorin::find()->sum('released_value')) ?>
            </th>
        </tr>
    </table>

</div>
