<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorin */

$this->title = 'Update Ppl Korin: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-korin-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
