<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapVendor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sap-vendor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Vendor')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Name of vendor')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'City')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Street')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Country')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Postal Code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Account group')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Search term')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Purch. Organization')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Purch. Org. Descr.')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Terms of Payment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Order currency')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Salesperson')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'One-time account')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Number of Purchasing Organizations')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Telephone')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Central purchasing block')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Block function')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Central deletion flag')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Purch. block for purchasing organization')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Delete flag for purchasing organization')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
