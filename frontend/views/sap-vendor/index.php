<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SapVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sap Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sap Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Vendor:ntext',
            'Name of vendor:ntext',
            'City:ntext',
            'Street:ntext',
            'Country:ntext',
            // 'Postal Code:ntext',
            // 'Account group:ntext',
            // 'Search term:ntext',
            // 'Purch. Organization:ntext',
            // 'Purch. Org. Descr.:ntext',
            // 'Terms of Payment:ntext',
            // 'Order currency:ntext',
            // 'Salesperson:ntext',
            // 'One-time account:ntext',
            // 'Number of Purchasing Organizations:ntext',
            // 'Telephone:ntext',
            // 'Central purchasing block:ntext',
            // 'Block function:ntext',
            // 'Central deletion flag:ntext',
            // 'Purch. block for purchasing organization:ntext',
            // 'Delete flag for purchasing organization:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
