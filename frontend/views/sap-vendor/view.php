<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapVendor */

$this->title = $model->Vendor;
$this->params['breadcrumbs'][] = ['label' => 'Sap Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-vendor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Vendor' => $model->Vendor, 'Purch. Organization' => $model->Purch. Organization], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Vendor' => $model->Vendor, 'Purch. Organization' => $model->Purch. Organization], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Vendor:ntext',
            'Name of vendor:ntext',
            'City:ntext',
            'Street:ntext',
            'Country:ntext',
            'Postal Code:ntext',
            'Account group:ntext',
            'Search term:ntext',
            'Purch. Organization:ntext',
            'Purch. Org. Descr.:ntext',
            'Terms of Payment:ntext',
            'Order currency:ntext',
            'Salesperson:ntext',
            'One-time account:ntext',
            'Number of Purchasing Organizations:ntext',
            'Telephone:ntext',
            'Central purchasing block:ntext',
            'Block function:ntext',
            'Central deletion flag:ntext',
            'Purch. block for purchasing organization:ntext',
            'Delete flag for purchasing organization:ntext',
        ],
    ]) ?>

</div>
