<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\SapVendor */

$this->title = 'Create Sap Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Sap Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-vendor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
