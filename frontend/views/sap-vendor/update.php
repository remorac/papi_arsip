<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapVendor */

$this->title = 'Update Sap Vendor: ' . ' ' . $model->Vendor;
$this->params['breadcrumbs'][] = ['label' => 'Sap Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Vendor, 'url' => ['view', 'Vendor' => $model->Vendor, 'Purch. Organization' => $model->Purch. Organization]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sap-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
