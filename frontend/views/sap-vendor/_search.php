<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapVendorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sap-vendor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Vendor') ?>

    <?= $form->field($model, 'Name of vendor') ?>

    <?= $form->field($model, 'City') ?>

    <?= $form->field($model, 'Street') ?>

    <?= $form->field($model, 'Country') ?>

    <?php // echo $form->field($model, 'Postal Code') ?>

    <?php // echo $form->field($model, 'Account group') ?>

    <?php // echo $form->field($model, 'Search term') ?>

    <?php // echo $form->field($model, 'Purch. Organization') ?>

    <?php // echo $form->field($model, 'Purch. Org. Descr.') ?>

    <?php // echo $form->field($model, 'Terms of Payment') ?>

    <?php // echo $form->field($model, 'Order currency') ?>

    <?php // echo $form->field($model, 'Salesperson') ?>

    <?php // echo $form->field($model, 'One-time account') ?>

    <?php // echo $form->field($model, 'Number of Purchasing Organizations') ?>

    <?php // echo $form->field($model, 'Telephone') ?>

    <?php // echo $form->field($model, 'Central purchasing block') ?>

    <?php // echo $form->field($model, 'Block function') ?>

    <?php // echo $form->field($model, 'Central deletion flag') ?>

    <?php // echo $form->field($model, 'Purch. block for purchasing organization') ?>

    <?php // echo $form->field($model, 'Delete flag for purchasing organization') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
