<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorinBayar */

$this->title = 'Update Ppl Korin Bayar: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korin Bayars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-korin-bayar-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
