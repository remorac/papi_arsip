<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplKorinBayar */

$this->title = 'Create Ppl Korin Bayar';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Korin Bayars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-korin-bayar-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
