<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplKorinBayarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Korin Bayars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-korin-bayar-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Korin Bayar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ppl_korin_id',
            'ppl_vendor_id',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
