<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TmpListPoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import & Search PO';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-list-po-index">

<div class="box box-primary">
    <div class="box-header with-border">
        <?= Html::a('<i class="fa fa-arrow-down"></i>&nbsp; Import File List PO', ['import'], ['class' => 'btn btn-default']) ?>        
        <?= Html::a('<i class="fa fa-file-zip-o"></i>&nbsp; Create ZIP for all', ['create-zip-all'], ['class' => 'btn btn-default']) ?>        
        <?= Html::a('<i class="fa fa-file-zip-o"></i>&nbsp; Re-Create ZIP for all', ['create-zip-all', 'overwrite' => 1], ['class' => 'btn btn-default']) ?>        
        <?= Html::a('<i class="fa fa-arrow-right"></i>&nbsp; Move to Folder', ['/audit-folder/create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('<i class="fa fa-folder-o"></i>&nbsp; Manage Folders', ['/audit-folder/index'], ['class' => 'btn btn-default']) ?>

        <?php echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => '',
                    'format' => 'raw',
                    'value' => function($data) {
                        if ($data->pengadaan) {
                            if (!file_exists(Yii::getAlias('@bundles/'.$data->pengadaan->nama.'.zip'))) {
                                $style = 'yellow';
                            } else {
                                $style = 'lightgreen';
                            }
                        } else {
                            $style = 'pink';
                        }
                        return $style;
                    }
                ],
                'po',
                [
                    'attribute' => 'id',
                    'value' => function ($data) {
                        return $data->pengadaan ? $data->pengadaan->id : null;
                    }
                ],
                [
                    'attribute' => 'remark',
                    'value' => function ($data) {
                        return $data->pengadaan ? $data->pengadaan->nama : null;
                    }
                ],
                [
                    'attribute' => '',
                    'label' => 'Metoda',
                    'value' => function ($data) {
                        return $data->pengadaan ? $data->pengadaan->metoda->nama : null;
                    }
                ],
            ],
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true,
            'pjaxContainerId' => 'kv-pjax-container',
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
        ]); ?>
        
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => function($data) {
                    $style = 'width:70px;';
                    if ($data->pengadaan) {
                        if (!file_exists(Yii::getAlias('@bundles/'.$data->pengadaan->nama.'.zip'))) {
                            $style.= 'background:yellow !important;';
                        } else {
                            $style.= 'background:lightgreen !important;';
                        }
                    } else {
                        $style.= 'background:pink !important;';
                    }
                    return ['style' => $style];
                },
                'value' => function($data) {
                    $opt = '';
                    if ($data->pengadaan) {
                        $page = isset(\Yii::$app->request->queryParams['page']) ? \Yii::$app->request->queryParams['page'] : '';
                        $opt.= "<a class='btn btn-xs btn-default' href='?r=semua-pengadaan/view&id=".$data->pengadaan->id."' data-pjax=0> <span class='text-primary'><i class='glyphicon glyphicon-eye-open'></i></span></a>";
                        $opt.= "<a class='btn btn-xs btn-default' href='".Url::to(['create-zip', 'id' => $data->pengadaan->id, 'page' => $page])."' data-method='post'> <span class='text-warning'><i class='fa fa-file-zip-o'></i></span></a>";
                    
                        if (file_exists(Yii::getAlias('@bundles/'.$data->pengadaan->nama.'.zip'))) $opt.= '<a class="btn btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->pengadaan->nama.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i></span></a>';
                        for ($i=0; $i < 100; $i++) {
                            if (file_exists(Yii::getAlias('@bundles/'.$data->pengadaan->nama.'.part-'.$i.'.zip'))) $opt.= '<a class="btn btn-xs" data-pjax=0 href="'.Url::to(['dokumen-upload/download-zip', 'filename' => $data->pengadaan->nama.'.part-'.$i.'.zip']).'"><span class="text-success"><i class="fa fa-download"></i></span></a>';
                        }
                    }
                    return $opt;
                }
            ],

            'po',
            [
                'attribute' => 'id',
                'value' => function ($data) {
                    return $data->pengadaan ? $data->pengadaan->id : null;
                }
            ],
            [
                'attribute' => 'remark',
                'value' => function ($data) {
                    return $data->pengadaan ? $data->pengadaan->nama : null;
                }
            ],
            [
                'attribute' => '',
                'label' => 'Metoda',
                'value' => function ($data) {
                    return $data->pengadaan ? $data->pengadaan->metoda->nama : null;
                }
            ],

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
</div>
