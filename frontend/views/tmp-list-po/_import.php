<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Package */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group">
    <?php 
        echo FileInput::widget([
            'id' => 'package-file',
            'name' => 'package-file',
            'options' => ['accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        ]);
    ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
