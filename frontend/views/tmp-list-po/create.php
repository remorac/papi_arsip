<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TmpListPo */

$this->title = 'Create Tmp List Po';
$this->params['breadcrumbs'][] = ['label' => 'Tmp List Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmp-list-po-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
