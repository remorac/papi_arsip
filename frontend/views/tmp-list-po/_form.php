<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpListPo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmp-list-po-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'po')->textInput(['maxlength' => 191]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
