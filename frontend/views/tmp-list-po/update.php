<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TmpListPo */

$this->title = 'Update Tmp List Po: ' . ' ' . $model->po;
$this->params['breadcrumbs'][] = ['label' => 'Tmp List Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->po, 'url' => ['view', 'id' => $model->po]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tmp-list-po-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
