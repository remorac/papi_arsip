<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Package */

$this->title = Yii::t('app', 'Import {modelClass} ', [
    'modelClass' => 'List File PO',
]);
$this->params['breadcrumbs'][] = ['label' => 'Import & Search PO', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success box-body">

    <?= $this->render('_import') ?>

</div>
