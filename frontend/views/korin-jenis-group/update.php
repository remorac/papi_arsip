<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinJenisGroup */

$this->title = 'Update KORIN Group: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'KORIN Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-jenis-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
