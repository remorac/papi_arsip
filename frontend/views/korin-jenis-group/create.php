<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinJenisGroup */

$this->title = 'Create KORIN Group';
$this->params['breadcrumbs'][] = ['label' => 'KORIN Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-jenis-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
