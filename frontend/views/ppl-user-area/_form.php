<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplUserArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-user-area-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ppl_area_id')->textInput() ?>

    <?= $form->field($model, 'ppl_user_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
