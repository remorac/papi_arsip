<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplUserAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl User Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-user-area-index">

    <p>
        <?= Html::a('Create Ppl User Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ppl_area_id',
            'ppl_user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
