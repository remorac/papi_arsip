<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use frontend\models\ArsipUnit;
use frontend\models\ArsipFileCategory;

/* @var $this yii\web\View */
/* @var $model frontend\models\ArsipFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="arsip-file-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?php echo $form->field($model, 'pdfFile')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'application/pdf',
            'multiple' => true,
        ],
        'pluginOptions' => [
            // 'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'allowedFileExtensions' => ['pdf'],
            'maxFileCount'=>100,
        ],
    ])->hint('Format: pdf. File dapat diupload beberapa sekaligus menggunakan <code>Ctrl+klik</code>.'); ?>

    <?= $form->field($model, 'arsip_file_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ArsipFileCategory::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'purchasing_document')->textInput(['maxlength' => true]) ?>

    
    <div class="form-panel col-sm-12">
        <div class="row">
    	    <div class="col-sm-6 col-sm-offset-3">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
