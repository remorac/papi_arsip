<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Activity;
use frontend\models\DokumenJenis;

/* @var $this yii\web\View */
/* @var $model frontend\models\ActivityDokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-dokumen-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'activity_id')->dropDownList(ArrayHelper::map(Activity::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

    <?= $form->field($model, 'dokumen_jenis_id')->dropDownList(ArrayHelper::map(DokumenJenis::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
