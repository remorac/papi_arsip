<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ActivityDokumen */

$this->title = 'Create Activity - Document';
$this->params['breadcrumbs'][] = ['label' => 'Activity - Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-dokumen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
