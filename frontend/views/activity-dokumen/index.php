<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ActivityDokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activity - Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-dokumen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Activity - Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activity){
                        return Html::a($data->activity->nama, '?r=activity&ActivitySearch[nama]=' . $data->activity->nama);
                    }
                },
            ],
            //'dokumen_jenis_id',
            [
                'attribute' => 'dokumenJenis.nama',
                'label' => 'Jenis Dokumen',
                'format' => 'raw',
                'value' => function($data){
                    if($data->dokumenJenis){
                        return Html::a($data->dokumenJenis->nama, '?r=dokumen-jenis&dokumenJenisSearch[nama]=' . $data->dokumenJenis->nama);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
