<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanClosed */

$this->title = 'Update Pengadaan Closed: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan Closeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-closed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
