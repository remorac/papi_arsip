<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use frontend\models\Pengadaan;
use frontend\models\AccessGroup;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanAccessGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-access-group-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'pengadaan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Pengadaan::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'access_group_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AccessGroup::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    
    <div class="form-panel col-sm-12">
        <div class="row">
    	    <div class="col-sm-6 col-sm-offset-3">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
