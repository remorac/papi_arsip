<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PenomoranJenis */

$this->title = 'Create Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Penomoran - Jenis Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penomoran-jenis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
