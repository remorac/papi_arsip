<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PenomoranJenisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Dokumen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penomoran-jenis-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jenis Dokumen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=penomoran-jenis/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=penomoran-jenis/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                    $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['penomoran-jenis/delete', 'id' => $data->id], [
                                    'title' => 'delete',
                                    'class' => 'btn btn-xs btn-default',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                    $opt.="</div>";
                    return $opt;
                }
            ],

            //'id',
            'nama',
            'kode',
            /*'created_at',
            'updated_at',*/
            // 'created_by',
            // 'updated_by',

        ],
    ]); ?>

</div>
