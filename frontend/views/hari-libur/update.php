<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\HariLibur */

$this->title = 'Update Hari Libur: ' . ' ' . $model->tanggal;
$this->params['breadcrumbs'][] = ['label' => 'Hari Libur', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tanggal, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hari-libur-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
