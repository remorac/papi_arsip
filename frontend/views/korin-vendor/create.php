<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinVendor */

$this->title = 'Create Korin PP - Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Korin PP - Vendor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-vendor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
