<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Korin PP - Vendor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Korin PP - Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'korin_id',
            [
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin/view&id='.$data->korin->id);
                    }
                },
            ],
            //'vendor_id',
            [
                'label' => 'Vendor',
                'format' => 'raw',
                'value' => function($data){
                    if($data->vendor){
                        return Html::a($data->vendor->kode.' : '.$data->vendor->nama, '?r=vendor/view&id='.$data->vendor->id);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
