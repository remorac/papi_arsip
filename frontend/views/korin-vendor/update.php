<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinVendor */

$this->title = 'Update Korin PP - Vendor: ' . ' ' . $model->korin->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Korin PP - Vendor', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->korin->no_surat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
