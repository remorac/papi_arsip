<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Unit;
use frontend\models\Posisi;

/* @var $this yii\web\View */
/* @var $model frontend\models\Personel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personel-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'nik')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'singkatan')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map(Unit::find()->all(), 'id', 'DropdownDisplayItem')) ?>
    
    <?= $form->field($model, 'posisi_id')->dropDownList(ArrayHelper::map(Posisi::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'gmail')->textInput(['maxlength' => 100]) ?>


    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
