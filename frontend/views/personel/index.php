<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PersonelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Personel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nik',
            'nama',
            'singkatan',
            //'unit_id',
            [
                'attribute' => 'unit.nama',
                'label' => 'Unit Kerja',
                'format' => 'raw',
                'value' => function($data){
                    if($data->unit){
                        return Html::a($data->unit->nama, '?r=unit&UnitSearch[nama]=' . $data->unit->nama);
                    }
                },
            ],
            [
                'attribute' => 'posisi.nama',
                'label' => 'Posisi',
                'format' => 'raw',
                'value' => function($data){
                    if($data->posisi){
                        return Html::a($data->posisi->nama, '?r=posisi&PosisiSearch[nama]=' . $data->posisi->nama);
                    }
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => [
                        'style' => 'width:5em'
                    ],
            ],
        ],
    ]); ?>

</div>
