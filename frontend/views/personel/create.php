<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Personel */

$this->title = 'Create Personel';
$this->params['breadcrumbs'][] = ['label' => 'Personel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
