<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Personel */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Personel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'nik',
            'nama',
            'singkatan',
            //'unit_id',
            [
                'attribute' => 'unit.nama',
                'label' => 'Unit Kerja',
                'format' => 'raw',
                'value' => Html::a(
                    $model->unit ? $model->unit->nama : '', 
                    $model->unit ? '?r=/unit/view&id=' . $model->unit->id : ''
                    )
            ],
            //'posisi_id',
            [
                'attribute' => 'posisi.nama',
                'label' => 'Posisi',
                'format' => 'raw',
                'value' => Html::a(
                    $model->posisi ? $model->posisi->nama : '', 
                    $model->posisi ? '?r=/posisi/view&id=' . $model->posisi->id : ''
                    )
            ],
            'email:email',
            'gmail:email',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'createdBy.username',
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->createdBy ? $model->createdBy->username : '', 
                    $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    )
            ],
            [
                //'attribute' => 'updatedBy.username',
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->updatedBy ? $model->updatedBy->username : '', 
                    $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    )
            ],
        ],
    ]) ?>

</div>
