<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\file\FileInput;
use kartik\widgets\Growl;
use frontend\models\KpiReport;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Import');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-body box-primary">

<?php
/*
foreach($messages as $message){
	//print_r($message);
	//die();
	echo Growl::widget([
	    'type' => $message['growlType'],
	    'title' => $message['title'],
	    'icon' => 'glyphicon glyphicon-remove-sign',
	    'body' => $message['body'],
	    'showSeparator' => true,
	    //'delay' => 4500,
	    'pluginOptions' => [
	        'showProgressbar' => true,
	        'placement' => [
	            'from' => 'top',
	            'align' => 'right',
	        ]
	    ]
	]);
}
*/
?>

<?php
$kpiDurasiQuery = KpiReport::getKpiDurasi();
$kpiDurasiDataProvider = new yii\data\ActiveDataProvider(['query' => $kpiDurasiQuery]);
//var_dump($kpiDurasiDataProvider);
//die();

?>


    <?= GridView::widget([
        'dataProvider' => $kpiDurasiDataProvider,
        //'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'tahun_bulan',
                'label' => 'Tahun Bulan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->kpiDurasi){
                    	var_dump($data->kpiDurasi);
                    	die();
                        return Html::a($data->tahunBulan, '?PengadaanSearch[barang]=' . $data->tahun_bulan.'&r=pengadaan');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'purchaserPersonel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonel){
                        return Html::a($data->purchaserPersonel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>