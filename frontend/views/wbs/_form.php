<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Wbs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wbs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_wbs')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'jenis_wbs')->textInput(['maxlength' => 20]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
