<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\WbsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Work Breakdown Structure';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wbs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create WBS', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kode_wbs',
            'nama',
            'jenis_wbs',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
