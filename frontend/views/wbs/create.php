<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Wbs */

$this->title = 'Create WBS';
$this->params['breadcrumbs'][] = ['label' => 'WBS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wbs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
