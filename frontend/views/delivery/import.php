<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\file\FileInput;

/* @var $this yii\web\View */
$this->title = 'Import ME2M File';
$this->params['breadcrumbs'][] = 'Delivery';
$this->params['breadcrumbs'][] = 'Import ME2M File';
?>
<h1>import ME2M file</h1>

<p>
    Import ME2M file exported from SAP after converted into CSV format with UTF-8 Encoding.
</p>

<div class="import-create">
<div class="import-form">

<?php $form = ActiveForm::begin([
    'options' => [
    	'enctype' => 'multipart/form-data',
    	//'class' => 'form-horizontal'
    ],
    /*'fieldConfig' => [
        'template' => "<div class=\"col-md-3 text-right\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-offset-3 col-md-9\">{error}</div>",
    ],*/
]); ?>

<?= $form->field($model, 'file')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
    'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<div class="form-group">
    <!-- <div class="col-md-offset-3 col-md-6"> -->
        <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
        <?php	
        if(Yii::$app->user->can('import_create-table')){
        	echo Html::submitButton('Create Table', ['name' => 'create-table', 'class' => 'btn btn-primary']);
        }
        ?>	
    <!-- </div> -->
</div>

<?php ActiveForm::end() ?>

</div>
</div>

<h2>Column Ordering Example</h2>
<p>Before importing, make sure your file has column ordering as described as image below.</p>
<div style="width:100%;height:200px;overflow:auto; border: 2px solid darkred; border-radius: 2px">
<img src="img/me2m.jpg"></img>
</div>