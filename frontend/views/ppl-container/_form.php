<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplContainer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-container-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'month')->widget(DatePicker::classname(), [
        // 'options' => ['placeholder' => Yii::t('app', 'Starting Date')],
        // 'attribute2'=>'date',
        // 'type' => DatePicker::TYPE_RANGE,
        'pluginOptions' => [
            'autoclose' => true,
            // 'startView'=>'year',
            'minViewMode'=>'months',
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'uploadedFile1')->widget(FileInput::classname(), [
        'options' => ['placeholder' => 'browse document...'],
        'pluginOptions' => [
            'showPreview' => false,
            'showUpload' => false,
        ],
    ]) ?>

    <?= $form->field($model, 'uploadedFile2')->widget(FileInput::classname(), [
        'options' => ['placeholder' => 'browse document...'],
        'pluginOptions' => [
            'showPreview' => false,
            'showUpload' => false,
        ],
    ]) ?>

    <?= $form->field($model, 'locator')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
