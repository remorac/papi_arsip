<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplContainer */

$this->title = 'Create Ppl Container';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-container-create box box-body box-success">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
