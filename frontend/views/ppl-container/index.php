<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplContainerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Containers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-container-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Container', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['update', 'id' => $model->id]).'" class="btn btn-xs btn-default option-update"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],

            // 'id',
            [
                'attribute' => 'month',
                'value' => function($model) {
                    return date('F Y', strtotime($model->month));
                }
            ],
            [
                'attribute' => 'file_in',
                'format' => 'raw',
                'value' => function($model) {
                    return '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $model->file_in]).'">'.$model->file_in.'</a>';
                }
            ],
            [
                'attribute' => 'file_out',
                'format' => 'raw',
                'value' => function($model) {
                    return '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $model->file_out]).'">'.$model->file_out.'</a>';
                }
            ],
            'locator',
        ],
    ]); ?>

</div>
