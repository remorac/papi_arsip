<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplContainer */

$this->title = 'Update Ppl Container: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-container-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
