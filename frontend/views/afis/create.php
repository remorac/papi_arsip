<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Afis */

$this->title = 'Create AFIS Document';
$this->params['breadcrumbs'][] = ['label' => 'AFIS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="afis-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
