<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AfisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'AFIS';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="afis-index box box-primary box-body">

    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'id',
            'filename',
            'clearing_document',
            'year',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:text:Created By',
            'updatedBy.username:text:Updated By',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Afis',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);

        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'contentOptions' => ['class' => 'action-column nowrap text-left'],
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', ['rename', 'filename' => $model->filename, 'year' => $model->year, 'id' => $model->id], ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
                    },
                    'delete' => function ($url) {
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                            'data-method' => 'post', 
                            'data-confirm' => 'Are you sure you want to delete this item?']);
                    },
                ],
            ],
            // 'id',
            [
                'attribute' => 'filename',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-wrap'],
                'value' => function($model) {
                    return Html::a($model->filename, ['download', 'filename' => $model->filename, 'year' => $model->year], ['target' => '_blank']);
                }
            ],
            'clearing_document',
            'year',
            /*[
                'attribute' => '',
                'format' => 'raw',
                'label' => 'Purchase Order',
                'value' => function($model) {
                    $po = [];
                    foreach ($model->zcps1013s as $zcps1013) {
                        // if ($model->year == substr($zcps1013->Clearing_Date, -4)) {
                            $po[] = intval($zcps1013->Purchase_Order);
                        // }
                    }
                    return implode(', ', $po);
                }
            ],*/            
            [
                'attribute' => 'groupedPo',
                'format' => 'raw',
                'label' => 'Purchase Order',
                'value' => function ($model) {
                    return $model->groupedPo;
                }
            ],
            // 'created_at:integer',
            // 'updated_at:integer',
            // 'created_by:integer',
            // 'updated_by:integer',
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Upload', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            Html::a('<i class="fa fa-refresh"></i> ' . 'Populate', ['populate'], ['class' => 'btn btn-default']),
            Html::a('<i class="fa fa-refresh"></i> ' . 'Patch', ['patch'], ['class' => 'btn btn-default']),
            Html::a('<i class="fa fa-unlink"></i> ' . 'Unsaved Files', ['orphaned'], ['class' => 'btn btn-default']),
            '{toggleData}',
            // $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>