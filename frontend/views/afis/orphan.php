<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AfisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unsaved Files ('.count($orphans).')';
$this->params['breadcrumbs'][] = ['label' => 'AFIS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="afis-index box box-primary box-body">

    <?php foreach ($orphans as $orphan) { ?>
        <div class="panel panel-default panel-body"> 
            <table class="table">
                <tr><td>
            <?= Html::a('Open File', ['download', 'filename' => $orphan['filename'], 'year' => $orphan['year']], [
                'class' => 'btn btn-sm btn-success',
                'target' => '_blank',
            ]) ?> 
            <?= Html::a('Rename', ['rename', 'filename' => $orphan['filename'], 'year' => $orphan['year']], ['class' => 'btn btn-sm btn-warning']) ?> 
            <?= Html::a('Delete', ['remove', 'filename' => $orphan['filename'], 'year' => $orphan['year']], [
                'class' => 'btn btn-sm btn-danger',
                'data-confirm' => 'Are you sure you want to delete this item?',
                'data-method' => 'post',
            ]) ?> 
            &nbsp;
            <big class="text-primary"><?= $orphan['filename'] ?> &nbsp; </big><span class="bdg text-muted"><?= $orphan['year'] ?></span>
                </td></tr>
                <tr><td>
            <?php if ($orphan['existed']) { ?>
            <?= Html::a('Open File', ['download', 'filename' => $orphan['existed'], 'year' => $orphan['year']], [
                'class' => 'btn btn-xs btn-success',
                'target' => '_blank',
            ]) ?> 
            <?= Html::a('Rename', ['rename', 'filename' => $orphan['existed'], 'year' => $orphan['year'], 'id' => $orphan['id']], ['class' => 'btn btn-xs btn-warning']) ?> 
            <?= Html::a('Delete', ['remove', 'filename' => $orphan['existed'], 'year' => $orphan['year'], 'id' => $orphan['id']], [
                'class' => 'btn btn-xs btn-danger',
                'data-confirm' => 'Are you sure you want to delete this item?',
                'data-method' => 'post',
            ]) ?> 
            &nbsp;
            <span class="text-warning">Existed Clearing Document Number in the same year: <b><?= $orphan['existed'] ?></b></span>
            <?php } else { ?>
                <?php if ($orphan['message']) { ?>
                    <span class="text-danger"><?= $orphan['message'] ?></span>
                <?php } else { ?>
                    <span class="text-success">No existed Clearing Document Number in the same year. Try patching or populating again to save this file.</span>
                <?php } ?>
            <?php } ?>
                </td></tr>
            </table>
        </div>
    <?php } ?>

</div>