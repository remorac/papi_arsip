<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Afis */

$this->title = 'Update Afis: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'AFIS', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="afis-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
