<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Afis */

$this->title = $model->filename;
$this->params['breadcrumbs'][] = ['label' => 'AFIS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->year .'-'. $model->clearing_document;
?>

<div class="afis-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                [
                    'attribute' => 'filename',
                    'format' => 'raw',
                    'value' => Html::a($model->filename, ['download', 'filename' => $model->filename, 'year' => $model->year], ['target' => '_blank']),
                ],
                'clearing_document',
                'year',                          
                [
                    'attribute' => 'groupedPo',
                    'format' => 'raw',
                    'label' => 'Purchase Order',
                    'value' => $model->groupedPo,
                ],
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
    </div>
</div>
