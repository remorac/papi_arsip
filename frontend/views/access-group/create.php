<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\AccessGroup */

$this->title = 'Create Access Group';
$this->params['breadcrumbs'][] = ['label' => 'Access Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="access-group-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
