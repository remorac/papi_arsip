<?php
ini_set('memory_limit','256M');

$url = \yii\helpers\Url::to(['pengadaan-activity/list']);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;
use frontend\models\PengadaanActivity;
use frontend\models\DokumenUpload;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanActivityDokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-activity-dokumen-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?php /*echo $form->field($model, 'pengadaan_activity_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(PengadaanActivity::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]);*/ ?>

<?php 
    //$cityDesc = empty($model->city) ? '' : City::findOne($model->city)->description;
 
    echo $form->field($model, 'pengadaan_activity_id')->widget(Select2::classname(), [
        //'initValueText' => $cityDesc, // set the initial display text
        'options' => ['placeholder' => 'Search for a Pengadaan - Activity ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
        ],
    ]);

?>
    <?= $form->field($model, 'dokumen_upload_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(DokumenUpload::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
    echo "<h4><br>Memory Usage";
    echo "<br><small><b>".number_format((memory_get_peak_usage(true)/1024/1024),'2')." MiB | </b>allocated by system";
    echo "<br><b>".number_format((memory_get_peak_usage(false)/1024/1024),'2')." MiB | </b>used by script</small></h4>";
?>