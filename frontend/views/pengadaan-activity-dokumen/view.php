<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanActivityDokumen */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-activity-dokumen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'pengadaan_activity_id',
            [
                'attribute' => 'pengadaan_activity_id',
                'label' => 'Aktivitas Pengadaan',
                'format' => 'raw',
                'value' => Html::a(
                    $model->pengadaanActivity ? $model->pengadaanActivity->pengadaan->nama.' : '.$model->pengadaanActivity->activity->nama : '', 
                    $model->pengadaanActivity ? '?r=/pengadaan-activity/view&id=' . $model->pengadaanActivity->id : ''
                    )
            ],
            //'dokumenUpload.pdf_filename',
            [
                'attribute' => 'dokumenUpload.no_dokumen',
                'label' => 'No. Dokumen',
                'format' => 'raw',
                'value' => Html::a(
                    $model->dokumenUpload ? $model->dokumenUpload->no_dokumen.' ('.$model->dokumenUpload->dokumenJenis->nama.')' : '', 
                    $model->dokumenUpload ? '?r=/dokumen-upload/view&id=' . $model->dokumenUpload->id : ''
                    )
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
