<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanActivityDokumen */

$this->title = 'Update Dokumen Pengadaan: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dokumen Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-activity-dokumen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
