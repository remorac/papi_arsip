<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanActivityDokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumen Pengadaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-activity-dokumen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dokumen Pengadaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pengadaan_activity_id',
            [
                'label' => 'Aktivitas Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaanActivity){
                        return Html::a($data->pengadaanActivity->pengadaan->nama.' : '.$data->pengadaanActivity->activity->nama, '?r=pengadaan-activity&PengadaanActivitySearch[id]='.$data->pengadaanActivity->id);
                    }
                },
            ],
            //'dokumen_upload_id',
            [
                'label' => 'No. Dokumen',
                'format' => 'raw',
                'value' => function($data){
                    if($data->dokumenUpload){
                        return Html::a($data->dokumenUpload->no_dokumen.' ('.$data->dokumenUpload->dokumenJenis->nama.')', 
                            '?r=/dokumen-upload/view&id='.$data->dokumenUpload->id);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
