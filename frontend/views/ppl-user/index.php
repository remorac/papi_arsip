<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-user-index box box-body box-primary">

    <p>
        <?= Html::a('Create Ppl User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
