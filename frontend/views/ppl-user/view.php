<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplUser */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-user-view">

    <div class="box">
        <div class="box-header with-border">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>

        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                ],
            ]) ?>

            <br>
            <h4>Area</h4>
            <table class="table">
            <?php 
                foreach ($model->pplUserAreas as $pplUserArea) {
                    echo '<tr><td>'.$pplUserArea->pplArea->name.'</td></tr>';
                }
            ?>
            </table>
        </div>
    </div>

</div>
