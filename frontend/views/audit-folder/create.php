<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\AuditFolder */

$this->title = 'Create Audit Folder';
$this->params['breadcrumbs'][] = ['label' => 'Audit Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-folder-create box box-primary box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
