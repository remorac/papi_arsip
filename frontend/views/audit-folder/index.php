<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AuditFolderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Audit Folders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-folder-index box box-primary box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'description:ntext',
            'is_visible',
            'created_at:datetime',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
