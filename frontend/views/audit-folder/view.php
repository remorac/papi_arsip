<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuditFolder */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Audit Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-folder-view box box-info box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'is_visible',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:text:Created By',
            'updatedBy.username:text:Updated By',
        ],
    ]) ?>

    <h4>FILES</h4>
    <?php 
        $i = 0;
        foreach ($model->auditFiles as $auditFile) {
            echo '<a href="'.Url::to(['audit-folder/view', 'id' => $model->id, 'auditFile_id' => $auditFile->id]).'" 
                class="btn btn-default btn-xs"
                data-confirm = "Are you sure you want to delete this item?"
                data-method = "post"
            ],
                ><i class="fa fa-trash text-danger"></i></a> &nbsp;'
            .++$i.'. <a href="'.Url::to(['audit-folder/download', 'file' => $auditFile->filename]).'">'.$auditFile->filename."</a><br>";
        }
    ?>

</div>
