<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AuditFolderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files for Auditors';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-folder-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-body box-primary">
        <br>
    <?php foreach ($dataProvider->getModels() as $model) { ?>
        <div class="col-md-2 text-center">
            <a href="<?= Url::to(['view', 'id' => $model->id]) ?>">
                <i class="fa fa-folder" style="color:orange; font-size:50pt"></i> <br>
                <big><?= $model->name ?></big><br>
                <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
            </a>
        </div>
    <?php } ?>
    </div>

</div>
