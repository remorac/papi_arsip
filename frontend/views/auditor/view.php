<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuditFolder */

$this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Audit Folders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-folder-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::$app->formatter->asDatetime($model->created_at) ?></p>
    <p><?= Html::a('<i class="fa fa-arrow-left"></i>&nbsp; Back', ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?></p>

    <div class="box box-info">
    <table class="table table-hover">
    <?php 
        $i = 0;
        foreach ($model->auditFiles as $auditFile) {
    ?>
            <tr>
                <td class="text-center"><?= ++$i ?></td>
                <td class="text-right">
                    <a href="<?= Url::to(['auditor/download', 'file' => $auditFile->filename]) ?>">
                        <table style="background: transparent;">
                            <tr>
                                <td><i class="fa fa-file-zip-o"></i> &nbsp; </td>
                                <td><?= $auditFile->filename ?></td>
                            </tr>
                        </table>
                    </a>
                </td>
                <td class="text-right">
                    <a href="<?= Url::to(['auditor/download', 'file' => $auditFile->filename]) ?>">
                        <i class="fa fa-download"></i> &nbsp; </td>
                    </a>
                </td>
            </tr>
    <?php
        }
    ?>
    </table>
    </div>
</div>
