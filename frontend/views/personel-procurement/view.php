<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PersonelProcurement */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Procurement Personel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-procurement-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Personel',
                'format' => 'raw',
                'value' => Html::a(
                    $model->personel ? $model->personel->nama : '', 
                    $model->personel ? '?r=/personel/view&id=' . $model->personel->id : ''
                    )
            ],
            //'barang',
            [
                'attribute' => 'barangJasa',
                'label' => 'Barang / Jasa',
            ],
            'jabatan',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
