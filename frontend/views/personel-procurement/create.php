<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PersonelProcurement */

$this->title = 'Create Procurement Personel';
$this->params['breadcrumbs'][] = ['label' => 'Procurement Personel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-procurement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
