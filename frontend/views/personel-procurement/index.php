<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PersonelProcurementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Procurement Personel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-procurement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Procurement Personel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Personel',
                'format' => 'raw',
                'value' => function($data){
                    if($data->personel){
                        return Html::a($data->personel->nama, '?PersonelProcurementSearch[personel.nama]=' . $data->personel->nama.'&r=personel-procurement');
                    }
                },
            ],
            //'barang',
            [
                'attribute' => 'barangJasa',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa, '?PersonelProcurementSearch[barangJasa]=' . $data->barangJasa.'&r=personel-procurement');
                    }
                },
            ],
            'jabatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
