<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Personel;

/* @var $this yii\web\View */
/* @var $model frontend\models\PersonelProcurement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personel-procurement-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'personel_id')->dropDownList(ArrayHelper::map(Personel::find()->all(), 'id', 'DropdownDisplayItem')) ?>
    
    <?= $form->field($model,'barang')->dropDownList(array("2"=>"All", "1"=>"Barang", "0"=>"Jasa"), array('empty'=>'Select Value')); ?>

    <?= $form->field($model,'jabatan')->dropDownList(array("Senior Manager"=>"Senior Manager", "Manager"=>"Manager", "Purchaser"=>"Purchaser"), array('empty'=>'Select Value')); ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
