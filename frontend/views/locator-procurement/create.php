<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\LocatorProcurement */

$this->title = 'Create Locator Procurement';
$this->params['breadcrumbs'][] = ['label' => 'Locator Procurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locator-procurement-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form') ?>
    </div>

</div>
