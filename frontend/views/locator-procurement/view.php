<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\LocatorProcurement */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Locator Procurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="locator-procurement-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'kode_klasifikasi',
                'purchase_order',
                'nama_pengadaan:ntext',
                'tahun:integer',
                'box:integer',
                'locator',
            ],
        ]) ?>
    </div>
</div>
