<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model frontend\models\LocatorProcurement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locator-procurement-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group">
    <?php 
        echo FileInput::widget([
            'id' => 'package-file',
            'name' => 'package-file',
            'options' => ['accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        ]);
    ?>
    </div>

    <?php ActiveForm::end(); ?>

    <style type="text/css">
        .table-excel {
            font-family: sans-serif;
        }
        .table-excel th {
            text-align: center;
            color: #999;
        }
        .table-excel tr.data-header td {
            /*font-weight: bold;*/
            text-align: center;
            background: #ff06;
        }
    </style>
    <br>
    Format file Excel yang dapat diimport:
    <center>
    <table class="table table-bordered table-condensed table-excel small text-muted">
        <tr>
            <th width="50px" style="background: #e7e7e7">&nbsp;</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
            <th>E</th>
            <th>F</th>
            <th>G</th>
        </tr>
        <tr class="data-header">
            <th>1</th>
            <td>No</td>
            <td>Kode Klasifikasi</td>
            <td>Purchase Order</td>
            <td>Nama Pengadaan</td>
            <td>Tahun</td>
            <td>Box</td>
            <td>Locator</td>
        </tr>
        <tr>
            <th>2</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>3</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    </center>

    <p>
        Keterangan:
        <ul>
            <li>Data dimulai pada row 2 dan seterusnya (row 1 sebagai header).</li>
        </ul>
    </p>

</div>
