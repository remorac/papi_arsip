<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\LocatorProcurement */

$this->title = 'Update Locator Procurement: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Locator Procurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="locator-procurement-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
