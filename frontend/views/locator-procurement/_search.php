<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LocatorProcurementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locator-procurement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode_klasifikasi') ?>

    <?= $form->field($model, 'purchase_order') ?>

    <?= $form->field($model, 'nama_pengadaan') ?>

    <?= $form->field($model, 'tahun') ?>

    <?php // echo $form->field($model, 'box') ?>

    <?php // echo $form->field($model, 'locator') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
