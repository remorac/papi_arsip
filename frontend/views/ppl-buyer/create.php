<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplBuyer */

$this->title = 'Create Ppl Buyer';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Buyers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-buyer-create box box-body box-success">
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
