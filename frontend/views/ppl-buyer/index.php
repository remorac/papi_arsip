<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplBuyerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Buyers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-buyer-index box box-body box-primary">
    
    <p>
        <?= Html::a('Create Ppl Buyer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'initial',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
