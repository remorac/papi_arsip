<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;
use frontend\models\PengadaanIssue;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

//temporary handling error for setting planned duration
if ($model->nilai_oe==null) $model->nilai_oe = 0;

$this->title = $model->nama." (".$model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT).")";
$this->params['breadcrumbs'][] = ['label' => 'PCT', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-view">
<h1></h1>
<p>
    <div class="row">
        <div class="col-md-6">
            <?php // echo Html::button('Print', [ 'class' => 'btn btn-primary', 'onclick' => 'window.print();' ]); ?>
            <?= Html::a('Export to Excel', ['export-excel', 'id' => $model->id], ['class' => 'btn btn-success']); ?>
        </div>
        <div class="col-md-6 text-right">
            <?php
                $next = Pengadaan::find()
                        ->where('id >'.$model->id)
                        ->orderBy('id')
                        ->limit(1)
                        ->all();
                $prev = Pengadaan::find()
                        ->where('id <'.$model->id)
                        ->orderBy(['id'=> SORT_DESC])
                        ->limit(1)
                        ->all();
            ?>
            <?php if ($prev) echo Html::a('Prev', '?r=pct/view&id='.$prev[0]->id, [ 'class' => 'btn btn-default']); ?>
            <?php if ($next) echo Html::a('Next', '?r=pct/view&id='.$next[0]->id, [ 'class' => 'btn btn-default']); ?>
            <script type="text/javascript">
                //window.location.assign('?r=pct/view&id='.$prev[0]->id);
            </script>
        </div>
    </div>
</p>
    <div class="paper" style="padding:20px">
    <div class="printable">
        <center><h2 class="text-success">PROCUREMENT CYCLE TIME</h2></center>
        <table class="vtop" width="100%">
        <tr>
        <td width="50%">
            <table class="vtop">
                <tr>
                    <td><b>JENIS (BARANG/JASA)</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->barangJasa->nama?></td>
                </tr>
                <tr>
                    <td><b>UNIT USER</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>
                        <?php 
                            $pic = "";
                            if ($model->pengadaanKorins) {
                                foreach ($model->pengadaanKorins as $row) {
                                    $arr_korin_id = explode('/', $row->korin->no_surat);
                                    $kodeSurat = $arr_korin_id[3];
                                    $unitPosisi = UnitPosisi::find()->where(['=', 'kode_surat', $arr_korin_id[3]])->one();
                                    if (!$unitPosisi) $unitPosisi = UnitPosisi::find()->where(['=', 'kode_surat', $arr_korin_id[4]])->one();
                                    if ($unitPosisi) echo $unitPosisi->nama."<br>"; else echo $arr_korin_id[3]."<br>";
                                    $pic .= $row->korin->pic_initial."<br>";
                                }
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><b>PIC USER</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$pic?></td>
                </tr>
            </table>
        </td>
        <td>
            <table class="vtop">
                <tr>
                    <td style="white-space:nowrap"><b>NAMA PENGADAAN</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->nama?></td>
                </tr>
                <tr>
                    <td><b>PURCHASER</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->purchaserPersonelProcurement->personel->nama?></td>
                </tr>
                <tr>
                    <td><b>METODA</b></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?=$model->metoda->nama?></td>
                </tr>
            </table>
        </td>
        </tr>
        </table>
        <br>
        
        <table class="table table-bordered table-condensed table-hover" width="100%">
            <tr style="background:#eee;">
                <th rowspan="2">NO</th>
                <th rowspan="2">ACTIVITY</th>
                <th colspan="3">PCT Planning</th>
                <th colspan="3">PCT Real</th>
                <th colspan="2">Durasi</th>
                <th>Schedule</th>
            </tr>
             <tr>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Durasi</th>
                <th>Comply/Not</th>
                <th>Saving</th>
                <th>E/O/D</th>
            </tr>
            <?php

                //init vars
                $no                 = 0;
                $adder              = 0;
                $std_selesai        = "0000-00-00"; 
                $workdays_start     = "0000-00-00";
                $workdays_end       = "0000-00-00";
                $total_durasi_plan  = 0;
                $total_durasi_real  = 0;
                $total_saving       = 0;
                $durasi_eva_tek     = 0;
                $real_eva_tek       = 0;
                $durasi_ko_meeting  = 0;
                $real_ko_meeting    = 0;
                $currentActivity    = "Tidak Ada";


                //loop activity based on method
                $rec_metodaActivity = MetodaActivity::find()->where(['metoda_id' => $model->metoda_id])->all();
                foreach ($rec_metodaActivity as $row) {
                    $no++;

                    //get durasi standar
                    if (is_object($model->level_approval_id))
                    $durasiStandar = DurasiStandar::find()
                            ->where(['=', 'activity_id', $row->activity_id])
                            ->andWhere(['=','level_approval_id', 
                                    LevelApproval::find()->where("id = '".$model->level_approval_id."'")->one()->id])
                            ->one();
                    else $durasiStandar = 0;
                    $std_durasi = (is_object($durasiStandar)) ? $durasiStandar->durasi : 0;
                    if ($row->activity_id==5) $durasi_eva_tek = $std_durasi;
                    if ($row->activity_id==12) $durasi_ko_meeting = $std_durasi;

                    //get durasi adjusted
                    $pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()
                            ->where(['=', 'activity_id', $row->activity_id])
                            ->andWhere(['=','pengadaan_id', $model->id])
                            ->one();
                    if (is_object($pengadaanDurasiAdjusted)) $std_durasi = $pengadaanDurasiAdjusted->durasi;
                    $total_durasi_plan+=$std_durasi;
                    if ($row->activity_id==5) $durasi_eva_tek = $std_durasi;
                    if ($row->activity_id==12) $durasi_ko_meeting = $std_durasi;

                    //get real date
                    $pengadaanActivity = PengadaanActivity::find()
                            ->where(['=', 'activity_id', $row->activity_id])
                            ->andWhere(['=','pengadaan_id',$model->id])
                            ->one();
                    if (is_object($pengadaanActivity)){
                        $dStart = new DateTime($pengadaanActivity->start_date);
                        $dEnd   = new DateTime($pengadaanActivity->end_date);
                        if ($pengadaanActivity->end_date == "0000-00-00") $dEnd = new DateTime(date('Y-m-d'));
                        $dDiff  = $dStart->diff($dEnd);

                        $real_mulai     = $pengadaanActivity->start_date;
                        $real_selesai   = $pengadaanActivity->end_date;
                        if ($real_selesai=="0000-00-00" || is_null($real_selesai)) $real_selesai = date('Y-m-d');
                        $real_durasi    = $dDiff->days+1;

                        //adjust real_durasi except for Task Force and Rush Order
                        //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                            $adder = HariLibur::countHoliday($real_mulai, $real_selesai);
                            $real_durasi    -= $adder;
                        //}
                        
                        $saving         = $std_durasi - $real_durasi;
                        $comply         = $saving >=0 ? "Comply" : "Not Comply";

                        //set durasi real evaluasi teknis
                        if (($model->metoda_id == 1 || $model->metoda_id == 2 || $model->metoda_id == 5 || $model->metoda_id == 6) && ($row->activity_id == 5 && $real_mulai != 0))
                        {
                            $real_eva_tek = $real_durasi;
                            $real_ko_meeting = $real_durasi;
                        }
                        //set current activity
                        // if ($std_durasi!=0 && $real_mulai!="0000-00-00" && $pengadaanActivity->end_date=="0000-00-00") 
                        //         $currentActivity = Activity::find()->where(['=', 'id', $row->activity_id])->one()->nama;
                    }

                    //set standard date
                    if  ($std_durasi > 0) {
                        if (!empty($model->start_plan)) {
                            $std_mulai[1] = $model->start_plan;
                        } else if (isset($real_mulai)) {
                            $std_mulai[1] = $real_mulai;
                        } else {
                            $std_mulai[1] = date('Y-m-d');
                        }
                        //$std_mulai[1] = isset($real_mulai) ? $real_mulai : date('Y-m-d');
                        if ($no > 1) $std_mulai[$no] = $std_selesai;
                        $date = new DateTime($std_mulai[$no]);
                        if ($no > 1) $date->add(new DateInterval('P1D'));
                        $std_mulai[$no] = $date->format('Y-m-d');

                        //adjust start date except for Task Force and Rush Order
                        //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                            while (HariLibur::checkHoliday($std_mulai[$no])) {
                                $date->add(new DateInterval('P1D'));                         
                                $std_mulai[$no] = $date->format('Y-m-d');
                            }
                        //}
                        
                        //set end date 
                        $durasi_adder = $std_durasi > 1 ? $std_durasi-1 : 0; 
                        $date->add(new DateInterval('P'.($durasi_adder).'D'));   
                        $std_selesai = $date->format('Y-m-d');

                        //adjust end date except for Task Force and Rush Order
                        //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                            $adder = HariLibur::countHoliday($std_mulai[$no], $std_selesai);
                            $date->add(new DateInterval('P'.($adder).'D'));                         
                            $std_selesai = $date->format('Y-m-d');

                            while (HariLibur::checkHoliday($std_selesai)) {
                                $date->add(new DateInterval('P1D'));                         
                                $std_selesai = $date->format('Y-m-d');
                            }
                        //}
                    }

                    //set status
                    if (!isset($real_selesai) || empty($real_selesai)) $real_selesai = date('Y-m-d');
                    if (is_object($pengadaanActivity)){
                        if ($real_selesai == $std_selesai) $eod = "On Schedule";
                        if ($real_selesai > $std_selesai) $eod = "Delay";
                        if ($real_selesai < $std_selesai) $eod = "Early";
                    }

                    //set sum variable
                    if (is_object($pengadaanActivity)){
                        if ($workdays_start == 0) $workdays_start = $pengadaanActivity->start_date;
                        if ($pengadaanActivity->end_date != "0000-00-00") $workdays_end = $pengadaanActivity->end_date;
                    }
                    $activity = Activity::find()->where(['=', 'id', $row->activity_id])->one();

                    //showing data
                    ?>
                    <tr>
                        <td><?=$no?></td>
                        <td><?=$activity->nama?></td>
                        <td><?=$std_durasi > 0 ? $std_mulai[$no] : ''?></td>
                        <td><?=$std_durasi > 0 ? $std_selesai : ''?></td>
                        <td><?=$std_durasi?></td>
                        <?php 
                            $disabled = "disabled";
                            $pengadaanActivity_id   = "";
                            $show_real_mulai        = "";
                            $show_real_selesai      = "";
                            if($std_durasi > 0){
                                if(is_object($pengadaanActivity) && $real_mulai!="0000-00-00"){ 
                                    $disabled = "";
                                    $pengadaanActivity_id = $pengadaanActivity->id;
                                    if ($pengadaanActivity_id != "") {
                                        $show_real_mulai     = $real_mulai;
                                        $show_real_selesai   = $real_selesai;
                                    }
                                    ?>
                                    <td><?=$real_mulai?></td>
                                    <!-- <td><?=$real_selesai?></td> -->
                                    <td><?=$pengadaanActivity->end_date?></td>
                                    <td><?=$real_durasi?></td>
                                    <td class="<?=str_replace(' ', '', $comply)?>">
                                        <?php 
                                            echo $pengadaanActivity->end_date=="0000-00-00" || is_null($pengadaanActivity->end_date) ? "Not Finish" : $comply;
                                            $lastComply = $pengadaanActivity->end_date=="0000-00-00" || is_null($pengadaanActivity->end_date) ? "Not Finish" : $comply;
                                        ?>
                                    </td>
                                    <td><?=$saving?></td>
                                    <td class="<?=str_replace(' ', '', $eod)?>"><?=$eod?></td>
                                <?php 
                                } else { 
                                    echo "<td colspan=2>N/A (BELUM dilaksanakan)</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    "; 
                                } ?>
                                    
                        <?php
                            } else { 
                                echo "<td colspan=2>N/A (TIDAK dilaksanakan)</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                "; 
                            }
                        ?>
                    </tr>
                    <?php
                } //end of activity loop

                $dStart = new DateTime($workdays_start);
                $dEnd   = new DateTime($workdays_end);
                $dDiff  = $dStart->diff($dEnd);
                $preDiff = $dDiff->days+1;
                //adjust
                $adder = HariLibur::countHoliday($workdays_start, $workdays_end);
                
                $workdays_standard      = $total_durasi_plan;
                $workdays_standard_real = $preDiff - $adder;
                $workdays_excluded      = $workdays_standard - $durasi_eva_tek - $durasi_ko_meeting;
                $workdays_excluded_real = $workdays_standard_real - $real_eva_tek - $real_ko_meeting;

                $saving             = $workdays_standard - $workdays_standard_real;
                $saving_excluded    = $workdays_excluded - $workdays_excluded_real;

                $comply             = $saving >= 0 ? "Comply" : "Not Comply";
                $comply_excluded    = $saving_excluded >= 0 ? "Comply" : "Not Comply";
            ?>


            <tr>
                <th colspan="4">Workdays</th>
                <th><?=$workdays_standard?></th> 
                <th colspan="2"></th>
                <th><?php // echo $workdays_standard_real?></th>
                <th class="<?=str_replace(' ', '', $comply)?>"><?=$comply?></th>
                <th></th>
                <th></th>
            </tr>
            <?php if ($model->metoda_id == 1 || $model->metoda_id == 2 || $model->metoda_id == 5 || $model->metoda_id == 6 ) { ?>
            <tr>
                <th colspan="4">Workdays (exclude Evaluasi Teknis & Kick Off Meeting)</th>
                <th><?=$workdays_excluded?></th> 
                <th colspan="2"></th>
                <th><?php // echo $workdays_excluded_real?></th>
                <th class="<?=str_replace(' ', '', $comply_excluded)?>"><?=$comply_excluded?></th>
                <th></th>
                <th></th>
            </tr>
            <?php } ?>
        </table>
        
        <?php
            if (is_object(PengadaanActivity::find()->where("
                (start_date != '0000-00-00' and start_date is not null) and 
                (end_date = '0000-00-00' or end_date is null)
                ")->orderBy('activity_id','DESC')->limit('1')->one())) {

                    $currentActivity = PengadaanActivity::find()->where("
                    (start_date != '0000-00-00' and start_date is not null) and 
                    (end_date = '0000-00-00' or end_date is null)
                    ")->orderBy('activity_id','DESC')->limit('1')->one()->activity;

                echo "<div class='alert alert-info text-center' style='padding:7px'><b>CURRENT ACTIVITY : ".strtoupper($currentActivity->nama)."</b></div>";
            }

            foreach ($model->pengadaanActivities as $row) {
               if (isset($row->pengadaanIssues[0]->id)) {
                    echo "<ul>";
                    echo "<li><b>".strtoupper($row->activity->nama)."</b><br>";
                    foreach ($row->pengadaanIssues as $row1) {
                        echo $row1->tanggal." - ".nl2br($row1->keterangan)."</li>";
                    }
                    echo "</ul>";
               }
            }
        ?>

    </div>
    </div>

</div>


<?php 
    if ($prev) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 37:
                    window.location="?r=pct/view&id="+'.$prev[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault();
        });
        ', \yii\web\VIEW::POS_READY
    ); 

    if ($next) 
    $this->registerJs('
        $(document).keydown(function(e) {
            switch(e.which) {
                case 39:
                    window.location="?r=pct/view&id="+'.$next[0]->id.';
                break;
                default: return; 
            }
            e.preventDefault(); 
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>