<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

use frontend\models\BarangJasa;
use frontend\models\Personel;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Procurement Cycle Time';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'opsi'],
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barang_jasa_id]=' . $data->barang_jasa_id.'&r=pengadaan');
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'barang_jasa_id', ArrayHelper::map(BarangJasa::find()->asArray()->where("id!='4'")->all(), 'id', 'nama'),['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'personel.nama', ArrayHelper::map(Personel::find()->asArray()->joinWith("personelProcurement")->where("jabatan = 'Purchaser'")->all(), 'nama', 'nama'),['class'=>'form-control','prompt' => '[ ALL ]']),
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            [
                'label' => '',
                'format' => 'raw',
                'contentOptions' => ['class'=>'opsi text-center'],
                'value' => function($data){
                    if($data->id){
                        return '
                            <div class="btn-group">
                                '.Html::a('<i class="glyphicon glyphicon-eye-open text-primary" title="view"></i>', '?r=pct/view&id='.$data->id, ['class'=>'btn btn-xs btn-default']).'
                                '.Html::a('<i class="glyphicon glyphicon-export text-warning" title="export to excel"></i>', '?r=pct/export-excel&id='.$data->id, ['class'=>'btn btn-xs btn-default']).'
                            </div>
                        ';
                    }
                },
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
