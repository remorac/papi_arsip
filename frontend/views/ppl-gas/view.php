<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplGas */

$this->title = date('F Y', strtotime($model->month));
$this->params['breadcrumbs'][] = ['label' => 'Ppl Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-gas-view box box-body box-info">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'month',
                'value' =>  date('F Y', strtotime($model->month)),
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => '<a target="_blank" href="'.Url::to(['/dokumen-upload/download-ppl', 'filename' => $model->file]).'">'.$model->file.'</a>',
            ],
            'locator',
        ],
    ]) ?>

</div>
