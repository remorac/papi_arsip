<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PplGas */

$this->title = 'Create Ppl Gas';
$this->params['breadcrumbs'][] = ['label' => 'Ppl Gas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-gas-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
