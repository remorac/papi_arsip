<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanKorinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan - KPP';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-korin-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengadaan - KPP', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pengadaan_id',
            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaan){
                        return Html::a($data->pengadaan->nama, '?r=pengadaan&PengadaanSearch[nama]=' . $data->pengadaan->nama);
                    }
                },
            ],
            //'korin_id',
            [
                'attribute' => 'korin.no_surat',
                'label' => 'Korin',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korin){
                        return Html::a($data->korin->no_surat, '?r=korin&KorinSearch[nama]=' . $data->korin->no_surat);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
