<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanKorin */

$this->title = 'Create Pengadaan - KPP';
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - KPP', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-korin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
