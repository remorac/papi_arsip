<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanKorin */

$this->title = 'Update Pengadaan - KPP: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - KPP', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-korin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
