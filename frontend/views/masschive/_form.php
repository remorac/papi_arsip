<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use frontend\models\KorinJenis;
use frontend\models\BarangJasa;
use frontend\models\PersonelProcurement;
use frontend\models\Metoda;
use frontend\models\LevelApproval;


/* @var $this yii\web\View */
/* @var $model frontend\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- <div class="masschive-form"> -->

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <div class="box box-success">
        <div class="box-header">
            <div class="box-title">KPP</div>
        </div>
        <div class="box-body">
            <div id="suratunique" class="col-md-8 col-md-offset-4"></div>    
            <?= $form->field($modelKorin, 'no_surat')->textInput(['maxlength' => 100, 'onchange' => '
                $.get("index.php?r=korin/suratunique&no_surat="+$(this).val(), function(data) {
                    $("#suratunique").html(data);
                });
            ']) ?>
            
            <?= $form->field($modelKorin, 'korin_jenis_id')->dropDownList(ArrayHelper::map(KorinJenis::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>
            
            <?= $form->field($modelKorin, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>

            <?= $form->field($modelKorin, 'tgl_terima')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ],
                ]); ?>

            <?= $form->field($modelKorin, 'tanggal_surat')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>

            <?= $form->field($modelKorin, 'pic_initial')->textInput() ?>

            <?= $form->field($modelKorin, 'yg_menandatangani')->textInput(['maxlength' => 200]) ?>

            <?php //echo $form->field($model, 'ringkasan_isi')->textarea(['rows' => 3]) ?>

            <?= $form->field($modelKorin, 'perihal')->textInput() ?>

            <?= $form->field($modelKorin, 'tembusan')->textInput(['maxlength' => 200]) ?>

            <?= $form->field($modelKorin, 'lampiran')->textInput(['maxlength' => 200]) ?>

            <?= $form->field($modelKorin, 'isi_disposisi')->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelKorin, 'tanggal_disposisi')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>

            <?php //echo $form->field($model, 'kebutuhan_disposisi')->textInput(['maxlength' => 200]) ?>

            <?= $form->field($modelKorin, 'requirement_date')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header">
            <div class="box-title">DISPOSISI SENIOR MANAGER</div>
        </div>
        <div class="box-body">
        <?= $form->field($modelAssignment, 'korin_id')->textInput(['maxlength' => 100, 'readonly' => 'readonly']) ?>

            <?= $form->field($modelAssignment, 'tanggal_assignment')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>

            <?= $form->field($modelAssignment, 'manager_personel_procurement_id')
                ->dropDownList(ArrayHelper::map(PersonelProcurement::find()
                        ->where(['=', 'jabatan', 'Manager'])
                        ->orWhere(['=', 'id', '4'])
                        ->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '-- select --']) ?>

            <?= $form->field($modelAssignment, 'keterangan')->textarea(['rows' => 2]) ?>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header">
            <div class="box-title">DISPOSISI MANAGER</div>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'nama')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>

            <?= $form->field($model, 'metoda_id')->dropDownList(ArrayHelper::map(Metoda::find()->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>

            <?= $form->field($model, 'purchaser_personel_procurement_id')->dropDownList(ArrayHelper::map(PersonelProcurement::find()->where("jabatan='Purchaser'")->all(), 'id', 'DropdownDisplayItem'), ['prompt' => '- select -']) ?>
            
            <?= $form->field($model, 'level_approval_id')->dropDownList(ArrayHelper::map(LevelApproval::find()->orderBy("id desc")->all(), 'id', 'approver')) ?>

            <?php // echo $form->field($model, 'nilai_oe')->textInput() ?>
            <!-- 
            <?= $form->field($model, 'start_plan')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>
            -->
            <?= $form->field($model, 'requirement_date')
                ->widget(DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]); ?>
                
        <!-- 
            <?= $form->field($model, 'delivery_time')->textInput() ?>

            <?= $form->field($model, 'po_reference')->textInput() ?>
        -->

            <?= $form->field($model, 'isi_disposisi')->textarea(['maxlength' => 200, 'rows' => 1, 'value' => 'Untuk diproses.']) ?>

            <?= $form->field($model, 'subgroup')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Search Subgroup ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['sap-subgroup/list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                    'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                ],
            ]);?>
        </div>
    </div>

    

    <div class="box box-success">
        <div class="box-header">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="form-group">
        
    </div>

    <?php ActiveForm::end(); ?>

<!-- </div> -->

<?php //setUmum
    $this->registerJs('
        $("#korin-korin_jenis_id").change(function() {
            if ($("#korin-korin_jenis_id").val() == "4") {
                $("#korin-barang_jasa_id").val("4");
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>