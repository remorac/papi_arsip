<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Activity */

$this->title = Yii::t('app', 'Create Pengadaan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengadaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelKorin' => $modelKorin,
        'modelAssignment' => $modelAssignment,
    ]) ?>

</div>
