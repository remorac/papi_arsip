<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use frontend\models\Pengadaan;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanPo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-po-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'pengadaan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Pengadaan::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <?= $form->field($model, 'purchasing_document')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
