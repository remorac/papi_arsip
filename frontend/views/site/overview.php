<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use frontend\models\Pengadaan;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\HariLibur;

use miloschuman\highcharts\Highcharts;


	/* @var $this yii\web\View */
	$this->title = "Procurement Activities & Progress Informations";

	$c_brg = Pengadaan::find()
		->joinWith(['purchaserPersonelProcurement.personel'])
        ->joinWith(['metoda'])
        ->joinWith(['barangJasa'])
        ->joinWith(['pengadaanPos'])
        ->joinWith(['pengadaanActivities'])
        ->joinWith(['pengadaanDurasiAdjusteds'])
		->where("
			(barang_jasa_id = 1 or barang_jasa_id = 3) and
            pengadaan_durasi_adjusted.pengadaan_id is not null and
            pengadaan_durasi_adjusted.durasi != 0 and
            pengadaan_durasi_adjusted.pengadaan_id = pengadaan_activity.pengadaan_id and
            (pengadaan_activity.pengadaan_id not in
                (select pengadaan_id from pengadaan_activity where (end_date ='0000-00-00' or end_date is null))
            )
            and
            (select count(*) from pengadaan_durasi_adjusted where durasi !=0 and pengadaan_id = pengadaan.id) =
            (select count(*) from pengadaan_activity where end_date is not null and pengadaan_id = pengadaan.id)
        ")
		->groupBy("pengadaan.id")
		->count();
	$c_jsa = Pengadaan::find()
		->joinWith(['purchaserPersonelProcurement.personel'])
        ->joinWith(['metoda'])
        ->joinWith(['barangJasa'])
        ->joinWith(['pengadaanPos'])
        ->joinWith(['pengadaanActivities'])
        ->joinWith(['pengadaanDurasiAdjusteds'])
		->where("
			(barang_jasa_id = 2 or barang_jasa_id = 4) and
            pengadaan_durasi_adjusted.pengadaan_id is not null and
            pengadaan_durasi_adjusted.durasi != 0 and
            pengadaan_durasi_adjusted.pengadaan_id = pengadaan_activity.pengadaan_id and
            (pengadaan_activity.pengadaan_id not in
                (select pengadaan_id from pengadaan_activity where (end_date ='0000-00-00' or end_date is null))
            )
            and
            (select count(*) from pengadaan_durasi_adjusted where durasi !=0 and pengadaan_id = pengadaan.id) =
            (select count(*) from pengadaan_activity where end_date is not null and pengadaan_id = pengadaan.id)
        ")
		->groupBy("pengadaan.id")
		->count();

	$i = 0;
	$brg = "";
	$jsa = "";
	$arr_name = "";

	$cb_ip = 0;
	$cj_ip = 0;

	$activities = \frontend\models\Activity::find()->all();
	foreach ($activities as $activity) {

		if ($i >= 1 && $i <= 10) {

			$arr_name.= "'".$activity->shortname."', ";

			$brg.= "{";
			$brg.= "y : ".PengadaanActivity::find()->joinWith("pengadaan")
						 		->where("
							 		activity_id = '".$activity->id."'
							 		and start_date is not null and start_date != '0000-00-00'
							 		and (end_date is null or end_date = '0000-00-00')
							 		and barang_jasa_id = '1'
							 	")->groupBy("pengadaan_id")->count().", ";
			$brg.= "url : '?r=highlight/list&barang_jasa_id=1&activity_id=".$activity->id."', ";
			$brg.= "}, ";

			$jsa.= "{";
			$jsa.= "y : ".PengadaanActivity::find()->joinWith("pengadaan")
						 		->where("
							 		activity_id = '".$activity->id."'
							 		and start_date is not null and start_date != '0000-00-00'
							 		and (end_date is null or end_date = '0000-00-00')
							 		and barang_jasa_id = '2'
							 	")->groupBy("pengadaan_id")->count().", ";
			$jsa.= "url : '?r=highlight/list&barang_jasa_id=2&activity_id=".$activity->id."', ";
			$jsa.= "}, ";

			$cb_ip+= PengadaanActivity::find()->joinWith("pengadaan")
						 		->where("
							 		activity_id = '".$activity->id."'
							 		and start_date is not null and start_date != '0000-00-00'
							 		and (end_date is null or end_date = '0000-00-00')
							 		and barang_jasa_id = '1'
							 	")->groupBy("pengadaan_id")->count();
			$cj_ip+= PengadaanActivity::find()->joinWith("pengadaan")
						 		->where("
							 		activity_id = '".$activity->id."'
							 		and start_date is not null and start_date != '0000-00-00'
							 		and (end_date is null or end_date = '0000-00-00')
							 		and barang_jasa_id = '2'
							 	")->groupBy("pengadaan_id")->count();

		}

		$i++;
	}

?>

<div class="site-index">

	<!-- <div class="row">
		<div class="col-md-6">
			<p class="lead">
		    	Procurement Activities &amp; Progress Informations
		    </p>
		</div>
		<div class="col-md-6">
			<p style="float:right">
				<?= Html::a('<i class="fa fa-calendar"></i>&nbsp; Daily Report &raquo;', ['daily/daily-report/highlight'], ['class' => 'btn btn-warning']) ?>
	    	<?= Html::a('<i class="fa fa-book"></i>&nbsp; Arsip &raquo;', ['semua-pengadaan/index'], ['class' => 'btn btn-warning']) ?>
	    	<?= Html::a('<i class="fa fa-fire-extinguisher"></i>&nbsp; Gas &raquo;', ['gas/gas-report/index'], ['class' => 'btn btn-warning']) ?>
	    </p>
		</div>
	</div> -->

	<?php if (Yii::$app->user->id == "13") { ?>
	<!-- <div class="col-md-4 paper" style="height: 400px; margin-bottom:20px; padding:0 !important; background: url('img/ifz1.jpeg'); background-size: auto 400px !important; text-shadow: 0 0 2px white"></div> -->
	<div class="col-md-3 paper" style="height: 400px; margin-bottom:20px; padding:0 !important; background: url('img/ifz1.jpeg'); background-size: auto 400px !important; text-shadow: 0 0 2px white"></div>
	<div class="col-md-2 paper" style="height: 400px; margin-bottom:20px; padding:0 !important; background: url('img/ifz2.jpeg'); background-size: auto 400px !important; text-shadow: 0 0 2px white"></div>
	<div class="col-md-7 paper" style="height: 400px; margin-bottom:20px; padding:0 !important; background: url('img/ifz4.jpeg'); background-size: auto 400px !important; text-shadow: 0 0 2px white"></div>
	<!-- <div class="col-md-2 paper" style="height: 400px; margin-bottom:20px; padding:0 !important; background: url('img/ifz1.jpeg'); background-size: auto 400px !important; text-shadow: 0 0 2px white"></div> -->
		<!-- <img src="img/buaya.png" width="100%" height="100%"> -->
		<!-- <div class="row">
			<div class="col-sm-12 text-danger"><span class="pull-right" style="font-size:20px; font-weight:300">buayo albino &nbsp; </span></div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12 text-info"><b class="pull-right">dipersembahkan oleh: &nbsp; </b></div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-danger"><b class="pull-right"><big><big>ICH</big></big> &nbsp;</b></div>
		</div> -->
	<!-- </div> -->
	<?php } ?>

	<div class="row">
	<div class="col-md-12" style="display:flex">
    <div id="" class="paper col-md-9 col-sm-12" style="padding-top:0px;padding-bottom: 10px;">
    	<div class="col-xs-12">
    		<br>
    		<h5>
    			<span class="text-muted"><b><?=$cb_ip?></b> BARANG, <b><?=$cj_ip?></b> JASA.</span>
    			<big><span class="pull-right"><b><?=$cb_ip+$cj_ip?></b>&nbsp; In Progress</span></big>
    		</h5>
    	</div>
    	<div id="chart" class="col-xs-12">
    		<?= Highcharts::widget(); ?>
    	</div>
    </div>

    <div id="" class="paper col-md-3 col-sm-12" style="padding-top:0px;padding-bottom: 10px;">

    	<div class="col-xs-12"><h3><b><?=$c_brg+$c_jsa?></b>&nbsp; Completed</h3></div>

    	<div class="col-xs-12">
          <div class="small-box bg-aqua paper">
            <div class="inner">
              <h3 style="color:white; font-weight:100"><?=$c_brg?><sup style="font-size: 20px"></sup></h3>
              <p>Pengadaan Barang</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <p class="small-box-footer"> </p>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

    	<div class="col-xs-12">
          <div class="small-box bg-aqua paper">
            <div class="inner">
              <h3 style="color:white; font-weight:100"><?=$c_jsa?><sup style="font-size: 20px"></sup></h3>
              <p>Pengadaan Jasa</p>
            </div>
            <div class="icon">
              <i class="fa fa-male"></i>
            </div>
            <p class="small-box-footer"> </p>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>

        <div class="col-xs-12">
        	<a href="<?=Url::to(['semua-pengadaan/index'])?>" class="small-box-footer pull-right">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>

    </div>
    </div>
    </div>

</div>

<?php
	$this->registerJs("
		$(function() {
		  	$('#chart').highcharts({
		    	chart: {
		      		type: 'column'
		    	},
		    	title: false,
		    	xAxis: {
	         		'categories': [".$arr_name."]
		    	},
	      		yAxis: {
	         		title: { 'text': 'Jumlah Pengadaan' }
	      		},
			    plotOptions: {
			      	series: {
		        		cursor: 'pointer',
		        		point: {
		          			events: {
		            			click: function() {
		              				window.open(this.options.url);
		            			}
		          			}
		        		}
		      		},
		      		column: {
		                dataLabels: {
		                    enabled: true
		                },
		            }
		    	},

		    	series: [
		    		{
		    			name: 'Barang',
		    			color: '#3EA99F',
		      			data: [".$brg."],
		    		},
		    		{
		    			name: 'Jasa',
		    			color: '#E4287C',
		      			data: [".$jsa."],
		    		}
		    	]
		  });
	});
	", \yii\web\VIEW::POS_READY);
?>
