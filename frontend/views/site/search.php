<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Pengadaan;
use frontend\models\PengadaanPo;
use frontend\models\Afis;
use frontend\models\Legal;
use frontend\models\Warehouse;
use frontend\models\Logistic;

$this->title = 'Quick Search Result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search">
    
    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
    <div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <?= Html::activeTextInput($post, 'keyword', ['class' => 'form-control', 'placeholder' => 'quick search...']) ?>
            <span class="input-group-btn">
                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>
            </span>
        </div>
        <p></p>
        <div class="col-md-12 text-muted font-weight-force-normal">
            <?= Html::activeCheckbox($post, 'flag_description', ['label' => 'Description']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_purchase_order', ['label' => 'Purchase Order']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_afis', ['label' => 'Payment']) ?> &nbsp; 
            
            <?= Html::activeCheckbox($post, 'flag_legal', ['label' => 'Contract']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_warehouse', ['label' => 'GR/IR']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_logistic', ['label' => 'Shipment']) ?> &nbsp; 
            <!-- <?= Html::activeCheckbox($post, 'flag_material', ['label' => 'Material']) ?> &nbsp;  -->
            
            <?= Html::activeCheckbox($post, 'flag_hrga', ['label' => 'HRGA']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_utility', ['label' => 'Utility']) ?> &nbsp; 
            <?= Html::activeCheckbox($post, 'flag_technical_adm', ['label' => 'Technical Adm']) ?> &nbsp; 
            
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

    <br>

    <?php

        if ($post->flag_description) {
            $models = Pengadaan::find()->where(['like', 'nama', $post->keyword])->all();
            foreach ($models as $model) {
                ?>
                <div class="paper box-body">
                    <span class="bdg pull-right small">Nama Pengadaan</span>
                    <a href="<?= Url::to(['semua-pengadaan/view', 'id' => $model->id]) ?>">
                        <big class="text-primary"><?= $model->nama ?></big>
                    </a>
                    <div class="small"><?= strtoupper($model->barangJasa->nama) ?> - <?= strtoupper($model->metoda->nama) ?></div>
                </div>
                <?php
            }
        }

        if ($post->flag_purchase_order) {
        	$models = PengadaanPo::find()->where(['like', 'purchasing_document', $post->keyword])->all();
        	foreach ($models as $model) {
        		?>
        		<div class="paper box-body">
        			<span class="bdg pull-right small">Nomor PO</span>
                    <a href="<?= Url::to(['semua-pengadaan/view', 'id' => $model->pengadaan_id]) ?>">
            			<big class="text-primary"><?= $model->purchasing_document ?></big>
                    </a>
        			<div class="small"><?= $model->pengadaan->nama ?></div>
        		</div>
        		<?php
            }
    	}
        

        if ($post->flag_afis) {
            $models = Afis::find()
                ->andFilterWhere([
                    'or',
                    ['like', 'clearing_document', $post->keyword],
                    ['like', 'filename', $post->keyword],
                ])->all();
            foreach ($models as $model) {
                ?>
                <div class="paper box-body">
                    <span class="bdg pull-right small">Clearing Document</span>
                    <a href="<?= Url::to(['afis/view', 'id' => $model->id]) ?>">
                        <big class="text-primary"><?= $model->clearing_document ?></big>
                    </a>
                    <div class="small"><?= $model->filename ?></div>
                </div>
                <?php
            }
        }
        

        if ($post->flag_legal) {
            $models = Legal::find()->where(['like', 'filename', $post->keyword])->all();
            foreach ($models as $model) {
                ?>
                <div class="paper box-body">
                    <span class="bdg pull-right small">Contract</span>
                    <a href="<?= Url::to(['legal/view', 'id' => $model->id]) ?>">
                        <big class="text-primary"><?= $model->filename ?></big>
                    </a>
                    <div class="small"><?= $model->filename ?></div>
                </div>
                <?php
            }
        }
        

        if ($post->flag_warehouse) {
            $models = Warehouse::find()->where(['like', 'filename', $post->keyword])->all();
            foreach ($models as $model) {
                ?>
                <div class="paper box-body">
                    <span class="bdg pull-right small">Goods Receipt</span>
                    <a href="<?= Url::to(['warehouse/view', 'id' => $model->id]) ?>">
                        <big class="text-primary"><?= $model->filename ?></big>
                    </a>
                    <div class="small"><?= $model->filename ?></div>
                </div>
                <?php
            }
        }
        

        if ($post->flag_logistic) {
        	$models = Logistic::find()->where(['like', 'filename', $post->keyword])->all();
        	foreach ($models as $model) {
        		?>
        		<div class="paper box-body">
                    <span class="bdg pull-right small">Shipment</span>
                    <a href="<?= Url::to(['warehouse/view', 'id' => $model->id]) ?>">
            			<big class="text-primary"><?= $model->filename ?></big>
                    </a>
        			<div class="small"><?= $model->filename ?></div>
        		</div>
        		<?php
            }
    	}
    ?>
</div>
