<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Arsip Proyek Indarung VI';
Yii::$app->params['showTitle'] = false;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>arsip</h1>

        <p class="lead">Proyek Indarung VI</p>

        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
        <div class="col-md-6 col-md-offset-3">
            <div class="input-group">
                <?= Html::activeTextInput($model, 'keyword', ['class' => 'form-control input-lg', 'placeholder' => 'quick search...']) ?>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-lg btn-success"><i class="glyphicon glyphicon-search"></i></button>
                </span>
	        </div>
            <p></p>
            <div class="col-md-12 text-muted font-weight-force-normal">
                <?= Html::activeCheckbox($model, 'flag_description', ['label' => 'Description']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_purchase_order', ['label' => 'Purchase Order']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_afis', ['label' => 'Payment']) ?> &nbsp; 

                <?= Html::activeCheckbox($model, 'flag_legal', ['label' => 'Contract']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_warehouse', ['label' => 'GR/IR']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_logistic', ['label' => 'Shipment']) ?> &nbsp; 
                <!-- <?= Html::activeCheckbox($model, 'flag_material', ['label' => 'Material']) ?> &nbsp;  -->
                <br>
                <?= Html::activeCheckbox($model, 'flag_hrga', ['label' => 'HRGA']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_utility', ['label' => 'Utility']) ?> &nbsp; 
                <?= Html::activeCheckbox($model, 'flag_technical_adm', ['label' => 'Technical Adm']) ?> &nbsp; 
                
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
