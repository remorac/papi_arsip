<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Logistic */

$this->title = 'Create Logistic';
$this->params['breadcrumbs'][] = ['label' => 'Logistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logistic-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
