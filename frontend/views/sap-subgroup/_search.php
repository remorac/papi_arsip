<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapSubgroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sap-subgroup-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Material Master') ?>

    <?= $form->field($model, 'Description') ?>

    <?= $form->field($model, 'Long Text') ?>

    <?= $form->field($model, 'Material Group SGG') ?>

    <?= $form->field($model, 'Material Group Desc SGG') ?>

    <?php // echo $form->field($model, 'Sub Material Group SGG') ?>

    <?php // echo $form->field($model, 'Sub Material Group Desc SGG') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
