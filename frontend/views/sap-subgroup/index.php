<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SapSubgroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sap Subgroups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-subgroup-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sap Subgroup', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Material Master',
            'Description',
            'Long Text:ntext',
            'Material Group SGG',
            'Material Group Desc SGG',
            // 'Sub Material Group SGG',
            // 'Sub Material Group Desc SGG',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
