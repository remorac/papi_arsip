<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\SapSubgroup */

$this->title = 'Create Sap Subgroup';
$this->params['breadcrumbs'][] = ['label' => 'Sap Subgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-subgroup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
