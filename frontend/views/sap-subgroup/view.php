<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapSubgroup */

$this->title = $model->{"Material Master"};
$this->params['breadcrumbs'][] = ['label' => 'Sap Subgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sap-subgroup-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->{"Material Master"}], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->{"Material Master"}], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Material Master',
            'Description',
            'Long Text:ntext',
            'Material Group SGG',
            'Material Group Desc SGG',
            'Sub Material Group SGG',
            'Sub Material Group Desc SGG',
        ],
    ]) ?>

</div>
