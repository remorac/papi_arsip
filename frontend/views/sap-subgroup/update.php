<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapSubgroup */

$this->title = 'Update Sap Subgroup: ' . ' ' . $model->{"Material Master"};
$this->params['breadcrumbs'][] = ['label' => 'Sap Subgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->{"Material Master"}, 'url' => ['view', 'id' => $model->{"Material Master"}]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sap-subgroup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
