<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SapSubgroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sap-subgroup-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Material Master')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Long Text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Material Group SGG')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Material Group Desc SGG')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Sub Material Group SGG')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Sub Material Group Desc SGG')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
