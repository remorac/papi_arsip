<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ActivityGroup */

$this->title = Yii::t('app', 'Create Activity Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activity Group'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
