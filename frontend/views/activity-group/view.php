<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ActivityGroup */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activity Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            'nama',
            'created_at',
            'updated_at',
            'unit_terkait',
            [
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->createdBy ? $model->createdBy->username : '', 
                    $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    )
            ],
            [
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    $model->updatedBy ? $model->updatedBy->username : '', 
                    $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    )
            ],

        ],
    ]) ?>

</div>
