<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ActivityGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Activity Group');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Activity Group'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions'=> ['style' => 'width:4em'],
            ],
            //'id',
            [
                'attribute' => 'id',
                'headerOptions'=> ['style' => 'width:4em'],
            ],
            'nama',
            'unit_terkait',
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=> ['style' => 'width:5em'],
            ],
        ],
    ]); ?>

</div>
