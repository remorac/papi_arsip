<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AuditFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Audit Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-file-index box box-body box-primary">

    <!-- <p>
        <?= Html::a('Create Audit File', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'filename:ntext',
            'audit_folder_id',
            'created_at',
            'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
