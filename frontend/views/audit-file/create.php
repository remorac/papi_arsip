<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\AuditFile */

$this->title = 'Create Audit File';
$this->params['breadcrumbs'][] = ['label' => 'Audit Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
