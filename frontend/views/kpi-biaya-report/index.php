<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KpiBiayaReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kpi Biaya Report');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-biaya-report-index box box-body box-primary">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => [
                    'style' => 'text-align:center;width:4em'
                ],
                'contentOptions' => [
                    'style' => 'text-align:right'
                ],
            ],

            [
                'attribute' => 'tahun_bulan',
                'headerOptions' => [
                    'style' => 'width:7em'
                ],
                'contentOptions' => [
                    'style' => 'text-align:center'
                ],
            ],
            [
                'attribute' => 'teksJenis',
                'label' => 'Jenis',
            ],
            [
                'attribute' => 'realisasi',
                'format' => 'decimal',
                'contentOptions' => [
                    'style' => 'text-align:right'
                ],
            ],
            [
                'attribute' => 'nilai_oe',
                'format' => 'decimal',
                'label' => 'Nilai OE',
                'contentOptions' => [
                    'style' => 'text-align:right'
                ],
            ],
            [
                'attribute' => 'index',
                'format' => 'decimal',
                'headerOptions' => [
                    'style' => 'width:7em'
                ],
                'contentOptions' => [
                    'style' => 'text-align:right'
                ],
            ],
            [
                'attribute' => 'teksStatus',
                'label' => 'Status',
                'format' => 'raw',
                'value' => function($data){
                    if($data->teksStatus){
                        if($data->teksStatus == 'OK'){
                            return '<i class="glyphicon glyphicon-thumbs-up"> OK</i>';
                        }
                        return '<i class="glyphicon glyphicon-thumbs-down"> Bad</i>';
                    }
                },
                'headerOptions' => [
                    'style' => 'width:7em'
                ],
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
