<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KpiBiayaReport */

$this->title = $model->tahun_bulan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Biaya Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-biaya-report-view box box-body box-info">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'tahun_bulan' => $model->tahun_bulan, 'jenis' => $model->jenis], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'tahun_bulan' => $model->tahun_bulan, 'jenis' => $model->jenis], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tahun_bulan',
            'jenis',
            'realisasi',
            'nilai_oe',
            'index',
            'status',
        ],
    ]) ?>

</div>
