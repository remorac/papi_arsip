<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\KpiDurasiKuartalReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-durasi-kuartal-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tahun_kuartal') ?>

    <?= $form->field($model, 'jenis') ?>

    <?= $form->field($model, 'durasi_plan') ?>

    <?= $form->field($model, 'durasi_real') ?>

    <?= $form->field($model, 'index') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
