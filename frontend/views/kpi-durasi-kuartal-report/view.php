<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KpiDurasiKuartalReport */

$this->title = $model->tahun_kuartal;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Durasi Kuartal Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-durasi-kuartal-report-view box box-body box-info">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'tahun_kuartal' => $model->tahun_kuartal, 'jenis' => $model->jenis], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'tahun_kuartal' => $model->tahun_kuartal, 'jenis' => $model->jenis], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tahun_kuartal',
            'jenis',
            'durasi_plan',
            'durasi_real',
            'index',
            'status',
        ],
    ]) ?>

</div>
