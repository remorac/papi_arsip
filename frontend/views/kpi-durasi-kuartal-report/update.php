<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\KpiDurasiKuartalReport */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Kpi Durasi Kuartal Report',
]) . ' ' . $model->tahun_kuartal;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Durasi Kuartal Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tahun_kuartal, 'url' => ['view', 'tahun_kuartal' => $model->tahun_kuartal, 'jenis' => $model->jenis]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kpi-durasi-kuartal-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
