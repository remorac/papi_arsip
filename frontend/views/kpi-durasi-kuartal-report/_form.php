<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\KpiDurasiKuartalReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-durasi-kuartal-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun_kuartal')->textInput(['maxlength' => 7]) ?>

    <?= $form->field($model, 'jenis')->textInput() ?>

    <?= $form->field($model, 'durasi_plan')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'durasi_real')->textInput(['maxlength' => 34]) ?>

    <?= $form->field($model, 'index')->textInput(['maxlength' => 38]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
