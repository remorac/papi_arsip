<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KpiDurasiKuartalReport */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Kpi Durasi Kuartal Report',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Durasi Kuartal Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-durasi-kuartal-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
