<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\LevelApproval */

$this->title = 'Create Level Approval';
$this->params['breadcrumbs'][] = ['label' => 'Level Approval', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-approval-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
