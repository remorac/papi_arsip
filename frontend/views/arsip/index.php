<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Arsip';
$this->params['breadcrumbs'][] = $this->title;

?>

<h1>Arsip</h1>

<p>
    Pencarian arsip dapat dilakukan berdasarkan nomor PO atau PR.
</p>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'arsip-form']); ?>
            <?php // echo $form->field($model, 'keyword') ?>
            <div class="form-group">
	            <input type="text" class="form-control" name="keyword" placeholder="search...">
	        </div>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'arsip-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>