<?php

use yii\helpers\Html;
use frontend\models\DevArsipAfterPO;


/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */

$this->title = 'Search Result';
$this->params['breadcrumbs'][] = ['label' => 'Arsip', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-result">

    <h1>Arsip</h1>

	<!-- <p>
		Hasil pencarian : 
	</p>
	
	<?php 
		$pengadaan 	= array('Pengadaan A', 'Pengadaan B');
		$doc_group	= array(
				[
					'Unit User',
					'Procurement',
					'Legal',
					'Logistic',
					'Warehouse',
					'Project Control & Risk Management',
					'Accounting & Finance',
					'Safety, Health, Environment, & Security',
				],
				[
					'Unit User',
					'Procurement',
					'Legal',
					'Logistic',
					'Warehouse',
					'Project Control & Risk Management',
					'Accounting & Finance',
					'Safety, Health, Environment, & Security',
				],
			);
		$activity	= array(
				[
					'KPP',
					'Dokumen Setelah PO',
				],
				[
					'RFQ',
					'Aanwijzing',
					'Pemasukan Penawaran',
					'Evaluasi Teknis',
					'Izin Pembukaan Penawaran Harga',
					'Pembukaan Penawaran Harga',
					'Evaluasi Harga',
					'Proposal PO',
					'Penandatanganan PO',
					'Kick Off Meeting',
				],
				[
					'Contract Drafting',
					'Contract Monitoring',
					'Contract Closing',
				],
				'',
				[
					'Pembuatan PR Stock',
					'Penerimaan Barang',
					'Penyimpanan Barang',
					'Pengeluaran Barang',
					'Mutasi Barang',
					'PO Outstanding',
				],
				'',
				[
					'Terima Dokumen',
					'Verifikasi Tagihan',
					'Verifikasi Faktur Pajak',
					'Posting SAP',
					'Pembayaran',
				],
				'',
			);
	?>
	
	<div class="panel-group" id="accordion">
	<?php for ($i=0;$i<2;$i++) { ?>
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>">
	          <?=$pengadaan[$i]?>
	        </a>
	      </h4>
	    </div> 
	    <div id="collapse<?=$i?>" class="panel-collapse collapse">
	      <div class="panel-body">
	
	      	<div class="panel-group" id="accordion<?=$i?>">
	      	<?php for ($j=0;$j<8;$j++) { ?>
			  
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?=$i?>" href="#collapse<?=$i.$j?>">
			          <?=$doc_group[$i][$j]?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapse<?=$i.$j?>" class="panel-collapse collapse">
			      <div class="panel-body">
	
					<?php if (!empty($activity[$j])) { ?>
					<div class="panel-group" id="accordion<?=$i.$j?>">
			     	<?php $k = 1; for ($k=0;$k<count($activity[$j]);$k++) { ?>
			  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?=$i.$j?>" href="#collapse<?=$i.$j.$k?>">
					          <?php echo $activity[$j][$k]?>
					        </a>
					      </h4>
					    </div>
					    <div id="collapse<?=$i.$j.$k?>" class="panel-collapse collapse">
					      <div class="panel-body">
					      	 <ul class="list-group">
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
					      	 </ul>
					      </div>
					    </div>
					  </div>
	
					<?php } ?>
					</div>
					<?php } else {
						?>
							<ul class="list-group">
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
								<li class="list-group-item" style="padding: 5px 10px;">dokumen.pdf</li>
					      	 </ul>
						<?php 
					} ?>
	
			      </div>
			    </div>
			  </div>
	      	<?php } ?>
			</div>
	
	      </div>
	    </div>
	  </div>
	
	<?php } ?>
	</div> -->

	<!-- real -->
	<p>
		Hasil pencarian : 
	</p>

	<?php 
		$pengadaan 	= array('Pengadaan A', 'Pengadaan B');
		$doc_group	= array(
				[
					'Unit User',
					'Procurement',
					'Legal',
					'Logistic',
					'Warehouse',
					'Project Control & Risk Management',
					'Accounting & Finance',
					'Safety, Health, Environment, & Security',
				],
				[
					'Unit User',
					'Procurement',
					'Legal',
					'Logistic',
					'Warehouse',
					'Project Control & Risk Management',
					'Accounting & Finance',
					'Safety, Health, Environment, & Security',
				],
			);
		$activity	= array(
				[
					'KPP',
					'Dokumen Setelah PO',
				],
				[
					'RFQ',
					'Aanwijzing',
					'Pemasukan Penawaran',
					'Evaluasi Teknis',
					'Izin Pembukaan Penawaran Harga',
					'Pembukaan Penawaran Harga',
					'Evaluasi Harga',
					'Negosiasi Harga',
					'Proposal PO',
					'Penandatanganan PO',
					'Kick Off Meeting',
				],
				[
					'Contract Drafting',
					'Contract Monitoring',
					'Contract Closing',
				],
				'',
				[
					'Pembuatan PR Stock',
					'Penerimaan Barang',
					'Penyimpanan Barang',
					'Pengeluaran Barang',
					'Mutasi Barang',
					'PO Outstanding',
				],
				'',
				[
					'Terima Dokumen',
					'Verifikasi Tagihan',
					'Verifikasi Faktur Pajak',
					'Posting SAP',
					'Pembayaran',
				],
				'',
			);
	?>

	<div class="panel-group" id="accordion">
	<?php for ($i=0;$i<count($model_pengadaan);$i++) { ?>
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>">
	          Pengadaan : <?=$model_pengadaan[$i]->nama?>
	        </a>
	      </h4>
	    </div> 
	    <div id="collapse<?=$i?>" class="panel-collapse collapse">
	      <div class="panel-body">

	      	<div class="panel-group" id="accordion<?=$i?>">
	      	<?php for ($j=0;$j<8;$j++) { ?>
			  
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?=$i?>" href="#collapse<?=$i.$j?>">
			          <?=$doc_group[$i][$j]?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapse<?=$i.$j?>" class="panel-collapse collapse">
			      <div class="panel-body">

					<?php if (!empty($activity[$j])) { ?>
					<div class="panel-group" id="accordion<?=$i.$j?>">
			     	<?php $k = 1; for ($k=0;$k<count($activity[$j]);$k++) { ?>
			  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?=$i.$j?>" href="#collapse<?=$i.$j.$k?>">
					          <?php echo $activity[$j][$k]?>
					        </a>
					      </h4>
					    </div>
					    <div id="collapse<?=$i.$j.$k?>" class="panel-collapse collapse">
					      <div class="panel-body">
						      <ul class="list-group">
					      		<?php 
					      			if ($activity[$j][$k] == "KPP") {
					      				foreach ($model_pengadaan[$i]->pengadaanKorins as $pengadaanKorins) {
					      					foreach ($pengadaanKorins->korin->korinDokumens as $korinDokumens) {
					      					 	echo "<li class='list-group-item' style='padding: 5px 10px;'>
					      					 		<a target='_blank' href='document/".$korinDokumens->dokumenUpload->pdf_filename."'>"
					      					 		.$korinDokumens->dokumenUpload->pdf_filename."</a></li>";
					      					 }
					      				}
					      			}

					      			for ($l=0; $l < count($activity[1]); $l++) { 
					      				if ($activity[$j][$k] == $activity[1][$l]) {
						      				foreach ($model_pengadaan[$i]->pengadaanActivities as $pengadaanActivities) {
						      					foreach ($pengadaanActivities->pengadaanActivityDokumens as $pengadaanActivityDokumens) {
						      					 	if ($pengadaanActivities->activity_id == $l+2)
						      					 		echo "<li class='list-group-item' style='padding: 5px 10px;'>
						      					 		<a target='_blank' href='document/".$korinDokumens->dokumenUpload->pdf_filename."'>"
						      					 		.$pengadaanActivityDokumens->dokumenUpload->pdf_filename."</a></li>";
						      					 }
						      				}
						      			}
					      			}

					      			/*for ($l=2; $l <=4 ; $l++) { 
					      				for ($m=0; $m < count($activity[$l]); $m++) { 
						      				if ($activity[$j][$k] == $activity[$l][$m]) {*/
						      					$devArsipAfterPO = DevArsipAfterPO::find()->where("jenis_dokumen = '".$activity[$j][$k]."'")->all();

							      				foreach ($devArsipAfterPO as $arsipAfterPO) {
							      					echo "<li class='list-group-item' style='padding: 5px 10px;'>
							      					 		<a target='_blank' href='document/".$arsipAfterPO->pdf_filename."'>"
							      					 		.$arsipAfterPO->pdf_filename."</a></li>";
							      				}
							      			/*}
						      			}
					      			}*/
					      		?>
					      	 </ul>
					      </div>
					    </div>
					  </div>

					<?php } ?>
					</div>
					<?php } ?>

			      </div>
			    </div>
			  </div>
	      	<?php } ?>
			</div>

	      </div>
	    </div>
	  </div>

	<?php } ?>
	</div>
	<!-- end real -->


</div>
