<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DevArsipAfterPo */

$this->title = 'Create Dev Arsip After Po';
$this->params['breadcrumbs'][] = ['label' => 'Dev Arsip After Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-arsip-after-po-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
