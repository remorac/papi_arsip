<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\DevArsipAfterPo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dev-arsip-after-po-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "<div class='col-md-2 text-right'>{label}</div>
                            \n<div class='col-md-10'>{input}</div>
                            \n<div class='col-md-offset-2 col-md-10'>{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'dokumen')->widget(FileInput::classname(), [
        'pluginOptions'=>[
                'showUpload' => false,
        ], 
    ]); ?>

    <?= $form->field($model, 'no_dokumen')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'jenis_dokumen')->dropDownList([
            'Contract Drafting'         =>'Contract Drafting',
            'Contract Monitoring'       =>'Contract Monitoring',
            'Contract Closing'          =>'Contract Closing',
            'Pembuatan PR Stock'        =>'Pembuatan PR Stock',
            'Penerimaan Barang'         =>'Penerimaan Barang',
            'Penyimpanan Barang'        =>'Penyimpanan Barang',
            'Pengeluaran Barang'        =>'Pengeluaran Barang',
            'Mutasi Barang'             =>'Mutasi Barang',
            'PO Outstanding'            =>'PO Outstanding',
            'Terima Dokumen'            =>'Terima Dokumen',
            'Verifikasi Tagihan'        =>'Verifikasi Tagihan',
            'Verifikasi Faktur Pajak'   =>'Verifikasi Faktur Pajak',
            'Posting SAP'               =>'Posting SAP',
            'Pembayaran'                =>'Pembayaran',
        ], ['prompt'=>'select...']) ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <?= $form->field($model, 'tgl_dokumen')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php //autodetect file serial
    $this->registerJs('
        $(document).on("change", "#devarsipafterpo-dokumen", function(){
            filename = $("#devarsipafterpo-dokumen").val();
            arr_file = filename.split("_");
            last_arr = arr_file.pop();
            arr_numb = last_arr.split(".");
            arr_numb.pop();
            serialno = arr_numb.join(".");
            $("#devarsipafterpo-no_dokumen").val(serialno);
        });

        $(document).on("click", ".fileinput-remove", function(){
            $("#devarsipafterpo-no_dokumen").val("");
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>
