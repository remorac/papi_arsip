<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DevArsipAfterPoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dev Arsip After Pos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-arsip-after-po-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dev Arsip After Po', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'pdf_filename:ntext',
            'no_dokumen',
            'jenis_dokumen',
            'tgl_dokumen',
            // 'keterangan:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
