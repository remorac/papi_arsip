<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DevArsipAfterPo */

$this->title = 'Update Dev Arsip After Po: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dev Arsip After Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dev-arsip-after-po-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
