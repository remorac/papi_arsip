<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DevArsipAfterPo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dev Arsip After Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-arsip-after-po-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pdf_filename:ntext',
            'no_dokumen',
            'jenis_dokumen',
            'tgl_dokumen',
            'keterangan:ntext',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
