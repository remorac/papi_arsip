<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use frontend\models\Pengadaan;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\HariLibur;

/* @var $this yii\web\View */

$this->title = 'Today Report';
$this->params['breadcrumbs'][] = "Report";
?>
<div class="weekly-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Export', ['weekly/export'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<div style="overflow: auto; border:1px solid #ddd;">
    <table class="table table-condensed table-bordered">
    <thead>
    	<tr>
    		<th rowspan=2 align="right">No</th>
    		<th rowspan=2>Pengadaan</th>
			<th rowspan=2>PIC</th>
    		<th rowspan=2>User</th>

    		<th colspan=5>Monitoring Aktifitas</th>

    		<th colspan=7>Monitoring Pengadaan</th>

    		<th rowspan=2>Issue</th>
    		<th rowspan=2>Butuh Kontrak</th>
    		<th rowspan=2>PR</th>
    		<th rowspan=2>Penawaran Terendah Sebelum Nego</th>
    		<th rowspan=2>Penawaran Terendah Sesudah Nego</th>
		</tr>
		<tr>
    		<th>Aktivitas</th>
    		<th>Progress</th>
    		<th>Start</th>
    		<th>Finish</th>
    		<th>Status Aktifitas</th>

    		<th>Rencana Start</th>
    		<th>Rencana End</th>
    		<th>Rencana Durasi</th>
    		<th>Realisasi Start</th>
    		<th>Realisasi End</th>
    		<th>Realisasi Durasi</th>
    		<th>Realisasi Status</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>

    <?php 
    	$i = 0;
    	$_BJ = "";
    	$pengadaans = Pengadaan::find()->orderBy('barang_jasa_id, id desc')->all();
    	foreach ($pengadaans as $pengadaan) {
    		if ($_BJ != $pengadaan->barang_jasa_id and $pengadaan->barang_jasa_id != '3') {
    			$_BJ = $pengadaan->barang_jasa_id;
    			$i = 0;
			?>
			<tr style="border-top:1px solid #444 !important;border-bottom:1px solid #444 !important;">
	    		<th colspan=21><?=strtoupper($pengadaan->barangJasa->nama)?></th>
			</tr>

			<?php
    		}
    ?>

    	<?php
    		$pengadaanActivity = PengadaanActivity::find()
					->where("
						pengadaan_id = '".$pengadaan->id."' and
						start_date <= current_date() and 
						(end_date is null or end_date >= current_date())
					")
					->orderBy("activity_id desc")->one();

			if (is_object($pengadaanActivity)) {
				$i++;
    	?>

		<tr>
			<td align="right"><?=$i?></td>
			<td><?=Html::a($pengadaan->nama,'?r=highlight/view&id='.$pengadaan->id)?></td>
			<td><?=$pengadaan->purchaserPersonelProcurement->personel->singkatan?></td>
			<td>
				<?php
					$pengadaanKorins = PengadaanKorin::find()->where("pengadaan_id = '".$pengadaan->id."'")->all();
					$pic_initial = "";
					foreach ($pengadaanKorins as $pengadaanKorin) {
						if ($pic_initial != $pengadaanKorin->korin->pic_initial) {
							$pic_initial = $pengadaanKorin->korin->pic_initial;
							echo $pengadaanKorin->korin->pic_initial."<br>";
						}
					}
				?>
			</td>
			<td><?=$pengadaanActivity->activity->nama?></td>
			<td><?=$pengadaanActivity->activity->bobot."%"?></td>
			<td><?=$pengadaanActivity->start_date?></td>
			<td>
				<?php
					$end_date = $pengadaanActivity->end_date;
					if ($end_date == "") {
						$date = new DateTime($pengadaanActivity->start_date);
						$date->add(new DateInterval('P'.PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->one()->durasi.'D'));
						$date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaanActivity->start_date, $date->format('Y-m-d')).'D'));  
						while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
					        $date->add(new DateInterval('P1D'));                         
					    }
					}
				    echo $date->format('Y-m-d');
				?>
			</td>
			<td><?= $date->format('Y-m-d') < date('Y-m-d') ? "Over Due Date" : "In Progress" ?></td>
			<td><?= $pengadaan->start_plan ?></td>
			<td>
				<?php
					$date = new DateTime($pengadaan->start_plan);
					$date->add(new DateInterval('P'.PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->sum('durasi').'D'));
					$date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaan->start_plan, $date->format('Y-m-d')).'D'));  
					while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
				        $date->add(new DateInterval('P1D'));                         
				    }
				    echo $date->format('Y-m-d');
				?>
			</td>
			<td>
				<?php
					$pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->sum('durasi');
					echo $pengadaanDurasiAdjusted;
				?>
			</td>
			<td><?=$pengadaan->start_plan?></td>
			<td>
				<?php 
					$date = new DateTime($pengadaan->start_plan);
					$date->add(new DateInterval('P'.$pengadaanDurasiAdjusted.'D'));
					$date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaan->start_plan, $date->format('Y-m-d')).'D'));  
					while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
				        $date->add(new DateInterval('P1D'));                         
				    }
					echo $date->format('Y-m-d');
				?>
			</td>
			<td style="white-space: nowrap">
				<?php
					$start_date = PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."'")->orderBy("activity_id ASC")->one()->start_date;
					$end_date = PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."'")->orderBy("activity_id DESC")->one()->end_date;
					if (is_null($end_date)) $end_date = date('Y-m-d');
					$interval 	= date_diff(date_create($start_date), date_create($end_date))->format('%d');
    				$real_all_durasi = $interval - HariLibur::countHoliday($start_date, $end_date);
    				echo $start_date.' - '.$end_date.' : '.$interval.'<br>';
    				echo $start_date.' - '.$end_date.' : '.HariLibur::countHoliday($start_date, $end_date);
				?>
			</td>
			<td><?= $pengadaanDurasiAdjusted >= $real_all_durasi ? "On Schedule" : "Delay" ?></td>
			<td>
				<?php 
					$keterangan = "";
					foreach ($pengadaanActivity->pengadaanIssues as $pengadaanIssue) {
						$keterangan = $pengadaanIssue->keterangan;
					}
					echo $keterangan;
				?>
			</td>
			<td><?=$pengadaan->kontrak_butuh?></td>
			<td><?=$pengadaan->pr_ada?></td>
			<td><?=$pengadaan->total_harga_sebelum_diskon?></td>
			<td><?=$pengadaan->total_harga_setelah_diskon?></td>
		</tr>
		
	    <?php
	    	}
	    ?>

    <?php
    	}
    ?>

	</tbody>
    </table>
    </div>

</div>