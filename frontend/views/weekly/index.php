<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Today Report';
$this->params['breadcrumbs'][] = "Report";
?>
<div class="weekly-index box-box-body box-primary">

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->nama;
                    }
                },
            ],
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return $data->metoda->nama;
                    }
                },
            ],
            [
                'attribute' => 'personel.singkatan',
                'label' => 'PIC',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return $data->purchaserPersonelProcurement->personel->singkatan;
                    }
                },
            ],
            [
                'attribute' => '',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if ($data->pengadaanActivities){
                    	$activity = "KPP";
                    	$activity_order = 1;
                    	foreach ($data->pengadaanActivities as $pengadaanActivity) {
							if ($activity_order < $pengadaanActivity->activity->id) {  
								$activity_order = $pengadaanActivity->activity->id;
								$activity 		= $pengadaanActivity->activity->nama;  
								$start_date 	= $pengadaanActivity->start_date;
							}                 		
                    	}
                        return $activity.' - '.$start_date;
                    }
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
