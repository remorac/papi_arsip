<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use frontend\models\Pengadaan;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\HariLibur;

/* @var $this yii\web\View */

$this->title = 'Today Status';
$this->params['breadcrumbs'][] = "Report";
?>

<div class="weekly-index box box-body box-primary">
		
	<?php
		$searchModel 	= new \frontend\models\TmpTodaySearch();
	    $dataProvider 	= $searchModel->search(Yii::$app->request->queryParams);
	    // $dataProvider->pagination = false;
	?>
	
	<?= GridView::widget([
	    'dataProvider' => $dataProvider,
	    'filterModel' => $searchModel,
	    'tableOptions' => ['class' => 'table  table-hover paper'],    
	    'floatHeader'=>true,
    	'floatHeaderOptions'=>['scrollingTop'=>'0'],
    	'pjax' => true,
	    'columns' => [
	        ['class' => 'yii\grid\SerialColumn'],
	        ['format' => 'html', 'attribute' => 'brg_jsa', 'filter' => ['Barang' => 'Barang', 'Jasa' => 'Jasa']],
	        ['format' => 'html', 'attribute' => 'nama_pengadaan'],
	        ['format' => 'html', 'attribute' => 'monitoring_aktifitas'],
	        ['format' => 'html', 'attribute' => 'rencana_pengadaan'],
	        ['format' => 'html', 'attribute' => 'realisasi_pengadaan'],
	        ['format' => 'html', 'attribute' => 'issue_aktifitas', 'value' => function($data) { return nl2br($data->issue_aktifitas); }],
	        ['format' => 'html', 'attribute' => 'keterangan'],
	    ],
	    'toolbar' => [
	        [
	            'content'=>
	                Html::a('<i class="glyphicon glyphicon-repeat"></i> &nbsp;Refresh', ['weekly/refresh'], [
	                    'class' => 'btn btn-default', 
	                    'title' => 'Refresh',
	                ]),
	        ],
	        '{export}',
	        '{toggleData}',
	    ],
	    'export' => ['label' => '&nbsp;Export&nbsp;'],
	    'toggleDataOptions' => ['all' => ['label' => '&nbsp;All'], 'page' => ['label' => '&nbsp;Page']],
	    'exportConfig' => [
	        GridView::EXCEL => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
	        GridView::CSV => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],
	        // GridView::PDF => ['filename' => 'Procurement_Today_Status_'.date('Y-m-d')],

	    ],
	    'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
	]); ?>

</div>