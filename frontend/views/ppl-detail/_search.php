<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tgl_permintaan_barang') ?>

    <?= $form->field($model, 'no_ppl') ?>

    <?= $form->field($model, 'requestor') ?>

    <?= $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'tool_non') ?>

    <?php // echo $form->field($model, 'status_pengadaan') ?>

    <?php // echo $form->field($model, 'status_order') ?>

    <?php // echo $form->field($model, 'status_gr') ?>

    <?php // echo $form->field($model, 'status_gi_stock') ?>

    <?php // echo $form->field($model, 'status_pembayaran') ?>

    <?php // echo $form->field($model, 'buyer') ?>

    <?php // echo $form->field($model, 'vendor_rfq') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <?php // echo $form->field($model, 'barang_datang') ?>

    <?php // echo $form->field($model, 'kekurangan_brg') ?>

    <?php // echo $form->field($model, 'tgl_terima_barang') ?>

    <?php // echo $form->field($model, 'penerima_barang') ?>

    <?php // echo $form->field($model, 'lokasi_penyimpanan') ?>

    <?php // echo $form->field($model, 'jml_sdh_diambil') ?>

    <?php // echo $form->field($model, 'tgl_pengambilan') ?>

    <?php // echo $form->field($model, 'pengambil_barang') ?>

    <?php // echo $form->field($model, 'stock_di_kontainer') ?>

    <?php // echo $form->field($model, 'tgl_terima_invoice') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'total_price') ?>

    <?php // echo $form->field($model, 'no_korin') ?>

    <?php // echo $form->field($model, 'tgl_diserahkan_ke_afis') ?>

    <?php // echo $form->field($model, 'tgl_pembayaran') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'real_time_days') ?>

    <?php // echo $form->field($model, 'no_invoice') ?>

    <?php // echo $form->field($model, 'harga_invoice') ?>

    <?php // echo $form->field($model, 'selisih') ?>

    <?php // echo $form->field($model, 'area_detail') ?>

    <?php // echo $form->field($model, 'sm') ?>

    <?php // echo $form->field($model, 'editor_data') ?>

    <?php // echo $form->field($model, 'no_release') ?>

    <?php // echo $form->field($model, 'no_wbs') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
