<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ppl Details';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ppl-detail-index box box-primary box-body">

    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'id',
            'tgl_permintaan_barang:date',
            'no_ppl',
            'requestor',
            'area',
            'deskripsi',
            'satuan',
            'qty',
            'tool_non',
            'status_pengadaan',
            'status_order',
            'status_gr',
            'status_gi_stock',
            'status_pembayaran',
            'buyer',
            'vendor_rfq',
            'supplier',
            'barang_datang',
            'kekurangan_brg',
            'tgl_terima_barang:date',
            'penerima_barang',
            'lokasi_penyimpanan',
            'jml_sdh_diambil',
            'tgl_pengambilan:date',
            'pengambil_barang',
            'stock_di_kontainer',
            'tgl_terima_invoice:date',
            'price',
            'total_price',
            'no_korin',
            'tgl_diserahkan_ke_afis:date',
            'tgl_pembayaran:date',
            'keterangan',
            'real_time_days',
            'no_invoice',
            'harga_invoice',
            'selisih',
            'area_detail',
            'sm',
            'editor_data',
            'no_release',
            'no_wbs',
            // 'deleted_at:datetime',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:text:Created By',
            'updatedBy.username:text:Updated By',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Ppl Details',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);

        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'contentOptions' => ['class' => 'action-column nowrap text-left'],
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
                    },
                    'update' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
                    },
                    'delete' => function ($url) {
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                            'data-method' => 'post', 
                            'data-confirm' => 'Are you sure you want to delete this item?']);
                    },
                ],
            ],
            // 'id',
            [
                'attribute' => 'tgl_permintaan_barang',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'no_ppl',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'requestor',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'area',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'deskripsi',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'satuan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'qty',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'tool_non',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'status_pengadaan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'status_order',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'status_gr',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'status_gi_stock',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'status_pembayaran',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'buyer',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'vendor_rfq',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'supplier',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'barang_datang',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'kekurangan_brg',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'tgl_terima_barang',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'penerima_barang',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'lokasi_penyimpanan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'jml_sdh_diambil',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'tgl_pengambilan',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'pengambil_barang',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'stock_di_kontainer',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'tgl_terima_invoice',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'price',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'total_price',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'no_korin',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'tgl_diserahkan_ke_afis',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'tgl_pembayaran',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            [
                'attribute' => 'keterangan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'real_time_days',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'no_invoice',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'harga_invoice',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'selisih',
                'format' => ['decimal', 2],
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'area_detail',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'sm',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'editor_data',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'no_release',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'no_wbs',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            /* [
                'attribute' => 'deleted_at',
                'format' => 'integer',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ], */
            // 'created_at:integer',
            // 'updated_at:integer',
            // 'created_by:integer',
            // 'updated_by:integer',
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            // $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>