<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplDetail */

$this->title = 'Update Ppl Detail: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppl-detail-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
