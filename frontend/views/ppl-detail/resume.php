<?php

use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;

use frontend\models\PplDetail;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PplDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resume PPL';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppl-detail-index">
  
    <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-2" style="padding-right: 0">
                    <select class="form-control">
                        <option>All WBS</option>
                    </select>
                </div>
                <div class="col-md-2" style="padding-right: 0">
                    <select class="form-control">
                        <option>All Area</option>
                    </select>
                </div>
                <div class="col-md-2" style="padding-right: 0">
                    <select class="form-control">
                        <option>All Vendor</option>
                    </select>
                </div>
                <div class="col-md-2" style="padding-right: 0">
                    <select class="form-control">
                        <option>All Period</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button style="line-height: 20px;" class="btn btn-default">Refresh</button>
                </div>
            </div>
        </div>
        <div class="box-body">
    
            <?php 
                $categoriesArea = [];
                $dataPrice = [];
                $dataVolume = [];
                $areas = PplDetail::find()->select('area')->distinct('area')->orderBy('area')->asArray()->all();
                foreach ($areas as $area) {
                    if ($area['area'] !== null) 
                    {
                        $categoriesArea[] = $area['area'];
                        $dataPrice[] = PplDetail::find()->where(['area' => $area['area']])->one()->total_price;
                        $dataVolume[] = PplDetail::find()->where(['area' => $area['area']])->one()->qty;
                    }
                }

                echo Highcharts::widget([
                    'options' => [
                        'chart' => [
                            'zoomType' => 'xy'
                        ],
                        'title' => [
                            'text' => 'Harga dan Volume Pembelian Langsung'
                        ],
                        'subtitle' => [
                            'text' => 'Procurement Proyek Indarung VI'
                        ],
                        'xAxis' => [
                          'categories' => $categoriesArea,
                          'crosshair' => true
                        ],
                        'yAxis' => [[ // Primary yAxis
                          'labels' => [
                              'format' => 'Rp {value}',
                              'style' => [
                                  'color' => 'lightblue'
                              ]
                          ],
                          'title' => [
                              'text' => 'Price',
                              'style' => [
                                  'color' => 'lightblue'
                              ]
                          ]
                        ], [ // Secondary yAxis
                          'title' => [
                              'text' => 'Volume',
                              'style' => [
                                  'color' => 'lightgreen'
                              ]
                          ],
                          'labels' => [
                              'format' => '{value} (UoM)',
                              'style' => [
                                  'color' => 'lightgreen'
                              ]
                          ],
                          'opposite' => true
                        ]],
                        'tooltip' => [
                          'shared' => true
                        ],
                        'legend' => [
                          'layout' => 'vertical',
                          'align' => 'left',
                          'x' => 120,
                          'verticalAlign' => 'top',
                          'y' => 100,
                          'floating' => true,
                          'backgroundColor' => '#FFFFFF'
                        ],
                        'series' => [[
                          'name' => 'Price',
                          'type' => 'column',
                          'yAxis' => 0,
                          'color' => 'lightblue',
                          'data' => $dataPrice,
                          'tooltip' => [
                              // 'valueSuffix' => ' mm'
                              'valuePrefix' => 'Rp '
                          ]

                        ], [
                          'name' => 'Volume',
                          'type' => 'column',
                          'yAxis' => 1,
                          'color' => 'lightgreen',
                          'data' => $dataVolume,
                          'tooltip' => [
                              // 'valueSuffix' => '°C'
                              'valueSuffix' => ' (UoM)'
                          ]
                        ]]
                    ]
                ]); 
            ?>

        </div>

    </div>

</div>
