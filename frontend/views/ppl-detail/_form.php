<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppl-detail-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'tgl_permintaan_barang')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'no_ppl')->textInput() ?>

    <?= $form->field($model, 'requestor')->textInput() ?>

    <?= $form->field($model, 'area')->textInput() ?>

    <?= $form->field($model, 'deskripsi')->textInput() ?>

    <?= $form->field($model, 'satuan')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'tool_non')->textInput() ?>

    <?= $form->field($model, 'status_pengadaan')->textInput() ?>

    <?= $form->field($model, 'status_order')->textInput() ?>

    <?= $form->field($model, 'status_gr')->textInput() ?>

    <?= $form->field($model, 'status_gi_stock')->textInput() ?>

    <?= $form->field($model, 'status_pembayaran')->textInput() ?>

    <?= $form->field($model, 'buyer')->textInput() ?>

    <?= $form->field($model, 'vendor_rfq')->textInput() ?>

    <?= $form->field($model, 'supplier')->textInput() ?>

    <?= $form->field($model, 'barang_datang')->textInput() ?>

    <?= $form->field($model, 'kekurangan_brg')->textInput() ?>

    <?= $form->field($model, 'tgl_terima_barang')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'penerima_barang')->textInput() ?>

    <?= $form->field($model, 'lokasi_penyimpanan')->textInput() ?>

    <?= $form->field($model, 'jml_sdh_diambil')->textInput() ?>

    <?= $form->field($model, 'tgl_pengambilan')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'pengambil_barang')->textInput() ?>

    <?= $form->field($model, 'stock_di_kontainer')->textInput() ?>

    <?= $form->field($model, 'tgl_terima_invoice')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'total_price')->textInput() ?>

    <?= $form->field($model, 'no_korin')->textInput() ?>

    <?= $form->field($model, 'tgl_diserahkan_ke_afis')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'tgl_pembayaran')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <?= $form->field($model, 'real_time_days')->textInput() ?>

    <?= $form->field($model, 'no_invoice')->textInput() ?>

    <?= $form->field($model, 'harga_invoice')->textInput() ?>

    <?= $form->field($model, 'selisih')->textInput() ?>

    <?= $form->field($model, 'area_detail')->textInput() ?>

    <?= $form->field($model, 'sm')->textInput() ?>

    <?= $form->field($model, 'editor_data')->textInput() ?>

    <?= $form->field($model, 'no_release')->textInput() ?>

    <?= $form->field($model, 'no_wbs')->textInput() ?>

    
    <div class="form-panel col-sm-12">
        <div class="row">
    	    <div class="col-sm-6 col-sm-offset-3">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
