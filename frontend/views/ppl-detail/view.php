<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PplDetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ppl Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ppl-detail-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'tgl_permintaan_barang',
                'no_ppl:ntext',
                'requestor:ntext',
                'area:ntext',
                'deskripsi:ntext',
                'satuan:ntext',
                [
                    'attribute' => 'qty',
                    'format' => ['decimal', 2],
                ],
                'tool_non:ntext',
                'status_pengadaan:ntext',
                'status_order:ntext',
                'status_gr:ntext',
                'status_gi_stock:ntext',
                'status_pembayaran:ntext',
                'buyer:ntext',
                'vendor_rfq:ntext',
                'supplier:ntext',
                [
                    'attribute' => 'barang_datang',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'kekurangan_brg',
                    'format' => ['decimal', 2],
                ],
                'tgl_terima_barang',
                'penerima_barang:ntext',
                'lokasi_penyimpanan:ntext',
                [
                    'attribute' => 'jml_sdh_diambil',
                    'format' => ['decimal', 2],
                ],
                'tgl_pengambilan',
                'pengambil_barang:ntext',
                [
                    'attribute' => 'stock_di_kontainer',
                    'format' => ['decimal', 2],
                ],
                'tgl_terima_invoice',
                [
                    'attribute' => 'price',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'total_price',
                    'format' => ['decimal', 2],
                ],
                'no_korin:ntext',
                'tgl_diserahkan_ke_afis',
                'tgl_pembayaran',
                'keterangan:ntext',
                [
                    'attribute' => 'real_time_days',
                    'format' => ['decimal', 2],
                ],
                'no_invoice:ntext',
                [
                    'attribute' => 'harga_invoice',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'selisih',
                    'format' => ['decimal', 2],
                ],
                'area_detail:ntext',
                'sm:ntext',
                'editor_data:ntext',
                'no_release:ntext',
                'no_wbs:ntext',
                // 'deleted_at:integer',
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
    </div>
</div>
