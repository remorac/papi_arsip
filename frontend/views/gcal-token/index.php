<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GcalTokenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Gcal Tokens');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gcal-token-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Gcal Token',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'client_email:email',
            'server_api_key',
            'calendar_owner_email:email',
            'calendar_private_key',
            // 'calendar_private_key_password',
            // 'calendar_id',
            // 'oauth_client_json:ntext',
            // 'selected',
            // 'first_access_token_json:ntext',
            // 'access_token_json:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
