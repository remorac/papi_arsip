<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GcalToken */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Gcal Token',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gcal Tokens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="gcal-token-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
