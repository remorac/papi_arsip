<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GcalTokenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gcal-token-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_email') ?>

    <?= $form->field($model, 'server_api_key') ?>

    <?= $form->field($model, 'calendar_owner_email') ?>

    <?= $form->field($model, 'calendar_private_key') ?>

    <?php // echo $form->field($model, 'calendar_private_key_password') ?>

    <?php // echo $form->field($model, 'calendar_id') ?>

    <?php // echo $form->field($model, 'oauth_client_json') ?>

    <?php // echo $form->field($model, 'selected') ?>

    <?php // echo $form->field($model, 'first_access_token_json') ?>

    <?php // echo $form->field($model, 'access_token_json') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
