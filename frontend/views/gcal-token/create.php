<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GcalToken */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Gcal Token',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gcal Tokens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gcal-token-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
