<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GcalToken */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gcal Tokens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gcal-token-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'First Token'), ['first-token', 'id' => $model->id], ['name' => 'first_token', 'class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Refresh Token'), ['refresh-token', 'id' => $model->id], ['name' => 'refresh_token', 'class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_email:email',
            'server_api_key',
            'calendar_owner_email:email',
            'calendar_private_key',
            'calendar_private_key_password',
            'calendar_id',
            'oauth_client_json:ntext',
            'selected',
            'first_access_token_json:ntext',
            'access_token_json:ntext',
        ],
    ]) ?>

</div>
