<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GcalToken */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gcal-token-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'client_email')->textInput(['maxlength' => 254]) ?>

    <?= $form->field($model, 'server_api_key')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'calendar_owner_email')->textInput(['maxlength' => 74]) ?>

    <?= $form->field($model, 'calendar_private_key')->textInput() ?>

    <?= $form->field($model, 'calendar_private_key_password')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'calendar_id')->textInput(['maxlength' => 254]) ?>

    <?= $form->field($model, 'oauth_client_json')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'selected')->textInput() ?>

    <?= $form->field($model, 'first_access_token_json')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'access_token_json')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
