<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */

$this->title = 'Technical Administration';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-default">
	<div class="box-header with-border">
        <div class="box-title">
            <i class="fa fa-folder-o text-yellow"></i>
            <?php 
                $links = [];
                $predecessor = '';
                $folders = explode('/', $folder);
                foreach ($folders as $path) {
                    $links[] = Html::a($path, ['index', 'folder' => $predecessor.$path]);
                    $predecessor.= $path.'/';
                }
                echo implode(' / ', $links);
            ?>
        </div>
		<div class="box-tools">
			<?= Html::a('New Folder', ['create', 'folder' => $folder], ['class' => 'btn btn-sm btn-default btn-text-success']) ?>
		</div>
	</div>

	<div class="box-body">

        <div class="" style="background: #fafafa; padding:0 10px; padding-bottom: 1px; margin-bottom: 10px; border: 1px solid #f4f4f4; border-radius: 4px">
            <div class="package-form">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="form-group">
                    <label>Upload Files To <code><?= $folder ?></code></label>
                <?php 
                    echo FileInput::widget([
                        'name' => 'pdf_files[]',
                        'options' => [
                            // 'accept' => 'application/pdf',
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => true,
                            // 'allowedFileExtensions' => ['pdf'],
                            'maxFileSize'=>3072000,
                            'maxFileCount'=>20,
                        ],
                    ]);
                ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <?php Pjax::begin(['timeout'=>10000, 'id'=>'folder-content']); ?>
        <table class="table table-condensed table-hover" style="border: 1px solid #f4f4f4">
        <?php 
            $is_empty = 1;
            $files = [];
            $unsorted_files = new \DirectoryIterator(Yii::getAlias('@uploads/'.$folder));
            

            foreach ($unsorted_files as $file) {
                $files[] = [
                    'sort_filename' => strtolower($file->getFilename()),
                    'filename' => $file->getFilename(),
                    'isDir' => $file->isDir(),
                    'isDot' => $file->isDot(),
                ];
            }
            asort($files);
            foreach ($files as $file) {
                if (!$file['isDot'] && $file['isDir']) {
                $is_empty = 0;
                ?>
                    <tr>
                        <td style="vertical-align: top" width="1px"><big><i class="fa fa-folder text-yellow"></i></big></td>
                        <td style="vertical-align: top"><?= Html::a($file['filename'], ['index', 'folder' => $folder.'/'.$file['filename']], ['data-pjax' => 0]) ?></td>
                        <td style="vertical-align: top" width="1px">
                            <a class="btn btn-xs btn-default btn-text-warning" href="javascript:formRename('<?= $file['filename'] ?>', 0)"><i class="glyphicon glyphicon-edit text-warning"></i></a>
                        </td>
                        <td style="vertical-align: top" width="1px"><?= Html::a('<i class="fa fa-remove text-danger"></i>', ['remove', 'filename' => $folder.'/'.$file['filename'], 'folder' => $folder], [
                            'class' => 'btn btn-xs btn-default btn-text-danger',                            
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item? <br> <small>Menghapus folder juga akan menghapus semua file yang berada dalam folder tersebut.</small>'),
                                'method' => 'post',
                            ],
                        ]) ?></td>
                    </tr>
                <?php 
                }
            }
            foreach ($files as $file) {
                if (!$file['isDot'] && !$file['isDir']) {
                $is_empty = 0;
                ?>
                    <tr>
                        <td style="vertical-align: top" width="1px"><big><i class="fa fa-file text-success"></i></big></td>
                        <td style="vertical-align: top"><?= Html::a($file['filename'], ['download', 'filename' => $folder.'/'.$file['filename']], ['data-pjax' => 0]) ?></td>
                        <td style="vertical-align: top" width="1px">
                            <a class="btn btn-xs btn-default btn-text-warning" href="javascript:formRename('<?= $file['filename'] ?>', 1)"><i class="glyphicon glyphicon-edit text-warning"></i></a>
                        </td>
                        <td style="vertical-align: top" width="1px"><?= Html::a('<i class="fa fa-remove text-danger"></i>', ['remove', 'filename' => $folder.'/'.$file['filename'], 'folder' => $folder], [
                            'class' => 'btn btn-xs btn-default btn-text-danger',                            
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?></td>
                    </tr>
                <?php 
                }
            }
        ?>
        <?= $is_empty ? '<tr><td><span class="text-muted">This folder is empty.</span></tr></td>' : '' ?>
        </table>
        <?php Pjax::end(); ?>
	</div>

</div>


<form class="form-horizontal" id="rename_form">
<div class="modal fade" id="modalRename">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="new_name" name="new_name">
                        <span class="small">Harap tidak mengubah atau menghilangkan ekstensi file (".pdf", ".docx", dll) saat melakukan rename file.</span>
                    </div>
                </div>
                <input type="hidden" id="old_name" name="old_name">
                <input type="hidden" id="is_file" name="is_file">
                <input type="hidden" id="folder" name="folder" value="<?= $folder ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn_rename btn btn-primary">Rename</button>
            </div>
        </div>
    </div>
</div>
</form

<?php //form 
    $this->registerJs('
        function formRename(old_name, is_file) {
            $("#modalRename").modal("show");
            $("#modalRename .modal-title").text("Rename");
            $("#modalRename .modal-title").prepend("<i class=\'fa fa-edit\'></i> &nbsp;");
            $("#modalRename #is_file").val(is_file);
            $("#modalRename #old_name").val(old_name);
            $("#modalRename #new_name").val(old_name);
        }
    ', \yii\web\VIEW::POS_END);

    $this->registerJs('
        $(document).on("click", ".btn_rename", function(){
            data = $("#rename_form").serialize();
            $.ajax({
                url: "?r=technical-administration-folder/rename",
                data: data,
                cache: "",
                success:function(r) {
                    if (r == "file_exists") {
                        alert("Rename gagal. Ada file atau folder lain dengan nama yang sama.");
                    } else {
                        $(".modal").modal("hide");
                        $.pjax.reload({container: "#folder-content", timeout: 10000});
                    }
                }
            });
        });

        $(document).on("submit", "#rename_form", function(e){
            e.preventDefault();
            $(".btn_rename").click();
            return false;
        });
    ', \yii\web\VIEW::POS_READY);
?>