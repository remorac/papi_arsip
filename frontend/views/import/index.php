<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\file\FileInput;
use kartik\widgets\Growl;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Import Data SAP');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php

foreach($messages as $message){
	//print_r($message);
	//die();
	echo Growl::widget([
	    'type' => $message['growlType'],
	    'title' => $message['title'],
	    'icon' => 'glyphicon glyphicon-remove-sign',
	    'body' => $message['body'],
	    'showSeparator' => true,
	    //'delay' => 4500,
	    'pluginOptions' => [
	        'showProgressbar' => true,
	        'placement' => [
	            'from' => 'top',
	            'align' => 'right',
	        ]
	    ]
	]);
}

?>

<div class="import-form">

<?php $form = ActiveForm::begin([
    'options' => [
    	'enctype' => 'multipart/form-data',
    	'class' => 'form-horizontal'
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3 text-right\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-offset-3 col-md-9\">{error}</div>",
    ],
]); ?>


<?php // $form->field($model, 'kpp')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text/csv'],
//]); ?>

<?= $form->field($model, 'material')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>


<?= $form->field($model, 'pr')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>


<?= $form->field($model, 'po')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'delivery')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'rfq')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'grsa')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'reservasi')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'tracking')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<?= $form->field($model, 'vendor')->widget(FileInput::classname(), [
    //'options' => ['accept' => 'text\csv'],
	'pluginOptions'=>[
	        //'allowedFileExtensions'=>['csv'],
	        'showUpload' => false,
	        'showPreview' => false,
	], 
]); ?>

<div class="form-group">
    <div class="col-md-offset-3 col-md-6">
        <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
        <?php	
        // if(Yii::$app->user->can('import_create-table')){
        	echo Html::submitButton('Create Table', ['name' => 'create-table', 'class' => 'btn btn-primary']);
        // }
        ?>	
    </div>
</div>


<?php ActiveForm::end() ?>

</div>
</div>