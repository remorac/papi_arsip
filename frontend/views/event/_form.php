<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Activity;
use frontend\models\Tempat;
use frontend\models\Pengadaan;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
if(is_array($alerts)){
    foreach($alerts as $alert){
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => $alert['class'],
            ],
            'body' => $alert['body'],
        ]);   
    }
}
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php
        echo $form->field($model, 'pengadaan_id')->widget(Select2::classname(), [
            'data' => $pengadaanSelect2Items,
            'options' => [
                'placeholder' => 'Pilih Pengadaan ...'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
        ->label('Pengadaan');
    ?>

    <?=
    $form->field($model, 'activity_id')
         ->dropDownList(
                ArrayHelper::map(Activity::find()->all(), 'id', 'DropdownDisplayItem')
                )
         ->label('Activity')
    ?>

    <?= $form->field($model,'tanggal')->widget(
        kartik\widgets\DatePicker::className(),
        [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'todayHighlight' => true,
                'autoWidget' => true,
            ],
        ])
        ->label('Tanggal')
    ?>

    <?= $form->field($model, 'jam')->widget(TimePicker::classname(), 
        [
            'pluginOptions' => [
                'showMeridian' => false, 
            ],
        ]
        ) 
    ?>

    <?php
        echo $form->field($model, 'tempat_id')->widget(Select2::classname(), [
            'data' => $tempatSelect2Items,
            'options' => [
                'placeholder' => 'Pilih tempat ...'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'addon' => [
                'append' => [
        //            'content' => Html::button(\kartik\helpers\Html::icon('map-marker'), [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus-sign" ></i>',
                        ['tempat/create', 'orig' => \Yii::$app->request->url], 
                        [
                            'class' => 'btn btn-primary', 
                            'title' => 'Tambah tempat', 
                            'data-toggle' => 'tooltip'
                    ]),
                    'asButton' => true
                ]
            ]
        ])

        ->label('Tempat');
    ?>

    <?php //= $form->field($model, 'gcal_event_json')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
