<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Event');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a(Yii::t('app', 'Create Event'), ['create'], ['class' => 'btn btn-success']) ?>
        <a id="deleteSelectedButton" class="btn btn-danger">Delete Selected Event</a>
    </p> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            /* [
                'class' => 'yii\grid\CheckboxColumn',
            ], */
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaan){
                        return Html::a($data->pengadaan->nama, '?r=event&EventSearch[pengadaan.nama]=' . $data->pengadaan->nama);
                    }
                },
            ],
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activity){
                        return Html::a($data->activity->nama, '?r=event&EventSearch[activity.nama]=' . $data->activity->nama);
                    }
                },
            ],
            [
                'attribute' => 'purchaserPersonelProcurement.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=event&EventSearch[purchaserPersonelProcurement.nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            [
                'attribute' => 'tanggal',
                'headerOptions'=> ['style' => 'width:9em'],
                'format' => 'date',
            ],
            [
                'attribute' => 'jam',
                //'format' => 'time',
                'headerOptions'=> ['style' => 'width:6em'],
            ],
            /*
            [
                'attribute' => 'gcal_event_json',
                'format' => 'raw',
                'value' => function(){
                    return !empty($data->gcal_event_json);
                }
            ],
            */
            // 'tempat_id',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            /* [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=> ['style' => 'width:5em'],
            ], */
        ],
        'rowOptions' => function($model, $key, $index, $grid){
            $tanggal 	= new DateTime($model->tanggal);
            $today 		= new DateTime();
            $interval 	= date_diff($today, $tanggal);
            $dayCount 	= $interval->format('%d');
            $formatDate = 'Y-m-d';
            
            if($tanggal->format($formatDate) != $today->format($formatDate)){
                $dayCount++;
            }
            switch($dayCount){
                case 0:
                    return ['style' => 'background-color:#C8E7FE'];
                    break;
                case 1:
                    return ['style' => 'background-color:#F9F9AC'];
                    break;
            }
        },
    ]
    ); ?>

</div>
<?php

$script = <<< JS
function deleteSelected(){
    if(!window.confirm("Yakin akan menghapus event-event yang dipilih?")){
        return;
    }
    var keys = $('#w0').yiiGridView('getSelectedRows');
    $.ajax({
       url: "?r=event/delete-selected", // your controller action
       method: "delete",
       //dataType: 'json',
       data: {"selection": keys},
       complete:function(jqXHR, textStatus){
            if(textStatus == "success"){
                window.location.href = "?r=event/index";
            }
       }
    });
}
$("#deleteSelectedButton").click(function(){
    deleteSelected();
});
JS;
$this->registerJs($script, View::POS_END);
?>
