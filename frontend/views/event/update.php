<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Event */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Event',
]) . ' ' . $model->tanggal . ': ' . $model->activity->nama . ' ' . $model->pengadaan->barangJasa->nama . ' ' . $model->pengadaan->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tanggal . ': ' . $model->activity->nama . ' ' . $model->pengadaan->barangJasa->nama . ' ' . $model->pengadaan->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pengadaanSelect2Items' => $pengadaanSelect2Items,
        'tempatSelect2Items' => $tempatSelect2Items,
        'alerts' => $alerts,
    ]) ?>

</div>
