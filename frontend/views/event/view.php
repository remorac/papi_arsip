<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Event */

$this->title = $model->tanggal . ': ' . $model->activity->nama . ' ' . $model->pengadaan->barangJasa->nama . ' ' . $model->pengadaan->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => Html::a(
                    $model->pengadaan ? $model->pengadaan->nama : '', 
                    $model->pengadaan ? '?r=/pengadaan/view&id=' . $model->pengadaan->id : ''
                    )
            ],
            [
                'attribute' => 'purchaserPersonelProcurement.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => Html::a(
                    $model->purchaserPersonelProcurement ? $model->purchaserPersonelProcurement->personel->nama : '', 
                    $model->purchaserPersonelProcurement ? '?r=/personel-procurement/view&id=' . $model->purchaserPersonelProcurement->id : ''
                    )
            ],
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => Html::a(
                    $model->activity ? $model->activity->nama : '', 
                    $model->activity ? '?r=/activity/view&id=' . $model->activity->id : ''
                    )
            ],
            'tanggal:date',
            'jam:time',
            [
                'attribute' => 'tempat.nama',
                'label' => 'Tempat',
                'format' => 'raw',
                'value' => Html::a(
                    $model->tempat ? $model->tempat->nama : '', 
                    $model->tempat ? '?r=/tempat/view&id=' . $model->tempat->id : ''
                    )
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'createdBy.username',
                'label' => 'Created By',
                'format' => 'raw',
                'value' => Html::a(
                    is_object($model->createdBy) ? $model->createdBy->username : '', 
                    is_object($model->createdBy) ? '?r=/user/view&id=' . $model->createdBy->id : ''
                    )
            ],
            [
                'label' => 'Updated By',
                'format' => 'raw',
                'value' => Html::a(
                    is_object($model->updatedBy) ? $model->updatedBy->username : '', 
                    is_object($model->updatedBy) ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                    )
            ],
        ],
    ]) ?>

</div>
