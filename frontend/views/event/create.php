<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Event */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Event',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pengadaanSelect2Items' => $pengadaanSelect2Items,
        'tempatSelect2Items' => $tempatSelect2Items,
        'alerts' => $alerts,
    ]) ?>

</div>
