<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ArsipFile */

$this->title = 'Upload File TA';
$this->params['breadcrumbs'][] = ['label' => 'Arsip TA', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-file-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
