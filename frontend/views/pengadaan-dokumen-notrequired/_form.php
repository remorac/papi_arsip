<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use frontend\models\Pengadaan;
use frontend\models\DokumenJenis;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanDokumenNotrequired */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-dokumen-notrequired-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'pengadaan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Pengadaan::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <?= $form->field($model, 'dokumen_jenis_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(DokumenJenis::find()->all(), 'id', 'DropdownDisplayItem'),
        'options' => ['placeholder' => '-- select --'],
    ]); ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
