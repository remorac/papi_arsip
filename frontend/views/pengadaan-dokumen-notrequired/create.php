<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanDokumenNotrequired */

$this->title = 'Create Not Required Document';
$this->params['breadcrumbs'][] = ['label' => 'Not Required Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-dokumen-notrequired-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
