<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanDokumenNotrequired */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Not Required Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-dokumen-notrequired-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'pengadaan_id',
            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => Html::a(
                    $model->pengadaan ? $model->pengadaan->nama : '', 
                    $model->pengadaan ? '?r=/pengadaan/view&id=' . $model->pengadaan->id : ''
                    )
            ],
            //'dokumen_jenis_id',
            [
                'attribute' => 'dokumenJenis.nama',
                'label' => 'Jenis Dokumen',
                'format' => 'raw',
                'value' => Html::a(
                    $model->dokumenJenis ? $model->dokumenJenis->nama : '', 
                    $model->dokumenJenis ? '?r=/dokumen-jenis/view&id=' . $model->dokumenJenis->id : ''
                    )
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
