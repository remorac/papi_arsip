<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanDokumenNotrequiredSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Not Required Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-dokumen-notrequired-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Not Required Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pengadaan_id',
            [
                'attribute' => 'pengadaan.nama',
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaan){
                        return Html::a($data->pengadaan->nama, '?r=pengadaan/view&id=' . $data->pengadaan->id);
                    }
                },
            ],
            //'dokumen_jenis_id',

            [
                'attribute' => 'dokumenJenis.nama',
                'label' => 'Jenis Dokumen',
                'format' => 'raw',
                'value' => function($data){
                    if($data->dokumenJenis){
                        return Html::a($data->dokumenJenis->nama, '?r=dokumen-jenis/view&id=' . $data->dokumenJenis->id);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
