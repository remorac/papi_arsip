<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DurasiStandar */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Durasi Standar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="durasi-standar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            'tanggal_berlaku:date',
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => Html::a(
                    $model->activity ? $model->activity->nama : '', 
                    $model->activity ? '?r=/activity/view&id=' . $model->activity->id : ''
                    )
            ],
            //'level_approval_id',
            [
                'attribute' => 'levelApproval.approver',
                'label' => 'Level Approval',
                'format' => 'raw',
                'value' => Html::a(
                    $model->levelApproval ? $model->levelApproval->approver : '', 
                    $model->levelApproval ? '?r=/levelApproval/view&id=' . $model->levelApproval->id : ''
                    )
            ],
            'durasi',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
