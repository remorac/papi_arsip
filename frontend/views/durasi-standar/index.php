<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DurasiStandarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Durasi Standar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="durasi-standar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Durasi Standar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tanggal_berlaku:date',
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activity){
                        return Html::a($data->activity->nama, '?r=durasi-standar&DurasiStandarSearch[activity_id]=' . $data->activity_id);
                    }
                },
            ],
            //'level_approval_id',
            [
                'attribute' => 'levelApproval.approver',
                'label' => 'Level Approval',
                'format' => 'raw',
                'value' => function($data){
                    if($data->levelApproval){
                        return Html::a($data->levelApproval->approver, '?r=levelApproval&LevelApprovalSearch[approver]=' . $data->levelApproval->approver);
                    }
                },
            ],
            'durasi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
