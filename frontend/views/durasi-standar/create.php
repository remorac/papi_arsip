<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DurasiStandar */

$this->title = 'Create Durasi Standar';
$this->params['breadcrumbs'][] = ['label' => 'Durasi Standar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="durasi-standar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
