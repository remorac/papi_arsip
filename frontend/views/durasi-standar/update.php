<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DurasiStandar */

$this->title = 'Update Durasi Standar: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Durasi Standar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="durasi-standar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
