<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\widgets\DatePicker;

use frontend\models\DokumenJenis;
use frontend\models\KorinAssignment;
use frontend\models\PengadaanKorin;

/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */

$this->title = $model->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Korin Masuk', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-view box box-body box-info">

    <div class="nav-tabs-custom paper">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#korin" aria-controls="home" role="tab" data-toggle="tab">KORIN</a></li>
            <li role="presentation"><a href="#document" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
            <!-- <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab" data-toggle="tab">Bidder List</a></li> -->
            <!-- <li role="presentation"><a href="#itemKorin" aria-controls="messages" role="tab" data-toggle="tab">Item KORIN PP</a></li> -->
            <!-- <li role="presentation"><a href="#itemPr" aria-controls="settings" role="tab" data-toggle="tab">Item PR</a></li> -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="korin">
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?php 
                    if (KorinAssignment::find()->where("korin_id='".$model->id."'")->count() == 0) {
                    ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php
                    }
                    ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'options'=> ['class' => 'table  detail-view paper'],
                    'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
                    'attributes' => [
                        'id',
                        'no_surat',
                        //'korin_jenis_id',
                        [
                            'attribute' => 'korinJenis.nama',
                            'label' => 'Jenis KORIN',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->korinJenis ? $model->korinJenis->nama : '', 
                                $model->korinJenis ? '?r=/korin-jenis/view&id=' . $model->korinJenis->id : ''
                                )
                        ],
                        //'barang_jasa_id',
                        [
                            'attribute' => 'barangJasa.nama',
                            'label' => 'Barang/Jasa',
                        ],
                        'pic_initial',
                        'tgl_terima',
                        'tanggal_surat',
                        //'ringkasan_isi:ntext',
                        'perihal',
                        'tembusan',
                        'yg_menandatangani',
                        'lampiran',
                        'isi_disposisi:ntext',
                        'tanggal_disposisi',
                        // 'kebutuhan_disposisi',
                        'requirement_date',
                        'revisi_referensi',
                        'created_at',
                        'updated_at',
                        [
                            'label' => 'Created By',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->createdBy ? $model->createdBy->username : '', 
                                // $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                                '#'
                                )
                        ],
                        [
                            'label' => 'Updated By',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->updatedBy ? $model->updatedBy->username : '', 
                                // $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                                '#'
                                )
                        ],
                    ],
                ]) ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="document">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#document_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Link Scanned Document</h4>
                    </div>
                    <div id="document_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-10">
                                <?php
                                    $url = \yii\helpers\Url::to(['dokumen-upload/list']);
                                    echo Select2::widget([
                                        'id' => 'dokumen_upload_id',
                                        'name' => 'dokumen_upload_id',
                                        'options' => [
                                            'placeholder' => 'select document ...',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'minimumInputLength' => 3,
                                            'ajax' => [
                                                'url' => $url,
                                                'dataType' => 'json',
                                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                            ],
                                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                        ],
                                    ]);
                                ?>
                                </div>
                                <div class="col-md-2" style="padding-left:0">
                                    <button type="button" id="btn_document_create" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'document_list']); ?>
                    <?= $this->render('dokumen', [
                        'searchModelKorinDokumen' => $searchModelKorinDokumen,
                        'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="vendor">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#vendor_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Add Vendor to this KPP</h4>
                    </div>
                    <div id="vendor_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-10">
                                <?php
                                    $url = \yii\helpers\Url::to(['vendor/list']);
                                    echo Select2::widget([
                                        'id' => 'vendor_id',
                                        'name' => 'vendor_id',
                                        'options' => [
                                            'placeholder' => 'select vendor ...',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'minimumInputLength' => 3,
                                            'ajax' => [
                                                'url' => $url,
                                                'dataType' => 'json',
                                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                            ],
                                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                        ],
                                    ]);
                                ?>
                                </div>
                                <div class="col-md-2" style="padding-left:0">
                                    <button type="button" id="btn_vendor_create" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'vendor_list']); ?>
                    <?= $this->render('vendor', [
                        'searchModelKorinVendor' => $searchModelKorinVendor,
                        'dataProviderKorinVendor' => $dataProviderKorinVendor,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="itemKorin">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#itemKorin_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Item described in this KORIN PP</h4>
                    </div>
                    <div id="itemKorin_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">

                                    <form class="form-horizontal" id="form-itemKorin">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemKorin-nama" class="form-control" id="itemKorin-nama">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Quantity</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemKorin-quantity" class="form-control" id="itemKorin-quantity">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Satuan</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemKorin-satuan" class="form-control" id="itemKorin-satuan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Keterangan</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemKorin-keterangan" class="form-control" id="itemKorin-keterangan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_itemKorin_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemKorin_list']); ?>
                    <?= $this->render('itemKorin', [
                        'searchModelKorinItem' => $searchModelKorinItem,
                        'dataProviderKorinItem' => $dataProviderKorinItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="itemPr">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#itemPr_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> PR Item related to this KORIN PP</h4>
                    </div>
                    <div id="itemPr_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-7">
                                    
                                    <form class="form-horizontal" id="form-itemPr">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">No. PR</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemPr-purchase_requisition" class="form-control" id="itemPr-purchase_requisition">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Item PR</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemPr-item_of_requisition" class="form-control" id="itemPr-item_of_requisition">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-sm-3 control-label">Item pada KORIN PP</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="itemPr-korin_item_id" class="form-control" id="itemPr-korin_item_id" placeholder="optional">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-3">
                                                <button type="button" id="btn_itemPr_create" class="btn btn-success btn-block">Add</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'itemPr_list']); ?>
                    <?= $this->render('itemPr', [
                        'searchModelKorinPrItem' => $searchModelKorinPrItem,
                        'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>
        </div>

    </div>

    <?php 
        $pengadaanKorin = PengadaanKorin::find()->where("korin_id = '".$model->id."'")->all();
        if (is_array($pengadaanKorin)) {
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Pengadaan Terkait</div>
        </div>
        <div class="panel-body">
            <?php foreach ($pengadaanKorin as $row) {
                echo "<a class='list-group-itemm' target='_blank' href='?r=semua-pengadaan/view&id=".$row->pengadaan_id."'>"
                    .$row->pengadaan->barangJasa->kode.str_pad($row->pengadaan->kode,5,"0",STR_PAD_LEFT)
                    ." - ".$row->pengadaan->nama
                    ."</a><br>";
            }?>
        </div>
    </div>
    <?php } ?>

</div>

<?php //documentJS
    $this->registerJs('
        $("#btn_document_create").click(function() {
            id = $("#dokumen_upload_id").val();
            $.ajax({
                url: "?r=korin/dokumen",
                data: "op=create&korin_id='.$model->id.'&dokumen_upload_id="+id,
                cache: "",
                success:function(r) {
                    $("#select2-dokumen_upload_id-container").html("<span class=\"select2-selection__placeholder\">select document ...</span>");
                    $.pjax.reload({container: "#document_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_document_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin/dokumen",
                    data: "op=delete&korin_id='.$model->id.'&korin_dokumen_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#document_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //vendorJS
    $this->registerJs('
        $("#btn_vendor_create").click(function() {
            id = $("#vendor_id").val();
            $.ajax({
                url: "?r=korin/vendor",
                data: "op=create&korin_id='.$model->id.'&vendor_id="+id,
                cache: "",
                success:function(r) {
                    $("#select2-vendor_id-container").html("<span class=\"select2-selection__placeholder\">select vendor ...</span>");
                    $.pjax.reload({container: "#vendor_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_vendor_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin/vendor",
                    data: "op=delete&korin_id='.$model->id.'&korin_vendor_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#vendor_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //itemKorinJS
    $this->registerJs('
        $("#btn_itemKorin_create").click(function() {
            data = $("form#form-itemKorin").serialize();
            $.ajax({
                url: "?r=korin/itemkorin",
                data: "op=create&korin_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#form-itemKorin").find("input").val("");
                    $.pjax.reload({container: "#itemKorin_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_itemKorin_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin/itemkorin",
                    data: "op=delete&korin_id='.$model->id.'&korin_itemKorin_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#itemKorin_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //itemPrJS
    $this->registerJs('
        $("#btn_itemPr_create").click(function() {
            data = $("form#form-itemPr").serialize();
            $.ajax({
                url: "?r=korin/itempr",
                data: "op=create&korin_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#form-itemPr").find("input").val("");
                    $.pjax.reload({container: "#itemPr_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_itemPr_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin/itempr",
                    data: "op=delete&korin_id='.$model->id.'&korin_itemPr_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#itemPr_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>

<?php //Doc Upload
    $this->registerJs('
        $("#form-doc").on("submit",(function(e) {
            e.preventDefault();
            dataform = $(this).serialize();
            $.ajax({
                url         : "?r=korin/uploaddokumen", // Url to which the request is send
                type        : "POST",  
                data        : new FormData(this),   // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                cache       : false,                // To unable request pages to be cached
                contentType : false,                // The content type used when sending data to the server.
                processData : false,                // To send DOMDocument or non processed data file it is set to false
                success     : function(r)           // A function to be called if request succeeds
                {
                    //registerDokumen($.trim(r));
                    alert(r);
                }
            });
        }));
        ', \yii\web\VIEW::POS_END
    ); 
?>

<?php //Doc Upload
    $this->registerJs('
        $(document).on("change", "#dokumen", function(){
            filename = $("#dokumen").val();
            arr_file = filename.split("_");
            last_arr = arr_file.pop();
            arr_numb = last_arr.split(".");
            arr_numb.pop();
            serialno = arr_numb.join(".");
            $("#doc-nomor").val(serialno);
        });

        $(document).on("click", ".fileinput-remove", function(){
            $("#doc-nomor").val("");
        });

        function registerDokumen() {
            data = $("form#form-doc").serialize();
            $.ajax({
                url: "?r=korin/registerdokumen",
                data: "op=create&korin_id='.$model->id.'&"+data,
                cache: "",
                success:function(r) {
                    $("form#form-doc").find("input").val("");
                    $.pjax.reload({container: "#document_list", timeout: 10000});
                }
            });
        }

        $(document).on("click", ".btn_document_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin/dokumen",
                    data: "op=delete&korin_id='.$model->id.'&korin_dokumen_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#document_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 

    $this->registerJs('
        $(document).on("submit", "#rename_document_form", function(e){
            e.preventDefault();
            $(".btn_rename_document").click();
            return false;
        });

        $(document).on("click", ".btn_rename_document", function(){
            data = $("#rename_document_form").serialize();
            korin_id = $("#modalRenameDocument #korin-dokumen_korin_id").val();
            $.ajax({
                url: "?r=korin/rename-file",
                data: "op=save&"+data,
                cache: "",
                success:function(r) {
                    $(".modal").modal("hide");
                    $.pjax.reload({container: "#document_list", timeout: 10000});
                }
            });
        });
    ', \yii\web\VIEW::POS_READY);

    $this->registerJs('
        $(document).on("submit", "#document_form", function(e){
            e.preventDefault();
            $(".btn_document_create").click();
            return false;
        });
    ', \yii\web\VIEW::POS_READY);

    $this->registerJs('
        function formRenameDocument(dokumen_upload_id, korin_id, dokumen_jenis, old_name) {
            $("#modalRenameDocument").modal("show");
            $("#modalRenameDocument .modal-title").text(dokumen_jenis);
            $("#modalRenameDocument .modal-title").prepend("<i class=\'fa fa-edit\'></i> &nbsp;");
            $("#modalRenameDocument #korin-dokumen_korin_id").val(korin_id);
            $("#modalRenameDocument #korin-dokumen_dokumen_upload_id").val(dokumen_upload_id);
            $("#modalRenameDocument #korin-dokumen_pdf_filename").val(old_name);
        }
    ', \yii\web\VIEW::POS_END);
?>



<form class="form-horizontal" id="rename_document_form">
<div class="modal fade" id="modalRenameDocument">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <big><span class="modal-title"><i class="glyphicon glyphicon-time"></i> Modal title</span></big>
            </div>
            <div class="modal-body" style="background:#fafafa">
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="korin-dokumen_pdf_filename" name="korin-dokumen_pdf_filename">
                        <span class="small">Harap tidak mengubah atau menghilangkan ekstensi file (".pdf", ".docx", dll) saat melakukan rename file.</span>
                    </div>
                </div>
                <input type="hidden" id="korin-dokumen_dokumen_upload_id" name="korin-dokumen_dokumen_upload_id">
                <input type="hidden" id="korin-dokumen_korin_id" name="korin-dokumen_korin_id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_link_document btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn_rename_document btn btn-primary">Rename</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>