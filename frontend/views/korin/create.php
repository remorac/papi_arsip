<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */

$this->title = 'Create Korin Masuk';
$this->params['breadcrumbs'][] = ['label' => 'Korin Masuk', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-create box box-body box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
