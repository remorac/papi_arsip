<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\SapPr;
?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinPrItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_itemPr_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],

        'purchase_requisition',
        'item_of_requisition',
        [
            'attribute' => '',
            'label' => 'Short Text',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Short Text"};
            }
        ],
        [
            'attribute' => '',
            'label' => 'Quantity Requested',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Quantity Requested"};
            }
        ],
        [
            'attribute' => '',
            'label' => 'Unit of Measure',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Unit of Measure"};
            }
        ],
    ],
]); ?>