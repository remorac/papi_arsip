<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */

$this->title = 'Update Korin Masuk: ' . ' ' . $model->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Korin Masuk', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_surat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="korin-update box box-body box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
