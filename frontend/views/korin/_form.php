<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use frontend\models\KorinJenis;
use frontend\models\BarangJasa;
use frontend\models\Unit;
use frontend\models\Personel;

/* @var $this yii\web\View */
/* @var $model frontend\models\Korin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-4 text-right\">{label}</div>\n<div class=\"col-md-8\">{input}</div>\n<div class=\"col-md-offset-4 col-md-8\">{error}</div>",
        ],
    ]); ?>

    <div class="row">
    <div class="col-md-6">

    <?php // echo $form->field($model, 'no_surat', ['enableAjaxValidation' => true])->textInput(['maxlength' => 100]) ?>
    
    <div id="suratunique" class="col-md-8 col-md-offset-4"></div>    
    <?= $form->field($model, 'no_surat')->textInput(['maxlength' => 100, 'onchange' => '
        $.get("index.php?r=korin/suratunique&no_surat="+$(this).val(), function(data) {
            $("#suratunique").html(data);
        });
    ']) ?>
    
    <?= $form->field($model, 'korin_jenis_id')->dropDownList(ArrayHelper::map(KorinJenis::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>
    
    <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>

    <?= $form->field($model, 'tgl_terima')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ],
        ]); ?>

    <?= $form->field($model, 'tanggal_surat')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?= $form->field($model, 'pic_initial')->textInput() ?>

    <?= $form->field($model, 'yg_menandatangani')->textInput(['maxlength' => 200]) ?>

    <?php //echo $form->field($model, 'ringkasan_isi')->textarea(['rows' => 3]) ?>

    </div>
    <div class="col-md-6">

    <?= $form->field($model, 'perihal')->textInput() ?>

    <?= $form->field($model, 'tembusan')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'lampiran')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'isi_disposisi')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'tanggal_disposisi')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>

    <?php //echo $form->field($model, 'kebutuhan_disposisi')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'requirement_date')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>
    
    <?php //$form->field($model, 'revisi_referensi')->textInput(['maxlength' => 50]) ?>
    
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-6">
    <div class="form-group">
        <div class="col-md-offset-4 col-md-8">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php //setUmum
    $this->registerJs('
        $("#korin-korin_jenis_id").change(function() {
            if ($("#korin-korin_jenis_id").val() == "4") {
                $("#korin-barang_jasa_id").val("4");
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>