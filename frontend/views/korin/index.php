<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use frontend\models\KorinAssignment;
use frontend\models\KorinDokumen;
use frontend\models\UnitPosisi;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Korin Masuk';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-index box box-body box-primary">

    <?php 
        $template = '{view}';
        if(Yii::$app->user->can('korin_update')){ $template.= ' {update}'; }
        if(Yii::$app->user->can('korin_delete')){ $template.= ' {delete}'; }
        
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);
        
        $gridColumns = [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],
            
            /*[
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:25px'],
                'value' => function($data) {
                    if (\frontend\models\KorinDokumen::find()->joinWith("dokumenUpload")->where("korin_id = '".$data->id."' and status_disposisi = '1'")->count() > 0) {
                        return "<i class='glyphicon glyphicon-paperclip text-success' title='disposisi lengkap'></i>";   
                    } else {
                        return "<i class='glyphicon glyphicon-paperclip text-danger' title='disposisi belum lengkap'></i>";
                    }
                }
            ], */   
            
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=korin/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    if (KorinAssignment::find()->where("korin_id='".$data->id."'")->count() == 0) {
                        $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=korin/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                        $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['korin/delete', 'id' => $data->id], [
                            'title' => 'delete',
                            'class' => 'btn btn-xs btn-default',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                    $opt.="</div>";
                    return $opt;
                }
            ],

            [
                'attribute' => 'hasDocument',
                'format' => 'raw',
                'contentOptions'=>['style'=>'width:25px'],
                'value' => function($data) {
                    if ($data->hasDocument()) {
                        return "<i class='glyphicon glyphicon-file text-success' title='sudah ada dokumen KPP'></i>";
                    } else {
                        return "<i class='glyphicon glyphicon-file text-danger' title='belum ada dokumen KPP'></i>";
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'hasDocument', ['1'=>'Dokumen Ada', '0'=>'Dokumen Belum Ada'], ['class'=>'form-control','prompt' => '[ ALL ]']),
            ],

            /* [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ], */
            'no_surat',
            //'korin_jenis_id',
            [
                'attribute' => 'korinJenis.nama',
                'label' => 'Jenis KORIN',
            ],
            //'barang_jasa_id',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang/Jasa',
            ],
            //'revisi_referensi',
            'pic_initial',
            // 'tgl_terima',
            [
                'attribute' => 'tgl_terima',
                'contentOptions'=>['style'=>'width: 100px;']
            ],
            // 'tanggal_surat',
            [
                'attribute' => 'tanggal_surat',
                'contentOptions'=>['style'=>'width: 100px;']
            ],
            // 'ringkasan_isi:ntext',
            'perihal',
            // 'tembusan',
            // 'yg_menandatangani',
            // 'lampiran',
            // 'isi_disposisi:ntext',
            // 'kebutuhan_disposisi',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            [
                'attribute' => 'no_surat',
                'label' => 'Unit',
                'format' => 'raw',
                'value' => function($data){
                    if($data){
                        $arr_noSurat = explode("/", $data->no_surat);
                        if (count($arr_noSurat) <= 1) $arr_noSurat = explode("-", $data->no_surat);
                        if (count($arr_noSurat) > 1) {
                            $arr_reverse = array_reverse($arr_noSurat);
                            if (UnitPosisi::find()->where("kode_surat='$arr_reverse[1]'")->one())
                            return UnitPosisi::find()->where("kode_surat='$arr_reverse[1]'")->one()->unit->nama;
                        } else {
                            return "";
                        }
                    }
                },
                'filter' => false,
            ],
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'filename' => 'Afis',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['data-pjax'=>0, 'class'=>'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => ['id' => 'grid'],
        ],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>
