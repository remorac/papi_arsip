<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_itemKorin_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],

        'nama',
        'quantity',
        'satuan',
        'keterangan',
    ],
]); ?>