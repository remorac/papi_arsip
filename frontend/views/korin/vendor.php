<?php 

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderKorinVendor,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_vendor_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],

        [
            'attribute' => 'vendor.vendor',
            'label' => 'Kode',
            'format' => 'raw',
            'value' => function($data){
                if($data->sapVendor->Vendor){
                    return $data->sapVendor->Vendor;
                }
            },
        ],
        [
            'attribute' => '{name of vendor}',
            'label' => 'Nama',
            'format' => 'raw',
            'value' => function($data){
                if($data->sapVendor->{'Name of vendor'}){
                    return $data->sapVendor->{'Name of vendor'};
                }
            },
        ],
    ],
]); ?>