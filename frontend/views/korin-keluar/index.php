<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinKeluarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Korin Keluar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-keluar-index box box-body box-primary">

    <p>
        <?= Html::a('Create Korin Keluar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "<a class='btn btn-xs btn-default' href='?r=korin-keluar/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-success'></i></a>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=korin-keluar/update&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-pencil text-primary'></i></a> &nbsp;";
                    $opt.= Html::a('<i class="glyphicon glyphicon-trash text-danger" ></i>', ['korin-keluar/delete', 'id' => $data->id], [
                        'title' => 'delete',
                        'class' => 'btn btn-xs btn-default',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                    $opt.="</div>";
                    return $opt;
                }
            ],

            // 'id',
            'no_surat',
            'barang_jasa_id',
            // 'no_referensi',
            'pic_initial',
            'tanggal_surat',
            // 'ringkasan_isi:ntext',
            'perihal',
            // 'tembusan',
            // 'yg_menandatangani',
            // 'lampiran',
            // 'created_at',
            'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>

</div>
