<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinKeluar */

$this->title = $model->no_surat;
$this->params['breadcrumbs'][] = ['label' => 'Korin Keluar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-keluar-view box box-body box-info">

    <div class="nav-tabs-custom paper">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#korin" aria-controls="home" role="tab" data-toggle="tab">KORIN</a></li>
            <li role="presentation"><a href="#document" aria-controls="profile" role="tab" data-toggle="tab">Documents</a></li>
            <!-- <li role="presentation"><a href="#vendor" aria-controls="profile" role="tab" data-toggle="tab">Bidder List</a></li> -->
            <!-- <li role="presentation"><a href="#itemKorin" aria-controls="messages" role="tab" data-toggle="tab">Item KORIN PP</a></li> -->
            <!-- <li role="presentation"><a href="#itemPr" aria-controls="settings" role="tab" data-toggle="tab">Item PR</a></li> -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="korin">

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'options'=> ['class' => 'table  detail-view paper'],
                    'template' => "<tr><th width='200px'>{label}</th><td>{value}</td></tr>",
                    'attributes' => [
                        'id',
                        'no_surat',
                        [
                            'attribute' => 'barangJasa.nama',
                            'label' => 'Barang/Jasa',
                        ],
                        // 'no_referensi',
                        'pic_initial',
                        'tanggal_surat',
                        'ringkasan_isi:ntext',
                        'perihal',
                        'tembusan',
                        'yg_menandatangani',
                        'lampiran',
                        'created_at',
                        'updated_at',
                        [
                            'label' => 'Created By',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->createdBy ? $model->createdBy->username : '', 
                                // $model->createdBy ? '?r=/user/view&id=' . $model->createdBy->id : ''
                                '#'
                                )
                        ],
                        [
                            'label' => 'Updated By',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->updatedBy ? $model->updatedBy->username : '', 
                                // $model->updatedBy ? '?r=/user/view&id=' . $model->updatedBy->id : ''
                                '#'
                                )
                        ],
                    ],
                ]) ?>

            </div>

            <div role="tabpanel" class="tab-pane" id="document">
                <div class="panel panel-success">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#document_form">
                        <h4 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> Link Scanned Document</h4>
                    </div>
                    <div id="document_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="row">
                                <div class="col-md-10">
                                <?php
                                    $url = \yii\helpers\Url::to(['dokumen-upload/list']);
                                    echo Select2::widget([
                                        'id' => 'dokumen_upload_id',
                                        'name' => 'dokumen_upload_id',
                                        'options' => [
                                            'placeholder' => 'select document ...',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'minimumInputLength' => 3,
                                            'ajax' => [
                                                'url' => $url,
                                                'dataType' => 'json',
                                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                            ],
                                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                            'templateResult' => new JsExpression('function(responseData) { return responseData.text; }'),
                                            'templateSelection' => new JsExpression('function (responseData) { return responseData.text; }'),
                                        ],
                                    ]);
                                ?>
                                </div>
                                <div class="col-md-2" style="padding-left:0">
                                    <button type="button" id="btn_document_create" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php Pjax::begin(['timeout'=>10000, 'id'=>'document_list']); ?>
                    <?= $this->render('dokumen', [
                        'searchModelKorinKeluarDokumen' => $searchModelKorinKeluarDokumen,
                        'dataProviderKorinKeluarDokumen' => $dataProviderKorinKeluarDokumen,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php //documentJS
    $this->registerJs('
        $("#btn_document_create").click(function() {
            id = $("#dokumen_upload_id").val();
            $.ajax({
                url: "?r=korin-keluar/dokumen",
                data: "op=create&korin_keluar_id='.$model->id.'&dokumen_upload_id="+id,
                cache: "",
                success:function(r) {
                    $("#select2-dokumen_upload_id-container").html("<span class=\"select2-selection__placeholder\">select document ...</span>");
                    $.pjax.reload({container: "#document_list", timeout: 10000});
                }
            });
        });

        $(document).on("click", ".btn_document_delete", function(){
            id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this item?")) {
                $.ajax({
                    url: "?r=korin-keluar/dokumen",
                    data: "op=delete&korin_keluar_id='.$model->id.'&korin_keluar_dokumen_id="+id,
                    cache: "",
                    success:function(r) {
                        $.pjax.reload({container: "#document_list", timeout: 10000});
                    }
                });
            }
        });
        ', \yii\web\VIEW::POS_READY
    ); 
?>