<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use frontend\models\BarangJasa;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinKeluar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-keluar-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'no_surat')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'barang_jasa_id')->dropDownList(ArrayHelper::map(BarangJasa::find()->all(), 'id', 'DropdownDisplayItem'),['prompt'=>'- select -']) ?>
<!--
    <?= $form->field($model, 'no_referensi')->textInput(['maxlength' => 50]) ?>
 -->
    <?= $form->field($model, 'pic_initial')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'yg_menandatangani')->dropDownList([''=>'- select -', 'ARL'=>'ARL', 'MIS'=>'MIS', 'MO'=>'MO', 'MIK'=>'MIK', 'GAP'=>'GAP', 'HAY'=>'HAY']) ?>

    <?= $form->field($model, 'tanggal_surat')
        ->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>
<!--
    <?= $form->field($model, 'ringkasan_isi')->textarea(['rows' => 2]) ?>
 -->
    <?= $form->field($model, 'perihal')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'tembusan')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'lampiran')->textInput(['maxlength' => 200]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
