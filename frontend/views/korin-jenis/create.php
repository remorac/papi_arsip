<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\KorinJenis */

$this->title = 'Create Jenis KORIN';
$this->params['breadcrumbs'][] = ['label' => 'Jenis KORIN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-jenis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
