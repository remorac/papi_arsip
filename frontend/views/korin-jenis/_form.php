<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\KorinJenisGroup;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinJenis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="korin-jenis-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-2 col-md-6\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => 50]) ?>
    
    <?= $form->field($model, 'korin_jenis_group_id')->dropDownList(ArrayHelper::map(KorinJenisGroup::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
