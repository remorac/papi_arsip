<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KorinJenisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis KORIN';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-jenis-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jenis KORIN', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',
            //'korin_jenis_group_id',
            [
                'attribute' => 'korinJenisGroup.nama',
                'label' => 'Group KORIN',
                'format' => 'raw',
                'value' => function($data){
                    if($data->korinJenisGroup){
                        return Html::a($data->korinJenisGroup->nama, '?r=korin-jenis-group&KorinJenisGroupSearch[nama]=' . $data->korinJenisGroup->nama);
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
