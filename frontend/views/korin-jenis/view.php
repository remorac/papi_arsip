<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\KorinJenis */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Jenis KORIN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="korin-jenis-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            'nama',
            //'korin_jenis_group_id',
            [
                'attribute' => 'korinJenisGroup.nama',
                'label' => 'Group KORIN',
                'format' => 'raw',
                'value' => Html::a(
                    $model->korinJenisGroup ? $model->korinJenisGroup->nama : '', 
                    $model->korinJenisGroup ? '?r=/korin-jenis-group/view&id=' . $model->korinJenisGroup->id : ''
                    )
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
