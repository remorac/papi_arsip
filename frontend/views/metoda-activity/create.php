<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MetodaActivity */

$this->title = 'Create Metoda - Activity';
$this->params['breadcrumbs'][] = ['label' => 'Metoda - Activity', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metoda-activity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
