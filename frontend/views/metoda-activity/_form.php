<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Metoda;
use frontend\models\Activity;

/* @var $this yii\web\View */
/* @var $model frontend\models\MetodaActivity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="metoda-activity-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-2 text-right\">{label}</div>\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-offset-2 col-md-10\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'metoda_id')->dropDownList(ArrayHelper::map(Metoda::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'activity_id')->dropDownList(ArrayHelper::map(Activity::find()->all(), 'id', 'DropdownDisplayItem')) ?>

    <?= $form->field($model, 'urutan')->textInput() ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
