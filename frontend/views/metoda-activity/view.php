<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\MetodaActivity */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Metoda - Activity', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metoda-activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => Html::a(
                    $model->metoda ? $model->metoda->nama : '', 
                    $model->metoda ? '?r=/metoda/view&id=' . $model->metoda->id : ''
                    )
            ],
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => Html::a(
                    $model->activity ? $model->activity->nama : '', 
                    $model->activity ? '?r=/activity/view&id=' . $model->activity->id : ''
                    )
            ],
            'urutan',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
