<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MetodaActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Metoda - Activity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metoda-activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Metoda - Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'activity_id',
            [
                'attribute' => 'activity.nama',
                'label' => 'Activity',
                'format' => 'raw',
                'value' => function($data){
                    if($data->activity){
                        return Html::a($data->activity->nama, '?r=activity&ActivitySearch[nama]=' . $data->activity->nama);
                    }
                },
            ],
            'urutan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
