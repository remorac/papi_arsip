<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Pengadaan';
$this->params['breadcrumbs'][] = ['label' => 'Highlight', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index box box-body box-primary">

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover paper'],
        'columns' => [
            [   
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'serial-column'],
                'contentOptions' => ['class' => 'serial-column'],
            ],

            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions'=>['class'=>'opsi-sm'],
                'value' => function($data) {
                    $opt = "<div class='btn-group' role='group'>";
                    $opt.= "&nbsp; <a class='btn btn-xs btn-default' href='?r=highlight/view&id=".$data->id."' data-pjax=0><i class='glyphicon glyphicon-eye-open text-primary'></i></a> &nbsp;";
                    $opt.="</div>";
                    return $opt;
                }
            ],

            /*[
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 70px;']
            ],*/
            [
                'attribute' => 'kode',
                'label' => 'Kode',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return $data->barangJasa->kode.str_pad($data->kode,5,"0",STR_PAD_LEFT);
                    }
                },
            ],
            'nama',
            [
                'attribute' => 'barangJasa.nama',
                'label' => 'Barang / Jasa',
                'format' => 'raw',
                'value' => function($data){
                    if($data->barangJasa){
                        return Html::a($data->barangJasa->nama, '?PengadaanSearch[barangJasa.nama]=' . $data->barangJasa->nama.'&r=worksheet-purchaser');
                    }
                },
            ],
            //'metoda_id',
            [
                'attribute' => 'metoda.nama',
                'label' => 'Metoda',
                'format' => 'raw',
                'value' => function($data){
                    if($data->metoda){
                        return Html::a($data->metoda->nama, '?r=metoda&MetodaSearch[nama]=' . $data->metoda->nama);
                    }
                },
            ],
            //'purchaser_personel_id',
            [
                'attribute' => 'personel.nama',
                'label' => 'Purchaser',
                'format' => 'raw',
                'value' => function($data){
                    if($data->purchaserPersonelProcurement){
                        return Html::a($data->purchaserPersonelProcurement->personel->nama, '?r=personel&PersonelSearch[nama]=' . $data->purchaserPersonelProcurement->personel->nama);
                    }
                },
            ],
            // 'start_plan',
            // 'requirement_date',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
