<?php 

use yii\helpers\Html;
use yii\grid\GridView;

use frontend\models\UnitPosisi;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanKorin,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => 'no_surat',
            'label' => 'No. Surat',
            'format' => 'raw',
            'value' => function($data){
                if($data->korin){
                    return '<a data-pjax=0 target="_blank" href="?r=korin/view&id='.$data->korin->id.'">'.$data->korin->no_surat.'</a>';
                }
            },
        ],
        [
            'attribute' => 'no_surat',
            'label' => 'Unit',
            'format' => 'raw',
            'value' => function($data){
                if($data->korin){
                    $arr_noSurat = explode("/", $data->korin->no_surat);
                    if (count($arr_noSurat) <= 1) $arr_noSurat = explode("-", $data->korin->no_surat);
                    if (count($arr_noSurat) > 1) {
                        $arr_reverse = array_reverse($arr_noSurat);
                        if (UnitPosisi::find()->where("kode_surat='$arr_reverse[1]'")->one())
                        return UnitPosisi::find()->where("kode_surat='$arr_reverse[1]'")->one()->unit->nama;
                    } else {
                        return "";
                    }
                }
            },
        ],
        [
            'attribute' => 'pic_initial',
            'label' => 'PIC',
            'format' => 'raw',
            'value' => function($data){
                if($data->korin){
                    return $data->korin->pic_initial;
                }
            },
        ],
    ],
]); ?>
