<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderEvent,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'opsi-xs'],
            'value' => function($data) {
                return '<span class="btn btn-xs btn-default">
                    <i id="'.$data->id.'" class="btn_event_delete glyphicon glyphicon-trash pointer text-danger"></i>
                </span>';
            }
        ],
        [
            'attribute' => 'pengadaan_id',
            'label' => 'Aktifitas',
            'format' => 'raw',
            'headerOptions' => ['width' => '150px'],
            'value' => function($data){
                if($data){
                    return $data->activity->nama;
                }
            },
        ],
        'tanggal',
        'jam',
        [
            'attribute' => 'tempat_id',
            'label' => 'Tempat',
            'format' => 'raw',
            'headerOptions' => ['width' => '150px'],
            'value' => function($data){
                if($data){
                    return $data->tempat->nama;
                }
            },
        ],
        /*[
            'attribute' => 'keterangan',
            'label' => 'Keterangan',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                	return nl2br($data->keterangan);
                }
            },
        ],*/
    ],
]); ?>
