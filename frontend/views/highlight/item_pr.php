<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\SapPr;
?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanPrItem,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table  table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],

        'purchase_requisition',
        'item_of_requisition',
        [
            'attribute' => '',
            'label' => 'Short Text',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Short Text"};
            }
        ],
        [
            'attribute' => '',
            'label' => 'Quantity Requested',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Quantity Requested"};
            }
        ],
        [
            'attribute' => '',
            'label' => 'Unit of Measure',
            'value' => function($data) {
                if ($sapPr = SapPr::find()->where("`Purchase Requisition`='".$data->purchase_requisition."' and `Item of Requisition`='".$data->item_of_requisition."'")->one())
                return $sapPr->{"Unit of Measure"};
            }
        ],
        [
            'attribute' => 'closed',
            'format' => 'raw',
            'label' => 'Status',
            'value' => function($data) {
                return $data->closed == 0 ? "<span class='text-success'>Continued</span>" : "<span class='text-danger'>Closed</span>";
            }
        ],
    ],
]); ?>