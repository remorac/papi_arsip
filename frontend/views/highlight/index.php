<?php

ini_set('memory_limit','256M');
use yii\helpers\Html;
use frontend\models\Activity;
use frontend\models\ActivityGroup;
use frontend\models\Personel;
use frontend\models\PersonelProcurement;
use frontend\models\Pengadaan;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Highlight';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index">
    
    <div class="box box-body box-primary">
        <div class="printable">

            <!-- <h1 class="text-center"><?= Html::encode($this->title) ?></h1> -->

            <!-- part 1 -->

            <?php
                /*$query = (new \yii\db\Query())
                ->select('pengadaan_id, activity_id, count(start_date) as lmt')
                ->from('pengadaan_activity')
                ->groupBy('pengadaan_id, activity_id')
                ->having('count(start_date)>=2')
                ->all();*/

                // dangerous query. will delete all records in pengadaan_activity
                /*foreach ($query as $row) {
                    $model = PengadaanActivity::find()
                        ->where("pengadaan_id = '".$row['pengadaan_id']."' and activity_id = '".$row['activity_id']."'")
                        ->orderBy("id ASC")
                        ->limit($row['lmt']-1)
                        ->all();
                    $model->delete();
                }*/
            ?>

            <h4>Total Pengadaan Per Aktifitas Procurement</h4>
            <table class="table table-bordered table-condensed">
                <tr>
                    <!-- <th>ACTIVITY GROUP</th> -->
                    <th>ACTIVITY</th>
                    <th>BARANG</th>
                    <th>JASA</th>
                    <th>BRG & JSA</th>
                    <th>LAIN-LAIN</th>
                    <th>TOTAL</th>
                </tr>
                <?php 
                    $tBrg = 0;
                    $tJsa = 0;
                    $tBnj = 0;
                    $tUmm = 0;
                    /*$activityGroup = ActivityGroup::find()->all();
                    foreach ($activityGroup as $row) {*/
                    
                    $activities = Activity::find()->all();
                    foreach ($activities as $activity) {
                        $brg = 0;
                        $jsa = 0;
                        $bnj = 0;
                        $umm = 0;

                        /*$pengadaan = Pengadaan::find()->all();
                        foreach ($pengadaan as $row1) {
                            $activity = Activity::find()->where("activity_group_id='".$row->id."'")->all();
                            foreach ($activity as $row2) {
                                $currentActivity = 0;
                                $pengadaanActivity= PengadaanActivity::find()->where("pengadaan_id='".$row1->id."' and activity_id='".$row2->id."'")->all();
                                foreach ($pengadaanActivity as $row3) {
                                    if ($row3->start_date!="0000-00-00" && ($row3->end_date=="0000-00-00" || is_null($row3->end_date)))
                                            $currentActivity = 1;
                                    if ($currentActivity == 1) { if ($row1->barangJasa->id == 1 ) $brg++; else $jsa++; }
                                }
                            }
                        }
                        $tBrg+= $brg;
                        $tJsa+= $jsa;*/

                        $brg = PengadaanActivity::find()->joinWith(['pengadaan'])->where("
                                activity_id = '".$activity->id."' 
                                and (pengadaan.barang_jasa_id = '1') 
                                and (start_date != '0000-00-00' and start_date is not null)
                                and (end_date = '0000-00-00' or end_date is null)
                                ")->groupBy("pengadaan_id")->distinct()->count();

                        $jsa = PengadaanActivity::find()->joinWith(['pengadaan'])->where("
                                activity_id = '".$activity->id."' 
                                and (pengadaan.barang_jasa_id = '2')  
                                and (start_date != '0000-00-00' and start_date is not null)
                                and (end_date = '0000-00-00' or end_date is null)
                                ")->groupBy("pengadaan_id")->distinct()->count();

                        $bnj = PengadaanActivity::find()->joinWith(['pengadaan'])->where("
                                activity_id = '".$activity->id."' 
                                and (pengadaan.barang_jasa_id = '3') 
                                and (start_date != '0000-00-00' and start_date is not null)
                                and (end_date = '0000-00-00' or end_date is null)
                                ")->groupBy("pengadaan_id")->distinct()->count();

                        $umm = PengadaanActivity::find()->joinWith(['pengadaan'])->where("
                                activity_id = '".$activity->id."' 
                                and (pengadaan.barang_jasa_id = '4')  
                                and (start_date != '0000-00-00' and start_date is not null)
                                and (end_date = '0000-00-00' or end_date is null)
                                ")->groupBy("pengadaan_id")->distinct()->count();

                        $tBrg+= $brg;
                        $tJsa+= $jsa;
                        $tBnj+= $bnj;
                        $tUmm+= $umm;
                        
                ?>
                        <tr>
                            <td><?=$activity->nama?></td>
                            <td><?php if ($brg > 0) echo Html::a($brg, ['list', 'barang_jasa_id' => 1, 'activity_group_id' => $activity->id]); else echo $brg?></td>
                            <td><?php if ($jsa > 0) echo Html::a($jsa, ['list', 'barang_jasa_id' => 2, 'activity_group_id' => $activity->id]); else echo $jsa?></td>
                            <td><?php if ($bnj > 0) echo Html::a($bnj, ['list', 'barang_jasa_id' => 3, 'activity_group_id' => $activity->id]); else echo $bnj?></td>
                            <td><?php if ($umm > 0) echo Html::a($umm, ['list', 'barang_jasa_id' => 4, 'activity_group_id' => $activity->id]); else echo $umm?></td>
                            <th><?php if ($brg+$jsa+$bnj+$umm > 0) echo Html::a($brg+$jsa+$bnj+$umm, ['list', 'activity_group_id' => $activity->id]); else echo $brg+$jsa+$bnj+$umm?></th>
                        </tr>
                <?php
                    }
                ?>

                <tr>
                    <th>TOTAL</th>
                    <th><?php if ($tBrg > 0) echo Html::a($tBrg, ['list', 'barang_jasa_id' => 1]); else echo $tBrg?></th>
                    <th><?php if ($tJsa > 0) echo Html::a($tJsa, ['list', 'barang_jasa_id' => 2]); else echo $tJsa?></th>
                    <th><?php if ($tBnj > 0) echo Html::a($tBnj, ['list', 'barang_jasa_id' => 3]); else echo $tBnj?></th>
                    <th><?php if ($tUmm > 0) echo Html::a($tUmm, ['list', 'barang_jasa_id' => 4]); else echo $tUmm?></th>
                    <th><?php if ($tBrg+$tJsa+$tBnj+$tUmm > 0) echo Html::a($tBrg+$tJsa+$tBnj+$tUmm, ['list']); else echo $tBrg+$tJsa+$tBnj+$tUmm?></th>
                </tr>
            </table>



            <!-- part 2 -->

            <br>
            <h4>Total Pengadaan Per Purchaser</h4>
            <table class="table table-bordered table-condensed">
                <tr>
                    <th rowspan="2">ACTIVITY</th>
                    <!-- <th rowspan="2">ACTIVITY GROUP</th> -->
                    <!-- <th rowspan="2">UNIT TERKAIT</th> -->
                    <th colspan="9">PURCHASER</th>
                    <th rowspan="2">TOTAL</th>
                </tr>
                <tr>
                    <?php 
                        $purchaser = PersonelProcurement::find()->where("jabatan='Purchaser'")->all();
                        foreach ($purchaser as $row) {
                            echo "<th>".Personel::find()->where(['=','id',$row->personel_id])->one()->singkatan."</th>";
                            $vTotal[$row->personel_id] = 0;
                        }
                    ?>
                </tr>
                <?php 
                    $gTotal = 0;
                    /*$activityGroup = ActivityGroup::find()->where("id>='2' and id<='5'")->all();
                    foreach ($activityGroup as $row) {*/
                    $activities = Activity::find()->all();
                    foreach ($activities as $activity) {
                ?>
                <tr>
                    <td><?=$activity->nama?></td>
                    <!-- <td><?php //echo $row->unit_terkait?></td> -->
                    <?php 
                        /*$hTotal = 0;
                        $purchaser = PersonelProcurement::find()->where("jabatan='Purchaser'")->all();
                        foreach ($purchaser as $row1) {
                            $jml = 0;
                            $pengadaan = Pengadaan::find()->where("purchaser_personel_procurement_id='".$row1->id."'")->all();
                            foreach ($pengadaan as $row2) {
                                $activity = Activity::find()->where("activity_group_id='".$row->id."'")->all();
                                foreach ($activity as $row3) {
                                    $currentActivity = 0;
                                    $pengadaanActivity= PengadaanActivity::find()->where("pengadaan_id='".$row2->id."' and activity_id='".$row3->id."'")->all();
                                    foreach ($pengadaanActivity as $row4) {
                                        if ($row4->start_date!="0000-00-00" && ($row4->end_date=="0000-00-00" || is_null($row4->end_date)))
                                                $currentActivity = 1;
                                        if ($currentActivity == 1) $jml++;
                                    }
                                }
                            }
                            ?><td><?php if ($jml > 0) echo Html::a($jml, ['list', 'purchaser_personel_procurement_id' => $row1->id]); else echo $jml?></td><?php
                            $hTotal+=$jml;
                            $vTotal[$row1->personel_id]+=$jml;
                        }*/
                    ?>
                    
                        <?php 
                        //if ($hTotal > 0) echo Html::a($hTotal, ['list', 'activity_group_id' => $row->id]); else echo $hTotal;
                        $hTotal = 0;
                        foreach ($purchaser as $row) {
                            $jml = PengadaanActivity::find()->joinWith(['pengadaan'])->where("
                                    activity_id = '".$activity->id."' 
                                    and pengadaan.purchaser_personel_procurement_id = '".$row->id."' 
                                    and (start_date != '0000-00-00' and start_date is not null)
                                    and (end_date = '0000-00-00' or end_date is null)
                                    ")->groupBy("pengadaan_id")->distinct()->count()."</th>";
                            echo "<td>".$jml."</td>";
                            // var_dump($hTotal); var_dump($jml); die();
                            $hTotal+= intval($jml);
                            $vTotal[$row->personel_id]+= intval($jml);
                        }
                        ?>
                        <th><?=$hTotal?></th>
                </tr>
                <?php 
                    $gTotal+=$hTotal;
                    }
                ?>

                <tr>
                    <th colspan="">TOTAL ONHAND DOC PROC PROCESS</th>
                    <?php 
                        $purchaser = PersonelProcurement::find()->where("jabatan='Purchaser'")->all();
                        foreach ($purchaser as $row) {
                            ?><th><?php if ($vTotal[$row->personel_id] > 0) echo Html::a($vTotal[$row->personel_id], ['list', 'purchaser_personel_procurement_id' => $row->id]); else echo $vTotal[$row->personel_id]?></th><?php
                        }
                    ?>
                    <th>
                        <?php if ($gTotal > 0) echo Html::a($gTotal, ['list']); else echo $gTotal?>
                    </th>
                </tr>
            </table>

        </div>
    </div>

</div>

<?php /*
    echo "<h4><br>Memory Usage";
    echo "<br><small><b>".number_format((memory_get_peak_usage(true)/1024/1024),'2')." MiB | </b>allocated by system";
    echo "<br><b>".number_format((memory_get_peak_usage(false)/1024/1024),'2')." MiB | </b>used by script</small></h4>";
*/?>