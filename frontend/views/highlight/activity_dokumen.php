<?php 

use yii\helpers\Html;
use frontend\models\PengadaanActivityDokumen;

?>                    

<table class="table  table-supercondensed table-bordered" style="margin-bottom: 0px;">
    <tr>
        <th>NO</th>
        <th>Jenis</th>
        <th>File</th>
        <th>No. Dokumen</th>
        <th>Tanggal</th>
    </tr>
    <?php
        $i = 0;
        $pengadaanActivityDokumen = PengadaanActivityDokumen::find()->where(['pengadaan_activity_id' => $pengadaanActivity_id])->all();
        foreach ($pengadaanActivityDokumen as $row2) {
            $i++;
    ?>
    <tr>
        <td><?=$i?></td>
        <td><?=$row2->dokumenUpload->dokumenJenis->nama?></td>
        <td><?=Html::a($row2->dokumenUpload->pdf_filename, 'document/'.$row2->dokumenUpload->pdf_filename, ['target'=>'blank', 'data-pjax'=>0]) ?></td>
        <td><?=Html::a($row2->dokumenUpload->no_dokumen, '?r=dokumen-upload/view&id='.$row2->dokumenUpload->id) ?></td>
        <td><?=$row2->dokumenUpload->tgl_dokumen?></td>
    </tr>
    <?php } ?>
</table>