<?php 

use yii\helpers\Html;
use yii\grid\GridView;

use frontend\models\SapPo;

?>

<?= GridView::widget([
    'dataProvider' => $dataProviderPengadaanPo,
    'filterModel' => null,
    'tableOptions' => ['class' => 'table table-hover paper'],
    'columns' => [
        [   
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'serial-column'],
            'contentOptions' => ['class' => 'serial-column'],
        ],
        
        [
            'attribute' => 'purchasing_document',
            'label' => 'Purchasing Document Number',
            'format' => 'raw',
            'value' => function($data){
                if($data){
                    return $data->purchasing_document;
                }
            },
        ],
        'contract_reference',
        [
            'attribute' => '',
            'label' => 'Vendor',
            'value' => function($data) {
                if ($sapPr = SapPo::find()->where("`Purchasing Document`='".$data->purchasing_document."'")->one())
                return $sapPr->{"Vendor/supplying plant"};
            }
        ],
        [
            'attribute' => '',
            'label' => 'Document Date',
            'value' => function($data) {
                if ($sapPr = SapPo::find()->where("`Purchasing Document`='".$data->purchasing_document."'")->one())
                return $sapPr->{"Document Date"};
            }
        ],
    ],
]); ?>
