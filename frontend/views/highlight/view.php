<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use yii\web\JsExpression;
use kartik\select2\Select2;

use frontend\models\LevelApproval;
use frontend\models\DurasiStandar;
use frontend\models\Activity;
use frontend\models\DokumenJenis;
use frontend\models\ActivityDokumen;
use frontend\models\MetodaActivity;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanKorin;
use frontend\models\Korin;
use frontend\models\Unit;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;
use frontend\models\Pengadaan;
use frontend\models\Tempat;
use frontend\models\Event;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pengadaan */

//temporary handling error for setting planned duration
if ($model->nilai_oe == null) $model->nilai_oe = 0;

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Purchaser', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-view box box-body box-info">

    <?php if (Yii::$app->user->can("/*") || Yii::$app->user->can("/worksheet-purchaser/*")) { ?>
        <p>
            <?= Html::a(
                'Edit in Worksheet', 
                '?r=worksheet-purchaser/view&id='.$model->id, 
                [ 'class' => 'btn btn-warning']); 
            ?>
        </p>
    <?php } ?>

    <div class="nav-tabs-custom paper">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#pengadaan" aria-controls="home" role="tab" data-toggle="tab">Pengadaan</a></li>
            <li role="presentation"><a href="#korin-pp" aria-controls="profile" role="tab" data-toggle="tab">KORIN PP</a></li>
            <!-- <li role="presentation"><a href="#pengadaan-item" aria-controls="profile" role="tab" data-toggle="tab">Item Pengadaan</a></li> -->
            <li role="presentation"><a href="#item-pr" aria-controls="profile" role="tab" data-toggle="tab">Item PR</a></li>
            <li role="presentation"><a href="#purchase-order" aria-controls="messages" role="tab" data-toggle="tab">Purchase Order</a></li>
            <li role="presentation"><a href="#activity" aria-controls="settings" role="tab" data-toggle="tab">Procurement Cycle Time</a></li>
            <li role="presentation"><a href="#activity-detail" aria-controls="settings" role="tab" data-toggle="tab">Documents</a></li>
            <li role="presentation"><a href="#today-status" aria-controls="settings" role="tab" data-toggle="tab">Issue (Today Status)</a></li>
            <li role="presentation"><a href="#event" aria-controls="settings" role="tab" data-toggle="tab">Event</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="pengadaan">
                
                <?= DetailView::widget([
                    'model' => $model,
                    'options'=> ['class' => 'table detail-view  table-bordered'],
                    'template' => "<tr><th width='138px'>{label}</th><td>{value}</td></tr>",
                    'attributes' => [
                        //'id',
                        [
                            'attribute' => 'kode',
                            'label' => 'Kode',
                            'format' => 'raw',
                            'value' => $model->barangJasa->kode.str_pad($model->kode,5,"0",STR_PAD_LEFT)
                        ],
                        'nama',
                        //'barang_jasa_id',
                        [
                            'attribute' => 'barangJasa.nama',
                            'label' => 'Barang/Jasa',
                        ],
                        //'metoda_id',
                        [
                            'attribute' => 'metoda.nama',
                            'label' => 'Metoda',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->metoda ? $model->metoda->nama : '', 
                                $model->metoda ? '?r=/metoda/view&id=' . $model->metoda->id : ''
                                )
                        ],
                        //'level_approval_id',
                        [
                            'attribute' => 'levelApproval.approver',
                            'label' => 'Level Approval',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->levelApproval ? $model->levelApproval->approver : '', 
                                $model->levelApproval ? '?r=/level-approval/view&id=' . $model->levelApproval->id : ''
                                )
                        ],
                        'nilai_oe:integer',
                        [
                            'attribute' => 'purchaserPersonelProcurement.personel.nama',
                            'label' => 'Purchaser',
                            'format' => 'raw',
                            'value' => Html::a(
                                $model->purchaserPersonelProcurement ? $model->purchaserPersonelProcurement->personel->nama : '', 
                                $model->purchaserPersonelProcurement ? '?r=/personel/view&id=' . $model->purchaserPersonelProcurement->id : ''
                                )
                        ],
                        'start_plan:date',
                        'requirement_date:date',
                        'po_reference',
                        'delivery_time',
                        'isi_disposisi',
                        /*'created_at',
                        'updated_at',
                        'created_by',
                        'updated_by',*/
                    ],
                ]) ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="korin-pp">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'korin-pp_list']); ?>
                    <?= $this->render('korin', [
                        'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
                        'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="pengadaan-item">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengadaan-item_list']); ?>
                    <?= $this->render('pengadaan-item', [
                        'searchModelPengadaanItem' => $searchModelPengadaanItem,
                        'dataProviderPengadaanItem' => $dataProviderPengadaanItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="item-pr">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'item-pr_list']); ?>
                    <?= $this->render('item_pr', [
                        'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
                        'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="purchase-order">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'purchase-order_list']); ?>
                    <?= $this->render('purchase_order', [
                        'searchModelPengadaanPo' => $searchModelPengadaanPo,
                        'dataProviderPengadaanPo' => $dataProviderPengadaanPo,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="activity">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity_list']); ?>
                    <?= $this->render('activity', [
                        'model' => $model,
                        'alerts' => $alerts,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <!-- activity detail -->
            <div role="tabpanel" class="tab-pane" id="activity-detail">

                <big>Dokumen Upload</big>
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'activity-detail_list']); ?>
                    <?= $this->render('activity_detail', [
                        'model' => $model,
                        'alerts' => $alerts,
                    ]) ?>
                <?php Pjax::end(); ?>

                <br>
                <big>Pengaturan Dokumen</big>
                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#notreqdoc_form" style="padding: 7px 10px !important">
                        <span class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i> &nbsp; Jenis Dokumen yang Tidak Diperlukan</span>
                    </div>
                    <div id="notreqdoc_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengaturan-dokumen_list']); ?>
                                <?= $this->render('pengaturan_dokumen', [
                                    'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,
                                ]) ?>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-success" style="margin-bottom:5px">
                    <div class="panel-heading pointer" data-toggle="collapse" href="#verify_form" style="padding: 7px 10px !important">
                        <span class="panel-title"><i class="glyphicon glyphicon-ok-circle"></i> &nbsp; Verifikasi Dokumen </span>
                    </div>
                    <div id="verify_form" class="panel-collapse collapse">
                        <div class="panel-body" style="background:#fafafa">
                            <div class="">
                                <?php $dv = Pengadaan::findOne($model->id)->documents_verification; ?>
                                <?php Pjax::begin(['timeout'=>10000, 'id'=>'documents_verification']); ?>
                                    <?php 
                                        echo $this->render('documents_verification', [
                                            'dv' => $dv
                                        ]);
                                    ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- activity detail -->

            <div role="tabpanel" class="tab-pane" id="today-status">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'pengadaan-issue_list']); ?>
                    <?= $this->render('today_status', [
                        'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,
                    ]) ?>
                <?php Pjax::end(); ?>
            </div>

            <div role="tabpanel" class="tab-pane" id="event">
                <?php Pjax::begin(['timeout'=>10000, 'id'=>'event_list']); ?>
                    <?= $this->render('event', [
                        'dataProviderEvent' => $dataProviderEvent,
                    ]) ?>
                <?php Pjax::end(); ?>

            </div>

        </div>
    </div>
</div>
