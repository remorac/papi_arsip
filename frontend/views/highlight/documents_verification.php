<?php 
    if ($dv == 1) {
        ?>
            <div class='alert alert-success'>
                <b>Documents Verification: <br> <big class='text-success'>VERIFIED</big></b>
            </div>
        <?php
    } else {
        ?>
            <div class='alert alert-danger'>
                <b>Documents Verification: <br> <big class='text-danger'>NOT VERIFIED</big></b>
            </div>
        <?php
    }
?>