<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */

$this->title = 'Create Folder';
$this->params['breadcrumbs'][] = ['label' => 'Uncategorized', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-default">

	<div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="form-group">
            <label>Create New Folder in <code><?= $folder ?>/</code></label>
        <?php 
            echo Html::textInput('newpath', null, ['class' => 'form-control']);
        ?>
        </div>

        <div class="form-group">
            <!-- <div class="col-md-12"> -->
                <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
            <!-- </div> -->
        </div>

        <?php ActiveForm::end(); ?>
	</div>
</div>