<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\SapPr;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanPrItem */

$this->title = $model->pengadaan->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - Item PR', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-pr-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options'=> ['class' => 'table  detail-view paper'],
        'attributes' => [
            'id',
            //'pengadaan_id',
            [
                'attribute' => 'pengadaan_id',
                'label' => 'No. KORIN',
                'format' => 'raw',
                'value' => Html::a(
                    $model->pengadaan ? $model->pengadaan->nama : '', 
                    $model->pengadaan ? '?r=/korin/view&id=' . $model->pengadaan->id
                    : ''),
            ],
            'purchase_requisition',
            //'item_of_requisition',
            [
                'attribute' => 'item_of_requisition',
                'label' => 'Item of Requisition',
                'format' => 'raw',
                'value' => $model->item_of_requisition.']'
                    .' <i>'.$model->sapPr->{'Short Text'}
                    .' ('.$model->sapPr->{'Quantity Requested'}." ".$model->sapPr->{'Unit of Measure'}.")</i>"
            ],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
