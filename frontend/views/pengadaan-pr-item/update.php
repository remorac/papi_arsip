<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanPrItem */

$this->title = 'Update Pengadaan - Item PR: ' . ' ' . $model->pengadaan->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - Item PR', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pengadaan->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengadaan-pr-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
