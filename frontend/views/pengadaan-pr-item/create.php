<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PengadaanPrItem */

$this->title = 'Create Pengadaan - Item PR';
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan - Item PR', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-pr-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
