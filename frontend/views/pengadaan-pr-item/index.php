<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\SapPr;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PengadaanPrItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan - Item PR';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-pr-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengadaan - Item PR', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table  table-hover paper'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'pengadaan_id',
            [
                'label' => 'Pengadaan',
                'format' => 'raw',
                'value' => function($data){
                    if($data->pengadaan){
                        return Html::a($data->pengadaan->nama, '?r=pengadaan/view&id='.$data->pengadaan->id);
                    }
                },
            ],
            'purchase_requisition',
            //'item_of_requisition',
            [
                'label' => 'Item of Requisition',
                'format' => 'raw',
                'value' => function($data){
                    if($data->sapPr){
                        return $data->item_of_requisition.']'
                    .' <i>'.$data->sapPr->{'Short Text'}
                    .' ('.$data->sapPr->{'Quantity Requested'}." ".$data->sapPr->{'Unit of Measure'}.")</i>";
                    }
                },
            ],
            'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
