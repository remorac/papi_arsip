<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kpi_durasi_report".
 *
 * @property string $tahun_bulan
 * @property integer $jenis
 * @property string $durasi_plan
 * @property string $durasi_real
 * @property string $index
 * @property integer $status
 */
class KpiDurasiReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_durasi_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun_bulan', 'jenis'], 'required'],
            [['jenis', 'status'], 'integer'],
            [['durasi_plan', 'durasi_real', 'index'], 'number'],
            [['tahun_bulan'], 'string', 'max' => 7]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun_bulan' => Yii::t('app', 'Tahun-Bulan'),
            'jenis' => Yii::t('app', 'Jenis'),
            'durasi_plan' => Yii::t('app', 'Durasi Plan'),
            'durasi_real' => Yii::t('app', 'Durasi Real'),
            'index' => Yii::t('app', 'Index'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function refreshKpiDurasi()
    {
        $connection = Yii::$app->db;
        
        try {
            $command = $connection->createCommand();
            $command->truncateTable('kpi_durasi_report');
            $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
        
        $sql = 'insert into kpi_durasi_report
select *
    , @index := durasi_real / durasi_plan `index`
    , if(@index <= 1, 1, 0) status
from (
    select date_format(pengadaan_activity.end_date, "%Y-%m") tahun_bulan
        , pengadaan.barang_jasa_id jenis
        , @durasi_plan := sum(pengadaan_durasi_adjusted.durasi) durasi_plan
        , @durasi_real := sum(
            datediff(pengadaan_activity.end_date, sap_po.tgl_terima)
            - count_sabtu_minggu(pengadaan_activity.end_date, sap_po.tgl_terima)
            - count_libur(pengadaan_activity.end_date, sap_po.tgl_terima)
        ) durasi_real
    from pengadaan_po
    inner join (
        select `Purchasing Document`
            , `Document Date`
            , korin.tgl_terima
        from sap_po
            inner join pengadaan_po on pengadaan_po.purchasing_document = sap_po.`Purchasing Document`
            inner join pengadaan_korin on pengadaan_korin.pengadaan_id = pengadaan_po.pengadaan_id    
            inner join korin on korin.id = pengadaan_korin.korin_id
        group by `Purchasing Document`
        ) sap_po on pengadaan_po.purchasing_document = sap_po.`Purchasing Document`
    inner join pengadaan on pengadaan.id = pengadaan_po.pengadaan_id
    inner join pengadaan_activity on pengadaan_activity.pengadaan_id = pengadaan.id
    inner join pengadaan_durasi_adjusted on pengadaan_durasi_adjusted.pengadaan_id = pengadaan.id 
        and pengadaan_durasi_adjusted.activity_id = pengadaan_activity.activity_id
    where pengadaan_activity.activity_id = 11
    group by date_format(sap_po.tgl_terima, "%Y-%m")
        , pengadaan.barang_jasa_id
) q';
        
        try {
            $command = $connection->createCommand($sql);
            return $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
    }



    public function getTeksJenis()
    {
        switch($this->jenis){
            case 0:
                return 'Jasa';
            case 1:
                return 'Barang';
        }
        return null;
    }

    public function getTeksStatus()
    {
        switch($this->status){
            case 0:
                return 'Bad';
            case 1:
                return 'OK';
        }
        return null;
    }

}
