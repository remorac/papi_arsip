<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "gas_circulation_tube".
 *
 * @property integer $id
 * @property integer $gas_circulation_id
 * @property integer $gas_tube_id
 * @property integer $count
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property GasCirculation $gasCirculation
 * @property GasTube $gasTube
 */
class GasCirculationTube extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gas_circulation_tube';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gas_circulation_id', 'gas_tube_id', 'count'], 'required'],
            [['gas_circulation_id', 'gas_tube_id', 'count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gas_circulation_id' => 'Gas Circulation ID',
            'gas_tube_id' => 'No. Tabung',
            'count' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasCirculation()
    {
        return $this->hasOne(GasCirculation::className(), ['id' => 'gas_circulation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasTube()
    {
        return $this->hasOne(GasTube::className(), ['id' => 'gas_tube_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasType()
    {
        return $this->hasOne(GasType::className(), ['id' => 'gas_type_id'])
                    ->via('gasTube');
    }
}
