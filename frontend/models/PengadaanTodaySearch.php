<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Pengadaan;

/**
 * PengadaanSearch represents the model behind the search form about `frontend\models\Pengadaan`.
 */
class PengadaanTodaySearch extends Pengadaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'barang_jasa_id', 'metoda_id', 'purchaser_personel_procurement_id', 'level_approval_id', 'nilai_oe', 'delivery_time', 'documents_verification', 'created_by', 'updated_by'], 'integer'],
            [['kode', 'nama', 'start_plan', 'requirement_date', 'created_at', 'updated_at'], 'safe'],
            [
                [
                    'personel.nama', 
                    'metoda.nama', 
                    'barangJasa.nama', 
                    'activity.activity_group_id', 
                    'pengadaan_activity.start_date', 
                    'pengadaan_activity.end_date', 
                    'pengadaan_durasi_adjusted.durasi',
                    'personel.singkatan'
                ], 
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'personel.nama' => Yii::t('app', 'Personel'),
            'personel.singkatan' => Yii::t('app', 'PIC'),
            'metoda.nama' => Yii::t('app', 'Metoda'),
            'barangJasa.nama' => Yii::t('app', 'Barang/Jasa'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengadaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['personel.singkatan'] = [
              'asc' => ['personel.singkatan' => SORT_ASC],
              'desc' => ['personel.singkatan' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['metoda.nama'] = [
              'asc' => ['metoda.nama' => SORT_ASC],
              'desc' => ['metoda.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['barangJasa.nama'] = [
              'asc' => ['barang_jasa.nama' => SORT_ASC],
              'desc' => ['barang_jasa.nama' => SORT_DESC],
        ];

        $query->joinWith(['purchaserPersonelProcurement.personel']);
        $query->joinWith(['metoda']);
        $query->joinWith(['barangJasa']);
        $query->joinWith(['pengadaanActivities']);
        //$query->leftJoin("pengadaan_activity", "pengadaan.id = pengadaan_activity.pengadaan_id");
        //$query->leftJoin("activity", "pengadaan_activity.activity_id = activity.id");
        //$query->leftJoin("activity_group", "activity.activity_group_id = activity_group.id");
        //$query->leftJoin("pengadaan_durasi_adjusted", "pengadaan.id = pengadaan_durasi_adjusted.pengadaan_id and activity.id = pengadaan_durasi_adjusted.activity_id");

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'barang_jasa_id' => $this->barang_jasa_id,
            'metoda_id' => $this->metoda_id,
            'purchaser_personel_procurement_id' => $this->purchaser_personel_procurement_id,
            'nilai_oe' => $this->nilai_oe,
            'start_plan' => $this->start_plan,
            'requirement_date' => $this->requirement_date,
            'delivery_time' => $this->delivery_time,
            'documents_verification' => $this->documents_verification,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activity.activity_group_id' => $this->getAttribute('activity.activity_group_id')
        ]);

        $query->andFilterWhere(['like', 'pengadaan.kode', $this->kode])
            ->andFilterWhere(['like', 'pengadaan.nama', $this->nama])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personel.nama')])
            ->andFilterWhere(['like', 'personel.singkatan', $this->getAttribute('personel.singkatan')])
            ->andFilterWhere(['like', 'metoda.nama', $this->getAttribute('metoda.nama')])
            ->andFilterWhere(['like', 'barang_jasa.nama', $this->getAttribute('barangJasa.nama')])
            ->andFilterWhere(['=', 'if(pengadaan_activity.start_date is null, "null", "!null")', $this->getAttribute('pengadaan_activity.start_date')])
            ->andFilterWhere(['=', 'if(pengadaan_activity.end_date is null, "null", "!null")', $this->getAttribute('pengadaan_activity.end_date')])
            ->andFilterWhere(['=', 'if(pengadaan_durasi_adjusted.durasi=0, "null", "!null")', $this->getAttribute('pengadaan_durasi_adjusted.durasi')]);

        /*if ($this->getAttribute('pengadaan_activity.end_date') == "null") {
            $query->andFilterWhere(['pengadaan_activity.end_date' => null]);
        }*/
        $query->groupBy("pengadaan.id");
        $query->orderBy("pengadaan.start_plan DESC");

        //echo $query->createCommand()->sql; die();
         
        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), 
        [  
            'personel.nama', 
            'personel.singkatan', 
            'metoda.nama', 
            'barangJasa.nama', 
            'activity.activity_group_id', 
            'pengadaan_activity.start_date', 
            'pengadaan_activity.end_date', 
            'pengadaan_durasi_adjusted.durasi'
        ]);
    }
}
