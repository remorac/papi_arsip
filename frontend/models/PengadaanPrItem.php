<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "pengadaan_pr_item".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property string $purchase_requisition
 * @property integer $item_of_requisition
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pengadaan $pengadaan
 * @property User $createdBy
 * @property User $updatedBy
 */
class PengadaanPrItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_pr_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'purchase_requisition', 'item_of_requisition'], 'required'],
            [['pengadaan_id', 'item_of_requisition', 'closed', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['purchase_requisition'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pengadaan_id' => Yii::t('app', 'Pengadaan ID'),
            'purchase_requisition' => Yii::t('app', 'Purchase Requisition'),
            'item_of_requisition' => Yii::t('app', 'Item Of Requisition'),
            'closed' => Yii::t('app', 'Closed'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSapPr()
    {
        return $this->hasOne(SapPr::className(), ['Purchase Requisition' => 'purchase_requisition', 'Item of Requisition' => 'item_of_requisition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanPo()
    {
        return $this->hasMany(PengadaanPo::className(), ['pengadaan_id' => 'id'])
                ->via("pengadaan");
    }
    public function getSapPo()
    {
        return $this->hasMany(SapPo::className(), ['Purchasing Document' => 'purchasing_document'])
                ->via("pengadaanPo");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
