<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "arsip_file".
 *
 * @property integer $id
 * @property string $name
 * @property integer $arsip_unit_id
 * @property integer $arsip_file_category_id
 * @property string $purchasing_document
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ArsipUnit $arsipUnit
 * @property ArsipFileCategory $arsipFileCategory
 * @property User $createdBy
 * @property User $updatedBy
 */
class ArsipFile extends \yii\db\ActiveRecord
{
    public $pdfFile;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arsip_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'arsip_unit_id'], 'required'],
            [['arsip_unit_id', 'arsip_file_category_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'purchasing_document'], 'string', 'max' => 191],
            [['arsip_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArsipUnit::className(), 'targetAttribute' => ['arsip_unit_id' => 'id']],
            [['arsip_file_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArsipFileCategory::className(), 'targetAttribute' => ['arsip_file_category_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['pdfFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'mimeTypes' => 'application/pdf'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'arsip_unit_id' => 'Unit',
            'arsip_file_category_id' => 'File Category',
            'purchasing_document' => 'Purchasing Document',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArsipUnit()
    {
        return $this->hasOne(ArsipUnit::className(), ['id' => 'arsip_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArsipFileCategory()
    {
        return $this->hasOne(ArsipFileCategory::className(), ['id' => 'arsip_file_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    public function upload()
    {
        $pdfFile    = $this->pdfFile;
        $path_unit  = Yii::getAlias('@uploads/arsip-file/'.$this->arsipUnit->name.'/');
        
        if (!is_dir($path_unit)) {
            mkdir($path_unit, 0775, true);
        }

        $filepath = $path_unit . $pdfFile->baseName . '.' . $pdfFile->extension;
        $this->name = $pdfFile->baseName . '.' . $pdfFile->extension;
        if (file_exists($filepath)) {
            $filepath.= '-'.date('YmdHis').'.'.$pdfFile->extension;
            $this->name = $pdfFile->baseName . '-'.date('YmdHis').'.'. $pdfFile->extension;
        }
        $pdfFile->saveAs($filepath); 
        return true;
    }
}
