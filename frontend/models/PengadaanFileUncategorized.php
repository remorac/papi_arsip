<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "pengadaan_files_uncategorized".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property string $filename
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pengadaan $pengadaan
 * @property User $createdBy
 * @property User $updatedBy
 */
class PengadaanFileUncategorized extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_file_uncategorized';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'filename'], 'required'],
            [['pengadaan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['filename'], 'string'],
            [['pengadaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pengadaan::className(), 'targetAttribute' => ['pengadaan_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pengadaan_id' => 'Pengadaan',
            'filename' => 'Filename',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
