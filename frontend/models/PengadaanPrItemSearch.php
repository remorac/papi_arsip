<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PengadaanPrItem;

/**
 * PengadaanPrItemSearch represents the model behind the search form about `frontend\models\PengadaanPrItem`.
 */
class PengadaanPrItemSearch extends PengadaanPrItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_id', 'item_of_requisition', 'closed', 'created_by', 'updated_by'], 'integer'],
            [['purchase_requisition', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengadaanPrItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_id' => $this->pengadaan_id,
            'item_of_requisition' => $this->item_of_requisition,
            'closed' => $this->closed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'purchase_requisition', $this->purchase_requisition]);

        return $dataProvider;
    }
}
