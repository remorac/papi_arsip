<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "event_gcal".
 *
 * @property string $id
 * @property integer $event_id
 * @property integer $gcal_action_id
 * @property resource $gcal
 *
 * @property Event $event
 * @property GcalAction $gcalAction
 */
class EventGcal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_gcal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'gcal_action_id'], 'integer'],
            [['gcal_action_id'], 'required'],
            [['gcal'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'gcal_action_id' => Yii::t('app', 'Gcal Action ID'),
            'gcal' => Yii::t('app', 'Gcal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcalAction()
    {
        return $this->hasOne(GcalAction::className(), ['id' => 'gcal_action_id']);
    }
}
