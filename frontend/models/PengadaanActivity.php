<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "pengadaan_activity".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property integer $activity_id
 * @property string $start_date
 * @property string $end_date
 * @property string $keterangan
 * @property string $kendala
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pengadaan $pengadaan
 * @property Activity $activity
 * @property User $createdBy
 * @property User $updatedBy
 */
class PengadaanActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'activity_id'], 'required'],
            [['pengadaan_id', 'activity_id', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pengadaan_id' => Yii::t('app', 'Pengadaan ID'),
            'activity_id' => Yii::t('app', 'Activity ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanDurasiAdjusted()
    {
    	return $this->hasOne(PengadaanDurasiAdjusted::className(), ['activity_id' => 'activity_id', 'pengadaan_id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanIssues()
    {
        return $this->hasMany(PengadaanIssue::className(), ['pengadaan_activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanActivityDokumens()
    {
        return $this->hasMany(PengadaanActivityDokumen::className(), ['pengadaan_activity_id' => 'id']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->pengadaan->nama.' : '.$this->activity->nama.' : ' . $this->id;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
