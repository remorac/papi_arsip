<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PplDetail;

/**
 * PplDetailSearch represents the model behind the search form about `frontend\models\PplDetail`.
 */
class PplDetailSearch extends PplDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['tgl_permintaan_barang', 'no_ppl', 'requestor', 'area', 'deskripsi', 'satuan', 'tool_non', 'status_pengadaan', 'status_order', 'status_gr', 'status_gi_stock', 'status_pembayaran', 'buyer', 'vendor_rfq', 'supplier', 'tgl_terima_barang', 'penerima_barang', 'lokasi_penyimpanan', 'tgl_pengambilan', 'pengambil_barang', 'tgl_terima_invoice', 'no_korin', 'tgl_diserahkan_ke_afis', 'tgl_pembayaran', 'keterangan', 'no_invoice', 'area_detail', 'sm', 'editor_data', 'no_release', 'no_wbs'], 'safe'],
            [['qty', 'barang_datang', 'kekurangan_brg', 'jml_sdh_diambil', 'stock_di_kontainer', 'price', 'total_price', 'real_time_days', 'harga_invoice', 'selisih'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PplDetail::find();
        $query->where(['deleted_at' => null]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_permintaan_barang' => $this->tgl_permintaan_barang,
            'qty' => $this->qty,
            'barang_datang' => $this->barang_datang,
            'kekurangan_brg' => $this->kekurangan_brg,
            'tgl_terima_barang' => $this->tgl_terima_barang,
            'jml_sdh_diambil' => $this->jml_sdh_diambil,
            'tgl_pengambilan' => $this->tgl_pengambilan,
            'stock_di_kontainer' => $this->stock_di_kontainer,
            'tgl_terima_invoice' => $this->tgl_terima_invoice,
            'price' => $this->price,
            'total_price' => $this->total_price,
            'tgl_diserahkan_ke_afis' => $this->tgl_diserahkan_ke_afis,
            'tgl_pembayaran' => $this->tgl_pembayaran,
            'real_time_days' => $this->real_time_days,
            'harga_invoice' => $this->harga_invoice,
            'selisih' => $this->selisih,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_ppl', $this->no_ppl])
            ->andFilterWhere(['like', 'requestor', $this->requestor])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'tool_non', $this->tool_non])
            ->andFilterWhere(['like', 'status_pengadaan', $this->status_pengadaan])
            ->andFilterWhere(['like', 'status_order', $this->status_order])
            ->andFilterWhere(['like', 'status_gr', $this->status_gr])
            ->andFilterWhere(['like', 'status_gi_stock', $this->status_gi_stock])
            ->andFilterWhere(['like', 'status_pembayaran', $this->status_pembayaran])
            ->andFilterWhere(['like', 'buyer', $this->buyer])
            ->andFilterWhere(['like', 'vendor_rfq', $this->vendor_rfq])
            ->andFilterWhere(['like', 'supplier', $this->supplier])
            ->andFilterWhere(['like', 'penerima_barang', $this->penerima_barang])
            ->andFilterWhere(['like', 'lokasi_penyimpanan', $this->lokasi_penyimpanan])
            ->andFilterWhere(['like', 'pengambil_barang', $this->pengambil_barang])
            ->andFilterWhere(['like', 'no_korin', $this->no_korin])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'no_invoice', $this->no_invoice])
            ->andFilterWhere(['like', 'area_detail', $this->area_detail])
            ->andFilterWhere(['like', 'sm', $this->sm])
            ->andFilterWhere(['like', 'editor_data', $this->editor_data])
            ->andFilterWhere(['like', 'no_release', $this->no_release])
            ->andFilterWhere(['like', 'no_wbs', $this->no_wbs]);

        return $dataProvider;
    }
}
