<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "gas_tube".
 *
 * @property integer $id
 * @property string $nomor
 * @property integer $gas_owner_id
 * @property integer $gas_type_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property GasOwner $gasOwner
 * @property GasType $gasType
 * @property GasTubeCirculation[] $gasTubeCirculations
 */
class GasTube extends \yii\db\ActiveRecord
{
    
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gas_tube';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor', 'count'], 'required'],
            [['count', 'gas_owner_id', 'gas_type_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nomor'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor' => 'Nomor',
            'count' => 'Jumlah',
            'gas_owner_id' => 'Gas Owner ID',
            'gas_type_id' => 'Gas Type ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasOwner()
    {
        return $this->hasOne(GasOwner::className(), ['id' => 'gas_owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasType()
    {
        return $this->hasOne(GasType::className(), ['id' => 'gas_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasCirculationTubes()
    {
        return $this->hasMany(GasCirculationTube::className(), ['gas_tube_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropdownDisplayItem()
    {
        return $this->nomor . '  (Isi: ' . $this->gasType->name . ', Owner: ' . $this->gasOwner->name . ') ';
    }
}
