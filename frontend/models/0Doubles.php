<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "0_doubles".
 *
 * @property integer $ordering
 * @property integer $id
 * @property string $nama
 * @property string $updated_at
 * @property integer $c_pct
 * @property integer $c_doc
 * @property integer $c_krn
 */
class Doubles0 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '0_doubles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering', 'id', 'c_pct', 'c_doc', 'c_krn'], 'integer'],
            [['updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ordering' => 'Ordering',
            'id' => 'ID',
            'nama' => 'Nama',
            'updated_at' => 'Updated At',
            'c_pct' => 'C Pct',
            'c_doc' => 'C Doc',
            'c_krn' => 'C Krn',
        ];
    }
}
