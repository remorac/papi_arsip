<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_vendor".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PplKorinBayar[] $pplKorinBayars
 * @property PplWa[] $pplWas
 */
class PplVendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplKorinBayars()
    {
        return $this->hasMany(PplKorinBayar::className(), ['ppl_vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplWas()
    {
        return $this->hasMany(PplWa::className(), ['ppl_vendor_id' => 'id']);
    }
}
