<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "dev_procurement".
 *
 * @property integer $id
 * @property string $kode
 * @property integer $barang
 * @property integer $user_purchaser_id
 * @property string $perihal
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property DevEvent[] $devEvents
 * @property User $updatedBy
 */
class DevProcurement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_procurement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang', 'user_purchaser_id', 'created_by', 'updated_by'], 'integer'],
            [['perihal'], 'required'],
            [['perihal'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode'], 'string', 'max' => 20],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'barang' => 'Barang',
            'user_purchaser_id' => 'User Purchaser',
            'perihal' => 'Perihal',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevEvents()
    {
        return $this->hasMany(DevEvent::className(), ['dev_procurement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
