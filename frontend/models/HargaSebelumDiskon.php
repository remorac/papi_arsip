<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "harga_sebelum_diskon".
 *
 * @property integer $id
 * @property string $purchasing_document
 * @property integer $item_po
 * @property double $harga
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class HargaSebelumDiskon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'harga_sebelum_diskon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purchasing_document', 'item_po', 'harga'], 'required'],
            [['item_po', 'created_by', 'updated_by'], 'integer'],
            [['harga'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['purchasing_document'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'purchasing_document' => Yii::t('app', 'Purchasing Document'),
            'item_po' => Yii::t('app', 'Item Po'),
            'harga' => Yii::t('app', 'Harga'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSapPo()
    {
        return $this->hasOne(SapPo::className(), ['Purchasing Document' => 'purchasing_document', 'Item' => 'item_po']);
    }
}
