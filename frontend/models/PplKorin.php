<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_korin".
 *
 * @property integer $id
 * @property string $name
 * @property string $date
 * @property string $received_at
 * @property string $paid_at
 * @property double $value
 * @property double $released_value
 * @property string $locator
 *
 * @property PplKorinDocument[] $pplKorinDocuments
 */
class PplKorin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_korin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date'], 'required'],
            [['date', 'received_at', 'paid_at'], 'safe'],
            [['value', 'released_value'], 'number'],
            [['name', 'locator'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'No. Korin',
            'date' => 'Tanggal Korin',
            'received_at' => 'Tanggal Diterima AFIS',
            'paid_at' => 'Tanggal Bayar',
            'value' => 'Nilai Korin',
            'released_value' => 'Nilai Release',
            'locator' => 'Locator',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplKorinDocuments()
    {
        return $this->hasMany(PplKorinDocument::className(), ['ppl_korin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplKorinBayars()
    {
        return $this->hasMany(PplKorinBayar::className(), ['ppl_korin_id' => 'id']);
    }

    public static function getTotal($provider, $fieldName)
    {
        $total = 0;
        foreach ($provider as $item) {
            $total += $item[$fieldName];
        }
        return $total;
    }
}
