<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "korin_dokumen".
 *
 * @property integer $id
 * @property integer $korin_id
 * @property integer $dokumen_upload_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Korin $korin
 * @property DokumenUpload $dokumenUpload
 * @property User $createdBy
 * @property User $updatedBy
 */
class KorinDokumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin_dokumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['korin_id', 'dokumen_upload_id'], 'required'],
            [['korin_id', 'dokumen_upload_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'korin_id' => Yii::t('app', 'Korin ID'),
            'dokumen_upload_id' => Yii::t('app', 'Dokumen Upload ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorin()
    {
        return $this->hasOne(Korin::className(), ['id' => 'korin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokumenUpload()
    {
        return $this->hasOne(DokumenUpload::className(), ['id' => 'dokumen_upload_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokumenJenis()
    {
        return $this->hasOne(DokumenJenis::className(), ['id' => 'dokumen_jenis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
