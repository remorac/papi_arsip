<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "pengadaan".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $barang
 * @property integer $metoda_id
 * @property integer $purchaser_personel_id
 * @property string $start_plan
 * @property string $requirement_date
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Metoda $metoda
 * @property Personel $purchaserPersonel
 * @property User $createdBy
 * @property User $updatedBy
 * @property PengadaanPermintaan[] $pengadaanPermintaans
 */
class Pengadaan extends \yii\db\ActiveRecord
{
    public $pdf_files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'barang_jasa_id', 'metoda_id', 'purchaser_personel_procurement_id', 'level_approval_id'], 'required'],
            [['barang_jasa_id', 'metoda_id', 'purchaser_personel_procurement_id', 'level_approval_id', 'delivery_time', 'created_by', 'updated_by'], 'integer'],
            [['kode', 'start_plan', 'requirement_date', 'po_reference', 'isi_disposisi', 'subgroup', 'created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 200],
            [['nama'], 'unique'],
            [['documents_verification', 'is_centerled'], 'integer', 'max' => 1],
            [['verified_by_arsiparis', 'verified_by_purchaser', 'verified_by_manager',], 'integer'],
            [['kontrak_butuh', 'pr_ada'], 'safe'],
            [['nilai_oe', 'total_harga_sebelum_diskon', 'total_harga_setelah_diskon'], 'number'],
            [['pdf_files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'mimeTypes' => 'application/pdf', 'maxSize' => 3072000000, 'maxFiles' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode' => Yii::t('app', 'Kode'),
            'nama' => Yii::t('app', 'Nama'),
            'barang_jasa_id' => Yii::t('app', 'Barang/Jasa'),
            'metoda_id' => Yii::t('app', 'Metoda'),
            'purchaser_personel_procurement_id' => Yii::t('app', 'Purchaser'),
            'level_approval_id' => Yii::t('app', 'Level Approval'),
            'nilai_oe' => Yii::t('app', 'Nilai OE (Rp)'),
            'start_plan' => Yii::t('app', 'Start Plan'),
            'requirement_date' => Yii::t('app', 'Requirement Date'),
            'delivery_time' => Yii::t('app', 'Delivery Time (days)'),
            'po_reference' => Yii::t('app', 'PO Reference'),
            'documents_verification' => Yii::t('app', 'Documents Verification'),
            'isi_disposisi' => Yii::t('app', 'Isi Disposisi'),
            'subgroup' => Yii::t('app', 'Subgroup'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),

            'kontrak_butuh' => Yii::t('app', 'Butuh Kontrak'),
            'pr_ada' => Yii::t('app', 'Ada PR'),
        ];
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetoda()
    {
        return $this->hasOne(Metoda::className(), ['id' => 'metoda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarangJasa()
    {
        return $this->hasOne(BarangJasa::className(), ['id' => 'barang_jasa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelApproval()
    {
        return $this->hasOne(LevelApproval::className(), ['id' => 'level_approval_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaserPersonelProcurement()
    {
        return $this->hasOne(PersonelProcurement::className(), ['id' => 'purchaser_personel_procurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanActivities()
    {
        return $this->hasMany(PengadaanActivity::className(), ['pengadaan_id' => 'id']);
    }
    public function getPengadaanActivityDokumens()
    {
        return $this->hasMany(PengadaanActivityDokumen::className(), ['pengadaan_activity_id' => 'id'])
                ->via("pengadaanActivities");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanCloseds()
    {
        return $this->hasMany(PengadaanClosed::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanDokumenNotrequireds()
    {
        return $this->hasMany(PengadaanDokumenNotrequired::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanDurasiAdjusteds()
    {
        return $this->hasMany(PengadaanDurasiAdjusted::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanItems()
    {
        return $this->hasMany(PengadaanItem::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanKorins()
    {
        return $this->hasMany(PengadaanKorin::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanAccessGroups()
    {
        return $this->hasMany(PengadaanAccessGroup::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanFileUncategorizeds()
    {
        return $this->hasMany(PengadaanFileUncategorized::className(), ['pengadaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanPos()
    {
        return $this->hasMany(PengadaanPo::className(), ['pengadaan_id' => 'id']);
    }
    public function getSapPo()
    {
        return $this->hasMany(SapPo::className(), ['Purchasing Document' => 'purchasing_document'])
                ->via("pengadaanPos");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanPrItems()
    {
        return $this->hasMany(PengadaanPrItem::className(), ['pengadaan_id' => 'id']);
    }
    public function getSapPr()
    {
        return $this->hasMany(SapPr::className(), ['Purchase Requisition' => 'purchase_requisition'])
                ->via("pengadaanPrItems");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifiedByArsiparis()
    {
        return $this->hasOne(User::className(), ['id' => 'verified_by_arsiparis']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifiedByPurchaser()
    {
        return $this->hasOne(User::className(), ['id' => 'verified_by_purchaser']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifiedByManager()
    {
        return $this->hasOne(User::className(), ['id' => 'verified_by_manager']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->nama.' : ['.$this->purchaserPersonelProcurement->personel->singkatan.'] : '.$this->id;
    }


    public function getZcps1013s()
    {
        return $this->hasMany(Zcps1013::className(), ['Purchase_Order' => 'purchasing_document'])
            ->via('pengadaanPos')
            ->orderBy('Clearing_Doc ASC');
    }
    public function getGroupedClearing()
    {
        $rtr = [];
        $old = "";
        foreach ($this->zcps1013s as $zcps1013) {
            if ($old != $zcps1013->Clearing_Doc) {
                if (($afis = Afis::findOne([
                    'clearing_document' => $zcps1013->Clearing_Doc,
                    'year' => substr($zcps1013->Clearing_Date, -4),
                ])) !== null) {
                    $rtr[] = \yii\helpers\Html::a($afis->filename, ['afis/view', 'id' => $afis->id], ['target' => '_blank']);
                } else {
                    $rtr[] = $zcps1013->Clearing_Doc;
                }
            }
            $old = $zcps1013->Clearing_Doc;
        }
        return implode(', ', $rtr);
    }

    public function getLegalPos()
    {
        return $this->hasMany(LegalPo::className(), ['Purchase_Order' => 'purchasing_document'])
            ->via('pengadaanPos')
            ->orderBy('No_Kontrak ASC');
    }
    public function getGroupedLegal()
    {
        $rtr = [];
        $old = "";
        foreach ($this->legalPos as $legalPo) {
            if ($old != $legalPo->filename) {
                if (($legal = Legal::findOne([
                    'filename' => $legalPo->filename,
                ])) !== null) {
                    $rtr[] = \yii\helpers\Html::a($legal->filename, ['legal/view', 'id' => $legal->id], ['target' => '_blank']);
                } else {
                    $rtr[] = $legalPo->No_Kontrak;
                }
            }
            $old = $legalPo->filename;
        }
        return implode(', ', $rtr);
    }
    public function getGroupedLegalLocator()
    {
        $rtr = [];
        $old = "";
        foreach ($this->legalPos as $legalPo) {
            if ($old != $legalPo->filename) {
                $rtr[] = $legalPo->Lokasi;
            }
            $old = $legalPo->filename;
        }
        return implode(', ', $rtr);
    }

    public function getGroupedPo()
    {
        $po = "";
        $oldPo = "";
        $separator = "";
        foreach ($this->pengadaanPos as $pengadaanPo) {
            if ($oldPo != $pengadaanPo->purchasing_document) $po.= $separator.$pengadaanPo->purchasing_document;
            $oldPo = $pengadaanPo->purchasing_document;
            $separator = ", ";
        }
        return $po;
    }

    public function getGroupedKpp()
    {
        $korin = "";
        $oldKorin = "";
        $separator = "";
        foreach ($this->pengadaanKorins as $pengadaanKorin) {
            if ($oldKorin != $pengadaanKorin->korin->no_surat) $korin.= $separator.$pengadaanKorin->korin->no_surat;
            $oldKorin = $pengadaanKorin->korin->no_surat;
            $separator = ", ";
        }
        return $korin;
    }

    public function getGroupedDocs()
    {
        $korin = "";
        $oldKorin = "";
        $separator = "";
        foreach ($this->pengadaanActivities as $pengadaanActivity) {
            foreach ($pengadaanActivity->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
                if ($pengadaanActivityDokumen->dokumenUpload->dokumen_jenis_id == '28') {
                    if ($oldKorin != $pengadaanActivityDokumen->dokumenUpload->pdf_filename) $korin.= $separator.$pengadaanActivityDokumen->dokumenUpload->pdf_filename;
                    $oldKorin = $pengadaanActivityDokumen->dokumenUpload->pdf_filename;
                    $separator = ", ";
                }                
            }
        }
        return $korin;
    }

    public function getGroupedAccess()
    {
        $korin = "";
        $oldKorin = "";
        $separator = "";
        foreach ($this->pengadaanAccessGroups as $pengadaanAccessGroup) {
            if ($oldKorin != $pengadaanAccessGroup->accessGroup->name) $korin .= $separator . $pengadaanAccessGroup->accessGroup->name;
            $oldKorin = $pengadaanAccessGroup->accessGroup->name;
            $separator = ", ";
        }
        return $korin;
    }

    /* public function getLocatorProcurements()
    {
        return $this->hasMany(LocatorProcurement::className(), ['purchase_order' => 'purchasing_document'])
            ->via('pengadaanPos');
            // ->orderBy('locator');
    } */
    public function getGroupedLocator()
    {
        $rtr = [];
        foreach ($this->pengadaanPos as $pengadaanPo) {
            $pos = LocatorProcurement::find()->where(['like', 'purchase_order', $pengadaanPo->purchasing_document])->all();
            foreach ($pos as $po) {
                if ($po->locator && !in_array($po->locator, $rtr)) $rtr[] = $po->locator;
            }
        }
        return implode(', ', $rtr);
    }

    /**
     * @return array
     */
    public function getSelect2Items()
    {
        $select2Items = Pengadaan::find()
            ->select(['concat(pengadaan.nama, " (" , barang_jasa.nama, ")" ," : ", personel.nama, if(start_plan <> "0000-00-00", concat(", Start Plan: ", start_plan), "")) as label',
                'pengadaan.id as value',
                'pengadaan.id',
                ])
            ->leftJoin('personel_procurement', 'personel_procurement.id = pengadaan.purchaser_personel_procurement_id')
            ->leftJoin('personel', 'personel.id = personel_procurement.personel_id')
            ->leftJoin('barang_jasa', 'barang_jasa.id = pengadaan.barang_jasa_id')
            ->asArray()
            ->all();

        foreach($select2Items as $row){
            $data[$row['value']] = $row['label'];
        }

        if(!isset($data)){
            $data = [];
        }

        return $data;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function createZip() 
    {
        $totalSize  = 0;
        $iteration  = 0;
        $filename   = Yii::getAlias('@bundles/'.$this->nama.'.zip');
        if (file_exists($filename)) {
            unlink($filename);
        }
        for ($i=0; $i < 100; $i++) { 
            $filepart = Yii::getAlias('@bundles/'.$this->nama.'.part-'.$i.'.zip');
            if (file_exists($filepart)) unlink($filepart);
        }
        $zip = new \ZipArchive();
        if ($zip->open($filename, \ZipArchive::CREATE)!==TRUE) {
            exit("cannot open <$filename>\n");
        }
        // $zip->addFromString($this->nama.'/'.$this->id.'.txt', $this->nama);

        foreach ($this->pengadaanKorins as $pengadaanKorin) {
            foreach ($pengadaanKorin->korin->korinDokumens as $korinDocument) {
                $file = Yii::getAlias('@uploads/'.$korinDocument->dokumenUpload->pdf_filename);
                if (file_exists($file)) {

                    if ($totalSize + filesize($file) > 2000000000) {
                        $totalSize = 0;
                        $zip->close();
                        $filename = Yii::getAlias('@bundles/' . $this->nama . '.part-' . ++$iteration . '.zip');
                        if (file_exists($filename)) {
                            unlink($filename);
                        }
                        $zip = new \ZipArchive();
                        if ($zip->open($filename, \ZipArchive::CREATE) !== true) {
                            exit("cannot open <$filename>\n");
                        }
                        // $zip->addFromString($this->nama . '/' . $this->id . '.txt', $this->nama);
                    }
                    
                    $zip->addFile($file, $this->nama.'/1. KPP/'.$korinDocument->dokumenUpload->pdf_filename);                    
                    $totalSize+= filesize($file);
                }
            }
        }

        $i      = 1;
        $old_id = 0;

        foreach ($this->pengadaanActivities as $pengadaanActivity) {
            foreach ($pengadaanActivity->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
                $file = Yii::getAlias('@uploads/'.$pengadaanActivityDokumen->dokumenUpload->pdf_filename);
                if (file_exists($file)) {

                    if ($totalSize + filesize($file) > 2000000000) {
                        $totalSize = 0;
                        $zip->close();
                        $filename = Yii::getAlias('@bundles/' . $this->nama . '.part-' . ++$iteration . '.zip');
                        if (file_exists($filename)) {
                            unlink($filename);
                        }
                        $zip = new \ZipArchive();
                        if ($zip->open($filename, \ZipArchive::CREATE) !== true) {
                            exit("cannot open <$filename>\n");
                        }
                        // $zip->addFromString($this->nama . '/' . $this->id . '.txt', $this->nama);
                    }

                    if ($pengadaanActivity->activity->id != $old_id) {
                        $i++;
                        $old_id = $pengadaanActivity->activity->id;
                    }
                    $zip->addFile($file, $this->nama.'/'.$i.'. '.$pengadaanActivity->activity->nama.'/'.$pengadaanActivityDokumen->dokumenUpload->pdf_filename);
                    $totalSize+= filesize($file);
                }
            }
        }

        foreach ($this->pengadaanFileUncategorizeds as $pengadaanFileUncategorized) {
            $file = Yii::getAlias('@uploads/uncategorized/' . $pengadaanFileUncategorized->filename);
            if (file_exists($file)) {

                if ($totalSize + filesize($file) > 2000000000) {
                    $totalSize = 0;
                    $zip->close();
                    $filename = Yii::getAlias('@bundles/' . $this->nama . '.part-' . ++$iteration . '.zip');
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                    $zip = new \ZipArchive();
                    if ($zip->open($filename, \ZipArchive::CREATE) !== true) {
                        exit("cannot open <$filename>\n");
                    }
                    // $zip->addFromString($this->nama . '/' . $this->id . '.txt', $this->nama);
                }
                
                $zip->addFile($file, $this->nama . '/' . $pengadaanFileUncategorized->filename);
                $totalSize += filesize($file);
            }
        }

        $zip->close();
        return true;
    }

    public function folderize()
    {
        $directory = Yii::getAlias('@frontend/folderized/' . $this->nama .'/');
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        
        foreach ($this->pengadaanKorins as $pengadaanKorin) {
            $sub_directory = $directory . '1. KPP/';
            mkdir($sub_directory);
            foreach ($pengadaanKorin->korin->korinDokumens as $korinDocument) {
                $file = Yii::getAlias('@uploads/' . $korinDocument->dokumenUpload->pdf_filename);
                if (file_exists($file)) {
                    $new_file = $sub_directory . $korinDocument->dokumenUpload->pdf_filename;
                    copy($file, $new_file);
                }
            }
        }

        $i = 1;
        foreach ($this->pengadaanActivities as $pengadaanActivity) {
            $sub_directory = $directory . $i++ . '. ' . $pengadaanActivity->activity->nama .'/';
            mkdir($sub_directory);
            foreach ($pengadaanActivity->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
                $file = Yii::getAlias('@uploads/' . $pengadaanActivityDokumen->dokumenUpload->pdf_filename);
                if (file_exists($file)) {
                    $new_file = $sub_directory . $pengadaanActivityDokumen->dokumenUpload->pdf_filename;
                    copy($file, $new_file);
                }
            }
        }

        foreach ($this->pengadaanFileUncategorizeds as $pengadaanFileUncategorized) {
            $file = Yii::getAlias('@uploads/uncategorized/' . $pengadaanFileUncategorized->filename);
            if (file_exists($file)) {
                $new_file = $directory . $pengadaanFileUncategorized->filename;
                copy($file, $new_file);
            }
        }
        return true;
    }
}
