<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_material".
 *
 * @property string $material
 * @property string $material_description
 * @property string $long_text
 * @property string $base_unit_of_measure
 * @property string $material_type
 * @property string $purchasing_group
 * @property string $unrestricted
 * @property string $reorder_point
 * @property string $maximum_stock_level
 * @property string $price
 * @property string $price_unit
 * @property string $lot_size
 * @property string $mrp_type
 * @property string $valuation_class
 * @property string $material_group
 * @property string $plant
 * @property string $safety_stock
 * @property string $valuation_type
 * @property string $last_change
 * @property string $abc_indicator
 * @property string $created_by
 * @property string $price_control
 * @property string $currency
 * @property string $created_on
 * @property string $acc
 * @property string $date
 * @property string $time
 */
class SapMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material', 'material_description', 'long_text', 'base_unit_of_measure', 'material_type', 'purchasing_group', 'unrestricted', 'reorder_point', 'maximum_stock_level', 'price', 'price_unit', 'lot_size', 'mrp_type', 'valuation_class', 'material_group', 'plant', 'safety_stock', 'valuation_type', 'last_change', 'abc_indicator', 'created_by', 'price_control', 'currency', 'created_on', 'acc', 'date', 'time'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'material' => 'Material',
            'material_description' => 'Material Description',
            'long_text' => 'Long Text',
            'base_unit_of_measure' => 'Base Unit Of Measure',
            'material_type' => 'Material Type',
            'purchasing_group' => 'Purchasing Group',
            'unrestricted' => 'Unrestricted',
            'reorder_point' => 'Reorder Point',
            'maximum_stock_level' => 'Maximum Stock Level',
            'price' => 'Price',
            'price_unit' => 'Price Unit',
            'lot_size' => 'Lot Size',
            'mrp_type' => 'Mrp Type',
            'valuation_class' => 'Valuation Class',
            'material_group' => 'Material Group',
            'plant' => 'Plant',
            'safety_stock' => 'Safety Stock',
            'valuation_type' => 'Valuation Type',
            'last_change' => 'Last Change',
            'abc_indicator' => 'Abc Indicator',
            'created_by' => 'Created By',
            'price_control' => 'Price Control',
            'currency' => 'Currency',
            'created_on' => 'Created On',
            'acc' => 'Acc',
            'date' => 'Date',
            'time' => 'Time',
        ];
    }
}
