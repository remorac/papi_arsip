<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_method".
 *
 * @property integer $id
 * @property string $name
 * @property integer $duration
 */
class DailyMethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_method';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'duration'], 'required'],
            [['duration'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'duration' => 'Duration',
        ];
    }
}
