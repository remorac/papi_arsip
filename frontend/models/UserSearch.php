<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\User;


/**
 * UserSearch represents the model behind the search form about `frontend\models\User`.
 */
class UserSearch extends User
{
    public $statusNama;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', /*'jenis_id',*/ 'status'], 'integer'],
            [['username', 'personel.nama', 'email', 'auth_key', 'created_at', 'updated_at', 'password_reset_token'], 'safe'],
            //[['jenis.nama'], 'safe'],
            [['statusNama'], 'safe'],
            [['updated_by'], 'safe'],
            [['created_by'], 'safe'],
            [['authAssignment.item_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'personel.nama' => Yii::t('app', 'Personel'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['statusNama'] = [
              'asc' => ['if(status = 10, "Active", "Inactive")' => SORT_ASC],
              'desc' => ['if(status = 10, "Active", "Inactive")' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        /*$dataProvider->sort->attributes['authAssignment.item_name'] = [
              'asc' => ['auth_assignment.item_name' => SORT_ASC],
              'desc' => ['auth_assignment.item_name' => SORT_DESC],
        ];*/

        $query->joinWith(['personel']);
        // $query->joinWith(['authAssignment']);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'jenis_id' => $this->jenis_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personel.nama')])
            ->andFilterWhere(['like', 'auth_assignment.item_name', $this->getAttribute('authAssignment.item_name')])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            //->andFilterWhere(['like', 'jenis.nama', $this->getAttribute('jenis.nama')])
            ->andFilterWhere(['like', 'if(status = 10, "Active", "Inactive")', $this->statusNama])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['authAssignment.item_name', 'personel.nama']);
    }
}
