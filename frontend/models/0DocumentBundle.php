<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "0_document_bundle".
 *
 * @property integer $id_pengadaan
 * @property string $pengadaan
 * @property string $metoda
 * @property string $purchaser
 * @property string $filename
 * @property integer $updated_at
 */
class DocumentBundle0 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '0_document_bundle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengadaan', 'updated_at'], 'integer'],
            [['filename'], 'string'],
            [['pengadaan'], 'string', 'max' => 200],
            [['metoda'], 'string', 'max' => 50],
            [['purchaser'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengadaan' => 'Id Pengadaan',
            'pengadaan' => 'Pengadaan',
            'metoda' => 'Metoda',
            'purchaser' => 'Purchaser',
            'filename' => 'Filename',
            'updated_at' => 'Updated At',
        ];
    }
}
