<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "level_approval".
 *
 * @property integer $id
 * @property integer $level
 * @property string $approver
 * @property string $nilai_min
 * @property string $nilai_max
 * @property string $tanggal_berlaku
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class LevelApproval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level_approval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'approver', 'nilai_min', 'nilai_max', 'tanggal_berlaku'], 'required'],
            [['level', 'nilai_min', 'nilai_max', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_berlaku', 'created_at', 'updated_at'], 'safe'],
            [['approver'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'level' => Yii::t('app', 'Level'),
            'approver' => Yii::t('app', 'Approver'),
            'nilai_min' => Yii::t('app', 'Nilai Min'),
            'nilai_max' => Yii::t('app', 'Nilai Max'),
            'tanggal_berlaku' => Yii::t('app', 'Tanggal Berlaku'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->approver . ' : ' . $this->id;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
