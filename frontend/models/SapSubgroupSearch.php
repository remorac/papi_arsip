<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\SapSubgroup;

/**
 * SapSubgroupSearch represents the model behind the search form about `frontend\models\SapSubgroup`.
 */
class SapSubgroupSearch extends SapSubgroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Material Master', 'Description', 'Long Text', 'Material Group SGG', 'Material Group Desc SGG', 'Sub Material Group SGG', 'Sub Material Group Desc SGG'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SapSubgroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'Material Master', $this->{"Material Master"}])
            ->andFilterWhere(['like', 'Description', $this->{"Description"}])
            ->andFilterWhere(['like', 'Long Text', $this->{"Long Text"}])
            ->andFilterWhere(['like', 'Material Group SGG', $this->{"Material Group SGG"}])
            ->andFilterWhere(['like', 'Material Group Desc SGG', $this->{"Material Group Desc SGG"}])
            ->andFilterWhere(['like', 'Sub Material Group SGG', $this->{"Sub Material Group SGG"}])
            ->andFilterWhere(['like', 'Sub Material Group Desc SGG', $this->{"Sub Material Group Desc SGG"}]);

        return $dataProvider;
    }
}
