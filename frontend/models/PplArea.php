<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_area".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property PplUserArea[] $pplUserAreas
 */
class PplArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplUserAreas()
    {
        return $this->hasMany(PplUserArea::className(), ['ppl_area_id' => 'id']);
    }
}
