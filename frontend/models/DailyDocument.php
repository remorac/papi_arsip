<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_document".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DailyReportDocument[] $dailyReportDocuments
 */
class DailyDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyReportDocuments()
    {
        return $this->hasMany(DailyReportDocument::className(), ['daily_document_id' => 'id']);
    }
}
