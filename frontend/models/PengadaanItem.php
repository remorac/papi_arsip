<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "pengadaan_item".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property string $nama
 * @property integer $quantity
 * @property string $satuan
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pengadaan $pengadaan
 * @property User $createdBy
 * @property User $updatedBy
 */
class PengadaanItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'nama', 'quantity', 'satuan', 'keterangan'], 'required'],
            [['pengadaan_id', 'quantity', 'created_by', 'updated_by'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 200],
            [['satuan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pengadaan_id' => Yii::t('app', 'Pengadaan ID'),
            'nama' => Yii::t('app', 'Nama'),
            'quantity' => Yii::t('app', 'Quantity'),
            'satuan' => Yii::t('app', 'Satuan'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
