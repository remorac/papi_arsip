<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "legal".
 *
 * @property integer $id
 * @property string $filename
 * @property string $purchase_order
 * @property string $location
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Legal extends \yii\db\ActiveRecord
{
    public $pdfFiles;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'legal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename'], 'required'],
            [['filename'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['purchase_order', 'location'], 'string', 'max' => 191],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['pdfFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'mimeTypes' => 'application/pdf', 'maxFiles' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'purchase_order' => 'Purchase Order',
            'location' => 'Location',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalPos()
    {
        return $this->hasMany(LegalPo::className(), ['filename' => 'filename']);
    }


    public function getGroupedPo()
    {
        $po = [];
        $oldPo = "";
        foreach ($this->legalPos as $legalPo) {
            if ($oldPo != $legalPo->Purchase_Order) {
                if (($pengadaanPo = PengadaanPo::findOne(['purchasing_document' => $legalPo->Purchase_Order])) !== null) {
                    $po[] = \yii\helpers\Html::a($legalPo->Purchase_Order, ['semua-pengadaan/view', 'id' => $pengadaanPo->pengadaan_id], ['target' => '_blank']);
                } else {
                    $po[] = $legalPo->Purchase_Order;
                }
            }
            $oldPo = $legalPo->Purchase_Order;
        }
        return implode(', ', $po);
    }

    public function getGroupedLocator()
    {
        $po = [];
        $oldPo = "";
        foreach ($this->legalPos as $legalPo) {
            if ($oldPo != $legalPo->Lokasi) {
                if (($pengadaanPo = PengadaanPo::findOne(['purchasing_document' => $legalPo->Lokasi])) !== null) {
                    $po[] = \yii\helpers\Html::a($legalPo->Lokasi, ['semua-pengadaan/view', 'id' => $pengadaanPo->pengadaan_id], ['target' => '_blank']);
                } else {
                    $po[] = $legalPo->Lokasi;
                }
            }
            $oldPo = $legalPo->Lokasi;
        }
        return implode(', ', $po);
    }


    public function upload()
    {
        foreach ($this->pdfFiles as $pdfFile) {
            $path = Yii::getAlias('@uploads/legal/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }

            $filepath = $path . $pdfFile->baseName . '.' . $pdfFile->extension;
            // if (file_exists($filepath)) {
            //     $filepath.= '-'.date('YmdHis').'.'.$pdfFile->extension;
            // }
            $pdfFile->saveAs($filepath);
        }
        return true;
    }
}
