<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $password_hash
 * @property string $inisial
 * @property string $AuthAssignment_id
 * @property string $jenis_id
 * @property string $email
 * @property string $status
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 * @property string $password_reset_token
 *
 * @property Jenis $jenis
 * @property AuthAssignment $authAssignment
 */
require_once __DIR__.'/../../common/models/User.php';
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\data\ActiveDataProvider;
use frontend\models\AuthAssignment;
use frontend\models\Personel;


class User extends \common\models\User
{
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['username'], 'string', 'max' => 72],
            //[['password_hash'], 'string', 'max' => 60],
            [['email', 'auth_key', 'password_reset_token'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['password'], 'string', 'max' => 60, 'min' => 8],
            [['personel_id'], 'safe'],
            // [['username'], 'match', 'pattern' => '/^([a-z]{2,}[\.]{1}[a-z0-9]{2,})+$/', 'message' => 'Username should only lowercase in 2 words (at least 2 character each) separated by 1 dot (.). Second word can be numbers'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            //'jenis_id' => Yii::t('app', 'Jenis ID'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasOne(Jenis::className(), ['id' => 'jenis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonel()
    {
        return $this->hasOne(Personel::className(), ['id' => 'personel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function beforeSave($insert) {
        $this->setPassword($this->password);
        $this->generateAuthKey();
        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusNama()
    {
        return $this->status == 10 ? 'Active' : 'Inactive';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropdownDisplayItem()
    {
        return $this->nama . ' : ' . $this->id;
    }
}
