<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "korin_keluar_dokumen".
 *
 * @property integer $id
 * @property integer $korin_keluar_id
 * @property integer $dokumen_upload_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Korin $korinKeluar
 * @property DokumenUpload $dokumenUpload
 */
class KorinKeluarDokumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin_keluar_dokumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['korin_keluar_id', 'dokumen_upload_id'], 'required'],
            [['korin_keluar_id', 'dokumen_upload_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'korin_keluar_id' => 'Korin Keluar ID',
            'dokumen_upload_id' => 'Dokumen Upload ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinKeluar()
    {
        return $this->hasOne(Korin::className(), ['id' => 'korin_keluar_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokumenUpload()
    {
        return $this->hasOne(DokumenUpload::className(), ['id' => 'dokumen_upload_id']);
    }
}
