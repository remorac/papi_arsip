<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_wa_user".
 *
 * @property integer $id
 * @property integer $ppl_user_id
 * @property integer $ppl_buyer_id
 * @property string $file
 * @property string $locator
 *
 * @property PplUser $pplUser
 * @property PplBuyer $pplBuyer
 */
class PplWaUser extends \yii\db\ActiveRecord
{
    public $uploadedFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_wa_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ppl_user_id', 'ppl_buyer_id'], 'required'],
            [['ppl_user_id', 'ppl_buyer_id'], 'integer'],
            [['file', 'locator'], 'string', 'max' => 255],
            [['uploadedFile'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ppl_user_id' => 'Ppl User ID',
            'ppl_buyer_id' => 'Ppl Buyer ID',
            'file' => 'File',
            'locator' => 'Locator',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplUser()
    {
        return $this->hasOne(PplUser::className(), ['id' => 'ppl_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplBuyer()
    {
        return $this->hasOne(PplBuyer::className(), ['id' => 'ppl_buyer_id']);
    }
}
