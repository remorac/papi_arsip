<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "audit_folder".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $is_visible
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property AuditFile[] $auditFiles
 * @property User $createdBy
 * @property User $updatedBy
 */
class AuditFolder extends \yii\db\ActiveRecord
{

    /** 
    * @inheritdoc 
    */ 
    public function behaviors() 
    { 
        return [ 
            \yii\behaviors\TimestampBehavior::className(), 
            \yii\behaviors\BlameableBehavior::className(), 
        ]; 
    } 
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_folder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['is_visible', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 191],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'is_visible' => 'Visibility',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditFiles()
    {
        return $this->hasMany(AuditFile::className(), ['audit_folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
