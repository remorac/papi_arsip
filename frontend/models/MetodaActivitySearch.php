<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MetodaActivity;

/**
 * MetodaActivitySearch represents the model behind the search form about `frontend\models\MetodaActivity`.
 */
class MetodaActivitySearch extends MetodaActivity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'metoda_id', 'activity_id', 'urutan', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metoda.nama', 'activity.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetodaActivity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['metoda.nama'] = [
              'asc' => ['metoda.nama' => SORT_ASC],
              'desc' => ['metoda.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['activity.nama'] = [
              'asc' => ['activity.nama' => SORT_ASC],
              'desc' => ['activity.nama' => SORT_DESC],
        ];

        $query->joinWith(['metoda']);
        $query->joinWith(['activity']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'metoda_id' => $this->metoda_id,
            'activity_id' => $this->activity_id,
            'urutan' => $this->urutan,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'metoda.nama', $this->getAttribute('metoda.nama')])
            ->andFilterWhere(['like', 'activity.nama', $this->getAttribute('activity.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['metoda.nama', 'activity.nama']);
    }
}
