<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "dokumen_upload".
 *
 * @property integer $id
 * @property integer $dokumen_jenis_id
 * @property string $pdf_filename
 * @property string $no_dokumen
 * @property string $tgl_dokumen
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property DokumenJenis $dokumenJenis
 * @property User $createdBy
 * @property User $updatedBy
 * @property KorinDokumen[] $korinDokumens
 * @property PengadaanActivityDokumen[] $pengadaanActivityDokumens
 */
class DokumenUpload extends \yii\db\ActiveRecord
{
    //added attribute to replace `pdf_filename` in create and update form
    public $dokumen;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dokumen_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dokumen_jenis_id', 'no_dokumen'], 'required'],
            [['dokumen_jenis_id', 'status_disposisi', 'created_by', 'updated_by'], 'integer'],
            [['pdf_filename', 'keterangan'], 'string'],
            //[['dokumen'], 'file', 'extensions' => 'pdf'],
            [['dokumen'], 'file'],
            [['tgl_dokumen', 'created_at', 'updated_at'], 'safe'],
            [['no_dokumen'], 'string', 'max' => 100],
            // [['no_dokumen'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dokumen_jenis_id' => Yii::t('app', 'Jenis Dokumen'),
            'pdf_filename' => Yii::t('app', 'Pdf Filename'),
            'dokumen' => Yii::t('app', 'File'),
            'no_dokumen' => Yii::t('app', 'No. Dokumen'),
            'tgl_dokumen' => Yii::t('app', 'Tanggal Dokumen'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'status_disposisi' => Yii::t('app', 'Status Disposisi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokumenJenis()
    {
        return $this->hasOne(DokumenJenis::className(), ['id' => 'dokumen_jenis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinDokumens()
    {
        return $this->hasMany(KorinDokumen::className(), ['dokumen_upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanActivityDokumens()
    {
        return $this->hasMany(PengadaanActivityDokumen::className(), ['dokumen_upload_id' => 'id']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->pdf_filename.' : '.$this->dokumenJenis->nama.' : ' . $this->id;
    }
	
	public function getFileExists() {
		return file_exists(Yii::getAlias('@uploads/'.$this->pdf_filename)) ? 1 : 0;
	}

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
