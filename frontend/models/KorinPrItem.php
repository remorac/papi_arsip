<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "korin_pr_item".
 *
 * @property integer $id
 * @property integer $korin_id
 * @property integer $purchase_requisition
 * @property integer $item_of_requisition
 * @property integer $korin_item_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Korin $korin
 * @property KorinItem $korinItem
 * @property User $createdBy
 * @property User $updatedBy
 */
class KorinPrItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin_pr_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['korin_id', 'purchase_requisition', 'item_of_requisition'], 'required'],
            [['korin_id', 'purchase_requisition', 'item_of_requisition', 'korin_item_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'korin_id' => Yii::t('app', 'Korin ID'),
            'purchase_requisition' => Yii::t('app', 'Purchase Requisition'),
            'item_of_requisition' => Yii::t('app', 'Item Of Requisition'),
            'korin_item_id' => Yii::t('app', 'Korin Item ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorin()
    {
        return $this->hasOne(Korin::className(), ['id' => 'korin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinItem()
    {
        return $this->hasOne(KorinItem::className(), ['id' => 'korin_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSapPr()
    {
        return $this->hasOne(SapPr::className(), ['Purchase Requisition' => 'purchase_requisition', 'Item of Requisition' => 'item_of_requisition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
