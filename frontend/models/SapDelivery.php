<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_delivery".
 *
 * @property string $Material
 * @property string $Short Text
 * @property string $Deletion Indicator
 * @property string $Purch. Organization
 * @property string $Purchasing Group
 * @property string $Material Group
 * @property string $Document Date
 * @property string $Delivery Date
 * @property integer $Purchasing Document
 * @property string $Item
 * @property string $Vendor/supplying plant
 * @property string $Plant
 * @property string $Order Quantity
 * @property string $Qty Delivered
 * @property string $Still to be delivered (qty)
 * @property string $Order Unit
 * @property string $Net price
 * @property string $Price Unit
 * @property string $Net Order Value
 * @property string $Currency
 * @property string $Acct Assignment Cat.
 * @property string $Item Category
 * @property string $Name of Vendor
 * @property string $PO history/release documentation
 * @property string $Purchase Requisition
 * @property string $Purchasing Doc. Type
 * @property string $Purch. Doc. Category
 * @property string $Item Category1
 * @property string $Req. Tracking Number
 * @property string $Storage Location
 * @property string $Quantity in SKU
 * @property string $Stockkeeping unit
 * @property string $Outline Agreement
 * @property string $Principal Agmt Item
 * @property string $Target Value
 * @property string $Total open value
 * @property string $Open value
 * @property string $Released value
 * @property string $Target Quantity
 * @property string $Open Target Quantity
 * @property string $Qty Released to Date
 * @property string $Validity Per. Start
 * @property string $Validity Period End
 * @property string $Still to be delivered (value)
 * @property string $Still to be invoiced (qty)
 * @property string $Still to be invoiced (val.)
 * @property string $Quotation Deadline
 * @property string $RFQ status
 * @property string $Collective Number
 * @property string $No. of Positions
 * @property string $Control indicator
 * @property string $Purchasing Info Rec.
 * @property string $Package number
 * @property string $Release group
 * @property string $Release Strategy
 * @property string $Release status
 * @property string $Release indicator
 * @property string $Issuing Storage Loc.
 * @property string $Order Price Unit
 * @property string $Tax Code
 * @property string $Tax Jurisdiction
 * @property string $Incomplete
 * @property string $Configurable Item Number
 * @property string $External Sort Number
 * @property string $Ext.Hierarchy Category
 * @property string $Requirement Urgency
 * @property string $Reqmt Priority
 * @property string $Long Procurement Document Number
 * @property string $Schedule Line
 * @property string $Time
 * @property string $Stat.-Rel. Del. Date
 * @property string $Scheduled Quantity
 * @property string $Previous Quantity
 * @property string $Issued Quantity
 * @property string $Qty Delivered1
 * @property string $Item of Requisition
 * @property string $Creation Indicator
 */
class SapDelivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Material', 'Short Text', 'Deletion Indicator', 'Purch. Organization', 'Purchasing Group', 'Material Group', 'Document Date', 'Delivery Date', 'Item', 'Vendor/supplying plant', 'Plant', 'Order Quantity', 'Qty Delivered', 'Still to be delivered (qty)', 'Order Unit', 'Net price', 'Price Unit', 'Net Order Value', 'Currency', 'Acct Assignment Cat.', 'Item Category', 'Name of Vendor', 'PO history/release documentation', 'Purchase Requisition', 'Purchasing Doc. Type', 'Purch. Doc. Category', 'Item Category1', 'Req. Tracking Number', 'Storage Location', 'Quantity in SKU', 'Stockkeeping unit', 'Outline Agreement', 'Principal Agmt Item', 'Target Value', 'Total open value', 'Open value', 'Released value', 'Target Quantity', 'Open Target Quantity', 'Qty Released to Date', 'Validity Per. Start', 'Validity Period End', 'Still to be delivered (value)', 'Still to be invoiced (qty)', 'Still to be invoiced (val.)', 'Quotation Deadline', 'RFQ status', 'Collective Number', 'No. of Positions', 'Control indicator', 'Purchasing Info Rec.', 'Package number', 'Release group', 'Release Strategy', 'Release status', 'Release indicator', 'Issuing Storage Loc.', 'Order Price Unit', 'Tax Code', 'Tax Jurisdiction', 'Incomplete', 'Configurable Item Number', 'External Sort Number', 'Ext.Hierarchy Category', 'Requirement Urgency', 'Reqmt Priority', 'Long Procurement Document Number', 'Schedule Line', 'Time', 'Stat.-Rel. Del. Date', 'Scheduled Quantity', 'Previous Quantity', 'Issued Quantity', 'Qty Delivered1', 'Item of Requisition', 'Creation Indicator'], 'string'],
            [['Purchasing Document'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Material' => 'Material',
            'Short Text' => 'Short  Text',
            'Deletion Indicator' => 'Deletion  Indicator',
            'Purch. Organization' => 'Purch   Organization',
            'Purchasing Group' => 'Purchasing  Group',
            'Material Group' => 'Material  Group',
            'Document Date' => 'Document  Date',
            'Delivery Date' => 'Delivery  Date',
            'Purchasing Document' => 'Purchasing  Document',
            'Item' => 'Item',
            'Vendor/supplying plant' => 'Vendor/supplying Plant',
            'Plant' => 'Plant',
            'Order Quantity' => 'Order  Quantity',
            'Qty Delivered' => 'Qty  Delivered',
            'Still to be delivered (qty)' => 'Still To Be Delivered (qty)',
            'Order Unit' => 'Order  Unit',
            'Net price' => 'Net Price',
            'Price Unit' => 'Price  Unit',
            'Net Order Value' => 'Net  Order  Value',
            'Currency' => 'Currency',
            'Acct Assignment Cat.' => 'Acct  Assignment  Cat',
            'Item Category' => 'Item  Category',
            'Name of Vendor' => 'Name Of  Vendor',
            'PO history/release documentation' => 'Po History/release Documentation',
            'Purchase Requisition' => 'Purchase  Requisition',
            'Purchasing Doc. Type' => 'Purchasing  Doc   Type',
            'Purch. Doc. Category' => 'Purch   Doc   Category',
            'Item Category1' => 'Item  Category1',
            'Req. Tracking Number' => 'Req   Tracking  Number',
            'Storage Location' => 'Storage  Location',
            'Quantity in SKU' => 'Quantity In  Sku',
            'Stockkeeping unit' => 'Stockkeeping Unit',
            'Outline Agreement' => 'Outline  Agreement',
            'Principal Agmt Item' => 'Principal  Agmt  Item',
            'Target Value' => 'Target  Value',
            'Total open value' => 'Total Open Value',
            'Open value' => 'Open Value',
            'Released value' => 'Released Value',
            'Target Quantity' => 'Target  Quantity',
            'Open Target Quantity' => 'Open  Target  Quantity',
            'Qty Released to Date' => 'Qty  Released To  Date',
            'Validity Per. Start' => 'Validity  Per   Start',
            'Validity Period End' => 'Validity  Period  End',
            'Still to be delivered (value)' => 'Still To Be Delivered (value)',
            'Still to be invoiced (qty)' => 'Still To Be Invoiced (qty)',
            'Still to be invoiced (val.)' => 'Still To Be Invoiced (val )',
            'Quotation Deadline' => 'Quotation  Deadline',
            'RFQ status' => 'Rfq Status',
            'Collective Number' => 'Collective  Number',
            'No. of Positions' => 'No  Of  Positions',
            'Control indicator' => 'Control Indicator',
            'Purchasing Info Rec.' => 'Purchasing  Info  Rec',
            'Package number' => 'Package Number',
            'Release group' => 'Release Group',
            'Release Strategy' => 'Release  Strategy',
            'Release status' => 'Release Status',
            'Release indicator' => 'Release Indicator',
            'Issuing Storage Loc.' => 'Issuing  Storage  Loc',
            'Order Price Unit' => 'Order  Price  Unit',
            'Tax Code' => 'Tax  Code',
            'Tax Jurisdiction' => 'Tax  Jurisdiction',
            'Incomplete' => 'Incomplete',
            'Configurable Item Number' => 'Configurable  Item  Number',
            'External Sort Number' => 'External  Sort  Number',
            'Ext.Hierarchy Category' => 'Ext  Hierarchy  Category',
            'Requirement Urgency' => 'Requirement  Urgency',
            'Reqmt Priority' => 'Reqmt  Priority',
            'Long Procurement Document Number' => 'Long  Procurement  Document  Number',
            'Schedule Line' => 'Schedule  Line',
            'Time' => 'Time',
            'Stat.-Rel. Del. Date' => 'Stat   Rel   Del   Date',
            'Scheduled Quantity' => 'Scheduled  Quantity',
            'Previous Quantity' => 'Previous  Quantity',
            'Issued Quantity' => 'Issued  Quantity',
            'Qty Delivered1' => 'Qty  Delivered1',
            'Item of Requisition' => 'Item Of  Requisition',
            'Creation Indicator' => 'Creation  Indicator',
        ];
    }
}
