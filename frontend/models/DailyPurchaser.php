<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_purchaser".
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 *
 * @property User $user
 * @property DailyReport[] $dailyReports
 */
class DailyPurchaser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_purchaser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id', 'daily_category_id'], 'required'],
            [['user_id', 'daily_category_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'daily_category_id' => 'Daily Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyCategory()
    {
        return $this->hasOne(DailyCategory::className(), ['id' => 'daily_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyReports()
    {
        return $this->hasMany(DailyReport::className(), ['daily_purchaser_id' => 'id']);
    }
}
