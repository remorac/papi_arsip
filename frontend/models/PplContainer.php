<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_container".
 *
 * @property integer $id
 * @property string $month
 * @property string $file_in
 * @property string $file_out
 */
class PplContainer extends \yii\db\ActiveRecord
{
    public $uploadedFile1;
    public $uploadedFile2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_container';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month'], 'required'],
            [['month'], 'safe'],
            [['file_in', 'file_out', 'locator'], 'string', 'max' => 255],
            [['uploadedFile1'], 'file', 'skipOnEmpty' => true],
            [['uploadedFile2'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month' => 'Month',
            'file_in' => 'File In',
            'file_out' => 'File Out',
            'uploadedFile1' => 'File In',
            'uploadedFile2' => 'File Out',
        ];
    }
}
