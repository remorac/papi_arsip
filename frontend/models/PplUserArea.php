<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_user_area".
 *
 * @property integer $id
 * @property integer $ppl_area_id
 * @property integer $ppl_user_id
 *
 * @property PplArea $pplArea
 * @property PplUser $pplUser
 */
class PplUserArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_user_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ppl_area_id', 'ppl_user_id'], 'required'],
            [['ppl_area_id', 'ppl_user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ppl_area_id' => 'Ppl Area ID',
            'ppl_user_id' => 'Ppl User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplArea()
    {
        return $this->hasOne(PplArea::className(), ['id' => 'ppl_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplUser()
    {
        return $this->hasOne(PplUser::className(), ['id' => 'ppl_user_id']);
    }
}
