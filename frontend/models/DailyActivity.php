<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_activity".
 *
 * @property integer $id
 * @property string $name
 * @property double $progress
 *
 * @property DailyReport[] $dailyReports
 */
class DailyActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'progress'], 'required'],
            [['progress'], 'number'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'progress' => 'Progress',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyReports()
    {
        return $this->hasMany(DailyReport::className(), ['pm_daily_activity_id' => 'id']);
    }
}
