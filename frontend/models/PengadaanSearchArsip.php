<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Pengadaan;

/**
 * PengadaanSearch represents the model behind the search form about `frontend\models\Pengadaan`.
 */
class PengadaanSearchArsip extends Pengadaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'barang_jasa_id', 'metoda_id', 'purchaser_personel_procurement_id', 'level_approval_id', 'nilai_oe', 'delivery_time', 'documents_verification', 'created_by', 'updated_by'], 'integer'],
            [['verified_by_arsiparis', 'verified_by_purchaser', 'verified_by_manager'], 'integer'],
            [['kode', 'nama', 'start_plan', 'requirement_date', 'created_at', 'updated_at'], 'safe'],
            [['groupedPo','groupedKpp', 'groupedDocs', 'groupedAccess','groupedLocator','personel.nama', 'metoda.nama', 'barangJasa.nama', 'activity.activity_group_id', 'pengadaan_activity.start_date', 'pengadaan_activity.end_date', 'pengadaan_durasi_adjusted.durasi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'groupedPo' => Yii::t('app', 'PO'),
            'groupedKpp' => Yii::t('app', 'KPP'),
            'groupedDocs' => Yii::t('app', 'Documents'),
            'groupedAccess' => Yii::t('app', 'Access Group'),
            'personel.nama' => Yii::t('app', 'Personel'),
            'metoda.nama' => Yii::t('app', 'Metoda'),
            'barangJasa.nama' => Yii::t('app', 'Barang/Jasa'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengadaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['groupedPo'] = [
              'asc' => ['group_concat(pengadaan_po.purchasing_document)' => SORT_ASC],
              'desc' => ['group_concat(pengadaan_po.purchasing_document)' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['groupedKpp'] = [
              'asc' => ['group_concat(korin.no_surat)' => SORT_ASC],
              'desc' => ['group_concat(korin.no_surat)' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['groupedDocs'] = [
            'asc' => ['group_concat(dokumen_upload.pdf_filename)' => SORT_ASC],
            'desc' => ['group_concat(dokumen_upload.pdf_filename)' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['groupedAccess'] = [
            'asc' => ['group_concat(pengadaan_access_group.access_group_id)' => SORT_ASC],
            'desc' => ['group_concat(pengadaan_access_group.access_group_id)' => SORT_DESC],
        ];

        /* $dataProvider->sort->attributes['groupedLocator'] = [
              'asc' => ['group_concat(locator_procurement.locator)' => SORT_ASC],
              'desc' => ['group_concat(locator_procurement.locator)' => SORT_DESC],
        ]; */

        $dataProvider->sort->attributes['personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['metoda.nama'] = [
              'asc' => ['metoda.nama' => SORT_ASC],
              'desc' => ['metoda.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['barangJasa.nama'] = [
              'asc' => ['barang_jasa.nama' => SORT_ASC],
              'desc' => ['barang_jasa.nama' => SORT_DESC],
        ];

        $query->joinWith(['purchaserPersonelProcurement.personel']);
        $query->joinWith(['metoda']);
        $query->joinWith(['barangJasa']);
        $query->joinWith(['pengadaanPos']);
        $query->joinWith(['pengadaanKorins.korin']);
        $query->joinWith(['pengadaanActivities']);
        $query->joinWith(['pengadaanDurasiAdjusteds']);
        $query->joinWith(['pengadaanAccessGroups']);
        // $query->joinWith(['locatorProcurements']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        // $query->where("pengadaan_durasi_adjusted.pengadaan_id is not null");

        // $query->where("
        //                 pengadaan_durasi_adjusted.pengadaan_id is not null and
        //                 pengadaan_durasi_adjusted.durasi != 0 and
        //                 pengadaan_durasi_adjusted.pengadaan_id = pengadaan_activity.pengadaan_id and
        //                 (pengadaan_activity.pengadaan_id not in
        //                     (select pengadaan_id from pengadaan_activity where (end_date ='0000-00-00' or end_date is null))
        //                 )
        //                 and
        //                 (select count(*) from pengadaan_durasi_adjusted where durasi !=0 and pengadaan_id = pengadaan.id) = (select count(*) from pengadaan_activity where end_date is not null and pengadaan_id = pengadaan.id)
        //             ");

        $query->andFilterWhere([
            'pengadaan.id' => $this->id,
            'pengadaan.barang_jasa_id' => $this->barang_jasa_id,
            'pengadaan.metoda_id' => $this->metoda_id,
            'pengadaan.purchaser_personel_procurement_id' => $this->purchaser_personel_procurement_id,
            'pengadaan.nilai_oe' => $this->nilai_oe,
            'pengadaan.start_plan' => $this->start_plan,
            'pengadaan.requirement_date' => $this->requirement_date,
            'pengadaan.delivery_time' => $this->delivery_time,
            'pengadaan.documents_verification' => $this->documents_verification,
            'pengadaan.verified_by_arsiparis' => $this->verified_by_arsiparis,
            'pengadaan.verified_by_purchaser' => $this->verified_by_purchaser,
            'pengadaan.verified_by_manager' => $this->verified_by_manager,
            'pengadaan.created_at' => $this->created_at,
            'pengadaan.updated_at' => $this->updated_at,
            'pengadaan.created_by' => $this->created_by,
            'pengadaan.updated_by' => $this->updated_by,
            'activity.activity_group_id' => $this->getAttribute('activity.activity_group_id')
        ]);

        $query->andFilterWhere(['like', 'pengadaan.kode', $this->kode])
            ->andFilterWhere(['like', 'pengadaan.nama', $this->nama])
            ->andFilterWhere([
                'like', 
                '(select (group_concat(purchasing_document)) as groupedPo from pengadaan_po 
                    where pengadaan_po.pengadaan_id = pengadaan.id
                )', 
                $this->getAttribute('groupedPo')
            ])
            ->andFilterWhere([
                'like', 
                '(select (group_concat(no_surat)) as groupedKpp from korin 
                    where pengadaan_korin.pengadaan_id = pengadaan.id 
                    and pengadaan_korin.korin_id = korin.id
                )', 
                $this->getAttribute('groupedKpp')
            ])
            ->andFilterWhere([
                'like', 
                "(select (group_concat(pdf_filename)) as groupedDocs from dokumen_upload, pengadaan_activity_dokumen,pengadaan_activity
                    where pengadaan_activity.pengadaan_id = pengadaan.id
                    and pengadaan_activity_dokumen.pengadaan_activity_id = pengadaan_activity.id
                    and pengadaan_activity_dokumen.dokumen_upload_id = dokumen_upload.id
                    and dokumen_upload.dokumen_jenis_id = '28'
                )", 
                $this->getAttribute('groupedDocs')
            ])
            ->andFilterWhere([
                'like',
                "(select (group_concat(access_group.name)) as groupedAccess from pengadaan_access_group, access_group
                    where pengadaan_access_group.access_group_id = access_group.id
                    and pengadaan_access_group.pengadaan_id = pengadaan.id
                )",
                $this->getAttribute('groupedAccess')
            ])
            ->andFilterWhere([
                "like",
                "(select (group_concat(locator_procurement.locator)) as groupedLocator from pengadaan_po, locator_procurement
                    where pengadaan_po.pengadaan_id = pengadaan.id
                    and locator like concat('%', purchasing_document, '%')
                )", 
                $this->getAttribute('groupedLocator')
            ])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personel.nama')])
            ->andFilterWhere(['like', 'metoda.nama', $this->getAttribute('metoda.nama')])
            ->andFilterWhere(['like', 'barang_jasa.nama', $this->getAttribute('barangJasa.nama')])
            ->andFilterWhere(['=', 'if(pengadaan_activity.start_date is null, "null", "!null")', $this->getAttribute('pengadaan_activity.start_date')])
            ->andFilterWhere(['=', 'if(pengadaan_activity.end_date is null, "null", "!null")', $this->getAttribute('pengadaan_activity.end_date')])
            ->andFilterWhere(['=', 'if(pengadaan_durasi_adjusted.durasi=0, "null", "!null")', $this->getAttribute('pengadaan_durasi_adjusted.durasi')]);

        $query->groupBy("pengadaan.id");
        
        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(),
        [
            'groupedPo',
            'groupedKpp',
            'groupedDocs',
            'groupedLocator',
            'groupedAccess',
            'personel.nama',
            'metoda.nama',
            'barangJasa.nama',
            'activity.activity_group_id',
            'pengadaan_activity.start_date',
            'pengadaan_activity.end_date',
            'pengadaan_durasi_adjusted.durasi'
        ]);
    }
}
