<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "korin_keluar".
 *
 * @property integer $id
 * @property string $no_surat
 * @property integer $barang_jasa_id
 * @property string $no_referensi
 * @property string $pic_initial
 * @property string $tanggal_surat
 * @property string $ringkasan_isi
 * @property string $perihal
 * @property string $tembusan
 * @property string $yg_menandatangani
 * @property string $lampiran
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property BarangJasa $barangJasa
 */
class KorinKeluar extends \yii\db\ActiveRecord
{
    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin_keluar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_surat', 'barang_jasa_id'], 'required'],
            [['barang_jasa_id', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_surat', 'created_at', 'updated_at'], 'safe'],
            [['ringkasan_isi'], 'string'],
            [['no_surat'], 'string', 'max' => 100],
            [['no_referensi', 'pic_initial'], 'string', 'max' => 50],
            [['perihal', 'tembusan', 'yg_menandatangani', 'lampiran'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_surat' => 'No Surat',
            'barang_jasa_id' => 'Barang Jasa ID',
            'no_referensi' => 'No Referensi',
            'pic_initial' => 'PIC Initial',
            'tanggal_surat' => 'Tanggal Surat',
            'ringkasan_isi' => 'Ringkasan Isi',
            'perihal' => 'Perihal',
            'tembusan' => 'Tembusan',
            'yg_menandatangani' => 'Yg Menandatangani',
            'lampiran' => 'Lampiran',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarangJasa()
    {
        return $this->hasOne(BarangJasa::className(), ['id' => 'barang_jasa_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function hasDocument() {
        return $this->hasMany(KorinDokumen::className(), ['korin_keluar_id' => 'id'])->count() == 0 ? 0 : 1;
    }
}
