<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PengadaanDurasiAdjusted;

/**
 * PengadaanDurasiAdjustedSearch represents the model behind the search form about `frontend\models\PengadaanDurasiAdjusted`.
 */
class PengadaanDurasiAdjustedSearch extends PengadaanDurasiAdjusted
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_id', 'activity_id', 'durasi'], 'integer'],
            [['pengadaan.nama', 'activity.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengadaanDurasiAdjusted::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['pengadaan.nama'] = [
              'asc' => ['pengadaan.nama' => SORT_ASC],
              'desc' => ['pengadaan.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['activity.nama'] = [
              'asc' => ['activity.nama' => SORT_ASC],
              'desc' => ['activity.nama' => SORT_DESC],
        ];

        $query->joinWith(['pengadaan']);
        $query->joinWith(['activity']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_id' => $this->pengadaan_id,
            'activity_id' => $this->activity_id,
            'durasi' => $this->durasi,
        ]);

        $query->andFilterWhere(['like', 'pengadaan.nama', $this->getAttribute('pengadaan.nama')])
            ->andFilterWhere(['like', 'activity.nama', $this->getAttribute('activity.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['pengadaan.nama', 'activity.nama']);
    }
}
