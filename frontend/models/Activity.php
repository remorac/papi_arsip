<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "activity".
 *
 * @property string $id
 * @property string $nama
 * @property string $activity_group_id
 *
 * @property ActivityGroup $activityGroup
 * @property DevEvent[] $devEvents
 */

class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'activity_group_id'], 'required'],
            [['activity_group_id'], 'integer'],
            [['nama'], 'string', 'max' => 100],
            [['singkatan'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'activity_group_id' => Yii::t('app', 'Activity Group ID'),
            'singkatan' => Yii::t('app', 'Singkatan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityGroup()
    {
        return $this->hasOne(ActivityGroup::className(), ['id' => 'activity_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevEvents()
    {
        return $this->hasMany(DevEvent::className(), ['activity_id' => 'id']);
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropdownDisplayItem()
    {
        return $this->nama . ' : ' . $this->id;
    }


}
