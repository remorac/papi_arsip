<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_subgroup".
 */
class Zcps1013 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zcps1013';
    }
}
