<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "gcal_token".
 *
 * @property string $id
 * @property string $client_email
 * @property string $server_api_key
 * @property string $calendar_owner_email
 * @property resource $calendar_private_key
 * @property string $calendar_private_key_password
 * @property string $calendar_id
 * @property string $oauth_client_json
 * @property integer $selected
 * @property string $first_access_token_json
 * @property string $access_token_json
 */
class GcalToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gcal_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_email', 'server_api_key', 'calendar_owner_email', 'calendar_private_key', 'calendar_private_key_password', 'calendar_id', 'oauth_client_json', 'selected'], 'required'],
            [['calendar_private_key', 'oauth_client_json', 'first_access_token_json', 'access_token_json'], 'string'],
            [['selected'], 'integer'],
            [['client_email', 'calendar_id'], 'string', 'max' => 254],
            [['server_api_key'], 'string', 'max' => 40],
            [['calendar_owner_email'], 'string', 'max' => 74],
            [['calendar_private_key_password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'client_email' => Yii::t('app', 'Client Email'),
            'server_api_key' => Yii::t('app', 'Server Api Key'),
            'calendar_owner_email' => Yii::t('app', 'Calendar Owner Email'),
            'calendar_private_key' => Yii::t('app', 'Calendar Private Key'),
            'calendar_private_key_password' => Yii::t('app', 'Calendar Private Key Password'),
            'calendar_id' => Yii::t('app', 'Calendar ID'),
            'oauth_client_json' => Yii::t('app', 'Oauth Client Json'),
            'selected' => Yii::t('app', 'Selected'),
            'first_access_token_json' => Yii::t('app', 'First Access Token Json'),
            'access_token_json' => Yii::t('app', 'Access Token Json'),
        ];
    }

    public function getOauthClient()
    {
        return json_decode($this->oauth_client_json);
    }

    public function getFirstAccessToken()
    {
        return json_decode($this->first_access_token_json);
    }

    public function getAccessToken()
    {
        return json_decode($this->access_token_json);
    }
}
