<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "korin".
 *
 * @property integer $id
 * @property integer $korin_jenis_id
 * @property string $no_surat
 * @property string $revisi_referensi
 * @property integer $pic_initial
 * @property string $tgl_terima
 * @property string $tanggal_surat
 * @property string $ringkasan_isi
 * @property string $perihal
 * @property string $tembusan
 * @property string $yg_menandatangani
 * @property string $lampiran
 * @property string $isi_disposisi
 * @property string $kebutuhan_disposisi
 * @property string $requirement_date
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property KorinJenis $korinJenis
 * @property KorinAssignment[] $korinAssignments
 * @property KorinDokumen[] $korinDokumens
 * @property KorinItem[] $korinItems
 * @property KorinPrItem[] $korinPrItems
 * @property KorinVendor[] $korinVendors
 * @property PengadaanKorin[] $pengadaanKorins
 */
class Korin extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['korin_jenis_id', 'barang_jasa_id', 'created_by', 'updated_by'], 'integer'],
            [['no_surat'], 'required'],
            // [['no_surat'], 'unique'],
            [['no_surat'], 'string', 'max' => 100],
            [['tgl_terima', 'tanggal_surat', 'tanggal_disposisi', 'requirement_date', 'created_at', 'updated_at'], 'safe'],
            [['perihal', 'ringkasan_isi', 'isi_disposisi'], 'string'],
            [['revisi_referensi', 'pic_initial'], 'string', 'max' => 50],
            [['tembusan', 'yg_menandatangani', 'lampiran', 'kebutuhan_disposisi'], 'string', 'max' => 200],
            [['file'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'korin_jenis_id' => Yii::t('app', 'Jenis KORIN'),
            'no_surat' => Yii::t('app', 'No KORIN'),
            'barang_jasa_id' => Yii::t('app', 'Barang/Jasa'),
            'revisi_referensi' => Yii::t('app', 'No. KORIN Referensi (Revisi)'),
            'pic_initial' => Yii::t('app', 'Inisial PIC'),
            'tgl_terima' => Yii::t('app', 'Tanggal Terima'),
            'tanggal_surat' => Yii::t('app', 'Tanggal Dokumen'),
            'ringkasan_isi' => Yii::t('app', 'Ringkasan Isi'),
            'perihal' => Yii::t('app', 'Perihal'),
            'tembusan' => Yii::t('app', 'Tembusan'),
            'yg_menandatangani' => Yii::t('app', 'Yang Menandatangani'),
            'lampiran' => Yii::t('app', 'Lampiran'),
            'isi_disposisi' => Yii::t('app', 'Isi Disposisi GM'),
            'tanggal_disposisi' => Yii::t('app', 'Tanggal Disposisi GM'),
            'kebutuhan_disposisi' => Yii::t('app', 'Kebutuhan Disposisi'),
            'requirement_date' => Yii::t('app', 'Requirement Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),

            'korin_id' => Yii::t('app', 'ID KORIN'),
            'tanggal_assignment' => Yii::t('app', 'Tanggal Disposisi'),
            'personel_procurement_id' => Yii::t('app', 'Manager'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinJenis()
    {
        return $this->hasOne(KorinJenis::className(), ['id' => 'korin_jenis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarangJasa()
    {
        return $this->hasOne(BarangJasa::className(), ['id' => 'barang_jasa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinAssignments()
    {
        return $this->hasMany(KorinAssignment::className(), ['korin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinDokumens()
    {
        return $this->hasMany(KorinDokumen::className(), ['korin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinItems()
    {
        return $this->hasMany(KorinItem::className(), ['korin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinPrItems()
    {
        return $this->hasMany(KorinPrItem::className(), ['korin_id' => 'id']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->no_surat . ' : ' . $this->id;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function templateAction()
    {
        return '{delete}';
    }

    public function hasDocument() {
        return $this->hasMany(KorinDokumen::className(), ['korin_id' => 'id'])->count() == 0 ? 0 : 1;
    }
}
