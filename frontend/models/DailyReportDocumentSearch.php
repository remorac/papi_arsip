<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\DailyReportDocument;

/**
 * DailyReportDocumentSearch represents the model behind the search form about `frontend\models\DailyReportDocument`.
 */
class DailyReportDocumentSearch extends DailyReportDocument
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'daily_report_id', 'daily_document_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DailyReportDocument::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'daily_report_id' => $this->daily_report_id,
            'daily_document_id' => $this->daily_document_id,
        ]);

        return $dataProvider;
    }
}
