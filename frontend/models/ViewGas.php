<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "view_gas".
 *
 * @property integer $gct_id
 * @property integer $gct_count
 * @property integer $gc_id
 * @property integer $gc_io
 * @property string $gc_date
 * @property string $gc_time
 * @property string $gc_no_reference
 * @property integer $tube_id
 * @property string $tube_nomor
 * @property integer $type_id
 * @property string $type_name
 * @property integer $owner_id
 * @property string $owner_name
 * @property integer $user_id
 * @property string $user_name
 * @property integer $created_at
 * @property integer $updated_at
 */
class ViewGas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_gas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gct_id', 'gct_count', 'gc_id', 'gc_io', 'tube_id', 'type_id', 'owner_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['gct_count', 'gc_io', 'gc_date', 'tube_nomor', 'type_name', 'owner_name', 'user_name'], 'required'],
            [['gc_date', 'gc_time'], 'safe'],
            [['gc_no_reference', 'type_name', 'owner_name', 'user_name'], 'string', 'max' => 200],
            [['tube_nomor'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gct_id' => 'Gct',
            'gct_count' => 'Gct Count',
            'gc_id' => 'Gc',
            'gc_io' => 'Gc Io',
            'gc_date' => 'Gc Date',
            'gc_time' => 'Gc Time',
            'gc_no_reference' => 'Gc No Reference',
            'tube_id' => 'Tube',
            'tube_nomor' => 'Tube Nomor',
            'type_id' => 'Type',
            'type_name' => 'Type Name',
            'owner_id' => 'Owner',
            'owner_name' => 'Owner Name',
            'user_id' => 'User',
            'user_name' => 'User Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
