<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\DokumenUpload;

/**
 * DokumenUploadSearch represents the model behind the search form about `frontend\models\DokumenUpload`.
 */
class DokumenUploadSearch extends DokumenUpload
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'dokumen_jenis_id', 'status_disposisi', 'created_by', 'updated_by'], 'integer'],
            [['pdf_filename', 'no_dokumen', 'tgl_dokumen', 'keterangan', 'created_at', 'updated_at'], 'safe'],
            [['dokumenJenis.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dokumenJenis.nama' => Yii::t('app', 'Jenis Dokumen'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DokumenUpload::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['dokumenJenis.nama'] = [
              'asc' => ['dokumenJenis.nama' => SORT_ASC],
              'desc' => ['dokumenJenis.nama' => SORT_DESC],
        ];

        $query->joinWith(['dokumenJenis']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dokumen_upload.id' => $this->id,
            'dokumen_jenis_id' => $this->dokumen_jenis_id,
            'tgl_dokumen' => $this->tgl_dokumen,
            'status_disposisi' => $this->status_disposisi,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'pdf_filename', $this->pdf_filename])
            ->andFilterWhere(['like', 'no_dokumen', $this->no_dokumen])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'dokumen_jenis.nama', $this->getAttribute('dokumenJenis.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['dokumenJenis.nama']);
    }
}
