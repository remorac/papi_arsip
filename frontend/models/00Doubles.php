<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "00_doubles".
 *
 * @property integer $ordering
 * @property integer $id
 * @property string $no_surat
 * @property string $perihal
 * @property string $updated_at
 * @property integer $c_asg
 * @property integer $c_pgd
 * @property integer $c_doc
 * @property integer $c_krn
 */
class Doubles00 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '00_doubles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering', 'id', 'c_asg', 'c_pgd', 'c_doc', 'c_krn'], 'integer'],
            [['perihal'], 'string'],
            [['updated_at'], 'safe'],
            [['no_surat'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ordering' => 'Ordering',
            'id' => 'ID',
            'no_surat' => 'No Surat',
            'perihal' => 'Perihal',
            'updated_at' => 'Updated At',
            'c_asg' => 'C Asg',
            'c_pgd' => 'C Pgd',
            'c_doc' => 'C Doc',
            'c_krn' => 'C Krn',
        ];
    }
}
