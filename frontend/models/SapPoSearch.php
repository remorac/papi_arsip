<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\SapPo;

/**
 * PersonelSearch represents the model behind the search form about `frontend\models\Personel`.
 */
class SapPoSearch extends SapPo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Purchasing Document', 'Vendor/supplying plant', 'Item', 'Material Group', 'Deletion Indicator', 'Material', 'Short Text', 'Order Quantity', 'Order Unit', 'Net price', 'Currency', 'Purchasing Group', 'Plant', 'Document Date', 'Price Unit', 'Release status', 'Release indicator', 'Open Target Quantity', 'Total open value', 'Open value', 'Target Quantity', 'Released value', 'Quantity in SKU', 'Purchasing Doc. Type', 'Purch. Doc. Category'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SapPo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /*$dataProvider->sort->attributes['unit.nama'] = [
              'asc' => ['unit.nama' => SORT_ASC],
              'desc' => ['unit.nama' => SORT_DESC],
        ];*/

        /*$query->joinWith(['unit']);
        $query->joinWith(['posisi']);*/

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Purchasing Document' => $this->{"Purchasing Document"},
        ]);

        return $dataProvider;
    }

    /*function attributes()
    {
        return array_merge(parent::attributes(), ['unit.nama', 'posisi.nama']);
    }*/
}
