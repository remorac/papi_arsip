<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_report".
 *
 * @property integer $id
 * @property string $name
 * @property string $user_initial
 * @property integer $daily_purchaser_id
 * @property integer $daily_method_id
 * @property string $activity_issue
 * @property string $is_need_contract
 * @property integer $am_daily_activity_id
 * @property string $am_target
 * @property integer $pm_daily_activity_id
 * @property string $pm_start
 * @property string $pm_finish
 * @property string $plan_start
 * @property string $plan_finish
 * @property string $real_start
 * @property string $real_finish
 *
 * @property DailyCategory $dailyCategory
 * @property DailyPurchaser $dailyPurchaser
 * @property DailyActivity $amDailyActivity
 * @property DailyActivity $pmDailyActivity
 * @property DailyReportDocument[] $dailyReportDocuments
 */
class DailyReport extends \yii\db\ActiveRecord
{
    public $documents;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_initial', 'daily_method_id', 'is_need_contract', 'pm_daily_activity_id'], 'required'],
            [['name', 'activity_issue'], 'string'],
            [['daily_purchaser_id', 'daily_method_id', 'am_daily_activity_id', 'pm_daily_activity_id', 'created_at', 'updated_at'], 'integer'],
            [['am_target', 'pm_start', 'pm_finish', 'plan_start', 'plan_finish', 'real_start', 'real_finish', 'created_at', 'updated_at'], 'safe'],
            [['user_initial', 'is_need_contract'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Pengadaan',
            'user_initial' => 'User',
            'daily_purchaser_id' => 'Purchaser',
            'daily_method_id' => 'Metode',
            'activity_issue' => 'Keterangan',
            'is_need_contract' => 'Butuh Kontrak',
            'am_daily_activity_id' => 'Aktivitas Pagi',
            'am_target' => 'Target Selesai Aktivitas Pagi',
            'pm_daily_activity_id' => 'Aktivitas Sore (Current Activity)',
            'pm_start' => 'Mulai Aktivitas Sore',
            'pm_finish' => 'Rencana Selesai Aktivitas Sore',
            'plan_start' => 'Rencana Start Pengadaan',
            'plan_finish' => 'Rencana Selesai Pengadaan',
            'real_start' => 'Mulai Pengadaan',
            'real_finish' => 'Realisasi Selesai Pengadaan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyCategory()
    {
        return $this->hasOne(DailyCategory::className(), ['id' => 'daily_category_id'])
            ->via('dailyPurchaser');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyPurchaser()
    {
        return $this->hasOne(DailyPurchaser::className(), ['id' => 'daily_purchaser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyMethod()
    {
        return $this->hasOne(DailyMethod::className(), ['id' => 'daily_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmDailyActivity()
    {
        return $this->hasOne(DailyActivity::className(), ['id' => 'am_daily_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPmDailyActivity()
    {
        return $this->hasOne(DailyActivity::className(), ['id' => 'pm_daily_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyReportDocuments()
    {
        return $this->hasMany(DailyReportDocument::className(), ['daily_report_id' => 'id'])->orderBy('daily_document_id ASC');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
}
