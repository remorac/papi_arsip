<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "pengadaan_activity_dokumen".
 *
 * @property integer $id
 * @property integer $pengadaan_activity_id
 * @property integer $dokumen_upload_id
 * @property integer $required
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property PengadaanActivity $pengadaanActivity
 * @property DokumenUpload $dokumenUpload
 * @property User $createdBy
 * @property User $updatedBy
 */
class PengadaanActivityDokumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_activity_dokumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_activity_id', 'dokumen_upload_id'], 'required'],
            [['pengadaan_activity_id', 'dokumen_upload_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pengadaan_activity_id' => Yii::t('app', 'Pengadaan Activity ID'),
            'dokumen_upload_id' => Yii::t('app', 'Dokumen Upload ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanActivity()
    {
        return $this->hasOne(PengadaanActivity::className(), ['id' => 'pengadaan_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokumenUpload()
    {
        return $this->hasOne(DokumenUpload::className(), ['id' => 'dokumen_upload_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
