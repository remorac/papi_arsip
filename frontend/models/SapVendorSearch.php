<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\SapVendor;

/**
 * SapVendorSearch represents the model behind the search form about `frontend\models\SapVendor`.
 */
class SapVendorSearch extends SapVendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Vendor', 'Name of vendor', 'City', 'Street', 'Country', 'Postal Code', 'Account group', 'Search term', 'Purch. Organization', 'Purch. Org. Descr.', 'Terms of Payment', 'Order currency', 'Salesperson', 'One-time account', 'Number of Purchasing Organizations', 'Telephone', 'Central purchasing block', 'Block function', 'Central deletion flag', 'Purch. block for purchasing organization', 'Delete flag for purchasing organization'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SapVendor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'Vendor', $this->Vendor])
            ->andFilterWhere(['like', 'Name of vendor', $this->{'Name of vendor'}])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'Street', $this->Street])
            ->andFilterWhere(['like', 'Country', $this->Country])
            ->andFilterWhere(['like', 'Postal Code', $this->{'Postal Code'}])
            ->andFilterWhere(['like', 'Account group', $this->{'Account group'}])
            ->andFilterWhere(['like', 'Search term', $this->{'Search term'}])
            ->andFilterWhere(['like', 'Purch. Organization', $this->{'Purch. Organization'}])
            ->andFilterWhere(['like', 'Purch. Org. Descr.', $this->{'Purch. Org. Descr.'}])
            ->andFilterWhere(['like', 'Terms of Payment', $this->{'Terms of Payment'}])
            ->andFilterWhere(['like', 'Order currency', $this->{'Order currency'}])
            ->andFilterWhere(['like', 'Salesperson', $this->Salesperson])
            ->andFilterWhere(['like', 'One-time account', $this->{'One-time account'}])
            ->andFilterWhere(['like', 'Number of Purchasing Organizations', $this->{'Number of Purchasing Organizations'}])
            ->andFilterWhere(['like', 'Telephone', $this->Telephone])
            ->andFilterWhere(['like', 'Central purchasing block', $this->{'Central purchasing block'}])
            ->andFilterWhere(['like', 'Block function', $this->{'Block function'}])
            ->andFilterWhere(['like', 'Central deletion flag', $this->{'Central deletion flag'}])
            ->andFilterWhere(['like', 'Purch. block for purchasing organization', $this->{'Purch. block for purchasing organization'}])
            ->andFilterWhere(['like', 'Delete flag for purchasing organization', $this->{'Delete flag for purchasing organization'}]);

        return $dataProvider;
    }
}
