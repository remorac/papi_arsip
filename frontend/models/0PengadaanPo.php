<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "0_pengadaan_po".
 *
 * @property integer $id
 * @property string $pengadaan
 * @property string $metoda
 * @property string $purchaser
 * @property string $purchase_order
 * @property string $updated_at
 */
class PengadaanPo0 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '0_pengadaan_po';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['updated_at'], 'safe'],
            [['pengadaan'], 'string', 'max' => 200],
            [['metoda'], 'string', 'max' => 50],
            [['purchaser'], 'string', 'max' => 100],
            [['purchase_order'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pengadaan' => 'Pengadaan',
            'metoda' => 'Metoda',
            'purchaser' => 'Purchaser',
            'purchase_order' => 'Purchase Order',
            'updated_at' => 'Updated At',
        ];
    }
}
