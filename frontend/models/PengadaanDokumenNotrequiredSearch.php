<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PengadaanDokumenNotrequired;

/**
 * PengadaanDokumenNotrequiredSearch represents the model behind the search form about `frontend\models\PengadaanDokumenNotrequired`.
 */
class PengadaanDokumenNotrequiredSearch extends PengadaanDokumenNotrequired
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_id', 'dokumen_jenis_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['pengadaan.nama', 'dokumenJenis.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengadaanDokumenNotrequired::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['pengadaan.nama'] = [
              'asc' => ['pengadaan.nama' => SORT_ASC],
              'desc' => ['pengadaan.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dokumenJenis.nama'] = [
              'asc' => ['dokumen_jenis.nama' => SORT_ASC],
              'desc' => ['dokumen_jenis.nama' => SORT_DESC],
        ];

        $query->joinWith(['pengadaan']);
        $query->joinWith(['dokumenJenis']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_id' => $this->pengadaan_id,
            'dokumen_jenis_id' => $this->dokumen_jenis_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'pengadaan.nama', $this->getAttribute('pengadaan.nama')])
            ->andFilterWhere(['like', 'dokumen_jenis.nama', $this->getAttribute('dokumenJenis.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['pengadaan.nama', 'dokumenJenis.nama']);
    }
}
