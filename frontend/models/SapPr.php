<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_pr".
 *
 * @property string $Purchase Requisition
 * @property string $Requisition Date
 * @property string $Short Text
 * @property string $Release Date
 * @property string $Material Group
 * @property string $Material
 * @property string $Unit of Measure
 * @property string $Purchase Order
 * @property string $Item of Requisition
 * @property string $Deletion Indicator
 * @property string $Purchasing Group
 * @property string $Plant
 * @property string $Quantity Requested
 * @property string $Delivery Date
 * @property string $Valuation Price
 * @property string $Price Unit
 * @property string $Purchase Order Item
 * @property string $Purchase Order Date
 * @property string $Quantity Ordered
 * @property string $Total Value
 * @property string $Currency
 * @property string $Deliv. date(From/to)
 */
class SapPr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_pr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Requisition Date', 'Short Text', 'Release Date', 'Material Group', 'Material', 'Unit of Measure', 'Purchase Order', 'Item of Requisition', 'Deletion Indicator', 'Purchasing Group', 'Plant', 'Quantity Requested', 'Delivery Date', 'Valuation Price', 'Price Unit', 'Purchase Order Item', 'Purchase Order Date', 'Quantity Ordered', 'Total Value', 'Currency', 'Deliv. date(From/to)'], 'string'],
            [['Purchase Requisition'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Purchase Requisition' => Yii::t('app', 'Purchase  Requisition'),
            'Requisition Date' => Yii::t('app', 'Requisition  Date'),
            'Short Text' => Yii::t('app', 'Short  Text'),
            'Release Date' => Yii::t('app', 'Release  Date'),
            'Material Group' => Yii::t('app', 'Material  Group'),
            'Material' => Yii::t('app', 'Material'),
            'Unit of Measure' => Yii::t('app', 'Unit Of  Measure'),
            'Purchase Order' => Yii::t('app', 'Purchase  Order'),
            'Item of Requisition' => Yii::t('app', 'Item Of  Requisition'),
            'Deletion Indicator' => Yii::t('app', 'Deletion  Indicator'),
            'Purchasing Group' => Yii::t('app', 'Purchasing  Group'),
            'Plant' => Yii::t('app', 'Plant'),
            'Quantity Requested' => Yii::t('app', 'Quantity  Requested'),
            'Delivery Date' => Yii::t('app', 'Delivery  Date'),
            'Valuation Price' => Yii::t('app', 'Valuation  Price'),
            'Price Unit' => Yii::t('app', 'Price  Unit'),
            'Purchase Order Item' => Yii::t('app', 'Purchase  Order  Item'),
            'Purchase Order Date' => Yii::t('app', 'Purchase  Order  Date'),
            'Quantity Ordered' => Yii::t('app', 'Quantity  Ordered'),
            'Total Value' => Yii::t('app', 'Total  Value'),
            'Currency' => Yii::t('app', 'Currency'),
            'Deliv. date(From/to)' => Yii::t('app', 'Deliv  Date( From/to)'),
        ];
    }
}
