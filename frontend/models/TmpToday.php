<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tmp_today".
 *
 * @property integer $id
 * @property string $brg_jsa
 * @property string $nama_pengadaan
 * @property string $monitoring_aktifitas
 * @property string $rencana_pengadaan
 * @property string $realisasi_pengadaan
 * @property string $issue_aktifitas
 * @property string $keterangan
 */
class TmpToday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tmp_today';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brg_jsa', 'nama_pengadaan', 'monitoring_aktifitas', 'rencana_pengadaan', 'realisasi_pengadaan', 'issue_aktifitas', 'keterangan'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brg_jsa' => 'Brg Jsa',
            'nama_pengadaan' => 'Pengadaan',
            'monitoring_aktifitas' => 'Current Activity',
            'rencana_pengadaan' => 'Rencana Pengadaan',
            'realisasi_pengadaan' => 'Realisasi Pengadaan',
            'issue_aktifitas' => 'Issue',
            'keterangan' => 'Keterangan Tambahan',
        ];
    }
}
