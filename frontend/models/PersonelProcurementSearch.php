<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PersonelProcurement;

/**
 * PersonelProcurementSearch represents the model behind the search form about `frontend\models\PersonelProcurement`.
 */
class PersonelProcurementSearch extends PersonelProcurement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'personel_id', 'barang', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['jabatan'], 'safe'],
            [['personel.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonelProcurement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $query->joinWith(['personel']);

        $query->andFilterWhere([
            'id' => $this->id,
            'personel_id' => $this->personel_id,
            'barang' => $this->barang,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'jabatan', $this->jabatan])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personel.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['personel.nama']);
    }
}
