<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Personel;

/**
 * PersonelSearch represents the model behind the search form about `frontend\models\Personel`.
 */
class PersonelSearch extends Personel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'posisi_id', 'created_by', 'updated_by'], 'integer'],
            [['nik', 'nama', 'singkatan', 'created_at', 'updated_at'], 'safe'],
            [['unit.nama', 'posisi.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Personel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['unit.nama'] = [
              'asc' => ['unit.nama' => SORT_ASC],
              'desc' => ['unit.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['posisi.nama'] = [
              'asc' => ['posisi.nama' => SORT_ASC],
              'desc' => ['posisi.nama' => SORT_DESC],
        ];

        $query->joinWith(['unit']);
        $query->joinWith(['posisi']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
            'posisi_id' => $this->posisi_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'personel.nama', $this->nama])
            ->andFilterWhere(['like', 'singkatan', $this->singkatan]);

        $query->andFilterWhere(['like', 'unit.nama', $this->getAttribute('unit.nama')])
            ->andFilterWhere(['like', 'posisi.nama', $this->getAttribute('posisi.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['unit.nama', 'posisi.nama']);
    }
}
