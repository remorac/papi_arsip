<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TmpToday;

/**
 * TmpTodaySearch represents the model behind the search form about `frontend\models\TmpToday`.
 */
class TmpTodaySearch extends TmpToday
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['brg_jsa', 'nama_pengadaan', 'monitoring_aktifitas', 'rencana_pengadaan', 'realisasi_pengadaan', 'issue_aktifitas', 'keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TmpToday::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'brg_jsa', $this->brg_jsa])
            ->andFilterWhere(['like', 'nama_pengadaan', $this->nama_pengadaan])
            ->andFilterWhere(['like', 'monitoring_aktifitas', $this->monitoring_aktifitas])
            ->andFilterWhere(['like', 'rencana_pengadaan', $this->rencana_pengadaan])
            ->andFilterWhere(['like', 'realisasi_pengadaan', $this->realisasi_pengadaan])
            ->andFilterWhere(['like', 'issue_aktifitas', $this->issue_aktifitas])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
