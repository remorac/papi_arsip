<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\GcalToken;

/**
 * GcalTokenSearch represents the model behind the search form about `frontend\models\GcalToken`.
 */
class GcalTokenSearch extends GcalToken
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'selected'], 'integer'],
            [['client_email', 'server_api_key', 'calendar_owner_email', 'calendar_private_key', 'calendar_private_key_password', 'calendar_id', 'oauth_client_json', 'first_access_token_json', 'access_token_json'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GcalToken::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'selected' => $this->selected,
        ]);

        $query->andFilterWhere(['like', 'client_email', $this->client_email])
            ->andFilterWhere(['like', 'server_api_key', $this->server_api_key])
            ->andFilterWhere(['like', 'calendar_owner_email', $this->calendar_owner_email])
            ->andFilterWhere(['like', 'calendar_private_key', $this->calendar_private_key])
            ->andFilterWhere(['like', 'calendar_private_key_password', $this->calendar_private_key_password])
            ->andFilterWhere(['like', 'calendar_id', $this->calendar_id])
            ->andFilterWhere(['like', 'oauth_client_json', $this->oauth_client_json])
            ->andFilterWhere(['like', 'first_access_token_json', $this->first_access_token_json])
            ->andFilterWhere(['like', 'access_token_json', $this->access_token_json]);

        return $dataProvider;
    }
}
