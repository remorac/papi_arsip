<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "gas_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property GasTube[] $gasTubes
 * @property User $createdBy
 * @property User $updatedBy
 */
class GasType extends \yii\db\ActiveRecord
{
    
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gas_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasTubes()
    {
        return $this->hasMany(GasTube::className(), ['gas_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function tubesUsedCountUser($owner_id = "")
    {
        $param = $owner_id == ""? "" : "gas_tube.gas_owner_id = '".$owner_id."' and ";
        $tubes = GasCirculationTube::find()
            ->joinWith(['gasCirculation'])
            ->joinWith(['gasTube'])
            ->where("
                $param 
                gas_tube.gas_type_id = '".$this->id."' and 
                gas_circulation.io = '2' and 
                gas_circulation.gas_user_unit_id != '' and 
                gas_circulation.`date` >
                    (select max(`date`) from gas_circulation a, gas_circulation_tube b where 
                        a.id = b.gas_circulation_id and
                        a.io = '1' and
                        b.gas_tube_id = gas_circulation_tube.gas_tube_id
                    )
                ")
            ->sum("gas_circulation_tube.count");
        return $tubes;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function tubesUsedCountVendor($owner_id = "")
    {
        $param = $owner_id == ""? "" : "gas_tube.gas_owner_id = '".$owner_id."' and ";
        $tubes = GasCirculationTube::find()
            ->joinWith(['gasCirculation'])
            ->joinWith(['gasTube'])
            ->where("
                $param 
                gas_tube.gas_type_id = '".$this->id."' and 
                gas_circulation.io = '2' and 
                gas_circulation.gas_vendor_unit_id != '' and 
                gas_circulation.`date` >
                    (select max(`date`) from gas_circulation a, gas_circulation_tube b where 
                        a.id = b.gas_circulation_id and
                        a.io = '1' and
                        b.gas_tube_id = gas_circulation_tube.gas_tube_id
                    )
                ")
            ->sum("gas_circulation_tube.count");
        return $tubes;
    }

}
