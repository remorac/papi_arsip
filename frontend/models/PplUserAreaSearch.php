<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PplUserArea;

/**
 * PplUserAreaSearch represents the model behind the search form about `frontend\models\PplUserArea`.
 */
class PplUserAreaSearch extends PplUserArea
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ppl_area_id', 'ppl_user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PplUserArea::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ppl_area_id' => $this->ppl_area_id,
            'ppl_user_id' => $this->ppl_user_id,
        ]);

        return $dataProvider;
    }
}
