<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_subgroup".
 *
 * @property string $Material Master
 * @property string $Description
 * @property string $Long Text
 * @property string $Material Group SGG
 * @property string $Material Group Desc SGG
 * @property string $Sub Material Group SGG
 * @property string $Sub Material Group Desc SGG
 */
class SapSubgroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_subgroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Material Master', 'Description', 'Long Text', 'Material Group SGG', 'Material Group Desc SGG', 'Sub Material Group SGG', 'Sub Material Group Desc SGG'], 'required'],
            [['Long Text'], 'string'],
            [['Material Master', 'Description', 'Material Group SGG', 'Material Group Desc SGG', 'Sub Material Group SGG', 'Sub Material Group Desc SGG'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Material Master' => Yii::t('app', 'Material  Master'),
            'Description' => Yii::t('app', 'Description'),
            'Long Text' => Yii::t('app', 'Long  Text'),
            'Material Group SGG' => Yii::t('app', 'Material  Group  Sgg'),
            'Material Group Desc SGG' => Yii::t('app', 'Material  Group  Desc  Sgg'),
            'Sub Material Group SGG' => Yii::t('app', 'Sub  Material  Group  Sgg'),
            'Sub Material Group Desc SGG' => Yii::t('app', 'Sub  Material  Group  Desc  Sgg'),
        ];
    }
}
