<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\GasCirculation;

/**
 * GasCirculationSearch represents the model behind the search form about `frontend\models\GasCirculation`.
 */
class GasCirculationSearch extends GasCirculation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'io', 'gas_vendor_unit_id', 'gas_vendor_pic_id', 'gas_user_unit_id', 'gas_user_pic_id', 'gas_pic_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date', 'time', 'no_reference'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GasCirculation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'io' => $this->io,
            'date' => $this->date,
            'time' => $this->time,
            'gas_vendor_unit_id' => $this->gas_vendor_unit_id,
            'gas_vendor_pic_id' => $this->gas_vendor_pic_id,
            'gas_user_unit_id' => $this->gas_user_unit_id,
            'gas_user_pic_id' => $this->gas_user_pic_id,
            'gas_pic_id' => $this->gas_pic_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_reference', $this->no_reference]);

        return $dataProvider;
    }
}
