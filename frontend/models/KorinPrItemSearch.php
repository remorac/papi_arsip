<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinPrItem;

/**
 * KorinPrItemSearch represents the model behind the search form about `frontend\models\KorinPrItem`.
 */
class KorinPrItemSearch extends KorinPrItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'korin_id', 'purchase_requisition', 'item_of_requisition', 'korin_item_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sapPr.shortText', 'sapPr.quantityRequested', 'sapPr.unitOfMeasure'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinPrItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

       /* $dataProvider->sort->attributes['sapPr.shortText'] = [
              'asc' => ['korinJenis.nama' => SORT_ASC],
              'desc' => ['korinJenis.nama' => SORT_DESC],
        ];*/
        
        $query->joinWith(['sapPr']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'korin_id' => $this->korin_id,
            'purchase_requisition' => $this->purchase_requisition,
            'item_of_requisition' => $this->item_of_requisition,
            'korin_item_id' => $this->korin_item_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        /*$query->andFilterWhere(['like', 'sap_pr.`Short Text`', $this->getAttribute('korinJenis.nama')])
            ->andFilterWhere(['like', 'sap_pr.nama', $this->getAttribute('korinJenis.nama')])
            ->andFilterWhere(['like', 'sap_pr.nama', $this->getAttribute('barangJasa.nama')]);*/

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['sapPr.shortText', 'sapPr.quantityRequested', 'sapPr.unitOfMeasure']);
    }
}
