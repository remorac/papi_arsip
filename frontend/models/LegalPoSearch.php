<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\LegalPo;

/**
 * LegalPoSearch represents the model behind the search form about `frontend\models\LegalPo`.
 */
class LegalPoSearch extends LegalPo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Klasifikasi', 'Purchase_Order', 'No_Kontrak', 'Isi_Berkas', 'Tahun', 'Vendor', 'Box', 'Lokasi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LegalPo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'Kode_Klasifikasi', $this->Kode_Klasifikasi])
            ->andFilterWhere(['like', 'Purchase_Order', $this->Purchase_Order])
            ->andFilterWhere(['like', 'No_Kontrak', $this->No_Kontrak])
            ->andFilterWhere(['like', 'Isi_Berkas', $this->Isi_Berkas])
            ->andFilterWhere(['like', 'Tahun', $this->Tahun])
            ->andFilterWhere(['like', 'Vendor', $this->Vendor])
            ->andFilterWhere(['like', 'Box', $this->Box])
            ->andFilterWhere(['like', 'Lokasi', $this->Lokasi]);

        return $dataProvider;
    }
}
