<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "0_document_activity".
 *
 * @property integer $id_pengadaan
 * @property string $pengadaan
 * @property string $metoda
 * @property string $purchaser
 * @property string $id_activity
 * @property string $activity
 * @property string $filename
 * @property string $tanggal_dokumen
 * @property string $jenis_dokumen
 * @property string $updated_at
 */
class DocumentActivity0 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '0_document_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengadaan'], 'integer'],
            [['filename'], 'string'],
            [['tanggal_dokumen', 'updated_at'], 'safe'],
            [['pengadaan'], 'string', 'max' => 200],
            [['metoda'], 'string', 'max' => 50],
            [['purchaser', 'activity', 'jenis_dokumen'], 'string', 'max' => 100],
            [['id_activity'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengadaan' => 'Id Pengadaan',
            'pengadaan' => 'Pengadaan',
            'metoda' => 'Metoda',
            'purchaser' => 'Purchaser',
            'id_activity' => 'Id Activity',
            'activity' => 'Activity',
            'filename' => 'Filename',
            'tanggal_dokumen' => 'Tanggal Dokumen',
            'jenis_dokumen' => 'Jenis Dokumen',
            'updated_at' => 'Updated At',
        ];
    }
}
