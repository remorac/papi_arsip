<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinDokumen;

/**
 * KorinDokumenSearch represents the model behind the search form about `frontend\models\KorinDokumen`.
 */
class KorinDokumenSearch extends KorinDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'korin_id', 'dokumen_upload_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['dokumenJenis.nama', 'dokumenUpload.pdf_filename', 'dokumenUpload.no_dokumen', 'dokumenUpload.tgl_dokumen',], 'safe'],
            [['tgl_dokumen'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinDokumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['tgl_dokumen' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['tgl_dokumen'] = [
              'asc' => ['dokumen_upload.tgl_dokumen' => SORT_ASC],
              'desc' => ['dokumen_upload.tgl_dokumen' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dokumenJenis.nama'] = [
              'asc' => ['dokumen_jenis.nama' => SORT_ASC],
              'desc' => ['dokumen_jenis.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dokumenUpload.pdf_filename'] = [
              'asc' => ['dokumen_upload.pdf_filename' => SORT_ASC],
              'desc' => ['dokumen_upload.pdf_filename' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dokumenUpload.no_dokumen'] = [
              'asc' => ['dokumen_upload.no_dokumen' => SORT_ASC],
              'desc' => ['dokumen_upload.no_dokumen' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dokumenUpload.tgl_dokumen'] = [
              'asc' => ['dokumen_upload.tgl_dokumen' => SORT_ASC],
              'desc' => ['dokumen_upload.tgl_dokumen' => SORT_DESC],
        ];
        
        $query->joinWith(['dokumenUpload']);
        $query->joinWith(['dokumenUpload.dokumenJenis']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'korin_id' => $this->korin_id,
            'dokumen_upload_id' => $this->dokumen_upload_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'dokumen_jenis.nama', $this->getAttribute('dokumenJenis.nama')])
            ->andFilterWhere(['like', 'dokumen_upload.pdf_filename', $this->getAttribute('dokumenUpload.pdf_filename')])
            ->andFilterWhere(['like', 'dokumen_upload.no_dokumen', $this->getAttribute('dokumenUpload.no_dokumen')])
            ->andFilterWhere(['like', 'dokumen_upload.tgl_dokumen', $this->getAttribute('dokumenUpload.tgl_dokumen')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), [
            'tgl_dokumen', 
            'dokumenJenis.nama', 
            'dokumenUpload.pdf_filename', 
            'dokumenUpload.no_dokumen', 
            'dokumenUpload.tgl_dokumen'
        ]);
    }
}
