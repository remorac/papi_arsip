<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinJenis;

/**
 * KorinJenisSearch represents the model behind the search form about `frontend\models\KorinJenis`.
 */
class KorinJenisSearch extends KorinJenis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['nama', 'created_at', 'updated_at'], 'safe'],
            [['korinJenisGroup.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinJenis::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['korinJenisGroup.nama'] = [
              'asc' => ['korinJenisGroup.nama' => SORT_ASC],
              'desc' => ['korinJenisGroup.nama' => SORT_DESC],
        ];

        $query->joinWith(['korinJenisGroup']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'korin_jenis_group.nama', $this->getAttribute('korinJenisGroup.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['korinJenisGroup.nama']);
    }
}
