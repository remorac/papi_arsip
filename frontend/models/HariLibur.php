<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "hari_libur".
 *
 * @property integer $id
 * @property string $tanggal
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class HariLibur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hari_libur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'keterangan'], 'required'],
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['keterangan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public static function countHoliday($start,$end) {
        $hariLibur = HariLibur::find()
            ->where("tanggal >= '$start' and tanggal <='$end' 
                and dayname(tanggal)!='Saturday' and dayname(tanggal)!='Sunday'")
            ->count();
        $satsun = 0;
        while ($start <= $end) {
            $dnm = date('l', strtotime($start));
            if ($dnm == "Saturday" || $dnm == "Sunday") $satsun+=1;
            $date = new \DateTime($start);
            $date->add(new \DateInterval('P1D'));                       
            $start = $date->format('Y-m-d');
        }
        return ($hariLibur+$satsun);
    }

    public static function checkHoliday($date) {
        $hariLibur = HariLibur::find()
            ->where("tanggal ='$date' 
                and dayname(tanggal)!='Saturday' and dayname(tanggal)!='Sunday'")
            ->count();
        $dnm = date('l', strtotime($date));
        $satsun = ($dnm == "Saturday" || $dnm == "Sunday") ? 1 : 0;
        return ($hariLibur || $satsun);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
