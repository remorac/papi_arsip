<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "dev_event".
 *
 * @property integer $id
 * @property integer $dev_procurement_id
 * @property integer $activity_id
 * @property string $tanggal
 * @property string $jam
 * @property integer $tempat_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property DevProcurement $devProcurement
 * @property Activity $activity
 * @property Tempat $tempat
 * @property User $updatedBy
 */
class DevEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dev_procurement_id', 'activity_id', 'tempat_id', 'created_by', 'updated_by'], 'integer'],
            [['tanggal', 'jam', 'created_at', 'updated_at'], 'safe'],
            [['dev_procurement_id'], 'exist', 'skipOnError' => true, 'targetClass' => DevProcurement::className(), 'targetAttribute' => ['dev_procurement_id' => 'id']],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activity_id' => 'id']],
            [['tempat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tempat::className(), 'targetAttribute' => ['tempat_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dev_procurement_id' => 'Dev Procurement',
            'activity_id' => 'Activity',
            'tanggal' => 'Tanggal',
            'jam' => 'Jam',
            'tempat_id' => 'Tempat',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevProcurement()
    {
        return $this->hasOne(DevProcurement::className(), ['id' => 'dev_procurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTempat()
    {
        return $this->hasOne(Tempat::className(), ['id' => 'tempat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
