<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_gas".
 *
 * @property integer $id
 * @property string $month
 * @property string $file
 */
class PplGas extends \yii\db\ActiveRecord
{
    public $uploadedFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_gas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month'], 'required'],
            [['month'], 'safe'],
            [['file', 'locator'], 'string', 'max' => 255],
            [['uploadedFile'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month' => 'Month',
            'file' => 'File',
            'uploadedFile' => 'File',
        ];
    }
}
