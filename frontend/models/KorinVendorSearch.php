<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinVendor;

/**
 * KorinVendorSearch represents the model behind the search form about `frontend\models\KorinVendor`.
 */
class KorinVendorSearch extends KorinVendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'korin_id', 'vendor_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            //[['vendor.kode', 'vendor.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinVendor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /*$dataProvider->sort->attributes['vendor.kode'] = [
              'asc' => ['vendor.kode' => SORT_ASC],
              'desc' => ['vendor.kode' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['vendor.nama'] = [
              'asc' => ['vendor.nama' => SORT_ASC],
              'desc' => ['vendor.nama' => SORT_DESC],
        ];
        
        $query->joinWith(['vendor']);*/

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'korin_id' => $this->korin_id,
            'vendor_id' => $this->vendor_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        /* $query->andFilterWhere(['like', 'sap_vendor.vendor', $this->getAttribute('vendor.kode')])
            ->andFilterWhere(['like', 'sap_vendor.`name of vendor`', $this->getAttribute('vendor.nama')]);
*/

        return $dataProvider;
    }

   /* function attributes()
    {
        return array_merge(parent::attributes(), ['vendor.kode', 'vendor.nama']);
    }*/
}
