<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Activity;

/**
 * ActivitySearch represents the model behind the search form about `frontend\models\Activity`.
 */
class ActivitySearch extends Activity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'activity_group_id'], 'integer'],
            [['nama'], 'safe'],
            [['singkatan'], 'safe'],
            [['activity_group.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['activity_group.nama'] = [
              'asc' => ['activity_group.nama' => SORT_ASC],
              'desc' => ['activity_group.nama' => SORT_DESC],
        ];

        $query->joinWith(['activityGroup']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'activity_group_id' => $this->activity_group_id,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);
        $query->andFilterWhere(['like', 'singkatan', $this->singkatan]);
        $query->andFilterWhere(['like', 'activity_group.nama', $this->getAttribute('activity_group.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['activity_group.nama']);
    }
}
