<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_wa".
 *
 * @property integer $id
 * @property integer $ppl_vendor_id
 * @property integer $ppl_buyer_id
 * @property string $file
 * @property string $locator
 *
 * @property PplVendor $pplVendor
 * @property PplBuyer $pplBuyer
 */
class PplWa extends \yii\db\ActiveRecord
{
    public $uploadedFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_wa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ppl_vendor_id', 'ppl_buyer_id'], 'required'],
            [['ppl_vendor_id', 'ppl_buyer_id'], 'integer'],
            [['file', 'locator'], 'string', 'max' => 255],
            [['uploadedFile'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ppl_vendor_id' => 'Ppl Vendor ID',
            'ppl_buyer_id' => 'Ppl Buyer ID',
            'file' => 'File',
            'locator' => 'Locator',
            'uploadedFile' => 'File',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplVendor()
    {
        return $this->hasOne(PplVendor::className(), ['id' => 'ppl_vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplBuyer()
    {
        return $this->hasOne(PplBuyer::className(), ['id' => 'ppl_buyer_id']);
    }
}
