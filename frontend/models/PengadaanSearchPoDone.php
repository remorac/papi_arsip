<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Pengadaan;

/**
 * PengadaanSearch represents the model behind the search form about `frontend\models\Pengadaan`.
 */
class PengadaanSearchPoDone extends Pengadaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'barang_jasa_id', 'metoda_id', 'purchaser_personel_procurement_id', 'level_approval_id', 'nilai_oe', 'delivery_time', 'created_by', 'updated_by'], 'integer'],
            [['kode', 'nama', 'start_plan', 'requirement_date', 'created_at', 'updated_at'], 'safe'],
            [['personel.nama', 'metoda.nama', 'barangJasa.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'personel.nama' => Yii::t('app', 'Personel'),
            'metoda.nama' => Yii::t('app', 'Metoda'),
            'barangJasa.nama' => Yii::t('app', 'Barang/Jasa'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengadaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['metoda.nama'] = [
              'asc' => ['metoda.nama' => SORT_ASC],
              'desc' => ['metoda.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['barangJasa.nama'] = [
              'asc' => ['barang_jasa.nama' => SORT_ASC],
              'desc' => ['barang_jasa.nama' => SORT_DESC],
        ];

        $query->joinWith(['purchaserPersonelProcurement.personel']);
        $query->joinWith(['metoda']);
        $query->joinWith(['barangJasa']);
        $query->joinWith(['pengadaanActivities']);
        $query->joinWith(['pengadaanDurasiAdjusteds']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->where("
                        pengadaan_durasi_adjusted.pengadaan_id is not null and 
                        pengadaan_durasi_adjusted.durasi != 0 and
                        pengadaan_durasi_adjusted.pengadaan_id = pengadaan_activity.pengadaan_id and
                        (pengadaan_activity.pengadaan_id not in 
                            (select pengadaan_id from pengadaan_activity where (end_date ='0000-00-00' or end_date is null))
                        )
                        and 
                        (select count(*) from pengadaan_durasi_adjusted where durasi !=0 and pengadaan_id = pengadaan.id) = (select count(*) from pengadaan_activity where end_date is not null and pengadaan_id = pengadaan.id)
                    ");

        $query->andFilterWhere([
            'id' => $this->id,
            'barang_jasa_id' => $this->barang_jasa_id,
            'metoda_id' => $this->metoda_id,
            'purchaser_personel_procurement_id' => $this->purchaser_personel_procurement_id,
            'nilai_oe' => $this->nilai_oe,
            'start_plan' => $this->start_plan,
            'requirement_date' => $this->requirement_date,
            'delivery_time' => $this->delivery_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'pengadaan.kode', $this->kode])
            ->andFilterWhere(['like', 'pengadaan.nama', $this->nama])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personel.nama')])
            ->andFilterWhere(['like', 'metoda.nama', $this->getAttribute('metoda.nama')])
            ->andFilterWhere(['like', 'barang_jasa.nama', $this->getAttribute('barangJasa.nama')]);

        $query->groupBy("pengadaan.id");
        $query->orderBy("pengadaan.start_plan DESC");

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['personel.nama', 'metoda.nama', 'barangJasa.nama']);
    }
}
