<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "locator_procurement".
 *
 * @property integer $id
 * @property string $kode_klasifikasi
 * @property string $purchase_order
 * @property string $nama_pengadaan
 * @property integer $tahun
 * @property integer $box
 * @property string $locator
 */
class LocatorProcurement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }*/
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locator_procurement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_klasifikasi', 'purchase_order', 'nama_pengadaan', 'tahun', 'box', 'locator'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_klasifikasi' => 'Kode Klasifikasi',
            'purchase_order' => 'Purchase Order',
            'nama_pengadaan' => 'Nama Pengadaan',
            'tahun' => 'Tahun',
            'box' => 'Box',
            'locator' => 'Locator',
        ];
    }
}
