<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kpi_biaya_report".
 *
 * @property string $tahun_bulan
 * @property integer $jenis
 * @property string $realisasi
 * @property string $nilai_oe
 * @property double $index
 * @property integer $status
 */
class KpiBiayaReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_biaya_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun_bulan', 'jenis'], 'required'],
            [['tahun_bulan'], 'safe'],
            [['jenis', 'status'], 'integer'],
            [['realisasi', 'nilai_oe', 'index'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun_bulan' => Yii::t('app', 'Tahun-Bulan'),
            'jenis' => Yii::t('app', 'Jenis'),
            'realisasi' => Yii::t('app', 'Realisasi'),
            'nilai_oe' => Yii::t('app', 'Nilai Oe'),
            'index' => Yii::t('app', 'Index'),
            'status' => Yii::t('app', 'Status'),
            'teksJenis' => Yii::t('app', 'Jenis'),
        ];
    }

    public function refreshKpiBiaya()
    {
        $connection = Yii::$app->db;

        
        //$sql = 'truncate kpi_durasi_report';

        try {
            $command = $connection->createCommand();
            $command->truncateTable('kpi_biaya_report');
            $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
        

        //$this->truncate();

        $sql = 'insert into kpi_biaya_report
select tahun_bulan
    , jenis
    , after_discount realisasi
    , nilai_oe
    , @index := after_discount / nilai_oe `index`
    , if(@index <= 1, 1, 0) status
from 
    (select
        date_format(pengadaan_activity.end_date, "%Y-%m") tahun_bulan
        , pengadaan_po.`Purchasing_Document` AS `Purchasing Document`
        , pengadaan_po.pengadaan_id AS pengadaan_id
        , pengadaan.barang_jasa_id AS jenis
        , @before_discount:= sum(harga) AS before_discount
        , @after_discount := sum((sap_po.`Order Quantity` * sap_po.`Net price`) / sap_po.`Price Unit`) AS after_discount
        , pengadaan.nilai_oe
    from pengadaan_po
    left join (
        select `Purchasing Document`
            , `Document Date`
            , `Order Quantity`
            , `Net price`
            , `Price Unit`
            , Item
            , korin.tgl_terima
        from sap_po
            inner join pengadaan_po on pengadaan_po.purchasing_document = sap_po.`Purchasing Document`
            inner join pengadaan_korin on pengadaan_korin.pengadaan_id = pengadaan_po.pengadaan_id    
            inner join korin on korin.id = pengadaan_korin.korin_id
        group by `Purchasing Document`
        ) sap_po on pengadaan_po.`Purchasing_Document` = sap_po.`Purchasing Document`
    left join pengadaan on pengadaan_po.pengadaan_id = pengadaan.id
    left join pengadaan_activity on pengadaan_activity.pengadaan_id = pengadaan.id
    left join harga_sebelum_diskon on harga_sebelum_diskon.purchasing_document = sap_po.`Purchasing Document` and harga_sebelum_diskon.item_po = sap_po.Item
    where pengadaan_activity.activity_id = 11
    group by date_format(sap_po.tgl_terima, "%Y-%m")
        , pengadaan.barang_jasa_id
) q';
        
        try {
            $command = $connection->createCommand($sql);
            return $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
    }

    public function getTeksJenis()
    {
        switch($this->jenis){
            case 0:
                return 'Jasa';
            case 1:
                return 'Barang';
        }
        return null;
    }

    public function getTeksStatus()
    {
        switch($this->status){
            case 0:
                return 'Bad';
            case 1:
                return 'OK';
        }
        return null;
    }


}
