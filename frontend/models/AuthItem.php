<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 */
class AuthItem extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getRoleSelect2Items()
    {
        $select2Items = AuthItem::find()
            //->select(['replace(name, "_", " ") as label',
            ->select(['name as label',
                'name as value']
                )
            ->where(['type' => \yii\rbac\Item::TYPE_ROLE])
            ->asArray()
            ->all();

        foreach($select2Items as $row){
            //$data[$row['value']] = ucwords($row['label']);
            $data[$row['value']] = $row['label'];
        }

        if(!isset($data)){
            $data = [];
        }

        return $data;
    }
}
