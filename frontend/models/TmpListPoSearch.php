<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TmpListPo;

/**
 * TmpListPoSearch represents the model behind the search form about `frontend\models\TmpListPo`.
 */
class TmpListPoSearch extends TmpListPo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['po', 'id', 'remark'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TmpListPo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['id'] = [
              'asc' => ['pengadaan.id' => SORT_ASC],
              'desc' => ['pengadaan.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['remark'] = [
              'asc' => ['pengadaan.nama' => SORT_ASC],
              'desc' => ['pengadaan.nama' => SORT_DESC],
        ];

        $query->joinWith(['pengadaanPo']);
        $query->joinWith(['pengadaan']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'po', $this->po]);
        $query->andFilterWhere(['like', 'pengadaan.id', $this->getAttribute('id')]);
        $query->andFilterWhere(['like', 'pengadaan.nama', $this->getAttribute('remark')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(),
        [
            'id',
            'remark',
        ]);
    }
}
