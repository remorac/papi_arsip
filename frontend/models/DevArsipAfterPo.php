<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "dev_arsip_after_po".
 *
 * @property integer $id
 * @property string $pdf_filename
 * @property string $no_dokumen
 * @property string $jenis_dokumen
 * @property string $tgl_dokumen
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class DevArsipAfterPo extends \yii\db\ActiveRecord
{
    //added attribute to replace `pdf_filename` in create and update form
    public $dokumen;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_arsip_after_po';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pdf_filename', 'no_dokumen'], 'required'],
            [['pdf_filename', 'keterangan'], 'string'],
            [['tgl_dokumen', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['no_dokumen'], 'string', 'max' => 100],
            [['jenis_dokumen'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pdf_filename' => Yii::t('app', 'Pdf Filename'),
            'no_dokumen' => Yii::t('app', 'No Dokumen'),
            'jenis_dokumen' => Yii::t('app', 'Jenis Dokumen'),
            'tgl_dokumen' => Yii::t('app', 'Tgl Dokumen'),
            'keterangan' => Yii::t('app', 'No. PO'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
