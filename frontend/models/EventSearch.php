<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Event;

/**
 * EventSearch represents the model behind the search form about `frontend\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_id', 'activity_id', 'tempat_id', 'created_by', 'updated_by'], 'integer'],
            [['tanggal', 'jam', 'created_at', 'updated_at'], 'safe'],
            [['pengadaan.nama', 'activity.nama', 'purchaserPersonelProcurement.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['pengadaan.nama'] = [
              'asc' => ['pengadaan.nama' => SORT_ASC],
              'desc' => ['pengadaan.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['activity.nama'] = [
              'asc' => ['activity.nama' => SORT_ASC],
              'desc' => ['activity.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['purchaserPersonelProcurement.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $query->joinWith(['pengadaan']);
        $query->joinWith(['activity']);
        $query->joinWith(['purchaserPersonelProcurement']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_id' => $this->pengadaan_id,
            'activity_id' => $this->activity_id,
            'tanggal' => $this->tanggal,
            'jam' => $this->jam,
            'tempat_id' => $this->tempat_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'pengadaan.nama', $this->getAttribute('pengadaan.nama')]);
        $query->andFilterWhere(['like', 'activity.nama', $this->getAttribute('activity.nama')]);
        $query->andFilterWhere(['like', 'purchaserPersonelProcurement.nama', $this->getAttribute('purchaserPersonelProcurement.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['pengadaan.nama', 'activity.nama', 'purchaserPersonelProcurement.nama']);
    }

}
