<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property integer $activity_id
 * @property string $tanggal
 * @property string $jam
 * @property integer $tempat_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Pengadaan $pengadaan
 * @property Activity $activity
 * @property Tempat $tempat
 */
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'activity_id', 'tempat_id', 'tanggal', 'jam'], 'required'],
            [['pengadaan_id', 'activity_id', 'tempat_id', 'created_by', 'updated_by'], 'integer'],
            //[['tanggal'], 'date'],
            [['tanggal', 'jam', 'created_at', 'updated_at'], 'safe'],
            [['pengadaan_id', 'activity_id', 'tanggal'], 'unique', 'targetAttribute' => ['pengadaan_id', 'activity_id', 'tanggal'], 'message' => 'Combination of Pengadaan, Activity, and Tanggal Dokumen is already taken'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pengadaan_id' => Yii::t('app', 'Pengadaan'),
            'activity_id' => Yii::t('app', 'Activity'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'jam' => Yii::t('app', 'Jam'),
            'tempat_id' => Yii::t('app', 'Tempat'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTempat()
    {
        return $this->hasOne(Tempat::className(), ['id' => 'tempat_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaserPersonelProcurement()
    {
        return $this->hasOne(PersonelProcurement::className(), ['id' => 'purchaser_personel_procurement_id'])
                ->via('pengadaan');
    }

}
