<?php
namespace frontend\models;

use yii\base\Model;
use yii\web\ImportedFile;
use Yii;

/**
 * ImportForm is the model behind the Import form.
 */
class ImportForm extends Model
{
    const CSV_SEPARATOR = ';';
    const CSV_DELIMITER = '"';

    /**
     * @var ImportedFile file attribute
     */
    public $material;
    public $pr;
    public $po;
    public $rfq;
    public $grsa;
    public $gi;
    public $reservasi;
    public $tracking;
    public $vendor;

    public $me2m;
    public $delivery;
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['material', 'pr', 'po', 'rfq', 'grsa', 'reservasi', 'tracking', 'vendor', 'me2m', 'delivery'], 'file', 'extensions' => 'csv'],//, 'mimeTypes' => 'text/csv'],
            [['material', 'pr', 'po', 'rfq', 'grsa', 'reservasi', 'tracking', 'vendor', 'me2m', 'delivery'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'material' => Yii::t('app', 'Material'),
            'pr' => Yii::t('app', 'Purchase Requisition'),
            'po' => Yii::t('app', 'Purchase Order'),
            'rfq' => Yii::t('app', 'Request For Quotation'),
            'grsa' => Yii::t('app', 'Goods Receipt/ Service Acceptance'),
            'reservasi' => Yii::t('app', 'Reservasi'),
            'tracking' => Yii::t('app', 'Tracking'),
            'vendor' => Yii::t('app', 'Vendor'),

            'me2m' => Yii::t('app', 'ME2M file'),
            'delivery' => Yii::t('app', 'ME2M delivery'),
        ];
    }

    function csvFileToSqlInsertTable($csvFileName, $tableName)
    {
        $sql = 'insert into `'. $tableName .'` (`';
        $file = fopen($csvFileName, 'r');
        $headerColumns = fgetcsv($file, 0, ';');
        //$headerColumns = explode(ImportForm::CSV_SEPARATOR, $header);
        $headerColumns = array_map('trim', $headerColumns);
        $headerColumns = $this->numberDuplicateItem($headerColumns);
        $sql .= implode('`,`', $headerColumns);
        $sql .= '`) values ';
        $isFirst = true;
        $values = '';
        //while($line = fgets($file)){
        while($line = fgetcsv($file, 0, ';')){
        //$i = 0;
        //while($i++ < 2000){
            //$line = fgetcsv($file, 0, ';');
            if($isFirst){
                $isFirst = false;
                $values .= "\n\r(";
            }
            else {
                $values .= ",\n\r(";
            }
            //$columns = explode(ImportForm::CSV_SEPARATOR, $line);
            $columns = array_map(array($this, 'encloseString'), $line);
            //array_walk($columns, array($this, 'encloseString'));
            $values .= implode(',', $columns);
            $values .= ')';
        }
        $sql .= $values . ' on duplicate key update ';
        $values = '';
        $isFirst = true;
        foreach($headerColumns as $columnName){
            if($isFirst){
                $isFirst = false;
            }
            else {
                $values .= ', ';
            }
            $values .= '`' . $columnName . '` = ' . 'values(`' . $columnName . '`)';
        }
        $sql .= $values;
        fclose($file);
        return $sql;
    }

    function insertSapMaterialFromCsvFile($csvFileName)
    {
        $tableName = 'sap_material';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapPoFromCsvFile($csvFileName)
    {
        $tableName = 'sap_po';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapDeliveryFromCsvFile($csvFileName)
    {
        $tableName = 'sap_delivery';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapPrFromCsvFile($csvFileName)
    {
        $tableName = 'sap_pr_me5a';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapRfqFromCsvFile($csvFileName)
    {
        $tableName = 'sap_rfq';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapGrSaFromCsvFile($csvFileName)
    {
        $tableName = 'sap_grsa';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapReservasiFromCsvFile($csvFileName)
    {
        $tableName = 'sap_reservasi';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapTrackingFromCsvFile($csvFileName)
    {
        $tableName = 'sap_tracking';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function insertSapVendorFromCsvFile($csvFileName)
    {
        $tableName = 'sap_vendor';
        return $this->insertSapTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapMaterialFromCsvFile($csvFileName)
    {
        $tableName = 'sap_material';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapPoFromCsvFile($csvFileName)
    {
        $tableName = 'sap_po';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapDeliveryFromCsvFile($csvFileName)
    {
        $tableName = 'sap_delivery';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapPrFromCsvFile($csvFileName)
    {
        $tableName = 'sap_pr_me5a';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapRfqFromCsvFile($csvFileName)
    {
        $tableName = 'sap_rfq';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapGrsaFromCsvFile($csvFileName)
    {
        $tableName = 'sap_grsa';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapReservasiFromCsvFile($csvFileName)
    {
        $tableName = 'sap_reservasi';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapTrackingFromCsvFile($csvFileName)
    {
        $tableName = 'sap_tracking';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

    function createTableSapVendorFromCsvFile($csvFileName)
    {
        $tableName = 'sap_vendor';
        return $this->createTableFromCsvFile($csvFileName, $tableName);
    }

   function insertSapTableFromCsvFile($csvFileName, $tableName)
   {
        $sql = $this->csvFileToSqlInsertTable($csvFileName, $tableName);
        $connection = Yii::$app->db;

        try {
            $command = $connection->createCommand($sql);
            $rowCount = $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
        return $rowCount;
    }

    function encloseString($string, $delimiter = '"', $leftDelimiter = null, $righDelimiter = null)
    {
        if(strlen($string) < 1){
            if(strlen($leftDelimiter) > 0 || strlen($righDelimiter) > 0){
                return $leftDelimiter . $righDelimiter;
            }
            return $delimiter . $delimiter;
        }
        $string = str_replace($delimiter, '\\' . $delimiter, $string);

        if(strlen($leftDelimiter) > 0 || strlen($righDelimiter) > 0){
            $enclosedString = str_pad($string, strlen($string) + strlen($leftDelimiter), $leftDelimiter, STR_PAD_LEFT);
            $enclosedString = str_pad($enclosedString, strlen($enclosedString) + strlen($righDelimiter), $righDelimiter, STR_PAD_RIGHT);
        }
        else{
            $enclosedString = str_pad($string, strlen($string) + strlen($delimiter) * 2, $delimiter, STR_PAD_BOTH);
        }
        return $enclosedString;
    }

    function createTableFromCsvFile($csvFileName, $tableName)
    {
        $file = fopen($csvFileName, 'r');
        $headerColumns = fgetcsv($file, 0, ';');
        //$headerColumns = explode(ImportForm::CSV_SEPARATOR, $header);
        $headerColumns = array_map('trim', $headerColumns);
        //$headerColumns = array_map([$this, 'encloseString'], $headerColumns, array_fill(0, count($headerColumns), '`'));

        for($i = 0; $i < count($headerColumns); $i++){
            $headerColumns[$i] = $this->encloseString($headerColumns[$i], '`');
        }

        $isFirst = true;
        $values = '';
        $values = [];

        while($line = fgetcsv($file, 0, ';')){
            $values[] = $line;
        }
        fclose($file);
        
        $sql = $this->csvFileToSqlCreateTable($csvFileName, $tableName);
        $connection = Yii::$app->db;

        try {
            $command = $connection->createCommand($sql);
            $rowCount = $command->execute();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
        return $rowCount;
    }

    function csvFileToSqlCreateTable($csvFileName, $tableName)
    {
        $sql = 'create table `'. $tableName .'` (`';
        $file = fopen($csvFileName, 'r');
        $headerColumns = fgetcsv($file, 0, ';');
        $headerColumns = $this->numberDuplicateItem($headerColumns);
        //$headerColumns = explode(ImportForm::CSV_SEPARATOR, $header);
        $headerColumns = array_map('trim', $headerColumns);
        $sql .= implode('` text,`', $headerColumns);
        $sql .= '` text)'; 
        fclose($file);
        return $sql;
    }

    function numberDuplicateItem($array)
    {
        $count = count($array);
        for($i = 0; $i < $count; $i++){
            $numbering = 0;
            for($j = $i + 1; $j < $count; $j++){
                if($array[$i] == $array[$j]){
                    $array[$j] = $array[$j] . ++$numbering;
                }
            }
        }
        return $array;
    }

    function createTable($tableName, $fields) {
        Yii::$app->db->createCommand("
                create table if not exists $tableName (
                    $fields
                );
            ")->execute();
    }

    function insertRow($tableName, $values) {
        Yii::$app->db->createCommand("
                insert into $tableName values (
                    $values
                );
            ")->execute();
    }

}
?>