<?php

namespace frontend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ppl_korin_document".
 *
 * @property integer $id
 * @property integer $ppl_korin_id
 * @property string $file
 */
class PplKorinDocument extends \yii\db\ActiveRecord
{
    public $uploadedFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_korin_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ppl_korin_id', 'file'], 'required'],
            [['ppl_korin_id'], 'integer'],
            [['file'], 'string', 'max' => 255],
            [['uploadedFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ppl_korin_id' => 'Ppl Korin ID',
            'file' => 'File',
        ];
    }
}
