<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "durasi_standar".
 *
 * @property integer $id
 * @property string $tanggal_berlaku
 * @property integer $activity_id
 * @property integer $level_approval_id
 * @property integer $durasi
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property LevelApproval $levelApproval
 * @property Metoda $metoda
 * @property Activity $activity
 * @property User $createdBy
 * @property User $updatedBy
 */
class DurasiStandar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'durasi_standar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_berlaku', 'activity_id', 'level_approval_id', 'durasi'], 'required'],
            [['tanggal_berlaku', 'created_at', 'updated_at'], 'safe'],
            [['activity_id', 'level_approval_id', 'durasi', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tanggal_berlaku' => Yii::t('app', 'Tanggal Berlaku'),
            'activity_id' => Yii::t('app', 'Activity ID'),
            'level_approval_id' => Yii::t('app', 'Level Approval ID'),
            'durasi' => Yii::t('app', 'Durasi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelApproval()
    {
        return $this->hasOne(LevelApproval::className(), ['id' => 'level_approval_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
