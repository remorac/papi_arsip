<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_buyer".
 *
 * @property integer $id
 * @property string $name
 * @property string $initial
 *
 * @property PplWa[] $pplWas
 * @property PplWaUser[] $pplWaUsers
 */
class PplBuyer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_buyer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'initial'], 'required'],
            [['name', 'initial'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'initial' => 'Initial',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplWas()
    {
        return $this->hasMany(PplWa::className(), ['ppl_buyer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplWaUsers()
    {
        return $this->hasMany(PplWaUser::className(), ['ppl_buyer_id' => 'id']);
    }
}
