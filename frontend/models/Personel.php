<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "personel".
 *
 * @property integer $id
 * @property string $nik
 * @property string $nama
 * @property string $singkatan
 * @property integer $unit_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Korin[] $korins
 * @property KorinAssignment[] $korinAssignments
 * @property Pengadaan[] $pengadaans
 * @property Unit $unit
 * @property User $createdBy
 * @property User $updatedBy
 */
class Personel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['unit_id', 'posisi_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nik'], 'string', 'max' => 20],
            [['nama'], 'string', 'max' => 100],
            [['singkatan'], 'string', 'max' => 10],
            [['email', 'gmail'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nik' => Yii::t('app', 'NIP'),
            'nama' => Yii::t('app', 'Nama'),
            'singkatan' => Yii::t('app', 'Singkatan'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'posisi_id' => Yii::t('app', 'Posisi ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'email' => Yii::t('app', 'Email'),
            'gmail' => Yii::t('app', 'Gmail'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorins()
    {
        return $this->hasMany(Korin::className(), ['mgr_personel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorinAssignments()
    {
        return $this->hasMany(KorinAssignment::className(), ['mgr_personel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonelProcurement()
    {
        return $this->hasOne(PersonelProcurement::className(), ['personel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosisi()
    {
        return $this->hasOne(Posisi::className(), ['id' => 'posisi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getDropdownDisplayItem()
    {
        return $this->nama . ' : ' . $this->id;
    }

    /**
     * @return array
     */
    public static function getSelect2Items()
    {
        $select2Items = Personel::find()
            ->select(['concat(nama, " : ", id ) as label',
                'id as value']
                )
            ->asArray()
            ->all();

        foreach($select2Items as $row){
            $data[$row['value']] = $row['label'];
        }

        if(!isset($data)){
            $data = [];
        }

        return $data;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
