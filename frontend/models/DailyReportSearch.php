<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\DailyReport;

/**
 * DailyReportSearch represents the model behind the search form about `frontend\models\DailyReport`.
 */
class DailyReportSearch extends DailyReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'daily_purchaser_id', 'daily_method_id', 'am_daily_activity_id', 'pm_daily_activity_id'], 'integer'],
            [['name', 'user_initial', 'activity_issue', 'is_need_contract', 'am_target', 'pm_start', 'pm_finish', 'plan_start', 'plan_finish', 'real_start', 'real_finish'], 'safe'],
            [['dailyCategory.id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DailyReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['dailyCategory.id'] = [
              'asc' => ['dailyCategory.id' => SORT_ASC],
              'desc' => ['dailyCategory.id' => SORT_DESC],
        ];

        $query->joinWith(['dailyPurchaser']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (is_object(DailyPurchaser::find()->where(['user_id'=>Yii::$app->user->id])->one())) {
            $query->where(['daily_purchaser_id' => DailyPurchaser::find()->where(['user_id'=>Yii::$app->user->id])->one()->id]);
        }

        $query->andFilterWhere([
            'daily_report.id' => $this->id,
            'daily_category_id' => $this->getAttribute('dailyCategory.id'),
            'daily_purchaser_id' => $this->daily_purchaser_id,
            'daily_method_id' => $this->daily_method_id,
            'am_daily_activity_id' => $this->am_daily_activity_id,
            'am_target' => $this->am_target,
            'pm_daily_activity_id' => $this->pm_daily_activity_id,
            'pm_start' => $this->pm_start,
            'pm_finish' => $this->pm_finish,
            'plan_start' => $this->plan_start,
            'plan_finish' => $this->plan_finish,
            'real_start' => $this->real_start,
            'real_finish' => $this->real_finish,
        ]);

        $query->andFilterWhere(['like', 'daily_report.name', $this->name])
            ->andFilterWhere(['like', 'user_initial', $this->user_initial])
            ->andFilterWhere(['like', 'activity_issue', $this->activity_issue])
            ->andFilterWhere(['like', 'is_need_contract', $this->is_need_contract]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), 
        [  
            'dailyCategory.id',
        ]);
    }
}
