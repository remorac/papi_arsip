<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DailyReport[] $dailyReports
 */
class DailyCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyPurchasers()
    {
        return $this->hasMany(DailyPurchaser::className(), ['daily_category_id' => 'id']);
    }
}
