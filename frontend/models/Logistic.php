<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "logistic".
 *
 * @property integer $id
 * @property string $filename
 * @property string $purchase_order
 * @property string $location
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Logistic extends \yii\db\ActiveRecord
{
    public $pdfFiles;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename'], 'required'],
            [['filename'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['purchase_order', 'location'], 'string', 'max' => 191],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['pdfFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'mimeTypes' => 'application/pdf', 'maxFiles' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'purchase_order' => 'Purchase Order',
            'location' => 'Location',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    public function upload()
    {
        foreach ($this->pdfFiles as $pdfFile) {
            $path = Yii::getAlias('@uploads/logistic/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }

            $filepath = $path . $pdfFile->baseName . '.' . $pdfFile->extension;
            // if (file_exists($filepath)) {
            //     $filepath.= '-'.date('YmdHis').'.'.$pdfFile->extension;
            // }
            $pdfFile->saveAs($filepath);
        }
        return true;
    }
}
