<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_detail".
 *
 * @property integer $id
 * @property string $tgl_permintaan_barang
 * @property string $no_ppl
 * @property string $requestor
 * @property string $area
 * @property string $deskripsi
 * @property string $satuan
 * @property double $qty
 * @property string $tool_non
 * @property string $status_pengadaan
 * @property string $status_order
 * @property string $status_gr
 * @property string $status_gi_stock
 * @property string $status_pembayaran
 * @property string $buyer
 * @property string $vendor_rfq
 * @property string $supplier
 * @property double $barang_datang
 * @property double $kekurangan_brg
 * @property string $tgl_terima_barang
 * @property string $penerima_barang
 * @property string $lokasi_penyimpanan
 * @property double $jml_sdh_diambil
 * @property string $tgl_pengambilan
 * @property string $pengambil_barang
 * @property double $stock_di_kontainer
 * @property string $tgl_terima_invoice
 * @property double $price
 * @property double $total_price
 * @property string $no_korin
 * @property string $tgl_diserahkan_ke_afis
 * @property string $tgl_pembayaran
 * @property string $keterangan
 * @property double $real_time_days
 * @property string $no_invoice
 * @property double $harga_invoice
 * @property double $selisih
 * @property string $area_detail
 * @property string $sm
 * @property string $editor_data
 * @property string $no_release
 * @property string $no_wbs
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class PplDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tgl_permintaan_barang', 'tgl_terima_barang', 'tgl_pengambilan', 'tgl_terima_invoice', 'tgl_diserahkan_ke_afis', 'tgl_pembayaran'], 'safe'],
            [['no_ppl', 'requestor', 'area', 'deskripsi', 'satuan', 'tool_non', 'status_pengadaan', 'status_order', 'status_gr', 'status_gi_stock', 'status_pembayaran', 'buyer', 'vendor_rfq', 'supplier', 'penerima_barang', 'lokasi_penyimpanan', 'pengambil_barang', 'no_korin', 'keterangan', 'no_invoice', 'area_detail', 'sm', 'editor_data', 'no_release', 'no_wbs'], 'string'],
            [['qty', 'barang_datang', 'kekurangan_brg', 'jml_sdh_diambil', 'stock_di_kontainer', 'price', 'total_price', 'real_time_days', 'harga_invoice', 'selisih'], 'number'],
            [['deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tgl_permintaan_barang' => 'Tgl Permintaan Barang',
            'no_ppl' => 'No Ppl',
            'requestor' => 'Requestor',
            'area' => 'Area',
            'deskripsi' => 'Deskripsi',
            'satuan' => 'Satuan',
            'qty' => 'Qty',
            'tool_non' => 'Tool Non',
            'status_pengadaan' => 'Status Pengadaan',
            'status_order' => 'Status Order',
            'status_gr' => 'Status Gr',
            'status_gi_stock' => 'Status Gi Stock',
            'status_pembayaran' => 'Status Pembayaran',
            'buyer' => 'Buyer',
            'vendor_rfq' => 'Vendor Rfq',
            'supplier' => 'Supplier',
            'barang_datang' => 'Barang Datang',
            'kekurangan_brg' => 'Kekurangan Brg',
            'tgl_terima_barang' => 'Tgl Terima Barang',
            'penerima_barang' => 'Penerima Barang',
            'lokasi_penyimpanan' => 'Lokasi Penyimpanan',
            'jml_sdh_diambil' => 'Jml Sdh Diambil',
            'tgl_pengambilan' => 'Tgl Pengambilan',
            'pengambil_barang' => 'Pengambil Barang',
            'stock_di_kontainer' => 'Stock Di Kontainer',
            'tgl_terima_invoice' => 'Tgl Terima Invoice',
            'price' => 'Price',
            'total_price' => 'Total Price',
            'no_korin' => 'No Korin',
            'tgl_diserahkan_ke_afis' => 'Tgl Diserahkan Ke Afis',
            'tgl_pembayaran' => 'Tgl Pembayaran',
            'keterangan' => 'Keterangan',
            'real_time_days' => 'Real Time Days',
            'no_invoice' => 'No Invoice',
            'harga_invoice' => 'Harga Invoice',
            'selisih' => 'Selisih',
            'area_detail' => 'Area Detail',
            'sm' => 'Sm',
            'editor_data' => 'Editor Data',
            'no_release' => 'No Release',
            'no_wbs' => 'No Wbs',
            'deleted_at' => 'Deleted At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
