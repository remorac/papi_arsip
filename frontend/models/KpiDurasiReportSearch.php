<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KpiDurasiReport;

/**
 * KpiDurasiReportSearch represents the model behind the search form about `frontend\models\KpiDurasiReport`.
 */
class KpiDurasiReportSearch extends KpiDurasiReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun_bulan'], 'safe'],
            [['jenis', 'status'], 'integer'],
            [['durasi_plan', 'durasi_real', 'index'], 'number'],
            [['teksJenis'], 'safe'],
            [['teksStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KpiDurasiReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['teksJenis'] = [
              'asc' => ['if(jenis = 1, "Barang", "Jasa")' => SORT_ASC],
              'desc' => ['if(jenis = 1, "Barang", "Jasa")' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['teksStatus'] = [
              'asc' => ['if(status = 1, "OK", "Bad")' => SORT_ASC],
              'desc' => ['if(status = 1, "OK", "Bad")' => SORT_DESC],
        ];

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'jenis' => $this->jenis,
            'durasi_plan' => $this->durasi_plan,
            'durasi_real' => $this->durasi_real,
            'index' => $this->index,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'tahun_bulan', $this->tahun_bulan]);

        $query->andFilterWhere(['like', 'if(jenis = 1, "Barang", "Jasa")', $this->getAttribute('teksJenis')]);
        $query->andFilterWhere(['like', 'if(status = 1, "OK", "Bad")', $this->getAttribute('teksStatus')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['teksJenis', 'teksStatus']);
    }
}
