<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_reservasi".
 *
 * @property string $Network
 * @property string $BOM item
 * @property string $Project definition
 * @property string $WBS element
 * @property string $Reservation
 * @property string $Item
 * @property string $Status
 * @property string $Material
 * @property string $Material text
 * @property string $Quantity in UnE
 * @property string $Unit of entry
 * @property string $Missing part
 * @property string $Price/LCurrency
 * @property string $Currency
 * @property string $Unloading Point
 * @property string $Purchase ord. exists
 * @property string $Price unit
 * @property string $G/L acct
 * @property string $Purchase requisition
 * @property string $Reservation/Purc.req
 * @property string $Item category
 * @property string $Requisition item
 * @property string $Reqmts quantity
 * @property string $Activity
 * @property string $Element
 * @property string $Component rqmts qty
 * @property string $Deletion Indicator
 * @property string $Requirements date
 * @property string $Requirement quantity
 * @property string $Base unit of measure
 * @property string $Quantity received
 * @property string $Quantity withdrawn
 * @property string $Qty for avail. cntrl.
 * @property string $Shortfall quantity
 * @property string $Factory calendar
 * @property string $Material group
 * @property string $Item text line 1
 * @property string $Movement Allowed
 * @property string $Movement type
 * @property string $Description
 * @property string $Order category
 * @property string $Delivery time (days)
 * @property string $PS text
 * @property string $Planned order
 */
class SapReservasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_reservasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Network', 'BOM item', 'Project definition', 'WBS element', 'Reservation', 'Item', 'Status', 'Material', 'Material text', 'Quantity in UnE', 'Unit of entry', 'Missing part', 'Price/LCurrency', 'Currency', 'Unloading Point', 'Purchase ord. exists', 'Price unit', 'G/L acct', 'Purchase requisition', 'Reservation/Purc.req', 'Item category', 'Requisition item', 'Reqmts quantity', 'Activity', 'Element', 'Component rqmts qty', 'Deletion Indicator', 'Requirements date', 'Requirement quantity', 'Base unit of measure', 'Quantity received', 'Quantity withdrawn', 'Qty for avail. cntrl.', 'Shortfall quantity', 'Factory calendar', 'Material group', 'Item text line 1', 'Movement Allowed', 'Movement type', 'Description', 'Order category', 'Delivery time (days)', 'PS text', 'Planned order'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Network' => Yii::t('app', 'Network'),
            'BOM item' => Yii::t('app', 'Bom Item'),
            'Project definition' => Yii::t('app', 'Project Definition'),
            'WBS element' => Yii::t('app', 'Wbs Element'),
            'Reservation' => Yii::t('app', 'Reservation'),
            'Item' => Yii::t('app', 'Item'),
            'Status' => Yii::t('app', 'Status'),
            'Material' => Yii::t('app', 'Material'),
            'Material text' => Yii::t('app', 'Material Text'),
            'Quantity in UnE' => Yii::t('app', 'Quantity In  Un E'),
            'Unit of entry' => Yii::t('app', 'Unit Of Entry'),
            'Missing part' => Yii::t('app', 'Missing Part'),
            'Price/LCurrency' => Yii::t('app', 'Price/ Lcurrency'),
            'Currency' => Yii::t('app', 'Currency'),
            'Unloading Point' => Yii::t('app', 'Unloading  Point'),
            'Purchase ord. exists' => Yii::t('app', 'Purchase Ord  Exists'),
            'Price unit' => Yii::t('app', 'Price Unit'),
            'G/L acct' => Yii::t('app', 'G/ L Acct'),
            'Purchase requisition' => Yii::t('app', 'Purchase Requisition'),
            'Reservation/Purc.req' => Yii::t('app', 'Reservation/ Purc Req'),
            'Item category' => Yii::t('app', 'Item Category'),
            'Requisition item' => Yii::t('app', 'Requisition Item'),
            'Reqmts quantity' => Yii::t('app', 'Reqmts Quantity'),
            'Activity' => Yii::t('app', 'Activity'),
            'Element' => Yii::t('app', 'Element'),
            'Component rqmts qty' => Yii::t('app', 'Component Rqmts Qty'),
            'Deletion Indicator' => Yii::t('app', 'Deletion  Indicator'),
            'Requirements date' => Yii::t('app', 'Requirements Date'),
            'Requirement quantity' => Yii::t('app', 'Requirement Quantity'),
            'Base unit of measure' => Yii::t('app', 'Base Unit Of Measure'),
            'Quantity received' => Yii::t('app', 'Quantity Received'),
            'Quantity withdrawn' => Yii::t('app', 'Quantity Withdrawn'),
            'Qty for avail. cntrl.' => Yii::t('app', 'Qty For Avail  Cntrl'),
            'Shortfall quantity' => Yii::t('app', 'Shortfall Quantity'),
            'Factory calendar' => Yii::t('app', 'Factory Calendar'),
            'Material group' => Yii::t('app', 'Material Group'),
            'Item text line 1' => Yii::t('app', 'Item Text Line 1'),
            'Movement Allowed' => Yii::t('app', 'Movement  Allowed'),
            'Movement type' => Yii::t('app', 'Movement Type'),
            'Description' => Yii::t('app', 'Description'),
            'Order category' => Yii::t('app', 'Order Category'),
            'Delivery time (days)' => Yii::t('app', 'Delivery Time (days)'),
            'PS text' => Yii::t('app', 'Ps Text'),
            'Planned order' => Yii::t('app', 'Planned Order'),
        ];
    }
}
