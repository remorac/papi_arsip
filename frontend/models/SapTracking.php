<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_tracking".
 *
 * @property string $Company Code
 * @property string $Name of resp.person
 * @property string $WBS Element
 * @property string $Description
 * @property string $Resp. cost cntr
 * @property string $Description1
 * @property string $Priority
 * @property string $Description2
 * @property string $Network. No
 * @property string $Oper./Act.
 * @property string $Purchase Requisition
 * @property string $PR Item
 * @property string $Material Num
 * @property string $Short Text
 * @property string $Valuation Price
 * @property string $Qty PR
 * @property string $Price
 * @property string $Currency Key
 * @property string $Bidder List
 * @property string $RFQ
 * @property string $Purchase Order
 * @property string $PO Item
 * @property string $Created Date
 * @property string $Document Date
 * @property string $PO Delivery Date
 * @property string $Net Price
 * @property string $PO Quantity
 * @property string $PO Unit
 * @property string $Net Value
 * @property string $Currency Key1
 * @property string $Exchange Rate
 * @property string $PO LC Amount
 * @property string $Local Currency
 * @property string $Vendor
 * @property string $Vendor1
 * @property string $Posting date Downpayment
 * @property string $Downpayment
 * @property string $DP Item
 * @property string $DP Local Amount
 * @property string $DP Local Currency
 * @property string $DP Amount
 * @property string $DP Currency
 * @property string $Posting date Good Receipt
 * @property string $Good Receipt
 * @property string $GR Item
 * @property string $Total Ammount(IDR)
 * @property string $Ammount
 * @property string $Currency Key2
 * @property string $Currency Key3
 * @property string $GR Quantity
 * @property string $Movement Type
 * @property string $Posting date Invoice
 * @property string $Invoice
 * @property string $Invoice Item
 * @property string $Total Ammount(IDR)1
 * @property string $Amount
 * @property string $FI Doc Year
 * @property string $FI Doc Number
 * @property string $Clearing Date
 * @property string $Clearing Doc
 * @property string $Clearing Amt LC
 * @property string $Clearing Amt DC
 * @property string $Clearing Currency
 * @property string $Clearing Date1
 * @property string $Clearing Doc1
 * @property string $Clearing Amt LC1
 * @property string $Clearing Amt DC1
 * @property string $Clearing Currency1
 */
class SapTracking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_tracking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Company Code', 'Name of resp.person', 'WBS Element', 'Description', 'Resp. cost cntr', 'Description1', 'Priority', 'Description2', 'Network. No', 'Oper./Act.', 'Purchase Requisition', 'PR Item', 'Material Num', 'Short Text', 'Valuation Price', 'Qty PR', 'Price', 'Currency Key', 'Bidder List', 'RFQ', 'Purchase Order', 'PO Item', 'Created Date', 'Document Date', 'PO Delivery Date', 'Net Price', 'PO Quantity', 'PO Unit', 'Net Value', 'Currency Key1', 'Exchange Rate', 'PO LC Amount', 'Local Currency', 'Vendor', 'Vendor1', 'Posting date Downpayment', 'Downpayment', 'DP Item', 'DP Local Amount', 'DP Local Currency', 'DP Amount', 'DP Currency', 'Posting date Good Receipt', 'Good Receipt', 'GR Item', 'Total Ammount(IDR)', 'Ammount', 'Currency Key2', 'Currency Key3', 'GR Quantity', 'Movement Type', 'Posting date Invoice', 'Invoice', 'Invoice Item', 'Total Ammount(IDR)1', 'Amount', 'FI Doc Year', 'FI Doc Number', 'Clearing Date', 'Clearing Doc', 'Clearing Amt LC', 'Clearing Amt DC', 'Clearing Currency', 'Clearing Date1', 'Clearing Doc1', 'Clearing Amt LC1', 'Clearing Amt DC1', 'Clearing Currency1'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Company Code' => Yii::t('app', 'Company  Code'),
            'Name of resp.person' => Yii::t('app', 'Name Of Resp Person'),
            'WBS Element' => Yii::t('app', 'Wbs  Element'),
            'Description' => Yii::t('app', 'Description'),
            'Resp. cost cntr' => Yii::t('app', 'Resp  Cost Cntr'),
            'Description1' => Yii::t('app', 'Description1'),
            'Priority' => Yii::t('app', 'Priority'),
            'Description2' => Yii::t('app', 'Description2'),
            'Network. No' => Yii::t('app', 'Network   No'),
            'Oper./Act.' => Yii::t('app', 'Oper / Act'),
            'Purchase Requisition' => Yii::t('app', 'Purchase  Requisition'),
            'PR Item' => Yii::t('app', 'Pr  Item'),
            'Material Num' => Yii::t('app', 'Material  Num'),
            'Short Text' => Yii::t('app', 'Short  Text'),
            'Valuation Price' => Yii::t('app', 'Valuation  Price'),
            'Qty PR' => Yii::t('app', 'Qty  Pr'),
            'Price' => Yii::t('app', 'Price'),
            'Currency Key' => Yii::t('app', 'Currency  Key'),
            'Bidder List' => Yii::t('app', 'Bidder  List'),
            'RFQ' => Yii::t('app', 'Rfq'),
            'Purchase Order' => Yii::t('app', 'Purchase  Order'),
            'PO Item' => Yii::t('app', 'Po  Item'),
            'Created Date' => Yii::t('app', 'Created  Date'),
            'Document Date' => Yii::t('app', 'Document  Date'),
            'PO Delivery Date' => Yii::t('app', 'Po  Delivery  Date'),
            'Net Price' => Yii::t('app', 'Net  Price'),
            'PO Quantity' => Yii::t('app', 'Po  Quantity'),
            'PO Unit' => Yii::t('app', 'Po  Unit'),
            'Net Value' => Yii::t('app', 'Net  Value'),
            'Currency Key1' => Yii::t('app', 'Currency  Key1'),
            'Exchange Rate' => Yii::t('app', 'Exchange  Rate'),
            'PO LC Amount' => Yii::t('app', 'Po  Lc  Amount'),
            'Local Currency' => Yii::t('app', 'Local  Currency'),
            'Vendor' => Yii::t('app', 'Vendor'),
            'Vendor1' => Yii::t('app', 'Vendor1'),
            'Posting date Downpayment' => Yii::t('app', 'Posting Date  Downpayment'),
            'Downpayment' => Yii::t('app', 'Downpayment'),
            'DP Item' => Yii::t('app', 'Dp  Item'),
            'DP Local Amount' => Yii::t('app', 'Dp  Local  Amount'),
            'DP Local Currency' => Yii::t('app', 'Dp  Local  Currency'),
            'DP Amount' => Yii::t('app', 'Dp  Amount'),
            'DP Currency' => Yii::t('app', 'Dp  Currency'),
            'Posting date Good Receipt' => Yii::t('app', 'Posting Date  Good  Receipt'),
            'Good Receipt' => Yii::t('app', 'Good  Receipt'),
            'GR Item' => Yii::t('app', 'Gr  Item'),
            'Total Ammount(IDR)' => Yii::t('app', 'Total  Ammount( Idr)'),
            'Ammount' => Yii::t('app', 'Ammount'),
            'Currency Key2' => Yii::t('app', 'Currency  Key2'),
            'Currency Key3' => Yii::t('app', 'Currency  Key3'),
            'GR Quantity' => Yii::t('app', 'Gr  Quantity'),
            'Movement Type' => Yii::t('app', 'Movement  Type'),
            'Posting date Invoice' => Yii::t('app', 'Posting Date  Invoice'),
            'Invoice' => Yii::t('app', 'Invoice'),
            'Invoice Item' => Yii::t('app', 'Invoice  Item'),
            'Total Ammount(IDR)1' => Yii::t('app', 'Total  Ammount( Idr)1'),
            'Amount' => Yii::t('app', 'Amount'),
            'FI Doc Year' => Yii::t('app', 'Fi  Doc  Year'),
            'FI Doc Number' => Yii::t('app', 'Fi  Doc  Number'),
            'Clearing Date' => Yii::t('app', 'Clearing  Date'),
            'Clearing Doc' => Yii::t('app', 'Clearing  Doc'),
            'Clearing Amt LC' => Yii::t('app', 'Clearing  Amt  Lc'),
            'Clearing Amt DC' => Yii::t('app', 'Clearing  Amt  Dc'),
            'Clearing Currency' => Yii::t('app', 'Clearing  Currency'),
            'Clearing Date1' => Yii::t('app', 'Clearing  Date1'),
            'Clearing Doc1' => Yii::t('app', 'Clearing  Doc1'),
            'Clearing Amt LC1' => Yii::t('app', 'Clearing  Amt  Lc1'),
            'Clearing Amt DC1' => Yii::t('app', 'Clearing  Amt  Dc1'),
            'Clearing Currency1' => Yii::t('app', 'Clearing  Currency1'),
        ];
    }
}
