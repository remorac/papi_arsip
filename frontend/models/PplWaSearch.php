<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PplWa;

/**
 * PplWaSearch represents the model behind the search form about `frontend\models\PplWa`.
 */
class PplWaSearch extends PplWa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ppl_vendor_id', 'ppl_buyer_id'], 'integer'],
            [['file', 'locator'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PplWa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ppl_vendor_id' => $this->ppl_vendor_id,
            'ppl_buyer_id' => $this->ppl_buyer_id,
        ]);

        $query->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'locator', $this->locator]);

        return $dataProvider;
    }
}
