<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PengadaanIssue;

/**
 * PengadaanIssueSearch represents the model behind the search form about `frontend\models\PengadaanIssue`.
 */
class PengadaanIssueSearch extends PengadaanIssue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_activity_id', 'created_by', 'updated_by'], 'integer'],
            [['tanggal', 'keterangan', 'created_at', 'updated_at'], 'safe'],
            [['pengadaanActivity.pengadaan_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengadaanIssue::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['pengadaanActivity.pengadaan_id'] = [
              'asc' => ['pengadaanActivity.pengadaan_id' => SORT_ASC],
              'desc' => ['pengadaanActivity.pengadaan_id' => SORT_DESC],
        ];

        $query->joinWith(['pengadaanActivity']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_activity_id' => $this->pengadaan_activity_id,
            'pengadaan_activity.pengadaan_id' => $this->getAttribute('pengadaanActivity.pengadaan_id'),
            'tanggal' => $this->tanggal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['pengadaanActivity.pengadaan_id']);
    }
}
