<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "penomoran_register".
 *
 * @property integer $id
 * @property integer $penomoran_jenis_id
 * @property string $nomor_dokumen
 * @property string $perihal
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property PenomoranJenis $penomoranJenis
 * @property User $createdBy
 * @property User $updatedBy
 */
class PenomoranRegister extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penomoran_register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['penomoran_jenis_id', 'nomor_dokumen', 'suffix', 'perihal'], 'required'],
            [['penomoran_jenis_id', 'penomoran_jenis_sub_id', 'nomor_dokumen', 'created_by', 'updated_by'], 'integer'],
            [['perihal'], 'string'],
            [['created_at', 'updated_at', 'sub_nomor', 'tanggal_dokumen', 'tujuan'], 'safe'],
            [['suffix'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'penomoran_jenis_id' => Yii::t('app', 'Jenis Dokumen'),
            'penomoran_jenis_sub_id' => Yii::t('app', 'Sub Jenis'),
            'nomor_dokumen' => Yii::t('app', 'Nomor Dokumen'),
            'sub_nomor' => Yii::t('app', 'Sub Nomor'),
            'suffix' => Yii::t('app', 'Nomor Dokumen'),
            'perihal' => Yii::t('app', 'Perihal'),
            'tanggal_dokumen' => Yii::t('app', 'Tanggal Dokumen'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenomoranJenis()
    {
        return $this->hasOne(PenomoranJenis::className(), ['id' => 'penomoran_jenis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenomoranJenisSub()
    {
        return $this->hasOne(PenomoranJenisSub::className(), ['id' => 'penomoran_jenis_sub_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
