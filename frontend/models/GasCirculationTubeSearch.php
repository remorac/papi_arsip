<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\GasCirculationTube;

/**
 * GasCirculationTubeSearch represents the model behind the search form about `frontend\models\GasCirculationTube`.
 */
class GasCirculationTubeSearch extends GasCirculationTube
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gas_circulation_id', 'gas_tube_id', 'count', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [[
              'gas_user_unit.name',
              'gas_tube.nomor',
              'gas_circulation.id',
              'gas_circulation.io',
              'gas_circulation.date',
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    function attributes()
    {
        return array_merge(parent::attributes(), [
          'gas_user_unit.name',
          'gas_tube.nomor',
          'gas_circulation.id',
          'gas_circulation.io',
          'gas_circulation.date',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GasCirculationTube::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // $dataProvider->sort->attributes['gas_user_unit.name'] = [
        //       'asc' => ['gas_user_unit.name' => SORT_ASC],
        //       'desc' => ['gas_user_unit.name' => SORT_DESC],
        // ];
        //
        // $dataProvider->sort->attributes['gas_circulation.io'] = [
        //       'asc' => ['gas_circulation.io' => SORT_ASC],
        //       'desc' => ['gas_circulation.io' => SORT_DESC],
        // ];
        //
        // $dataProvider->sort->attributes['gas_circulation.date'] = [
        //       'asc' => ['gas_circulation.date' => SORT_ASC],
        //       'desc' => ['gas_circulation.date' => SORT_DESC],
        // ];
        //
        // $dataProvider->sort->attributes['gas_tube.nomor'] = [
        //       'asc' => ['gas_tube.nomor' => SORT_ASC],
        //       'desc' => ['gas_tube.nomor' => SORT_DESC],
        // ];

        $dataProvider->setSort([
            'attributes' => [
                'gas_user_unit.name' => [
                  'asc' => ['gas_user_unit.name' => SORT_ASC],
                  'desc' => ['gas_user_unit.name' => SORT_DESC],
                ],
                'gas_circulation.id' => [
                  'asc' => ['gas_circulation.io' => SORT_ASC],
                  'desc' => ['gas_circulation.io' => SORT_DESC],
                ],
                'gas_circulation.io' => [
                  'asc' => ['gas_circulation.io' => SORT_ASC],
                  'desc' => ['gas_circulation.io' => SORT_DESC],
                ],
                'gas_circulation.date' => [
                  'asc' => ['gas_circulation.date' => SORT_ASC],
                  'desc' => ['gas_circulation.date' => SORT_DESC],
                ],
                'gas_tube.nomor' => [
                  'asc' => ['gas_tube.nomor' => SORT_ASC],
                  'desc' => ['gas_tube.nomor' => SORT_DESC],
                ]
            ]
        ]);

        $query->joinWith([
          'gasCirculation',
          'gasCirculation.gasUserUnit',
          'gasTube',
          'gasTube.gasOwner',
          'gasTube.gasType',
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gas_circulation_id' => $this->gas_circulation_id,
            'gas_tube_id' => $this->gas_tube_id,
            'count' => $this->count,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query
          ->andFilterWhere(['like', 'gas_user_unit.name', $this->getAttribute('gas_user_unit.name')])
          ->andFilterWhere(['like', 'gas_tube.nomor', $this->getAttribute('gas_tube.nomor')])
          ->andFilterWhere(['like', 'gas_circulation.id', $this->getAttribute('gas_circulation.id')])
          ->andFilterWhere(['like', 'gas_circulation.io', $this->getAttribute('gas_circulation.io')])
          ->andFilterWhere(['like', 'gas_circulation.date', $this->getAttribute('gas_circulation.date')])
        ;

        return $dataProvider;
    }
}
