<?php

namespace frontend\models;

use Yii;
use frontend\models\PengadaanPo;

/**
 * This is the model class for table "sap_po".
 *
 * @property string $Purchasing Document
 * @property string $Vendor/supplying plant
 * @property string $Item
 * @property string $Material Group
 * @property string $Deletion Indicator
 * @property string $Material
 * @property string $Short Text
 * @property string $Order Quantity
 * @property string $Order Unit
 * @property string $Net price
 * @property string $Currency
 * @property string $Purchasing Group
 * @property string $Plant
 * @property string $Document Date
 * @property string $Price Unit
 * @property string $Release status
 * @property string $Release indicator
 * @property string $Open Target Quantity
 * @property string $Total open value
 * @property string $Open value
 * @property string $Target Quantity
 * @property string $Released value
 * @property string $Quantity in SKU
 * @property string $Purchasing Doc. Type
 * @property string $Purch. Doc. Category
 */
class KpiReport extends PengadaanPo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
    	return array_merge(parent::rules(), [
    		[['kpiDurasi'], 'safe'],	
    	]);	

    }

	public function getKpiBiaya()
	{
		$Sql = 'select *
	, @index := adsc / oe `index`
	, if(@index <= 1, 1, 0) status
from 
	select date_format(sap_po.Document_Date, "%Y-%m") tahun_bulan
		, pengadaan_po.purchasing_document AS purchasing_document
		, pengadaan_po.pengadaan_id AS pengadaan_id
		, pengadaan.barang AS jenis
		, @bdsc:= sum(sap_po.Harga_Sebelum_Diskon) AS bdsc
		, @adsc := sum((sap_po.Order_Quantity * sap_po.Net_price) / sap_po.Price_Unit) AS adsc
		, sap_pr.oe
	from pengadaan_po
	left join (
		select Purchasing_Document
			, document_date
			, Harga_Sebelum_Diskon
			, Order_Quantity
			, Net_price
			, Price_Unit
		from sap_po
		group by Purchasing_Document
		) sap_po on pengadaan_po.purchasing_document = sap_po.Purchasing_Document
	left join pengadaan.on convert( pengadaan_po.pengadaan_id using utf8) = convert( pengadaan.prc_id using utf8)
	left join sap_pr on sap_pr.purchase_order = sap_po.Purchasing_Document
	group by date_format(sap_po.Document_Date, "%Y-%m")
		, pengadaan.barang
) q
';
        $connection = Yii::$app->db;
        
        try {
            $command = $connection->createCommand($sql);
            return $command->queryAll();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
	}

	public static function getKpiDurasi()
	{
		$sql = 'select *
	, @index := durasi_real / durasi_plan `index`
	, if(@index <= 1, 1, 0) status
from (
	select date_format(str_to_date(sap_po.`Document Date`, "%d/%m/%Y"), "%Y-%m") tahun_bulan
		, pengadaan.barang jenis
		, @durasi_plan := sum(pengadaan_durasi_adjusted.durasi) durasi_plan
		, @durasi_real := sum(
			datediff(pengadaan_activity.end_date, pengadaan_activity.start_date)
			- count_sabtu_minggu(pengadaan_activity.end_date, pengadaan_activity.start_date)
			- count_libur(pengadaan_activity.end_date, pengadaan_activity.start_date)
		) durasi_real
	from pengadaan_po
	left join (
		select `Purchasing Document`
			, `Document Date`
		from sap_po
		group by `Purchasing Document`
		) sap_po on pengadaan_po.purchasing_document = sap_po.`Purchasing Document`
	left join pengadaan on pengadaan.id = pengadaan_po.pengadaan_id
    left join pengadaan_activity on pengadaan_activity.pengadaan_id = pengadaan.id
    left join pengadaan_durasi_adjusted on pengadaan_durasi_adjusted.pengadaan_id = pengadaan.id and pengadaan_durasi_adjusted.activity_id = pengadaan_activity.activity_id
	group by date_format(sap_po.`Document Date`, "%Y-%m")
		, pengadaan.barang
) q';

		return self::findBySql($sql);
        $connection = Yii::$app->db;
        
        try {
            $command = $connection->createCommand($sql);
            return $command->queryAll();
        }
        catch(yii\db\Exception $e) {
            throw $e;
        }
        
	}

}