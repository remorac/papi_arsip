<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PengadaanActivityDokumen;

/**
 * PengadaanActivityDokumenSearch represents the model behind the search form about `frontend\models\PengadaanActivityDokumen`.
 */
class PengadaanActivityDokumenSearch extends PengadaanActivityDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengadaan_activity_id', 'dokumen_upload_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['tgl_dokumen'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengadaanActivityDokumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['tgl_dokumen' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['tgl_dokumen'] = [
              'asc' => ['dokumen_upload.tgl_dokumen' => SORT_ASC],
              'desc' => ['dokumen_upload.tgl_dokumen' => SORT_DESC],
        ];

        $query->joinWith(['dokumenUpload']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pengadaan_activity_id' => $this->pengadaan_activity_id,
            'dokumen_upload_id' => $this->dokumen_upload_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['tgl_dokumen']);
    }
}
