<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PenomoranRegister;

/**
 * PenomoranRegisterSearch represents the model behind the search form about `frontend\models\PenomoranRegister`.
 */
class PenomoranRegisterSearch extends PenomoranRegister
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'penomoran_jenis_id', 'penomoran_jenis_sub_id', 'created_by', 'updated_by'], 'integer'],
            [['nomor_dokumen', 'sub_nomor', 'suffix', 'perihal', 'tanggal_dokumen', 'tujuan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PenomoranRegister::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'penomoran_jenis_id' => $this->penomoran_jenis_id,
            'penomoran_jenis_sub_id' => $this->penomoran_jenis_sub_id,
            'nomor_dokumen' => $this->nomor_dokumen,
            'sub_nomor' => $this->sub_nomor,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'suffix', $this->suffix])
            ->andFilterWhere(['like', 'perihal', $this->perihal])
            ->andFilterWhere(['like', 'tujuan', $this->tujuan])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
