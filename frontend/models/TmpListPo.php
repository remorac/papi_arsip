<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tmp_list_po".
 *
 * @property string $po
 */
class TmpListPo extends \yii\db\ActiveRecord
{
    public $id;
    public $remark;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tmp_list_po';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['po'], 'required'],
            [['po'], 'string', 'max' => 191]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'po' => 'PO',
            'id' => 'ID Pengadaan',
            'remark' => 'Nama Pengadaan',
        ];
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPengadaanPo() 
   { 
       return $this->hasOne(PengadaanPo::className(), ['purchasing_document' => 'po']); 
   } 

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPengadaan() 
   { 
       return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id'])->via('pengadaanPo'); 
   } 

   public function getId()
    {
        return $this->pengadaan->id;
    }

   public function getRemark()
    {
        return $this->pengadaan->nama;
    }
}
