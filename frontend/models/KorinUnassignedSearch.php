<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Korin;

/**
 * KorinSearch represents the model behind the search form about `frontend\models\Korin`.
 */
class KorinUnassignedSearch extends Korin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'korin_jenis_id', 'pic_initial', 'created_by', 'updated_by'], 'integer'],
            [['no_surat', 'revisi_referensi', 'tgl_terima', 'tanggal_surat', 'ringkasan_isi', 'perihal', 'tembusan', 'yg_menandatangani', 'lampiran', 'isi_disposisi', 'kebutuhan_disposisi', 'requirement_date', 'created_at', 'updated_at'], 'safe'],
            [['unit.nama', 'korinJenis.nama', 'barangJasa.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->korin_jenis_id = '1';

        $query = Korin::find()->where("korin_assignment.korin_id is null");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['barangJasa.nama'] = [
              'asc' => ['barang_jasa.nama' => SORT_ASC],
              'desc' => ['barang_jasa.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['korinJenis.nama'] = [
              'asc' => ['korin_jenis.nama' => SORT_ASC],
              'desc' => ['korin_jenis.nama' => SORT_DESC],
        ];
        
        $query->joinWith(['barangJasa']);
        $query->joinWith(['korinAssignments']);
        $query->joinWith(['korinJenis']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'korin_jenis_id' => $this->korin_jenis_id,
            'pic_initial' => $this->pic_initial,
            'tgl_terima' => $this->tgl_terima,
            'tanggal_surat' => $this->tanggal_surat,
            'requirement_date' => $this->requirement_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'revisi_referensi', $this->revisi_referensi])
            ->andFilterWhere(['like', 'ringkasan_isi', $this->ringkasan_isi])
            ->andFilterWhere(['like', 'perihal', $this->perihal])
            ->andFilterWhere(['like', 'tembusan', $this->tembusan])
            ->andFilterWhere(['like', 'yg_menandatangani', $this->yg_menandatangani])
            ->andFilterWhere(['like', 'lampiran', $this->lampiran])
            ->andFilterWhere(['like', 'isi_disposisi', $this->isi_disposisi])
            ->andFilterWhere(['like', 'kebutuhan_disposisi', $this->kebutuhan_disposisi])
            ->andFilterWhere(['like', 'korin_jenis.nama', $this->getAttribute('korinJenis.nama')])
            ->andFilterWhere(['like', 'barang_jasa.nama', $this->getAttribute('barangJasa.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['unit.nama', 'korinJenis.nama', 'barangJasa.nama']);
    }
}
