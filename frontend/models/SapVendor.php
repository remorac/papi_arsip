<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_vendor".
 *
 * @property string $Vendor
 * @property string $Name of vendor
 * @property string $City
 * @property string $Street
 * @property string $Country
 * @property string $Postal Code
 * @property string $Account group
 * @property string $Search term
 * @property string $Purch. Organization
 * @property string $Purch. Org. Descr.
 * @property string $Terms of Payment
 * @property string $Order currency
 * @property string $Salesperson
 * @property string $One-time account
 * @property string $Number of Purchasing Organizations
 * @property string $Telephone
 * @property string $Central purchasing block
 * @property string $Block function
 * @property string $Central deletion flag
 * @property string $Purch. block for purchasing organization
 * @property string $Delete flag for purchasing organization
 */
class SapVendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Vendor', 'Purch. Organization'], 'required'],
            [['Vendor', 'Name of vendor', 'City', 'Street', 'Country', 'Postal Code', 'Account group', 'Search term', 'Purch. Organization', 'Purch. Org. Descr.', 'Terms of Payment', 'Order currency', 'Salesperson', 'One-time account', 'Number of Purchasing Organizations', 'Telephone', 'Central purchasing block', 'Block function', 'Central deletion flag', 'Purch. block for purchasing organization', 'Delete flag for purchasing organization'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Vendor' => Yii::t('app', 'Vendor'),
            'Name of vendor' => Yii::t('app', 'Name Of Vendor'),
            'City' => Yii::t('app', 'City'),
            'Street' => Yii::t('app', 'Street'),
            'Country' => Yii::t('app', 'Country'),
            'Postal Code' => Yii::t('app', 'Postal  Code'),
            'Account group' => Yii::t('app', 'Account Group'),
            'Search term' => Yii::t('app', 'Search Term'),
            'Purch. Organization' => Yii::t('app', 'Purch   Organization'),
            'Purch. Org. Descr.' => Yii::t('app', 'Purch   Org   Descr'),
            'Terms of Payment' => Yii::t('app', 'Terms Of  Payment'),
            'Order currency' => Yii::t('app', 'Order Currency'),
            'Salesperson' => Yii::t('app', 'Salesperson'),
            'One-time account' => Yii::t('app', 'One Time Account'),
            'Number of Purchasing Organizations' => Yii::t('app', 'Number Of  Purchasing  Organizations'),
            'Telephone' => Yii::t('app', 'Telephone'),
            'Central purchasing block' => Yii::t('app', 'Central Purchasing Block'),
            'Block function' => Yii::t('app', 'Block Function'),
            'Central deletion flag' => Yii::t('app', 'Central Deletion Flag'),
            'Purch. block for purchasing organization' => Yii::t('app', 'Purch  Block For Purchasing Organization'),
            'Delete flag for purchasing organization' => Yii::t('app', 'Delete Flag For Purchasing Organization'),
        ];
    }
}
