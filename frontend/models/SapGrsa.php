<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_grsa".
 *
 * @property string $Vendor
 * @property string $Material Description
 * @property string $Purchase Order
 * @property string $Item
 * @property string $Material Document
 * @property string $Plant
 * @property string $Posting Date
 * @property string $Document Header Text
 * @property string $Qty in Un. of Entry
 * @property string $Unit of Entry
 * @property string $Movement Type
 * @property string $Reference
 * @property string $Name 1
 * @property string $Movement Type Text
 * @property string $Item1
 * @property string $Material
 * @property string $Document Date
 * @property string $Special Stock
 * @property string $Asset
 * @property string $Subnumber
 * @property string $Base Unit of Measure
 * @property string $Qty in order unit
 * @property string $Entry Date
 * @property string $Counter
 * @property string $Order Unit
 * @property string $Time of Entry
 * @property string $Order
 * @property string $Routing number for operations
 * @property string $Order Price Unit
 * @property string $Company Code
 * @property string $Valuation Type
 * @property string $Batch
 * @property string $Long Procurement Document Number
 * @property string $Ext. Amount in Local Currency
 * @property string $Sales Value
 * @property string $Reason for Movement
 * @property string $Sales Order
 * @property string $Sales Order Schedule
 * @property string $Sales Order Item
 * @property string $Cost Center
 * @property string $Customer
 * @property string $Movement indicator
 * @property string $Consumption
 * @property string $Receipt Indicator
 * @property string $Material Doc. Year
 * @property string $Network
 * @property string $Operation/Activity
 * @property string $WBS Element
 * @property string $Reservation
 * @property string $Item No.Stock Transfer Reserv.
 * @property string $Debit/Credit Ind.
 * @property string $Trans./Event Type
 * @property string $Sales Value inc. VAT
 * @property string $Currency
 * @property string $Goods Receipt/Issue Slip
 * @property string $Item automatically created
 * @property string $User Name
 * @property string $Storage Location
 * @property string $Amount in LC
 * @property string $Qty in OPUn
 * @property string $Quantity
 */
class SapGrsa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_grsa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Vendor', 'Material Description', 'Purchase Order', 'Item', 'Material Document', 'Plant', 'Posting Date', 'Document Header Text', 'Qty in Un. of Entry', 'Unit of Entry', 'Movement Type', 'Reference', 'Name 1', 'Movement Type Text', 'Item1', 'Material', 'Document Date', 'Special Stock', 'Asset', 'Subnumber', 'Base Unit of Measure', 'Qty in order unit', 'Entry Date', 'Counter', 'Order Unit', 'Time of Entry', 'Order', 'Routing number for operations', 'Order Price Unit', 'Company Code', 'Valuation Type', 'Batch', 'Long Procurement Document Number', 'Ext. Amount in Local Currency', 'Sales Value', 'Reason for Movement', 'Sales Order', 'Sales Order Schedule', 'Sales Order Item', 'Cost Center', 'Customer', 'Movement indicator', 'Consumption', 'Receipt Indicator', 'Material Doc. Year', 'Network', 'Operation/Activity', 'WBS Element', 'Reservation', 'Item No.Stock Transfer Reserv.', 'Debit/Credit Ind.', 'Trans./Event Type', 'Sales Value inc. VAT', 'Currency', 'Goods Receipt/Issue Slip', 'Item automatically created', 'User Name', 'Storage Location', 'Amount in LC', 'Qty in OPUn', 'Quantity'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Vendor' => 'Vendor',
            'Material Description' => 'Material  Description',
            'Purchase Order' => 'Purchase  Order',
            'Item' => 'Item',
            'Material Document' => 'Material  Document',
            'Plant' => 'Plant',
            'Posting Date' => 'Posting  Date',
            'Document Header Text' => 'Document  Header  Text',
            'Qty in Un. of Entry' => 'Qty In  Un  Of  Entry',
            'Unit of Entry' => 'Unit Of  Entry',
            'Movement Type' => 'Movement  Type',
            'Reference' => 'Reference',
            'Name 1' => 'Name 1',
            'Movement Type Text' => 'Movement  Type  Text',
            'Item1' => 'Item1',
            'Material' => 'Material',
            'Document Date' => 'Document  Date',
            'Special Stock' => 'Special  Stock',
            'Asset' => 'Asset',
            'Subnumber' => 'Subnumber',
            'Base Unit of Measure' => 'Base  Unit Of  Measure',
            'Qty in order unit' => 'Qty In Order Unit',
            'Entry Date' => 'Entry  Date',
            'Counter' => 'Counter',
            'Order Unit' => 'Order  Unit',
            'Time of Entry' => 'Time Of  Entry',
            'Order' => 'Order',
            'Routing number for operations' => 'Routing Number For Operations',
            'Order Price Unit' => 'Order  Price  Unit',
            'Company Code' => 'Company  Code',
            'Valuation Type' => 'Valuation  Type',
            'Batch' => 'Batch',
            'Long Procurement Document Number' => 'Long  Procurement  Document  Number',
            'Ext. Amount in Local Currency' => 'Ext   Amount In  Local  Currency',
            'Sales Value' => 'Sales  Value',
            'Reason for Movement' => 'Reason For  Movement',
            'Sales Order' => 'Sales  Order',
            'Sales Order Schedule' => 'Sales  Order  Schedule',
            'Sales Order Item' => 'Sales  Order  Item',
            'Cost Center' => 'Cost  Center',
            'Customer' => 'Customer',
            'Movement indicator' => 'Movement Indicator',
            'Consumption' => 'Consumption',
            'Receipt Indicator' => 'Receipt  Indicator',
            'Material Doc. Year' => 'Material  Doc   Year',
            'Network' => 'Network',
            'Operation/Activity' => 'Operation/ Activity',
            'WBS Element' => 'Wbs  Element',
            'Reservation' => 'Reservation',
            'Item No.Stock Transfer Reserv.' => 'Item  No  Stock  Transfer  Reserv',
            'Debit/Credit Ind.' => 'Debit/ Credit  Ind',
            'Trans./Event Type' => 'Trans / Event  Type',
            'Sales Value inc. VAT' => 'Sales  Value Inc   Vat',
            'Currency' => 'Currency',
            'Goods Receipt/Issue Slip' => 'Goods  Receipt/ Issue  Slip',
            'Item automatically created' => 'Item Automatically Created',
            'User Name' => 'User  Name',
            'Storage Location' => 'Storage  Location',
            'Amount in LC' => 'Amount In  Lc',
            'Qty in OPUn' => 'Qty In  Opun',
            'Quantity' => 'Quantity',
        ];
    }
}
