<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\GasTube;

/**
 * GasTubeSearch represents the model behind the search form about `frontend\models\GasTube`.
 */
class GasTubeSearch extends GasTube
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gas_owner_id', 'gas_type_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nomor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GasTube::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gas_owner_id' => $this->gas_owner_id,
            'gas_type_id' => $this->gas_type_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nomor', $this->nomor]);

        return $dataProvider;
    }
}
