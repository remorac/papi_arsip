<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "afis".
 *
 * @property integer $id
 * @property string $filename
 * @property string $clearing_document
 * @property string $year
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Afis extends \yii\db\ActiveRecord
{
    public $pdfFiles;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'afis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'clearing_document', 'year'], 'required'],
            [['filename'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['clearing_document', 'location'], 'string', 'max' => 191],
            [['year'], 'string', 'max' => 4],
            [['clearing_document', 'year'], 'unique', 'targetAttribute' => ['clearing_document', 'year'], 'message' => 'The combination of Clearing Document and Year has already been taken.'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['pdfFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'mimeTypes' => 'application/pdf', 'maxFiles' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'clearing_document' => 'Clearing Document',
            'year' => 'Year',
            'location' => 'Location',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZcps1013s()
    {
        return $this->hasMany(Zcps1013::className(), ['Clearing_Doc' => 'clearing_document'])
            ->orderBy('Purchase_Order');
    }


    public function getGroupedPo()
    {
        $po = [];
        $oldPo = "";
        foreach ($this->zcps1013s as $zcps1013) {
            if ($oldPo != $zcps1013->Purchase_Order && $this->year == substr($zcps1013->Clearing_Date, -4)) {
                if (($pengadaanPo = PengadaanPo::findOne(['purchasing_document' => $zcps1013->Purchase_Order])) !== null) {
                    $po[] = \yii\helpers\Html::a($zcps1013->Purchase_Order, ['semua-pengadaan/view', 'id' => $pengadaanPo->pengadaan_id], ['target' => '_blank']);
                } else {
                    $po[] = $zcps1013->Purchase_Order;
                }
            }
            $oldPo = $zcps1013->Purchase_Order;
        }
        return implode(', ', $po);
    }


    public function upload()
    {
        foreach ($this->pdfFiles as $pdfFile) {
            $path_year = Yii::getAlias('@uploads/afis/'.$this->year.'/');
            if (!is_dir($path_year)) {
                mkdir($path_year, 0775, true);
            }

            $filepath = $path_year . $pdfFile->baseName . '.' . $pdfFile->extension;
            if (file_exists($filepath)) {
                $filepath.= '-'.date('YmdHis').'.'.$pdfFile->extension;
            }
            $pdfFile->saveAs($filepath);
        }
        return true;
    }
}
