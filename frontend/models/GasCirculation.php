<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "gas_circulation".
 *
 * @property integer $id
 * @property integer $io
 * @property string $date
 * @property string $time
 * @property integer $gas_vendor_unit_id
 * @property integer $gas_vendor_pic_id
 * @property integer $gas_user_unit_id
 * @property integer $gas_user_pic_id
 * @property integer $gas_pic_id
 * @property string $no_reference
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property GasVendorUnit $gasVendorUnit
 * @property GasVendorPic $gasVendorPic
 * @property GasUserUnit $gasUserUnit
 * @property GasUserPic $gasUserPic
 * @property GasPic $gasPic
 * @property User $createdBy
 * @property User $updatedBy
 * @property GasCirculationTube[] $gasCirculationTubes
 */
class GasCirculation extends \yii\db\ActiveRecord
{
    public $fromto;

    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gas_circulation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['io', 'date', 'gas_pic_id', 'fromto', 'no_reference'], 'required'],
            [['io', 'gas_vendor_unit_id', 'gas_vendor_pic_id', 'gas_user_unit_id', 'gas_user_pic_id', 'gas_pic_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date', 'time'], 'safe'],
            [['no_reference'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fromto' => 'From/To',
            'id' => 'ID',
            'io' => 'In/Out',
            'date' => 'Date',
            'time' => 'Time',
            'gas_vendor_unit_id' => 'Vendor Unit',
            'gas_vendor_pic_id' => 'Vendor PIC',
            'gas_user_unit_id' => 'User Unit',
            'gas_user_pic_id' => 'User PIC',
            'gas_pic_id' => 'Procurement PIC',
            'no_reference' => 'No. Form',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasVendorUnit()
    {
        return $this->hasOne(GasVendorUnit::className(), ['id' => 'gas_vendor_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasVendorPic()
    {
        return $this->hasOne(GasVendorPic::className(), ['id' => 'gas_vendor_pic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasUserUnit()
    {
        return $this->hasOne(GasUserUnit::className(), ['id' => 'gas_user_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasUserPic()
    {
        return $this->hasOne(GasUserPic::className(), ['id' => 'gas_user_pic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasPic()
    {
        return $this->hasOne(GasPic::className(), ['id' => 'gas_pic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasCirculationTubes()
    {
        return $this->hasMany(GasCirculationTube::className(), ['gas_circulation_id' => 'id']);
    }
}
