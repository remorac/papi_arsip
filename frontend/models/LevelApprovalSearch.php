<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\LevelApproval;

/**
 * LevelApprovalSearch represents the model behind the search form about `frontend\models\LevelApproval`.
 */
class LevelApprovalSearch extends LevelApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level', 'nilai_min', 'nilai_max', 'created_by', 'updated_by'], 'integer'],
            [['approver', 'tanggal_berlaku', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LevelApproval::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
            'nilai_min' => $this->nilai_min,
            'nilai_max' => $this->nilai_max,
            'tanggal_berlaku' => $this->tanggal_berlaku,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'approver', $this->approver]);

        return $dataProvider;
    }
}
