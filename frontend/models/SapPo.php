<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_po".
 *
 * @property string $Purchasing Document
 * @property string $Vendor/supplying plant
 * @property string $Item
 * @property string $Material Group
 * @property string $Deletion Indicator
 * @property string $Material
 * @property string $Short Text
 * @property string $Order Quantity
 * @property string $Order Unit
 * @property string $Net price
 * @property string $Currency
 * @property string $Purchasing Group
 * @property string $Plant
 * @property string $Document Date
 * @property string $Price Unit
 * @property string $Release status
 * @property string $Release indicator
 * @property string $Open Target Quantity
 * @property string $Total open value
 * @property string $Open value
 * @property string $Target Quantity
 * @property string $Released value
 * @property string $Quantity in SKU
 * @property string $Purchasing Doc. Type
 * @property string $Purch. Doc. Category
 */
class SapPo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Purchasing Document', 'Vendor/supplying plant', 'Item', 'Material Group', 'Deletion Indicator', 'Material', 'Short Text', 'Order Quantity', 'Order Unit', 'Net price', 'Currency', 'Purchasing Group', 'Plant', 'Document Date', 'Price Unit', 'Release status', 'Release indicator', 'Open Target Quantity', 'Total open value', 'Open value', 'Target Quantity', 'Released value', 'Quantity in SKU', 'Purchasing Doc. Type', 'Purch. Doc. Category'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Purchasing Document' => Yii::t('app', 'Purchasing  Document'),
            'Vendor/supplying plant' => Yii::t('app', 'Vendor/supplying Plant'),
            'Item' => Yii::t('app', 'Item'),
            'Material Group' => Yii::t('app', 'Material  Group'),
            'Deletion Indicator' => Yii::t('app', 'Deletion  Indicator'),
            'Material' => Yii::t('app', 'Material'),
            'Short Text' => Yii::t('app', 'Short  Text'),
            'Order Quantity' => Yii::t('app', 'Order  Quantity'),
            'Order Unit' => Yii::t('app', 'Order  Unit'),
            'Net price' => Yii::t('app', 'Net Price'),
            'Currency' => Yii::t('app', 'Currency'),
            'Purchasing Group' => Yii::t('app', 'Purchasing  Group'),
            'Plant' => Yii::t('app', 'Plant'),
            'Document Date' => Yii::t('app', 'Document  Date'),
            'Price Unit' => Yii::t('app', 'Price  Unit'),
            'Release status' => Yii::t('app', 'Release Status'),
            'Release indicator' => Yii::t('app', 'Release Indicator'),
            'Open Target Quantity' => Yii::t('app', 'Open  Target  Quantity'),
            'Total open value' => Yii::t('app', 'Total Open Value'),
            'Open value' => Yii::t('app', 'Open Value'),
            'Target Quantity' => Yii::t('app', 'Target  Quantity'),
            'Released value' => Yii::t('app', 'Released Value'),
            'Quantity in SKU' => Yii::t('app', 'Quantity In  Sku'),
            'Purchasing Doc. Type' => Yii::t('app', 'Purchasing  Doc   Type'),
            'Purch. Doc. Category' => Yii::t('app', 'Purch   Doc   Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHargaSebelumDiskon()
    {
        return $this->hasOne(HargaSebelumDiskon::className(), ['purchasing_document' => 'Purchasing Document', 'item_po' => 'Item']);
    }
}
