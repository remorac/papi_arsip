<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\DurasiStandar;

/**
 * DurasiStandarSearch represents the model behind the search form about `frontend\models\DurasiStandar`.
 */
class DurasiStandarSearch extends DurasiStandar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'activity_id', 'level_approval_id', 'durasi', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_berlaku', 'created_at', 'updated_at'], 'safe'],
            [['activity.nama', 'levelApproval.approver'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DurasiStandar::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['levelApproval.approver'] = [
              'asc' => ['levelApproval.approver' => SORT_ASC],
              'desc' => ['levelApproval.approver' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['activity.nama'] = [
              'asc' => ['activity.nama' => SORT_ASC],
              'desc' => ['activity.nama' => SORT_DESC],
        ];

        $query->joinWith(['levelApproval']);
        $query->joinWith(['activity']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tanggal_berlaku' => $this->tanggal_berlaku,
            'activity_id' => $this->activity_id,
            'level_approval_id' => $this->level_approval_id,
            'durasi' => $this->durasi,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'level_approval.approver', $this->getAttribute('levelApproval.approver')])
            ->andFilterWhere(['like', 'activity.nama', $this->getAttribute('activity.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['levelApproval.approver', 'activity.nama']);
    }
}
