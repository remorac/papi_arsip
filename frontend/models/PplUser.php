<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_user".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PplUserArea[] $pplUserAreas
 * @property PplWaUser[] $pplWaUsers
 */
class PplUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplUserAreas()
    {
        return $this->hasMany(PplUserArea::className(), ['ppl_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplWaUsers()
    {
        return $this->hasMany(PplWaUser::className(), ['ppl_user_id' => 'id']);
    }
}
