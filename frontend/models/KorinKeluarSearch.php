<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinKeluar;

/**
 * KorinKeluarSearch represents the model behind the search form about `frontend\models\KorinKeluar`.
 */
class KorinKeluarSearch extends KorinKeluar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'barang_jasa_id', 'created_by', 'updated_by'], 'integer'],
            [['no_surat', 'no_referensi', 'pic_initial', 'tanggal_surat', 'ringkasan_isi', 'perihal', 'tembusan', 'yg_menandatangani', 'lampiran', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinKeluar::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'barang_jasa_id' => $this->barang_jasa_id,
            'tanggal_surat' => $this->tanggal_surat,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'no_referensi', $this->no_referensi])
            ->andFilterWhere(['like', 'pic_initial', $this->pic_initial])
            ->andFilterWhere(['like', 'ringkasan_isi', $this->ringkasan_isi])
            ->andFilterWhere(['like', 'perihal', $this->perihal])
            ->andFilterWhere(['like', 'tembusan', $this->tembusan])
            ->andFilterWhere(['like', 'yg_menandatangani', $this->yg_menandatangani])
            ->andFilterWhere(['like', 'lampiran', $this->lampiran]);

        return $dataProvider;
    }
}
