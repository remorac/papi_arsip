<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "korin_assignment".
 *
 * @property integer $id
 * @property integer $korin_id
 * @property string $tanggal_assignment
 * @property integer $mgr_personel_id
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Korin $korin
 * @property Personel $mgrPersonel
 */
class KorinAssignment extends \yii\db\ActiveRecord
{
    public $jumlah;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'korin_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['korin_id'], 'required'],
            [['korin_id', 'manager_personel_procurement_id', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_assignment', 'created_at', 'updated_at'], 'safe'],
            [['keterangan'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'korin_id' => Yii::t('app', 'Korin ID'),
            'tanggal_assignment' => Yii::t('app', 'Tanggal Disposisi'),
            'manager_personel_procurement_id' => Yii::t('app', 'Manager'),
            'keterangan' => Yii::t('app', 'Isi Disposisi'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKorin()
    {
        return $this->hasOne(Korin::className(), ['id' => 'korin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonelProcurement()
    {
        return $this->hasOne(PersonelProcurement::className(), ['id' => 'manager_personel_procurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaanKorins()
    {
        return $this->hasMany(PengadaanKorin::className(), ['korin_id' => 'korin_id']);
    }

    public function getJumlahPengadaanKorin()
    {
        $this->jumlah = count($this->pengadaanKorins);
        return $this->jumlah;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
