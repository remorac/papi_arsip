<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Afis;

/**
 * AfisSearch represents the model behind the search form about `frontend\models\Afis`.
 */
class AfisSearch extends Afis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['filename', 'clearing_document', 'year'], 'safe'],
            [['groupedPo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Afis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['groupedPo'] = [
              'asc' => ['group_concat(zcps1013.Purchase_Order)' => SORT_ASC],
              'desc' => ['group_concat(zcps1013.Purchase_Order)' => SORT_DESC],
        ];

        $query->joinWith(['zcps1013s']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'clearing_document', $this->clearing_document])
            ->andFilterWhere(['like', 'year', $this->year])
            ->andFilterWhere([
                'like', 
                '(select (group_concat(Purchase_Order)) as groupedPo from zcps1013 
                    where zcps1013.Clearing_Doc = afis.clearing_document
                    and right(Clearing_Date, 4) = afis.year
                )', 
                $this->getAttribute('groupedPo')
            ]);

        $query->groupBy("afis.id");

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), [
            'groupedPo',
        ]);
    }
}
