<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\KorinAssignment;

/**
 * KorinAssignmentSearch represents the model behind the search form about `frontend\models\KorinAssignment`.
 */
class KorinAssignmentSearch extends KorinAssignment
{
    public $jumlah;
    public $statusNama;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'korin_id', 'manager_personel_procurement_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_assignment', 'keterangan', 'created_at', 'updated_at'], 'safe'],
            [['korin.no_surat', 'korin.perihal', 'personelProcurement.personel.nama', 'jumlahPengadaanKorin'], 'safe'],
            [['statusNama'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KorinAssignment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['jumlahPengadaanKorin' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['korin.no_surat'] = [
              'asc' => ['korin.no_surat' => SORT_ASC],
              'desc' => ['korin.no_surat' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['korin.perihal'] = [
              'asc' => ['korin.perihal' => SORT_ASC],
              'desc' => ['korin.perihal' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['personelProcurement.personel.nama'] = [
              'asc' => ['personel.nama' => SORT_ASC],
              'desc' => ['personel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['jumlahPengadaanKorin'] = [
              'asc' => ['(select (count(pengadaan_korin.id)) as countID from pengadaan_korin where pengadaan_korin.korin_id = korin.id)' => SORT_ASC],
              'desc' => ['(select (count(pengadaan_korin.id)) as countID from pengadaan_korin where pengadaan_korin.korin_id = korin.id)' => SORT_DESC],
        ];

        $query->joinWith(['korin']);
        $query->joinWith(['pengadaanKorins']);
        $query->joinWith(['personelProcurement.personel']);
      

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'korin_id' => $this->korin_id,
            'tanggal_assignment' => $this->tanggal_assignment,
            'manager_personel_procurement_id' => $this->manager_personel_procurement_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'korin.no_surat', $this->getAttribute('korin.no_surat')])
            ->andFilterWhere(['like', 'korin.perihal', $this->getAttribute('korin.perihal')])
            ->andFilterWhere(['like', 'personel.nama', $this->getAttribute('personelProcurement.personel.nama')])

            ->andFilterWhere([
                '=', 
                '(select (count(pengadaan_korin.id)) as countID from pengadaan_korin where pengadaan_korin.korin_id = korin.id)', 
                $this->getAttribute('jumlahPengadaanKorin')
            ]);

        $query->orderBy('status');

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['korin.no_surat','korin.perihal','personelProcurement.personel.nama', 'jumlahPengadaanKorin', 'jumlah']);
    }
}
