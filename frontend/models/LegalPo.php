<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "legal_po".
 *
 * @property integer $id
 * @property string $Kode_Klasifikasi
 * @property string $Purchase_Order
 * @property string $No_Kontrak
 * @property string $Isi_Berkas
 * @property string $Tahun
 * @property string $Vendor
 * @property string $Box
 * @property string $Lokasi
 */
class LegalPo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return [
    //         \yii\behaviors\TimestampBehavior::className(),
    //         \yii\behaviors\BlameableBehavior::className(),
    //     ];
    // }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'legal_po';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Klasifikasi', 'Purchase_Order', 'No_Kontrak', 'Isi_Berkas', 'Tahun', 'Vendor', 'Box', 'Lokasi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Kode_Klasifikasi' => 'Kode  Klasifikasi',
            'Purchase_Order' => 'Purchase  Order',
            'No_Kontrak' => 'No  Kontrak',
            'Isi_Berkas' => 'Isi  Berkas',
            'Tahun' => 'Tahun',
            'Vendor' => 'Vendor',
            'Box' => 'Box',
            'Lokasi' => 'Lokasi',
        ];
    }
}
