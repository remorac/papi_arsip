<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daily_report_document".
 *
 * @property integer $id
 * @property integer $daily_report_id
 * @property integer $daily_document_id
 *
 * @property DailyReport $dailyReport
 * @property DailyDocument $dailyDocument
 */
class DailyReportDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_report_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['daily_report_id', 'daily_document_id'], 'required'],
            [['daily_report_id', 'daily_document_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'daily_report_id' => 'Daily Report ID',
            'daily_document_id' => 'Daily Document ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyReport()
    {
        return $this->hasOne(DailyReport::className(), ['id' => 'daily_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDailyDocument()
    {
        return $this->hasOne(DailyDocument::className(), ['id' => 'daily_document_id']);
    }
}
