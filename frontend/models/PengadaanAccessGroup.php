<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "pengadaan_access_group".
 *
 * @property integer $id
 * @property integer $pengadaan_id
 * @property integer $access_group_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pengadaan $pengadaan
 * @property User $createdBy
 * @property User $updatedBy
 * @property AccessGroup $accessGroup
 */
class PengadaanAccessGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengadaan_access_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'access_group_id'], 'required'],
            [['pengadaan_id', 'access_group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['pengadaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pengadaan::className(), 'targetAttribute' => ['pengadaan_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['access_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccessGroup::className(), 'targetAttribute' => ['access_group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pengadaan_id' => 'Pengadaan',
            'access_group_id' => 'Access Group',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(Pengadaan::className(), ['id' => 'pengadaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessGroup()
    {
        return $this->hasOne(AccessGroup::className(), ['id' => 'access_group_id']);
    }
}
