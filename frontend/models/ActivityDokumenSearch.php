<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\ActivityDokumen;

/**
 * ActivityDokumenSearch represents the model behind the search form about `frontend\models\ActivityDokumen`.
 */
class ActivityDokumenSearch extends ActivityDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'activity_id', 'dokumen_jenis_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['dokumenJenis.nama', 'activity.nama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivityDokumen::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['dokumenJenis.nama'] = [
              'asc' => ['dokumenJenis.nama' => SORT_ASC],
              'desc' => ['dokumenJenis.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['activity.nama'] = [
              'asc' => ['activity.nama' => SORT_ASC],
              'desc' => ['activity.nama' => SORT_DESC],
        ];

        $query->joinWith(['dokumenJenis']);
        $query->joinWith(['activity']);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'activity_id' => $this->activity_id,
            'dokumen_jenis_id' => $this->dokumen_jenis_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'dokumen_jenis.nama', $this->getAttribute('dokumenJenis.nama')])
            ->andFilterWhere(['like', 'activity.nama', $this->getAttribute('activity.nama')]);

        return $dataProvider;
    }

    function attributes()
    {
        return array_merge(parent::attributes(), ['dokumenJenis.nama', 'activity.nama']);
    }
}
