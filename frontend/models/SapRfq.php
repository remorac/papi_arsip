<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sap_rfq".
 *
 * @property string $Vendor/supplying plant
 * @property string $Purchasing Document
 * @property string $Item
 * @property string $Purchasing Doc. Type
 * @property string $Quotation Deadline
 * @property string $RFQ status
 * @property string $Collective Number
 * @property string $Purch. Doc. Category
 * @property string $Purchasing Group
 * @property string $PO history/release documentation
 * @property string $Document Date
 * @property string $Material
 * @property string $Short Text
 * @property string $Material Group
 * @property string $Deletion Indicator
 * @property string $Item Category
 * @property string $Acct Assignment Cat.
 * @property string $Plant
 * @property string $Storage Location
 * @property string $Order Quantity
 * @property string $Order Unit
 * @property string $Quantity in SKU
 * @property string $Stockkeeping unit
 * @property string $Net price
 * @property string $Currency
 * @property string $Price Unit
 * @property string $No. of Positions
 */
class SapRfq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sap_rfq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Vendor/supplying plant', 'Purchasing Document', 'Item', 'Purchasing Doc. Type', 'Quotation Deadline', 'RFQ status', 'Collective Number', 'Purch. Doc. Category', 'Purchasing Group', 'PO history/release documentation', 'Document Date', 'Material', 'Short Text', 'Material Group', 'Deletion Indicator', 'Item Category', 'Acct Assignment Cat.', 'Plant', 'Storage Location', 'Order Quantity', 'Order Unit', 'Quantity in SKU', 'Stockkeeping unit', 'Net price', 'Currency', 'Price Unit', 'No. of Positions'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Vendor/supplying plant' => 'Vendor/supplying Plant',
            'Purchasing Document' => 'Purchasing  Document',
            'Item' => 'Item',
            'Purchasing Doc. Type' => 'Purchasing  Doc   Type',
            'Quotation Deadline' => 'Quotation  Deadline',
            'RFQ status' => 'Rfq Status',
            'Collective Number' => 'Collective  Number',
            'Purch. Doc. Category' => 'Purch   Doc   Category',
            'Purchasing Group' => 'Purchasing  Group',
            'PO history/release documentation' => 'Po History/release Documentation',
            'Document Date' => 'Document  Date',
            'Material' => 'Material',
            'Short Text' => 'Short  Text',
            'Material Group' => 'Material  Group',
            'Deletion Indicator' => 'Deletion  Indicator',
            'Item Category' => 'Item  Category',
            'Acct Assignment Cat.' => 'Acct  Assignment  Cat',
            'Plant' => 'Plant',
            'Storage Location' => 'Storage  Location',
            'Order Quantity' => 'Order  Quantity',
            'Order Unit' => 'Order  Unit',
            'Quantity in SKU' => 'Quantity In  Sku',
            'Stockkeeping unit' => 'Stockkeeping Unit',
            'Net price' => 'Net Price',
            'Currency' => 'Currency',
            'Price Unit' => 'Price  Unit',
            'No. of Positions' => 'No  Of  Positions',
        ];
    }
}
