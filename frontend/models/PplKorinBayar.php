<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ppl_korin_bayar".
 *
 * @property integer $id
 * @property integer $ppl_korin_id
 * @property integer $ppl_vendor_id
 * @property string $date
 *
 * @property PplKorin $pplKorin
 * @property PplVendor $pplVendor
 */
class PplKorinBayar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ppl_korin_bayar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ppl_korin_id', 'ppl_vendor_id'], 'required'],
            [['ppl_korin_id', 'ppl_vendor_id'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ppl_korin_id' => 'Ppl Korin ID',
            'ppl_vendor_id' => 'Ppl Vendor ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplKorin()
    {
        return $this->hasOne(PplKorin::className(), ['id' => 'ppl_korin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPplVendor()
    {
        return $this->hasOne(PplVendor::className(), ['id' => 'ppl_vendor_id']);
    }
}
