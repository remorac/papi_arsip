<?php

namespace frontend\controllers;

use Yii;
use frontend\models\KpiBiayaKuartalReport;
use frontend\models\KpiBiayaKuartalReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpiBiayaKuartalReportController implements the CRUD actions for KpiBiayaKuartalReport model.
 */
class KpiBiayaKuartalReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpiBiayaKuartalReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $kpiBiayaKuartalReport = new KpiBiayaKuartalReport();
        $kpiBiayaKuartalReport->refreshKpiBiayaKuartal();

        $searchModel = new KpiBiayaKuartalReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpiBiayaKuartalReport model.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return mixed
     */
    public function actionView($tahun_kuartal, $jenis)
    {
        return $this->render('view', [
            'model' => $this->findModel($tahun_kuartal, $jenis),
        ]);
    }

    /**
     * Finds the KpiBiayaKuartalReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return KpiBiayaKuartalReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tahun_kuartal, $jenis)
    {
        if (($model = KpiBiayaKuartalReport::findOne(['tahun_kuartal' => $tahun_kuartal, 'jenis' => $jenis])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
