<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\PengadaanSearch;
use frontend\models\Korin;
use frontend\models\KorinAssignment;
use frontend\models\KorinAssignmentSearch;
use frontend\models\PengadaanKorin;
use frontend\models\KorinDokumenSearch;
use frontend\models\KorinVendorSearch;
use frontend\models\KorinItemSearch;
use frontend\models\KorinPrItemSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengadaanController implements the CRUD actions for Pengadaan model.
 */
class PengadaanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new PengadaanSearch();
        $searchModel = new KorinAssignmentSearch();
        $queryParams = Yii::$app->request->queryParams;
        
        if(Yii::$app->user->identity->authAssignment->item_name == 'manager_barang'){
            if (!isset($queryParams['KorinAssignmentSearch'])) $queryParams['KorinAssignmentSearch'] = array();
            $queryParams['KorinAssignmentSearch'] = array_merge($queryParams['KorinAssignmentSearch'], 
                ['manager_personel_procurement_id' => '2']
            );
        }
        if(Yii::$app->user->identity->authAssignment->item_name == 'manager_jasa'){
            if (!isset($queryParams['KorinAssignmentSearch'])) $queryParams['KorinAssignmentSearch'] = array();

            if (Yii::$app->user->id == 6)
            $queryParams['KorinAssignmentSearch'] = array_merge($queryParams['KorinAssignmentSearch'], 
                ['manager_personel_procurement_id' => '4']
            );

            if (Yii::$app->user->id == 5)
            $queryParams['KorinAssignmentSearch'] = array_merge($queryParams['KorinAssignmentSearch'], 
                ['manager_personel_procurement_id' => '13']
            );
        }
        
        $dataProvider = $searchModel->search($queryParams);

        //$dataProvider->pagination->pageSize=15;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengadaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($is_centerled = 0)
    {
        $model = new Pengadaan();

        if ($model->load(Yii::$app->request->post())) {  

            $lastKode = 0;
            if (is_object(Pengadaan::find()->where("barang_jasa_id = '".$model->barang_jasa_id."'")->orderBy('kode DESC')->one())) {
                $lastKode = Pengadaan::find()->where("barang_jasa_id = '".$model->barang_jasa_id."'")->orderBy('kode DESC')->one()->kode;
            }
            $model->kode = $lastKode+1;
            
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'is_centerled' => $is_centerled,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'is_centerled' => $is_centerled,
            ]);
        }
    }

    /**
     * Updates an existing Pengadaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {  
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pengadaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        //reset statusDisposisi
        foreach ($model->pengadaanKorins as $pengadaanKorin) {
            $jumlahDisposisi = PengadaanKorin::find()->where("korin_id = '".$pengadaanKorin->korin_id."'")->count();
            if ($jumlahDisposisi ==1) {
                $korinAssignment = KorinAssignment::find()->where("korin_id = '".$pengadaanKorin->korin_id."'")->one();
                $korinAssignment->status = 0;
                $korinAssignment->save();
            }
        }
        //delete all documents
        foreach ($model->pengadaanActivities as $pengadaanActivity) {
            \frontend\models\PengadaanActivityDokumen::deleteAll("pengadaan_activity_id='".$pengadaanActivity->id."'");
        }
        \frontend\models\PengadaanKorin::deleteAll("pengadaan_id='$id'");
        \frontend\models\PengadaanActivity::deleteAll("pengadaan_id='$id'");
        \frontend\models\PengadaanDokumenNotrequired::deleteAll("pengadaan_id='$id'");
        \frontend\models\PengadaanDurasiAdjusted::deleteAll("pengadaan_id='$id'");
        \frontend\models\PengadaanPrItem::deleteAll("pengadaan_id='$id'");
        \frontend\models\PengadaanPo::deleteAll("pengadaan_id='$id'");
        $this->findModel($id)->delete();
        
        if (Yii::$app->request->get("double")) {
            return $this->redirect(['semua-pengadaan/undoubler']);
        } else {
            return $this->redirect(['outbox']);
        }
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }




    public function actionOutbox()
    {
        $searchModel = new PengadaanSearch();
        $queryParams = Yii::$app->request->queryParams;

        if(Yii::$app->user->identity->authAssignment->item_name == 'manager_barang'){
            if (!isset($queryParams['PengadaanSearch'])) $queryParams['PengadaanSearch'] = array();
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], 
                ['barang_jasa_id' => '1',]
            );
        }
        if(Yii::$app->user->identity->authAssignment->item_name == 'manager_jasa'){
            if (!isset($queryParams['PengadaanSearch'])) $queryParams['PengadaanSearch'] = array();
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], 
                ['barang_jasa_id' => '2',]
            );
        }
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('outbox', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAssign($id)
    {
        $model                  = new Pengadaan();
        $modelKorinAssignment   = $this->findKorinAssignment($id);
        $modelKorin             = $this->findKorin($modelKorinAssignment->korin_id);
        $modelPengadaanKorin    = new PengadaanKorin();

        $searchModelKorinDokumen        = new KorinDokumenSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
        
        $searchModelKorinVendor         = new KorinVendorSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinVendor        = $searchModelKorinVendor->search(Yii::$app->request->queryParams);
        
        $searchModelKorinItem           = new KorinItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
        
        $searchModelKorinPrItem         = new KorinPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {  

            $lastKode = 0;
            if (is_object(Pengadaan::find()->where("barang_jasa_id = '".$model->barang_jasa_id."'")->orderBy('kode DESC')->one())) {
                $lastKode = Pengadaan::find()->where("barang_jasa_id = '".$model->barang_jasa_id."'")->orderBy('kode DESC')->one()->kode;
            }

            $model->kode = $lastKode+1;
            if ($model->save()) {
                $modelPengadaanKorin->pengadaan_id  = $model->id;
                $modelPengadaanKorin->korin_id      = $modelKorin->id;
                $modelPengadaanKorin->save();
                if ($modelKorinAssignment->status == '0') 
                {
                    $modelKorinAssignment->status = '1';
                    $modelKorinAssignment->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('assign', [
                    'model' => $model,
                    'modelKorin' => $modelKorin,
                    'modelKorinAssignment' => $modelKorinAssignment,

                    'searchModelKorinDokumen' => $searchModelKorinDokumen,
                    'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

                    'searchModelKorinVendor' => $searchModelKorinVendor,
                    'dataProviderKorinVendor' => $dataProviderKorinVendor,
                    
                    'searchModelKorinItem' => $searchModelKorinItem,
                    'dataProviderKorinItem' => $dataProviderKorinItem,

                    'searchModelKorinPrItem' => $searchModelKorinPrItem,
                    'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
                ]);
            }
        } else {
            return $this->render('assign', [
                'model' => $model,
                'modelKorin' => $modelKorin,
                'modelKorinAssignment' => $modelKorinAssignment,

                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

                'searchModelKorinVendor' => $searchModelKorinVendor,
                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                
                'searchModelKorinItem' => $searchModelKorinItem,
                'dataProviderKorinItem' => $dataProviderKorinItem,

                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
            ]);
        }
    }
    


    public function actionMerge($id)
    {
    	$model = new PengadaanKorin();
        $modelKorinAssignment = $this->findKorinAssignment($id);
    	$model->korin_id = $modelKorinAssignment->korin_id;
    	
    	if ($model->load(Yii::$app->request->post()) && $model->save()) {
    		$modelKorinAssignment->status = '1';
    		$modelKorinAssignment->save();
    		return $this->redirect(['view', 'id' => $model->pengadaan_id]);
    	} else {
    		return $this->render('merge', [
    				'model' => $model,
    		]);
    	}
    }

    public function actionDone($id) {
        $modelKorinAssignment = $this->findKorinAssignment($id);
        $modelKorinAssignment->status = '2';
        $modelKorinAssignment->save();
        return $this->redirect(['index']); 
    }
    
    public function actionUndone($id) {
        $modelKorinAssignment = $this->findKorinAssignment($id);
        $modelKorinAssignment->status = '1';
        $modelKorinAssignment->save();
        return $this->redirect(['index']); 
    }

    protected function findKorinAssignment($id)
    {
        //if (($model = KorinAssignment::find()->where("id = '".$id."'")->one()) !== null) {
        if (($model = KorinAssignment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findKorin($id)
    {
        //if (($model = KorinAssignment::find()->where("id = '".$id."'")->one()) !== null) {
        if (($model = Korin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*select2 ajax handler*/
    public function actionList($q = null, $id = null) {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$out = ['results' => ['id' => '', 'text' => '']];
    	if (!is_null($q)) {
    		$query = new \yii\db\Query;
    		$query->select(["pengadaan.id as id",
    				new \yii\db\Expression("
                        concat (
    							barang_jasa.kode,
                                pengadaan.kode,
                                ' - ',
                                pengadaan.nama
                            )
                        as text")])
                            ->from("pengadaan, barang_jasa")
                            ->where("
                            		(pengadaan.kode like '%$q%' or
                            		pengadaan.nama like '%$q%')
                            		and pengadaan.barang_jasa_id = barang_jasa.id
                            		")
                            ->groupBy("pengadaan.id")
                            ->limit(100);
                            $command = $query->createCommand();
                            $data = $command->queryAll();
                            $out['results'] = array_values($data);
    	}
    	elseif ($id > 0) {
    		$out['results'] = ['id' => $id, 'text' => Pengadaan::find($id)->nama];
    	}
    	return $out;
    }
}
