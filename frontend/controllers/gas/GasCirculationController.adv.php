<?php

namespace frontend\controllers\gas;

use Yii;
use frontend\models\GasCirculationTube;
use frontend\models\GasCirculationTubeSearch;
use frontend\models\GasCirculation;
use frontend\models\GasCirculationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GasCirculationController implements the CRUD actions for GasCirculation model.
 */
class GasCirculationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GasCirculation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GasCirculationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GasCirculation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelTube = new GasCirculationTubeSearch();
        $dataProviderTube = $searchModelTube->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelTube' => $searchModelTube,
            'dataProviderTube' => $dataProviderTube,
        ]);

    }

    /**
     * Creates a new GasCirculation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GasCirculation();
        $modelTubes = [new \frontend\models\GasCirculationTube()];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post = Yii::$app->request->post();
            for ($i = 1; $i <= count($post['jumlah']); $i++) {
                if (isset($post['tabung'][$i])) {
                    $modelDetail = new GasCirculationTube();
                    $modelDetail->gas_circulation_id = $model->id;
                    $modelDetail->gas_tube_id = $post['tabung'][$i];
                    $modelDetail->count = $post['jumlah'][$i];
                    $modelDetail->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelTubes' => $modelTubes,
            ]);
        }
    }

    /**
     * Updates an existing GasCirculation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GasCirculation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GasCirculation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GasCirculation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GasCirculation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
