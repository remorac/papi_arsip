<?php

namespace frontend\controllers\gas;

use Yii;
use frontend\models\GasCirculation;
use frontend\models\GasCirculationSearch;
use frontend\models\GasCirculationTube;
use frontend\models\GasCirculationTubeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GasCirculationController implements the CRUD actions for GasCirculation model.
 */
class GasCirculationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GasCirculation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GasCirculationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GasCirculation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelDetail = new GasCirculationTube();

        if ($modelDetail->load(Yii::$app->request->post()) && $modelDetail->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {

            $searchModel = new GasCirculationTubeSearch();
            $queryParams = Yii::$app->request->queryParams;
                
            if (!isset($queryParams['GasCirculationTubeSearch'])) $queryParams['GasCirculationTubeSearch'] = array();
            $queryParams['GasCirculationTubeSearch'] = array_merge(
                $queryParams['GasCirculationTubeSearch'], 
                ['gas_circulation_id' => $id,]
            );
            $dataProvider = $searchModel->search($queryParams);

            $modelDetail = new GasCirculationTube();
            $modelDetail->gas_circulation_id = $id;
            $modelDetail->count = 1;
        
            return $this->render('view', [
                'model' => $this->findModel($id),
                'modelDetail' => $modelDetail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Creates a new GasCirculation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GasCirculation();

        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GasCirculation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GasCirculation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        GasCirculationTube::deleteAll("gas_circulation_id = '".$id."'");
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GasCirculation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GasCirculation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GasCirculation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function batchInsertTubes() {

    }
}
