<?php

namespace frontend\controllers\gas;

use Yii;
use frontend\models\GasCirculationTube;
use frontend\models\GasCirculationTubeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GasCirculationTubeController implements the CRUD actions for GasCirculationTube model.
 */
class GasCirculationTubeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GasCirculationTube models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GasCirculationTubeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GasCirculationTube model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GasCirculationTube model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($gas_circulation_id)
    {
        $model = new GasCirculationTube();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['gas/gas-circulation/view', 'gas_circulation_id' => $gas_circulation_id]);
        } else {
            $model->gas_circulation_id = $gas_circulation_id;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GasCirculationTube model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['gas/gas-circulation/view', 'id' => $model->gas_circulation_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GasCirculationTube model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $gas_circulation_id = $model->gas_circulation_id;
        $model->delete();

        return $this->redirect(['gas/gas-circulation/view', 'id' => $gas_circulation_id]);
    }

    /**
     * Finds the GasCirculationTube model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GasCirculationTube the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GasCirculationTube::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
