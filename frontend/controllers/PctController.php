<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\PengadaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\UnitPosisi;
use frontend\models\HariLibur;

/**
 * PengadaanController implements the CRUD actions for Pengadaan model.
 */
class PctController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengadaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getRowcount($text, $width=55) {
        $rc = 0;
        $line = explode("\n", $text);
        foreach($line as $source) {
            $rc += intval((strlen($source) / $width) +1);
        }
        return $rc;
    }

    public function actionExportExcel($id) {
        //styling arrays
        $fontBold = array(
            'font'  => array(
                'bold'  => true,
            )
        );
        $fontUnderline = array(
            'font'  => array(
                'underline'  => true,
            )
        );
        $default_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $subFooter = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFBE6B')
            )
        );
        $footer = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F39217')
            )
        );
        $horizontalLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $red = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'fc7768')
            )
        );
        $yellow = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'fff00')
            )
        );
        $green = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6eec6e')
            )
        );

        $model          = Pengadaan::find()->where("id='".$id."'")->one();
        $objReader      = \PHPExcel_IOFactory::createReader('Excel2007');

        //set template
        $template       = Yii::getAlias('@app/views/pct').'/_pct.xlsx';
        $objPHPExcel    = $objReader->load($template);
        $activeSheet    = $objPHPExcel->getActiveSheet();
        
        //header
        $pics = "";
        foreach ($model->pengadaanKorins as $pengadaanKorin) {
            $pics.= $pengadaanKorin->korin->pic_initial."\n";
        }
        $units = "";
        $kodeSurat = "";
        foreach ($model->pengadaanKorins as $row) {
            $arr_korin_id       = explode('/', $row->korin->no_surat);
            $arr_korin_id_rev   = array_reverse($arr_korin_id);
            if (isset($arr_korin_id_rev[1])) $kodeSurat = $arr_korin_id_rev[1];
            if (UnitPosisi::find()->where(['=', 'kode_surat', $kodeSurat])->one()) {
                $units.= UnitPosisi::find()->where(['=', 'kode_surat', $kodeSurat])->one()->unit->nama;
            } else {
                $units = "periksa lagi nomor Korin PP, pastikan pemisah antar segmen tetap menggunakan karakter '/'";
            }
        }
        $activeSheet
            ->setCellValue('D5', "".strtoupper($model->barangJasa->nama))
            ->setCellValue('D6', "".strtoupper($units))
            ->setCellValue('D7', "".strtoupper($pics))
            ->setCellValue('I5', "".strtoupper($model->purchaserPersonelProcurement->personel->nama))
            ->setCellValue('I6', "".strtoupper($model->nama));

        // $activeSheet->mergeCells('G6:K7');

        $activeSheet->getStyle('I6')->getAlignment()->setWrapText(true);

        /*$purpose = $model->nama;
        $numrows = $this->getRowcount($purpose);
        $activeSheet->getRowDimension(5)->setRowHeight($numrows * 12.75 + 2.25);*/
        

        //table content
        $baseRow            = 12;
        $start_plan         = $model->start_plan;
        $show_start_plan    = "";
        $show_end_plan      = "";
        $end_plan           = "";
        $end_real           = "";
        $evatek_plan        = 0;
        $evatek_real        = 0;
        $currentActivity    = "Delivery";

        foreach ($model->pengadaanDurasiAdjusteds as $pengadaanDurasiAdjusted) {
            if ($pengadaanDurasiAdjusted->durasi > 0) {
                
                //calculate values
                $pengadaanActivity = PengadaanActivity::find()
                        ->where("pengadaan_id='".$model->id."' and activity_id='".$pengadaanDurasiAdjusted->activity_id."'")
                        ->one();
                if ($pengadaanActivity['start_date'] != "" && $pengadaanActivity['end_date'] == "") $currentActivity = $pengadaanActivity['activity']->nama; 

                if ($start_plan != "") {
                    $date = new \DateTime($start_plan);

                    //adjust start plan except for Task Force and Rush Order
                    //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                        while (HariLibur::checkHoliday($start_plan)) {
                            $date->add(new \DateInterval('P1D'));                         
                            $start_plan = $date->format('Y-m-d');
                        }
                    //}

                    //set end date
                    $date->add(new \DateInterval('P'.($pengadaanDurasiAdjusted->durasi-1).'D'));
                    $end_plan = $date->format('Y-m-d');

                    //adjust end date except for Task Force and Rush Order
                    //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                        $adder = HariLibur::countHoliday($start_plan, $end_plan);
                        $date->add(new \DateInterval('P'.($adder).'D'));                         
                        $end_plan = $date->format('Y-m-d');

                        while (HariLibur::checkHoliday($end_plan)) {
                            $date->add(new \DateInterval('P1D'));                         
                            $end_plan = $date->format('Y-m-d');
                        }
                    //}

                    $show_start_plan    = date('d F Y', strtotime($start_plan));
                    $show_end_plan      = $date->format('d F Y');

                } else {
                    $end_plan = "";
                }
                
                $real_durasi    = "";
                $saving         = "";
                $comply         = "";
                $eod            = "";

                //set durasi real
                if ($pengadaanActivity['start_date'] != "") {

                    $dStart = new \DateTime($pengadaanActivity['start_date']);
                    $dEnd   = new \DateTime($pengadaanActivity['end_date']);

                    if ($pengadaanActivity['end_date'] == "0000-00-00") $dEnd = new \DateTime(date('Y-m-d'));
                    $dDiff  = $dStart->diff($dEnd);

                    $real_mulai     = $pengadaanActivity['start_date'];
                    $real_selesai   = $pengadaanActivity['end_date'];

                    if ($real_selesai == "0000-00-00" || is_null($real_selesai)) $real_selesai = date('Y-m-d');
                    $real_durasi    = $dDiff->days+1;

                    //adjust real_durasi except for Task Force and Rush Order
                    //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
                        $adder = HariLibur::countHoliday($real_mulai, $real_selesai);
                        $real_durasi-= $adder;
                    //}
                    
                    $saving     = $pengadaanDurasiAdjusted->durasi - $real_durasi;
                    $comply     = $saving >= 0 ? "Comply" : "Not Comply";

                    if ($real_selesai > $end_plan) $eod = "Delay";
                    if ($real_selesai == $end_plan) $eod = "On Schedule";
                    if ($real_selesai < $end_plan) $eod = "Early";
                } 

                $start_real = $pengadaanActivity['start_date'] != "" ? date('d F Y', strtotime($pengadaanActivity['start_date'])) : "";
                $end_real   = $pengadaanActivity['end_date'] != "" ? date('d F Y', strtotime($pengadaanActivity['end_date'])) : "";

                //fill content
                $activeSheet
                    ->setCellValue('A'.$baseRow, $baseRow-11)
                    ->setCellValue('B'.$baseRow, $pengadaanDurasiAdjusted->activity->nama)
                    ->mergeCells('B'.$baseRow.':C'.$baseRow)
                    ->setCellValue('D'.$baseRow, $show_start_plan)
                    ->setCellValue('E'.$baseRow, $show_end_plan)
                    ->setCellValue('F'.$baseRow, $pengadaanDurasiAdjusted->durasi)
                    ->setCellValue('G'.$baseRow, $start_real)
                    ->mergeCells('G'.$baseRow.':H'.$baseRow)
                    ->setCellValue('I'.$baseRow, $end_real)
                    ->setCellValue('J'.$baseRow, $real_durasi)
                    ->setCellValue('K'.$baseRow, $comply)
                    ->setCellValue('L'.$baseRow, $saving)
                    ->setCellValue('M'.$baseRow, $eod);

                if ($comply == "Comply") $activeSheet->getStyle('K'.($baseRow))->applyFromArray($green);
                if ($comply == "Not Comply") $activeSheet->getStyle('K'.($baseRow))->applyFromArray($yellow);
                
                if ($eod == "Early" || $eod == "On Schedule") $activeSheet->getStyle('M'.($baseRow))->applyFromArray($green);
                if ($eod == "Delay") $activeSheet->getStyle('M'.($baseRow))->applyFromArray($red);

                $baseRow++;

                if ($end_plan != "") {
                    $date = new \DateTime($end_plan);
                    $date->add(new \DateInterval('P1D'));
                    $start_plan = $date->format('Y-m-d');
                } else {
                    $start_plan = "";
                }

                //set if evatek
                if ($pengadaanDurasiAdjusted->activity_id == '5') { 
                    $evatek_plan = $pengadaanDurasiAdjusted->durasi;
                    $evatek_real = $real_durasi;
                }
            }
        }

        //footer
        $totalDurasiPlan = PengadaanDurasiAdjusted::find()
            ->where("pengadaan_id='".$model->id."'")
            ->sum('durasi');
        //total durasi real
        $dStart = new \DateTime($model->start_plan);
        $dEnd   = new \DateTime($end_real);
        if ($model->pengadaanActivities) {
            if ($pengadaanActivity['end_date'] == "0000-00-00") $dEnd = new \DateTime(date('Y-m-d'));
        }
        $dDiff  = $dStart->diff($dEnd);
        $totalDurasiReal = $dDiff->days+1;
        //adjust real_durasi except for Task Force and Rush Order
        //if ($model->metoda_id != 5 && $model->metoda_id != 6) {
            $adder = HariLibur::countHoliday($dStart->format('Y-m-d'), $dEnd->format('Y-m-d'));
            $totalDurasiReal-= $adder;
        //}

        $activeSheet
            ->setCellValue('A'.$baseRow, 'Workdays')
            ->setCellValue('A'.($baseRow+1), 'Workdays (Exclude Evaluasi Teknis)')
            ->setCellValue('F'.$baseRow, $totalDurasiPlan)
            ->setCellValue('F'.($baseRow+1), $totalDurasiPlan-$evatek_plan)
            ->setCellValue('I'.$baseRow, $totalDurasiReal)
            ->setCellValue('I'.($baseRow+1), $totalDurasiReal-$evatek_real)
            ->setCellValue('K'.$baseRow, $totalDurasiReal > $totalDurasiPlan ? "Not Comply" : "Comply")
            ->setCellValue('K'.($baseRow+1), $totalDurasiReal-$evatek_real > $totalDurasiPlan-$evatek_plan ? "Not Comply" : "Comply");
        

        $activeSheet->mergeCells('A'.$baseRow.':C'.$baseRow);
        $activeSheet->mergeCells('G'.$baseRow.':H'.$baseRow);
        $activeSheet->mergeCells('A'.($baseRow+1).':C'.($baseRow+1));
        $activeSheet->mergeCells('G'.($baseRow+1).':H'.($baseRow+1));

        //apply style
        $activeSheet->getStyle('A12:M'.($baseRow+1))->applyFromArray($default_border);
        $activeSheet->getStyle('A'.($baseRow).':M'.($baseRow))->applyFromArray($subFooter);
        $activeSheet->getStyle('A'.($baseRow+1).':M'.($baseRow+1))->applyFromArray($footer);
        $activeSheet->getStyle('A'.($baseRow).':M'.($baseRow))->applyFromArray($fontBold);
        $activeSheet->getStyle('A'.($baseRow+1).':M'.($baseRow+1))->applyFromArray($fontBold);
        $activeSheet->getStyle('A'.($baseRow).':A'.($baseRow+1))->applyFromArray($horizontalLeft);

        $baseRow+= 2;

        $activeSheet->setCellValue('A'.$baseRow, 'Current Activity : '.$currentActivity);
        $activeSheet->getStyle('A'.($baseRow).':A'.($baseRow+1))->applyFromArray($horizontalLeft);

        $baseRow+= 2;
        foreach ($model->pengadaanActivities as $pengadaanActivity) {
            if ($pengadaanActivity->pengadaanIssues) { 
                $activeSheet->setCellValue('A'.$baseRow, $pengadaanActivity->activity->nama);
                $activeSheet->getStyle('A'.$baseRow)->applyFromArray($horizontalLeft);
                $activeSheet->getStyle('A'.$baseRow)->applyFromArray($fontBold);
                $baseRow++;
                foreach ($pengadaanActivity->pengadaanIssues as $pengadaanIssue) {
                    $activeSheet->setCellValue('A'.$baseRow, date('d-m-Y', strtotime($pengadaanIssue->tanggal)).' : '.$pengadaanIssue->keterangan);
                    $activeSheet->getStyle('A'.$baseRow)->applyFromArray($horizontalLeft);
                    $baseRow++;
                }
                $baseRow++;
            }
        }

        $baseRow++;
        $activeSheet->setCellValue('E'.$baseRow, 'Padang, '.date('d F Y'));
        $activeSheet->mergeCells('E'.$baseRow.':G'.$baseRow);

        $baseRow++;
        $activeSheet->setCellValue('E'.$baseRow, 'PROCUREMENT PROYEK INDARUNG VI');
        $activeSheet->mergeCells('E'.$baseRow.':G'.$baseRow);

        $baseRow++;        
        $activeSheet->setCellValue('A'.$baseRow, 'Dibuat oleh');
        $activeSheet->setCellValue('E'.$baseRow, 'Diperiksa oleh');
        $activeSheet->setCellValue('I'.$baseRow, 'Disetujui oleh');
        $activeSheet->mergeCells('A'.$baseRow.':D'.$baseRow);
        $activeSheet->mergeCells('E'.$baseRow.':H'.$baseRow);
        $activeSheet->mergeCells('I'.$baseRow.':M'.$baseRow);

        $baseRow+= 5;
        $manager = $model->barang_jasa_id == 1 ? "Piery Togap" : "Hamdi Ayussa";
        $activeSheet->setCellValue('A'.$baseRow, $model->purchaserPersonelProcurement->personel->nama);
        $activeSheet->setCellValue('E'.$baseRow, $manager);
        $activeSheet->setCellValue('I'.$baseRow, 'Muhamad Ikrar');
        $activeSheet->mergeCells('A'.$baseRow.':D'.$baseRow);
        $activeSheet->mergeCells('E'.$baseRow.':H'.$baseRow);
        $activeSheet->mergeCells('I'.$baseRow.':M'.$baseRow);
        $activeSheet->getStyle('A'.$baseRow.':M'.$baseRow)->applyFromArray($fontBold);
        $activeSheet->getStyle('A'.$baseRow.':M'.$baseRow)->applyFromArray($fontUnderline);

        $baseRow++;
        $activeSheet->setCellValue('A'.$baseRow, 'Purchaser');
        $activeSheet->setCellValue('E'.$baseRow, 'Manager');
        $activeSheet->setCellValue('I'.$baseRow, 'Coordinator');
        $activeSheet->mergeCells('A'.$baseRow.':D'.$baseRow);
        $activeSheet->mergeCells('E'.$baseRow.':H'.$baseRow);
        $activeSheet->mergeCells('I'.$baseRow.':M'.$baseRow);

        
        //export
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="PCT '.$model->nama.'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('php://output');
        exit;
    }
}
