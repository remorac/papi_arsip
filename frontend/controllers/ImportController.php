<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ImportForm;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\widgets\Growl;

/**
 * ActivityController implements the CRUD actions for Activity model.
 */
class ImportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Activity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ImportForm();
        $messages = [];
        if(Yii::$app->session->getFlash('messages', null) !== null){
            $messages = Yii::$app->session->getFlash('messages');
        }
        if (Yii::$app->request->isPost) {

            ini_set('memory_limit', '1G');

            /*$model->material    = UploadedFile::getInstance($model, 'material');
            $model->pr          = UploadedFile::getInstance($model, 'pr');
            $model->po          = UploadedFile::getInstance($model, 'po');
            $model->delivery    = UploadedFile::getInstance($model, 'delivery');
            $model->rfq         = UploadedFile::getInstance($model, 'rfq');
            $model->grsa        = UploadedFile::getInstance($model, 'grsa');
            $model->reservasi   = UploadedFile::getInstance($model, 'reservasi');
            $model->tracking    = UploadedFile::getInstance($model, 'tracking');
            $model->vendor      = UploadedFile::getInstance($model, 'vendor');*/

            $currentTime    = date('YmdHis');
            $failMessage    = 'Gagal meng-import data';
            $successMessage = 'Berhasil meng-import data';
            $functionPrefix = 'insertSap';
            $bodyMessage    = ' yang di-import tidak valid.<br>Export ulang berkas CSV dari Excel.<br>Jika ingin memastikan, buka berkas CSV dengan notepad, tiap kolom diakhiri oleh semicolon (titik koma) bukan koma, dan datanya tidak menyatu pada satu baris saja.';

            if(Yii::$app->request->post('create-table') !== null) {
                $functionPrefix = 'createTableSap';
                $failMessage    = 'Gagal membuat struktur tabel';
                $successMessage = 'Berhasil membuat struktur tabel';
                $bodyMessage    = ' yang dijadikan referensi tidak valid.<br>Export ulang berkas CSV dari Excel.<br>Jika ingin memastikan, buka berkas CSV dengan notepad, tiap kolom diakhiri oleh semicolon (titik koma) bukan koma, dan datanya tidak menyatu pada satu baris saja.<br>Atau sudah ada tabel yang bernama sama di database';
            }

            $uploadKinds = ['material', 'pr', 'po','delivery', 'rfq', 'grsa', 'gi', 'reservasi', 'tracking', 'vendor'];
            Yii::$app->formatter->locale = 'ID-id';
            $request = Yii::$app->request;

            foreach($uploadKinds as $kind){
            // if ($model->validate()) {
                $model->$kind = UploadedFile::getInstance($model, $kind);

                if(is_object($model->$kind)){ 
                    $fileName = 'uploads/' . $currentTime . '-'. $kind . '.csv';
                    $model->$kind->saveAs($fileName);
                    $rowCount = 0;

                    try{
                        $rowCount = $model->{$functionPrefix . ucfirst($kind) . 'FromCsvFile'}($fileName);
                        $messages[] = [ 
                                'title' => $successMessage,
                                'body' => Yii::$app->formatter->asDecimal($rowCount) . ' data ' . ucfirst($kind) . ' telah berhasil import',
                                'growlType' => Growl::TYPE_SUCCESS,
                        ];
                    }
                    catch(yii\db\Exception $e){
                        unlink($fileName );
                        error_log(__FILE__.'('.__LINE__.'): '. $e);
                        $messages[] = [ 
                                'title' => $failMessage,
                                'body' => 'File ' . ucfirst($kind) . $bodyMessage . '<hr>' . $e->getName() ,
                                'growlType' => Growl::TYPE_DANGER,
                        ];
                    }
                }
            }
            Yii::$app->session->setFlash('messages', $messages);
            return $this->redirect(['index']);
        }

        return $this->render('index', ['model' => $model, 'messages' => $messages]);
    }

}
