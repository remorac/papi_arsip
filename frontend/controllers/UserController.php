<?php

namespace frontend\controllers;

use Yii;
use frontend\models\User;
use frontend\models\UserSearch;
use frontend\models\AuthItem;
use frontend\models\AuthAssignment;
use frontend\models\Personel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        // $authAssignmentModel = new AuthAssignment();

        if ($model->load(Yii::$app->request->post())
                // && $authAssignmentModel->load(Yii::$app->request->post())
                ) {

            $result = $model->save();

            if(!$result){
                error_log('model: ' . json_encode($model->getErrors()));
            }

            /*$authAssignmentModel->user_id = $model->id;
            $authAssignmentModel->created_at = time();
            $result = $authAssignmentModel->save();
            if(!$result){
                error_log('authAssignmentModel: '.json_encode($authAssignmentModel->getErrors()));
            }*/

            return $this->redirect(['view', 'id' => $model->id]);
        }
        else {
            return $this->render('create', [
                'model' => $model,
                // 'authAssignmentModel' => $authAssignmentModel,
                'personelSelect2Items' => Personel::getSelect2Items(),
                // 'authItemSelect2Items' => AuthItem::getRoleSelect2Items(),
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // $authAssignmentModel = AuthAssignment::findOne($model->id);

        if ($model->load(Yii::$app->request->post())
            // && $authAssignmentModel->load(Yii::$app->request->post())
            ) {
            $result = $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                // 'authAssignmentModel' => $authAssignmentModel,
                'personelSelect2Items' => Personel::getSelect2Items(),
                // 'authItemSelect2Items' => AuthItem::getRoleSelect2Items(),
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
