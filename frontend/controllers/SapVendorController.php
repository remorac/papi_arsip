<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SapVendor;
use frontend\models\SapVendorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SapVendorController implements the CRUD actions for SapVendor model.
 */
class SapVendorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SapVendor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SapVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SapVendor model.
     * @param string $Vendor
     * @param string $Purch_Organization
     * @return mixed
     */
    public function actionView($Vendor, $Purch_Organization)
    {
        return $this->render('view', [
            'model' => $this->findModel($Vendor, $Purch_Organization),
        ]);
    }

    /**
     * Creates a new SapVendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SapVendor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Vendor' => $model->Vendor, 'Purch. Organization' => $model->{'Purch. Organization'}]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SapVendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Vendor
     * @param string $Purch_Organization
     * @return mixed
     */
    public function actionUpdate($Vendor, $Purch_Organization)
    {
        $model = $this->findModel($Vendor, $Purch_Organization);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Vendor' => $model->Vendor, 'Purch. Organization' => $model->{'Purch. Organization'}]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SapVendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Vendor
     * @param string $Purch_Organization
     * @return mixed
     */
    public function actionDelete($Vendor, $Purch_Organization)
    {
        $this->findModel($Vendor, $Purch_Organization)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SapVendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Vendor
     * @param string $Purch_Organization
     * @return SapVendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Vendor, $Purch_Organization)
    {
        if (($model = SapVendor::findOne(['Vendor' => $Vendor, 'Purch. Organization' => $Purch_Organization])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
