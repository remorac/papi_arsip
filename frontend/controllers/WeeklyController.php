<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\PengadaanTodaySearch;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\HariLibur;

use yii\helpers\Html;
use \DateInterval;
use \DateTime;

class WeeklyController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new PengadaanTodaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index3', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        header("Content-Type        : application/vnd.ms-excel; charset=utf-8");
        header("Content-type        : application/x-msexcel; charset=utf-8");
        header("Content-Disposition : attachment; filename=Today_Report.xls"); 
        header("Cache-Control       : must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control       : private",false);
        header("Expires             : 0");

        $searchModel = new PengadaanTodaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRefresh()
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("truncate tmp_today");
        $command->execute();

        $i = 0;
        $_BJ = "";
        $pengadaans = Pengadaan::find()->orderBy('barang_jasa_id, id desc')->all();
        foreach ($pengadaans as $pengadaan) {
            if ($_BJ != $pengadaan->barang_jasa_id and $pengadaan->barang_jasa_id != '3') {
                $_BJ = $pengadaan->barang_jasa_id;
                $i = 0;
            }
            $pengadaanActivity = PengadaanActivity::find()
                    ->where("
                        pengadaan_id = '".$pengadaan->id."' and
                        start_date <= current_date() and 
                        (end_date is null or end_date >= current_date())
                    ")
                    ->orderBy("activity_id desc")->one();

            if (is_object($pengadaanActivity)) {
                $i++;
                $tmp_pengadaan = "<b>".Html::a($pengadaan->nama,'?r=highlight/view&id='.$pengadaan->id)."</b>";
                $pengadaanKorins = PengadaanKorin::find()->where("pengadaan_id = '".$pengadaan->id."'")->all();
                $tmp_pengadaan.= "<br> <b>User</b> : ";
                $pic_initial = "";
                foreach ($pengadaanKorins as $pengadaanKorin) {
                    if ($pic_initial != $pengadaanKorin->korin->pic_initial) {
                        $pic_initial = $pengadaanKorin->korin->pic_initial;
                        $tmp_pengadaan.= $pengadaanKorin->korin->pic_initial.", ";
                    }
                }
                $tmp_pengadaan.= "<br> <b>Purchaser</b> : ".$pengadaan->purchaserPersonelProcurement->personel->singkatan;
                
                $tmp_aktifitas = "<b>Aktivitas : </b>".$pengadaanActivity->activity->nama."
                <br><b>Progress : </b>".$pengadaanActivity->activity->bobot."%
                <br><b>Start : </b>".$pengadaanActivity->start_date."
                <br><b>Finish : </b>";
                $end_date = $pengadaanActivity->end_date;
                if ($end_date == "") {
                    $date = new DateTime($pengadaanActivity->start_date);
                    $date->add(new DateInterval('P'.PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->one()->durasi.'D'));
                    $date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaanActivity->start_date, $date->format('Y-m-d')).'D'));  
                    while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
                        $date->add(new DateInterval('P1D'));                         
                    }
                }
                $tmp_aktifitas.= $date->format('Y-m-d');
                $tmp_aktifitas.= "<br><b>Status Aktifitas : </b>"; 
                $tmp_aktifitas.= $date->format('Y-m-d') < date('Y-m-d') ? "<font color='red'><b>Over Due Date</b></font>" : "<font color='green'><b>In Progress</b></font>";

                $tmp_rencana = "<b>Start : </b>".$pengadaan->start_plan."
                <br>
                <b>End : </b>";
                $date = new DateTime($pengadaan->start_plan);
                $date->add(new DateInterval('P'.PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->sum('durasi').'D'));
                $date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaan->start_plan, $date->format('Y-m-d')).'D'));  
                while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
                    $date->add(new DateInterval('P1D'));                         
                }
                $tmp_rencana.= $date->format('Y-m-d');
                $tmp_rencana.= "<br>
                <b>Durasi : </b>";
                $pengadaanDurasiAdjusted = PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."'")->sum('durasi');
                $tmp_rencana.= $pengadaanDurasiAdjusted;

                $tmp_realisasi = "<b>Start : </b>".$pengadaan->start_plan."
                <br>
                <b>End : </b>";
                $date = new DateTime($pengadaan->start_plan);
                $date->add(new DateInterval('P'.$pengadaanDurasiAdjusted.'D'));
                $date->add(new DateInterval('P'.HariLibur::countHoliday($pengadaan->start_plan, $date->format('Y-m-d')).'D'));  
                while (HariLibur::checkHoliday($date->format('Y-m-d'))) {
                    $date->add(new DateInterval('P1D'));                         
                }
                $tmp_realisasi.= $date->format('Y-m-d');
                $tmp_realisasi.= "<br>
                <b>Durasi : </b>";
                $start_date = PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."'")->orderBy("activity_id ASC")->one()->start_date;
                $end_date = PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."'")->orderBy("activity_id DESC")->one()->end_date;
                
                if (is_null($end_date)) $end_date = date('Y-m-d');
                $interval   = date_diff(date_create($start_date), date_create($end_date))->format('%d');
                $real_all_durasi = $interval - HariLibur::countHoliday($start_date, $end_date);
                $tmp_realisasi.= $interval - HariLibur::countHoliday($start_date, $end_date);
                $tmp_realisasi.= "<br>
                <b>Status : </b>";
                $tmp_realisasi.= $pengadaanDurasiAdjusted >= $real_all_durasi ? "<font color='green'><b>On Schedule</b></font>" : "<font color='red'><b>Delay</b></font>";
                
                $keterangan = "";
                foreach ($pengadaanActivity->pengadaanIssues as $pengadaanIssue) {
                    $keterangan = $pengadaanIssue->keterangan;
                }
                $tmp_issue = $keterangan;
                
                $tmp_keterangan = "<b>PR : </b>"; 
                $tmp_keterangan.= count($pengadaan->pengadaanPrItems) > 0 ? "<font color='green'><b>Sudah ada</b></font>" : "<font color='red'><b>Belum ada</b></font>";
                $tmp_keterangan.= "<br><b>Butuh Kontrak : </b>"; 
                $tmp_keterangan.= $pengadaan->kontrak_butuh ? $pengadaan->kontrak_butuh : "<font color='gray'><i>n/a</i></font>";
                
                $tmp_brgjsa = $_BJ == 1 ? "Barang" : "Jasa";
                
                $command = $connection->createCommand("insert into tmp_today (
                        brg_jsa,
                        nama_pengadaan,
                        monitoring_aktifitas,
                        rencana_pengadaan,
                        realisasi_pengadaan,
                        issue_aktifitas,
                        keterangan
                    ) values (
                        '".addslashes($tmp_brgjsa)."',
                        '".addslashes($tmp_pengadaan)."',
                        '".addslashes($tmp_aktifitas)."',
                        '".addslashes($tmp_rencana)."',
                        '".addslashes($tmp_realisasi)."',
                        '".addslashes($tmp_issue)."',
                        '".addslashes($tmp_keterangan)."'
                    )");
                $command->execute();
                /*$tmp_model = new \frontend\models\TmpToday();
                $tmp_model->brg_jsa                 = $_BJ;
                $tmp_model->nama_pengadaan          = $tmp_pengadaan;
                $tmp_model->monitoring_aktifitas    = $tmp_aktifitas;
                $tmp_model->rencana_pengadaan       = $tmp_rencana;
                $tmp_model->realisasi_pengadaan     = $tmp_realisasi;
                $tmp_model->issue_aktifitas         = $tmp_issue;
                $tmp_model->keterangan              = $tmp_keterangan;
                $tmp_model->save();*/
            }
        }
        $this->redirect(['index']);
    }

}
