<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Korin;
use frontend\models\KorinSearch;
use frontend\models\KorinAssignment;
use frontend\models\KorinAssignmentSearch;
use frontend\models\KorinUnassignedSearch;

use frontend\models\KorinDokumen;
use frontend\models\KorinDokumenSearch;
use frontend\models\KorinVendor;
use frontend\models\KorinVendorSearch;
use frontend\models\KorinItem;
use frontend\models\KorinItemSearch;
use frontend\models\KorinPrItem;
use frontend\models\KorinPrItemSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KorinAssignmentController implements the CRUD actions for KorinAssignment model.
 */
class KorinAssignmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KorinAssignment models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new KorinAssignmentSearch();
        $searchModel = new KorinUnassignedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KorinAssignment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelKorin = $this->findKorin($model->korin_id);

        $searchModelKorinDokumen        = new KorinDokumenSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
        
        $searchModelKorinVendor         = new KorinVendorSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinVendor        = $searchModelKorinVendor->search(Yii::$app->request->queryParams);
        
        $searchModelKorinItem           = new KorinItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
        
        $searchModelKorinPrItem         = new KorinPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);

        $model->korin_id = $modelKorin->id;
        //$model->tanggal_assignment = date('Y-m-d');
        return $this->render('view', [
            'model' => $model,
            'modelKorin' => $modelKorin,

            'searchModelKorinDokumen' => $searchModelKorinDokumen,
            'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

            'searchModelKorinVendor' => $searchModelKorinVendor,
            'dataProviderKorinVendor' => $dataProviderKorinVendor,
            
            'searchModelKorinItem' => $searchModelKorinItem,
            'dataProviderKorinItem' => $dataProviderKorinItem,

            'searchModelKorinPrItem' => $searchModelKorinPrItem,
            'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
        ]);
    }

    /**
     * Creates a new KorinAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KorinAssignment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KorinAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelKorin = $this->findKorin($model->korin_id);

        $searchModelKorinDokumen        = new KorinDokumenSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
        
        $searchModelKorinVendor         = new KorinVendorSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinVendor        = $searchModelKorinVendor->search(Yii::$app->request->queryParams);
        
        $searchModelKorinItem           = new KorinItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
        
        $searchModelKorinPrItem         = new KorinPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->korin_id = $modelKorin->id;
            // $model->tanggal_assignment = date('Y-m-d');
            return $this->render('update', [
                'model' => $model,
                'modelKorin' => $modelKorin,

                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

                'searchModelKorinVendor' => $searchModelKorinVendor,
                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                
                'searchModelKorinItem' => $searchModelKorinItem,
                'dataProviderKorinItem' => $dataProviderKorinItem,

                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
            ]);
        }
    }

    /**
     * Deletes an existing KorinAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['outbox']);
    }

    /**
     * Finds the KorinAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KorinAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KorinAssignment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }






    public function actionOutbox()
    {
        $searchModel = new KorinAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('outbox', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAssign($id)
    {
        $model = new KorinAssignment();
        $modelKorin = $this->findKorin($id);

        $searchModelKorinDokumen        = new KorinDokumenSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
        
        $searchModelKorinVendor         = new KorinVendorSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinVendor        = $searchModelKorinVendor->search(Yii::$app->request->queryParams);
        
        $searchModelKorinItem           = new KorinItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
        
        $searchModelKorinPrItem         = new KorinPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $modelKorin->id]]);
        $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->korin_id = $modelKorin->id;
            // $model->tanggal_assignment = date('Y-m-d');
            return $this->render('assign', [
                'model' => $model,
                'modelKorin' => $modelKorin,

                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

                'searchModelKorinVendor' => $searchModelKorinVendor,
                'dataProviderKorinVendor' => $dataProviderKorinVendor,
                
                'searchModelKorinItem' => $searchModelKorinItem,
                'dataProviderKorinItem' => $dataProviderKorinItem,

                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
            ]);
        }
    }

    protected function findKorin($id)
    {
        if (($model = Korin::find()->where("id = '".$id."'")->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
