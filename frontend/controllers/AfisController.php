<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Afis;
use frontend\models\AfisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

/**
 * AfisController implements the CRUD actions for Afis model.
 */
class AfisController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Afis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AfisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Afis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Afis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Afis();

        if ($model->load(Yii::$app->request->post())) {
            $model->pdfFiles = UploadedFile::getInstances($model, 'pdfFiles');
            // print '<pre>'; print_r($model->pdfFiles); die();
            
            if ($model->upload()) {
                $this->getAllFiles();
                $alert = [
                    'type' => 'success',
                    'message' => 'Files uploaded successfully.',
                ];
            } else {
                // print_r($model->getErrors()); die();
                $alert = [
                    'type' => 'error',
                    'message' => 'Uploading files failed.',
                ];
            }
            Yii::$app->session->setFlash($alert['type'], $alert['message']);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Afis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Afis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            $model->delete();
            $path = Yii::getAlias('@uploads/afis/'.$model->year.'/'.$model->filename);
            if (file_exists($path)) unlink ($path);
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Afis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Afis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Afis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Populate all Afis models.
     * @return mixed
     */
    public function actionPopulate()
    {
        Yii::$app->db->createCommand('truncate afis')->execute();
        $this->getAllFiles();
        
        return $this->redirect(['index']);
    }

    /**
     * Populate all Afis models.
     * @return mixed
     */
    public function actionPatch()
    {
        $this->getAllFiles();
        return $this->redirect(['index']);
    }

    /**
     * Populate all Afis models.
     * @return mixed
     */
    public function getAllFiles()
    {
        $dirs = new \DirectoryIterator(Yii::getAlias('@uploads/afis'));
        foreach ($dirs as $dir) {
            if (!$dir->isDot()) {
                // var_dump($dir->getFilename());
                $files = new \DirectoryIterator(Yii::getAlias('@uploads/afis/'.$dir->getFilename()));
                foreach ($files as $file) {
                    if (!$file->isDot()) {
                        // var_dump($file->getFilename());
                        $model = new Afis();
                        $model->filename    = $file->getFilename();
                        $model->year        = $dir->getFilename();

                        $raw_filename = str_replace('  ', ' ', $model->filename);
                        $arr = explode(' ', $raw_filename);
                        if (substr($raw_filename, 0, 4) == "( ) ") {
                            if (count($arr) >= 3) $model->clearing_document = (string)intval($arr[2]);
                        } else {
                            if (count($arr) >= 2) $model->clearing_document = (string)intval($arr[1]);
                        }

                        if (Afis::findOne(['year' => $model->year, 'clearing_document' => $model->clearing_document]) === null) {
                            $model->save();
                        }

                        // print '<pre>'; print_r($model); print '</pre><br>';
                    }
                }
            }
        }        
        return;
    }

    /**
     * Check orphaned files.
     * @return mixed
     */
    public function actionOrphaned()
    {
        $orphans = [];
        
        $dirs = new \DirectoryIterator(Yii::getAlias('@uploads/afis'));
        foreach ($dirs as $dir) {
            if (!$dir->isDot()) {
                $files = new \DirectoryIterator(Yii::getAlias('@uploads/afis/'.$dir->getFilename()));
                foreach ($files as $file) {
                    if (!$file->isDot()) {
                        $filename    = $file->getFilename();
                        $year        = $dir->getFilename();
                        $message = 'Filename invalid. Please rename accordingly.';
                        $clearing_document = '';

                        $raw_filename = str_replace('  ', ' ', $filename);
                        $arr = explode(' ', $raw_filename);
                        if (substr($raw_filename, 0, 4) == "( ) ") {
                            if (isset($arr[2])) { $clearing_document = (string)intval($arr[2]);
                             $message = ''; }
                        } else {
                            if (isset($arr[1])) { $clearing_document = (string)intval($arr[1]);
                             $message = ''; }
                        }

                        if (Afis::findOne([
                            'filename' => $filename, 
                            'year' => $year, 
                            'clearing_document' => $clearing_document
                        ]) === null) {
                            $existed = '';
                            $id = '';
                            if (($model = Afis::findOne(['year' => $year, 'clearing_document' => $clearing_document])) !== null) {
                                $existed = $model->filename;
                                $id = $model->id;
                            }
                            $orphans[] = [
                                'filename' => $filename,
                                'clearing_document' => $clearing_document,
                                'year' => $year,
                                'existed' => $existed,
                                'id' => $id,
                                'message' => $message,
                            ];
                        }

                        // print '<pre>'; print_r($model); print '</pre><br>';
                    }
                }
            }
        }
        
        return $this->render('orphan', [
            'orphans' => $orphans,
        ]);
    }

    public function actionDownload($filename, $year) 
    {
        $filepath = Yii::getAlias('@uploads/afis/'.$year.'/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }


    public function actionRename($filename, $year, $id = "") 
    {
        $post = Yii::$app->request->post();
        $oldfile = Yii::getAlias('@uploads/afis/'.$year.'/'.$filename);

        if ($post && $post['filename'] != $filename) {
            $filepath = Yii::getAlias('@uploads/afis/'.$year.'/'.$post['filename']);
            if (file_exists($filepath)) {
                Yii::$app->session->setFlash('error', 'There is a file with same name. Try another name.');
            } else {
                if (file_exists($oldfile)) rename($oldfile, $filepath);
                if ($post['id']) {
                    $this->findModel($post['id'])->delete();
                }
                $this->getAllFiles();
                return $this->redirect(['index']);
            }
        }
        return $this->render('rename', [
            'filename' => $filename,
            'year' => $year,
            'id' => $id,
        ]);        
    }


    public function actionRemove($filename, $year, $id = "") 
    {
        $filepath = Yii::getAlias('@uploads/afis/'.$year.'/'.$filename);
        if (file_exists($filepath)) unlink($filepath);
        if ($id) {
            try {
                $this->findModel($id)->delete();
                $this->getAllFiles();
            } catch (IntegrityException $e) {
                throw new \yii\web\HttpException(500,"Failed. This data can not be deleted.", 405);
            }
        }
        return $this->redirect(['orphaned']);
    }
}
