<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PplKorin;
use frontend\models\PplKorinSearch;
use frontend\models\PplKorinDocument;
use frontend\models\PplKorinDocumentSearch;
use frontend\models\PplKorinBayar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * PplKorinController implements the CRUD actions for PplKorin model.
 */
class PplKorinController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PplKorin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PplKorinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PplKorin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelDocument = new PplKorinDocument();
        $modelBayar = new PplKorinBayar();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelDocument' => $modelDocument,
            'modelBayar' => $modelBayar,
        ]);
    }

    /**
     * Upload documents.
     * @param integer $id
     * @return mixed
     */
    public function actionDocument()
    {
        if ($post = Yii::$app->request->post()) {
            $modelDocument = new PplKorinDocument();
            $modelDocument->load($post);
            $modelDocument->uploadedFile = UploadedFile::getInstance($modelDocument, 'uploadedFile');

            if (is_object($modelDocument->uploadedFile)) {
                $modelDocument->file    = $modelDocument->uploadedFile->name;
                $filename               = "ppl/".$modelDocument->uploadedFile->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$modelDocument->uploadedFile->name." (".$i.").".$ext;
                    $modelDocument->file    = $modelDocument->uploadedFile->name." (".$i.").".$ext;
                }
                $modelDocument->uploadedFile->saveAs($filename);
            }            
            // $modelDocument->uploadedFile = null;
            if (!$modelDocument->save()) {print '<pre>'; print_r($modelDocument->getErrors()); die();}
        }
        return $this->redirect(['view', 'id' => $modelDocument->ppl_korin_id]);
    }

    /**
     * Save payments.
     * @param integer $id
     * @return mixed
     */
    public function actionPayment()
    {
        if ($post = Yii::$app->request->post()) {
            $modelBayar = new PplKorinBayar();
            $modelBayar->load($post);
            if (!$modelBayar->save()) {print '<pre>'; print_r($modelBayar->getErrors()); die();}
        }
        return $this->redirect(['view', 'id' => $modelBayar->ppl_korin_id]);
    }

    /**
     * Deletes an existing PplKorinDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteFile($id, $document_id)
    {
        $model = $this->findModelKorinDocument($document_id);
        $file = Yii::getAlias('@webroot').'/ppl/'.$model->file;
        if (file_exists($file)) unlink($file);
        $model->delete();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing PplKorinDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletePayment($id, $payment_id)
    {
        $model = $this->findModelKorinBayar($payment_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Creates a new PplKorin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PplKorin();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PplKorin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PplKorin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PplKorin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PplKorin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PplKorin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelKorinDocument($id)
    {
        if (($model = PplKorinDocument::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelKorinBayar($id)
    {
        if (($model = PplKorinBayar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
