<?php

namespace frontend\controllers;

use Yii;
use frontend\models\DokumenUpload;
use frontend\models\Korin;
use frontend\models\KorinSearch;
use frontend\models\KorinDokumen;
use frontend\models\KorinDokumenSearch;
use frontend\models\KorinVendor;
use frontend\models\KorinVendorSearch;
use frontend\models\KorinItem;
use frontend\models\KorinItemSearch;
use frontend\models\KorinPrItem;
use frontend\models\KorinPrItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * KorinController implements the CRUD actions for Korin model.
 */
class KorinController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Korin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KorinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Korin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelKorinDokumen        = new KorinDokumenSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $id]]);
        $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
        
        $searchModelKorinVendor         = new KorinVendorSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $id]]);
        $dataProviderKorinVendor        = $searchModelKorinVendor->search(Yii::$app->request->queryParams);
        
        $searchModelKorinItem           = new KorinItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $id]]);
        $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
        
        $searchModelKorinPrItem         = new KorinPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $id]]);
        $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);

        $modelUpload = new DokumenUpload();
            
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelKorinDokumen' => $searchModelKorinDokumen,
            'dataProviderKorinDokumen' => $dataProviderKorinDokumen,

            'searchModelKorinVendor' => $searchModelKorinVendor,
            'dataProviderKorinVendor' => $dataProviderKorinVendor,
            
            'searchModelKorinItem' => $searchModelKorinItem,
            'dataProviderKorinItem' => $dataProviderKorinItem,

            'searchModelKorinPrItem' => $searchModelKorinPrItem,
            'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
        ]);
    }
    
    /**
     * Validates one model attribute.
     * 
     * @return json
     */
    public function ajaxValidation($model, $field) {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	return \yii\widgets\ActiveForm::validate($model, $field);
    }

    public function actionSuratunique() {
        $no_surat   = Yii::$app->request->get('no_surat');
        $result     = Korin::find()->where(['no_surat' => $no_surat])->one();
        if ($result) return "<font color='crimson'>No. KORIN $no_surat has already been taken.</font>";
    }

    /**
     * Creates a new Korin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$model = new Korin();
    	
    	if (Yii::$app->request->isAjax) {
    		if ($model->load(Yii::$app->request->post())) {
	    		return $this->ajaxValidation($model, 'no_surat');
    		}
    	} else {
    		if ($model->load(Yii::$app->request->post()) && $model->save()) {
    			return $this->redirect(['view', 'id' => $model->id]);
    		} else {
    			return $this->render('create', [
    					'model' => $model,
    			]);
    		}
    	}
    }

    /**
     * Updates an existing Korin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if (Yii::$app->request->isAjax) {
        	if ($model->load(Yii::$app->request->post())) {
        		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        		return \yii\widgets\ActiveForm::validate($model, 'no_surat');
        	}
        } else {
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	            return $this->redirect(['view', 'id' => $model->id]);
	        } else {
	            return $this->render('update', [
	                'model' => $model,
	            ]);
	        }
        }        
    }

    /**
     * Deletes an existing Korin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        KorinDokumen::deleteAll(['korin_id' => $id]);
        $this->findModel($id)->delete();

        if (Yii::$app->request->get("double")) {
            return $this->redirect(['semua-pengadaan/undoubler']);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Korin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Korin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Korin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    //AJAX handler
    public function actionDokumen() {
        $get = Yii::$app->request->get();
        
        $model = new KorinDokumen();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                $model->korin_id = $get['korin_id'];
                $model->dokumen_upload_id = $get['dokumen_upload_id'];
                $model->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinDokumen::findOne($get['korin_dokumen_id'])->delete();
            }

            $searchModelKorinDokumen        = new KorinDokumenSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $get['korin_id']]]);
            $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
            return $this->renderAjax('dokumen', [
                'korin_id' => $get['korin_id'],
                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
            ]);
        }
    }

    //AJAX handler
    public function actionUploaddokumen() {
        $get = Yii::$app->request->get();
        
        $modelDokumenUpload = new DokumenUpload();

        var_dump($_FILES); die();
        $modelDokumenUpload->tgl_dokumen        = $get['doc-tanggal'];
        $modelDokumenUpload->dokumen_jenis_id   = $get['doc-jenis'];
        $modelDokumenUpload->no_dokumen         = $get['doc-nomor'];
        $modelDokumenUpload->keterangan         = $get['doc-keterangan'];
        /*$modelDokumenUpload->dokumen = \yii\web\UploadedFile::getInstanceByName($get['dokumen']);            
        if (is_object($modelDokumenUpload->dokumen)) {
            $modelDokumenUpload->pdf_filename    = $modelDokumenUpload->dokumen->name;
            $filename                            = "document/".$modelDokumenUpload->dokumen->name;
            $i = 0;                
            while (file_exists($filename)) {
                $i++;
                $ext                                = end(explode('.',$filename));
                $filename                           = "document/".$modelDokumenUpload->dokumen->name." (".$i.").".$ext;
                $modelDokumenUpload->pdf_filename   = $modelDokumenUpload->dokumen->name." (".$i.").".$ext;
            }                
            $modelDokumenUpload->dokumen->saveAs($filename);
        }                
        $modelDokumenUpload->save();*/
        
    }

    //AJAX handler
    public function actionRegisterdokumen() {
        $get = Yii::$app->request->get();
        
        $modelDokumenUpload = new DokumenUpload();
        $modelKorinDokumen  = new KorinDokumen();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                $modelDokumenUpload->dokumen_jenis_id   = $get['doc-jenis'];
                $modelDokumenUpload->no_dokumen         = $get['doc-nomor'];
                $modelDokumenUpload->tgl_dokumen        = $get['doc-tanggal'];
                $modelDokumenUpload->keterangan         = $get['doc-keterangan'];
                $modelDokumenUpload->dokumen = \yii\web\UploadedFile::getInstanceByName($get['dokumen']);            
                if (is_object($modelDokumenUpload->dokumen)) {
                    $modelDokumenUpload->pdf_filename    = $modelDokumenUpload->dokumen->name;
                    $filename               = Yii::getAlias('@uploads/'.$modelDokumenUpload->dokumen->name);
                    $i = 0;                
                    while (file_exists($filename)) {
                        $i++;
                        $ext = end(explode('.',$filename));
                        $filename               = Yii::getAlias('@uploads/'.$modelDokumenUpload->dokumen->name)." (".$i.").".$ext;
                        $modelDokumenUpload->pdf_filename    = $modelDokumenUpload->dokumen->name." (".$i.").".$ext;
                    }                
                    $modelDokumenUpload->dokumen->saveAs($filename);
                }                
                $modelDokumenUpload->save();

                $modelKorinDokumen->korin_id = $get['korin_id'];
                $modelKorinDokumen->dokumen_upload_id = $modelDokumenUpload->id;
                $modelKorinDokumen->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinDokumen::findOne($get['korin_dokumen_id'])->delete();
            }

            $searchModelKorinDokumen        = new KorinDokumenSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $get['korin_id']]]);
            $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
            return $this->renderAjax('dokumen', [
                'korin_id' => $get['korin_id'],
                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
            ]);
        }
    }

    //AJAX handler
    public function actionVendor() {
        $get = Yii::$app->request->get();
        
        $model = new KorinVendor();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                $model->korin_id 	= $get['korin_id'];
                $model->vendor_id 	= $get['vendor_id'];
                $model->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinVendor::findOne($get['korin_vendor_id'])->delete();
            }

            $searchModelKorinVendor        	= new KorinVendorSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinVendorSearch' => ['korin_id' => $get['korin_id']]]);
            $dataProviderKorinVendor      	= $searchModelKorinVendor->search(Yii::$app->request->queryParams);
            return $this->renderAjax('vendor', [
                'korin_id' => $get['korin_id'],
                'searchModelKorinVendor' => $searchModelKorinVendor,
                'dataProviderKorinVendor' => $dataProviderKorinVendor,
            ]);
        }
    }

    //AJAX handler
    public function actionItemkorin() {
        $get = Yii::$app->request->get();
        
        $model = new KorinItem();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                $model->korin_id    = $get['korin_id'];
                $model->nama        = $get['itemKorin-nama'];
                $model->quantity    = $get['itemKorin-quantity'];
                $model->satuan      = $get['itemKorin-satuan'];
                $model->keterangan  = $get['itemKorin-keterangan'];
                $model->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinItem::findOne($get['korin_itemKorin_id'])->delete();
            }

            $searchModelKorinItem           = new KorinItemSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinItemSearch' => ['korin_id' => $get['korin_id']]]);
            $dataProviderKorinItem          = $searchModelKorinItem->search(Yii::$app->request->queryParams);
            return $this->renderAjax('itemKorin', [
                'korin_id' => $get['korin_id'],
                'searchModelKorinItem' => $searchModelKorinItem,
                'dataProviderKorinItem' => $dataProviderKorinItem,
            ]);
        }
    }

    //AJAX handler
    public function actionItempr() {
        $get = Yii::$app->request->get();        
        
        $model = new KorinPrItem();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                $model->korin_id                = $get['korin_id'];
                $model->purchase_requisition    = $get['itemPr-purchase_requisition'];
                $model->item_of_requisition     = $get['itemPr-item_of_requisition'];
                //$model->korin_item_id           = $get['itemPr-korin_item_id'];
                $model->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinPrItem::findOne($get['korin_itemPr_id'])->delete();
            }
            
            $searchModelKorinPrItem         = new KorinPrItemSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinPrItemSearch' => ['korin_id' => $get['korin_id']]]);
            $dataProviderKorinPrItem        = $searchModelKorinPrItem->search(Yii::$app->request->queryParams);
            return $this->renderAjax('itemPr', [
                'korin_id' => $get['korin_id'],
                'searchModelKorinPrItem' => $searchModelKorinPrItem,
                'dataProviderKorinPrItem' => $dataProviderKorinPrItem,
            ]);
        }
    }

    /*select2 ajax handler*/
    public function actionList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(["id as id", 
                    new \yii\db\Expression("CONCAT(
                        no_surat 
                        ,' : ', perihal) 
                        as text")])
                ->from("korin")
                ->where("no_surat LIKE '%".$q."%' or 
                        tanggal_surat LIKE '%".$q."%' or 
                        perihal LIKE '%".$q."%'
                        order by no_surat DESC")
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Korin::find($id)->no_surat];
        }
        return $out;
    }



    //AJAX handler
    public function actionRenameFile() {

        $get        = Yii::$app->request->get();
        $model      = DokumenUpload::findOne($get['korin-dokumen_dokumen_upload_id']);
        $oldname    = $model->pdf_filename;
        
        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save") 
            {   
                $model->pdf_filename    = $get['korin-dokumen_pdf_filename'];
                $arr                    = explode("_", $model->pdf_filename);
                $model->no_dokumen      = end($arr);

                if (substr($model->no_dokumen, -4) == ".pdf") {
                    $arr                = explode(".", $model->no_dokumen);
                    $filetype           = array_pop($arr);
                    $model->no_dokumen  = implode(".", $arr);
                }
                if ($model->save()) {
                    rename(Yii::getAlias('@uploads/'.$oldname), Yii::getAlias('@uploads/'.$model->pdf_filename));
                }
            }

            $searchModelKorinDokumen        = new KorinDokumenSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['KorinDokumenSearch' => ['korin_id' => $get['korin-dokumen_korin_id']]]);
            $dataProviderKorinDokumen       = $searchModelKorinDokumen->search(Yii::$app->request->queryParams);
            
            return $this->renderAjax('dokumen', [
                'korin_id' => $get['korin-dokumen_korin_id'],
                    
                'searchModelKorinDokumen' => $searchModelKorinDokumen,
                'dataProviderKorinDokumen' => $dataProviderKorinDokumen,
            ]);
        }
    }

}
