<?php

namespace frontend\controllers\daily;

use Yii;
use frontend\models\DailyReport;
use frontend\models\DailyReportSearch;
use frontend\models\DailyReportDocument;
use frontend\models\DailyPurchaser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DailyReportController implements the CRUD actions for DailyReport model.
 */
class DailyReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DailyReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DailyReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DailyReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DailyReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DailyReport();
        $post  = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            if (is_object(DailyPurchaser::find()->where(['user_id'=>Yii::$app->user->id])->one())) {
                $model->daily_purchaser_id = DailyPurchaser::find()->where(['user_id'=>Yii::$app->user->id])->one()->id;
            } else {
                $model->daily_purchaser_id = Yii::$app->user->id;
            }
            if ($model->save()) {
                if (isset($post['c_rep_doc'])) {
                    foreach ($post['c_rep_doc'] as $daily_document_id) {
                        $modelDailyReportDocument                       = new DailyReportDocument();
                        $modelDailyReportDocument->daily_report_id      = $model->id;
                        $modelDailyReportDocument->daily_document_id    = $daily_document_id;
                        $modelDailyReportDocument->save();
                    }
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DailyReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post  = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            DailyReportDocument::deleteAll(['daily_report_id'=>$model->id]);
            if (isset($post['c_rep_doc'])) {
                foreach ($post['c_rep_doc'] as $daily_document_id) {
                    $modelDailyReportDocument                       = new DailyReportDocument();
                    $modelDailyReportDocument->daily_report_id      = $model->id;
                    $modelDailyReportDocument->daily_document_id    = $daily_document_id;
                    $modelDailyReportDocument->save();
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DailyReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        DailyReportDocument::deleteAll(['daily_report_id'=>$id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DailyReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DailyReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DailyReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionHighlight() {
      return $this->render('highlight');
    }

    public function actionUpdateAll() {
      $daily_purchaser_id = DailyPurchaser::find()->where(['user_id'=>Yii::$app->user->id])->one()->id;
      DailyReport::updateAll(['updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()')], 'daily_purchaser_id = '.$daily_purchaser_id);
      return $this->redirect(['index']);
    }

    public function actionNoChanges($id) {
      $model = $this->findModel($id);
      $model->touch('updated_at');
      return $this->redirect(['index']);
    }
}
