<?php

namespace frontend\controllers;

use Yii;
use frontend\models\AuditFolder;
use frontend\models\AuditorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuditorController implements the CRUD actions for AuditFolder model.
 */
class AuditorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuditFolder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'auditor';
        $searchModel = new AuditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuditFolder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'auditor';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the AuditFolder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuditFolder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuditFolder::findOne(['id' => $id, 'is_visible' => '1'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownload($file) 
    {
        $this->layout = 'auditor';
        $filepath = Yii::getAlias('@bundles/'.$file);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath);
    }
}
