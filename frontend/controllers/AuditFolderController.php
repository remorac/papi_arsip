<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TmpListPo;
use frontend\models\AuditFile;
use frontend\models\AuditFolder;
use frontend\models\AuditFolderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuditFolderController implements the CRUD actions for AuditFolder model.
 */
class AuditFolderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuditFolder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditFolderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuditFolder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $auditFile_id = "")
    {
        
        if ($auditFile_id) {
            AuditFile::findOne($auditFile_id)->delete();
        } 
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuditFolder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuditFolder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $listPo = TmpListPo::find()->all();
            foreach ($listPo as $po) {
                if ($po->pengadaan !== null) {
                    if (file_exists(Yii::getAlias('@bundles/'.$po->pengadaan->nama.'.zip'))) {
                        if (AuditFile::findOne(['filename' => $po->pengadaan->nama.'.zip', 'audit_folder_id' => $model->id]) === null) {
                            $auditFile = new AuditFile();
                            $auditFile->audit_folder_id = $model->id;
                            $auditFile->filename = $po->pengadaan->nama.'.zip';
                            $auditFile->save();
                        }
                    }
                    for ($i=0; $i < 100; $i++) { 
                        if (file_exists(Yii::getAlias('@bundles/'.$po->pengadaan->nama.'.part-'.$i.'.zip'))) {
                            if (AuditFile::findOne(['filename' => $po->pengadaan->nama.'.part-'.$i.'.zip', 'audit_folder_id' => $model->id]) === null) {
                                $auditFile = new AuditFile();
                                $auditFile->audit_folder_id = $model->id;
                                $auditFile->filename = $po->pengadaan->nama.'.part-'.$i.'.zip';
                                $auditFile->save();
                            }
                        }
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuditFolder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuditFolder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        AuditFile::deleteAll(['audit_folder_id' => $model->id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuditFolder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuditFolder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuditFolder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownload($file) 
    {
        $filepath = Yii::getAlias('@bundles/'.$file);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath);
    }
}
