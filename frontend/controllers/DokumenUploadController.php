<?php

namespace frontend\controllers;

use Yii;
use frontend\models\DokumenUpload;
use frontend\models\DokumenUploadSearch;
use frontend\models\KorinDokumen;
use frontend\models\KorinKeluarDokumen;
use frontend\models\PengadaanActivityDokumen;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * DokumenUploadController implements the CRUD actions for DokumenUpload model.
 */
class DokumenUploadController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DokumenUpload models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DokumenUploadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DokumenUpload model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DokumenUpload model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DokumenUpload();

        if ($model->load(Yii::$app->request->post())) {
            $model->dokumen = \yii\web\UploadedFile::getInstance($model, 'dokumen');
            
            if (is_object($model->dokumen)) {
                $model->pdf_filename    = $model->dokumen->name;
                $filename               = Yii::getAlias('@uploads/'.$model->dokumen->name);
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $model->pdf_filename    = $model->dokumen->name.' ('.$i.').'.$ext;
                    $filename               = Yii::getAlias('@uploads/'.$model->pdf_filename);
                }
                $model->dokumen->saveAs($filename);
            }            
            $model->dokumen = null;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);

        } else {
        
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DokumenUpload model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->dokumen = \yii\web\UploadedFile::getInstance($model, 'dokumen');
            
            if (is_object($model->dokumen)) {
                $old_filename   = Yii::getAlias('@uploads/'.$model->dokumen->name);
                if (is_file($old_filename)) unlink($old_filename);
                $model->pdf_filename    = $model->dokumen->name;
                $filename               = Yii::getAlias('@uploads/'.$model->dokumen->name);
                $i                      = 0;
        
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $model->pdf_filename    = $model->dokumen->name.' ('.$i.').'.$ext;
                    $filename               = Yii::getAlias('@uploads/'.$model->pdf_filename);
                }
        
                $model->dokumen->saveAs($filename);
                // if (is_file($old_filename)) unlink($old_filename);
            }
            $model->dokumen = null;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        
        } else {
        
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DokumenUpload model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $old_filename = Yii::getAlias('@uploads/'.$model->pdf_filename);
        if (is_file($old_filename)) unlink($old_filename);
        return $this->redirect(['index']);
    }

    /**
     * Finds the DokumenUpload model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DokumenUpload the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DokumenUpload::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*select2 ajax handler*/
    public function actionList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(["dokumen_upload.id as id", 
                    new \yii\db\Expression("CONCAT(
                        dokumen_upload.pdf_filename 
                        ,' (',dokumen_upload.no_dokumen,') '
                        ,' : ', dokumen_jenis.nama) 
                        as text")])
                ->from("dokumen_upload, dokumen_jenis")
                ->where("dokumen_jenis.id = dokumen_upload.dokumen_jenis_id 
                        and 
                            (
                                dokumen_jenis.nama LIKE '%".$q."%' or 
                                dokumen_upload.pdf_filename LIKE '%".$q."%' or 
                                dokumen_upload.no_dokumen LIKE '%".$q."%' 
                            )
                        order by dokumen_upload.tgl_dokumen DESC")
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => DokumenUpload::find($id)->pdf_filename];
        }
        return $out;
    }

    //link to KORIN
    function actionLinkkorin() {

        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post("korin_id");

            $model = new KorinDokumen();
            $model->korin_id = $id;
            $model->dokumen_upload_id = Yii::$app->request->post("linkKorin_id");
            $model->save();
            
            return $this->redirect(['view', 'id' => $model->dokumen_upload_id]);
        }
    }

    //link to KORIN Keluar
    function actionLinkkorinkeluar() {

        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post("korin_keluar_id");

            $model = new KorinKeluarDokumen();
            $model->korin_keluar_id = $id;
            $model->dokumen_upload_id = Yii::$app->request->post("linkKorinKeluar_id");
            $model->save();
            
            return $this->redirect(['view', 'id' => $model->dokumen_upload_id]);
        }
    }
    
    //link to Pengadaan
    function actionLinkpengadaan() {

        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post("pengadaan-activity_id");

            $model = new PengadaanActivityDokumen();
            $model->pengadaan_activity_id = $id;
            $model->dokumen_upload_id = Yii::$app->request->post("linkPengadaan_id");
            $model->save();
            
            return $this->redirect(['view', 'id' => $dokumen_upload_id]);            
        }
    }

    /**
     * Validates one model attribute.
     * 
     * @return json
     */
    public function ajaxValidation($model, $field) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return \yii\widgets\ActiveForm::validate($model, $field);
    }

    public function actionDokumenUnique() {
        $no_dokumen = Yii::$app->request->get('no_dokumen');
        $result     = DokumenUpload::find()->where(['no_dokumen' => $no_dokumen])->one();
        if ($result) return "<font color='#dd4b39'>No. Dokumen $no_dokumen has already been taken.</font>";
    }

    public function zipping()
    {
        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open(Yii::getAlias('@uploads/TORs.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $dokumenUploads = DokumenUpload::find()->where("dokumen_jenis_id = '1'")->all();

        foreach ($dokumenUploads as $dokumenUpload)
        {
            $file = $dokumenUpload->pdf_filename;
            if (is_file(Yii::getAlias('@frontenddocument/'.$file))) {
                $zip->addFile(Yii::getAlias('@uploads/'.$file), $file);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
        return true;
    }

    public function actionDownloadTor() {
        ini_set('max_execution_time', 3600);
        if ($this->zipping() == true) {
            $filepath = Yii::getAlias('@uploads/TORs.zip');
            $external_path_file = Yii::getAlias('@frontend/../external_path.txt');
            if (file_exists($external_path_file)) {
                $external_path = file_get_contents($external_path_file);
                if ($external_path) {
                    $filepath = $external_path . '/TORs.zip';
                }
            }
            if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
        }
    }

    public function actionDownload($filename) 
    {
        $filepath = Yii::getAlias('@uploads/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }

    public function actionDownloadUncategorized($filename) 
    {
        $filepath = Yii::getAlias('@uploads/uncategorized/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }

    public function actionDownloadZip($filename) 
    {
        $filepath = Yii::getAlias('@bundles/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }

    public function actionDownloadPpl($filename) 
    {
        $filepath = Yii::getAlias('@uploads/ppl/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }
    
}
