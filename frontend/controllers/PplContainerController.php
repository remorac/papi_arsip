<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PplContainer;
use frontend\models\PplContainerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PplContainerController implements the CRUD actions for PplContainer model.
 */
class PplContainerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PplContainer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PplContainerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PplContainer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PplContainer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PplContainer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // $model = new PplKorinDocument();
            // $model->load($post);
            $model->uploadedFile1 = UploadedFile::getInstance($model, 'uploadedFile1');
            $model->uploadedFile2 = UploadedFile::getInstance($model, 'uploadedFile2');

            if (is_object($model->uploadedFile1)) {
                $model->file_in         = $model->uploadedFile1->name;
                $filename               = "ppl/".$model->uploadedFile1->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile1->name." (".$i.").".$ext;
                    $model->file_in         = $model->uploadedFile1->name." (".$i.").".$ext;
                }
                $model->uploadedFile1->saveAs($filename);
            }

            if (is_object($model->uploadedFile2)) {
                $model->file_out        = $model->uploadedFile2->name;
                $filename               = "ppl/".$model->uploadedFile2->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile2->name." (".$i.").".$ext;
                    $model->file_out        = $model->uploadedFile2->name." (".$i.").".$ext;
                }
                $model->uploadedFile2->saveAs($filename);
            }            
            $model->uploadedFile1 = null;
            $model->uploadedFile2 = null;
            if (!$model->save()) {print '<pre>'; print_r($model->getErrors()); die();}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PplContainer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // $model = new PplKorinDocument();
            // $model->load($post);
            $model->uploadedFile1 = UploadedFile::getInstance($model, 'uploadedFile1');
            $model->uploadedFile2 = UploadedFile::getInstance($model, 'uploadedFile2');

            if (is_object($model->uploadedFile1)) {
                $model->file_in         = $model->uploadedFile1->name;
                $filename               = "ppl/".$model->uploadedFile1->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile1->name." (".$i.").".$ext;
                    $model->file_in         = $model->uploadedFile1->name." (".$i.").".$ext;
                }
                $model->uploadedFile1->saveAs($filename);
            }

            if (is_object($model->uploadedFile2)) {
                $model->file_out        = $model->uploadedFile2->name;
                $filename               = "ppl/".$model->uploadedFile2->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile2->name." (".$i.").".$ext;
                    $model->file_out        = $model->uploadedFile2->name." (".$i.").".$ext;
                }
                $model->uploadedFile2->saveAs($filename);
            }            
            $model->uploadedFile1 = null;
            $model->uploadedFile2 = null;
            if (!$model->save()) {print '<pre>'; print_r($model->getErrors()); die();}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PplContainer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $file_in = Yii::getAlias('@webroot').'/ppl/'.$model->file_in;
        $file_out = Yii::getAlias('@webroot').'/ppl/'.$model->file_out;
        if (file_exists($file_in)) unlink($file_in);
        if (file_exists($file_out)) unlink($file_out);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PplContainer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PplContainer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PplContainer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
