<?php

namespace frontend\controllers;

use Yii;
use frontend\models\GcalToken;
use frontend\models\GcalTokenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GcalTokenController implements the CRUD actions for GcalToken model.
 */
class GcalTokenController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GcalToken models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GcalTokenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GcalToken model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GcalToken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GcalToken();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GcalToken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GcalToken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GcalToken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return GcalToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GcalToken::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFirstToken()
    {
        $id = Yii::$app->request->get('id');
        if($id == null){
            if(Yii::$app->session->getFlash('id') == null){
                // show error page
                die();
            }
            $id = Yii::$app->session->getFlash('id');
        }

        $gcalToken = \frontend\models\GcalToken::find()
            ->where(['id' => $id])
            ->one();

        $oauthClient = $gcalToken->oauthClient; 
        $clientId = $oauthClient->web->client_id;// Yii::$app->params['googleClientId'];
        $clientEmail = $gcalToken->client_email;// Yii::$app->params['googleClientEmail'];
        $clientSecret = $oauthClient->web->client_secret;// Yii::$app->params['googleClientSecret'];
        $clientRedirectUri = 'http://'. $_SERVER['SERVER_NAME'] . \yii\helpers\Url::to(['gcal-token/first-token']);
        $calendarPrivateKey = $gcalToken->calendar_private_key;// Yii::$app->params['googleCalendarPrivateKey'];
        $calendarOwner = $gcalToken->calendar_owner_email; // Yii::$app->params['googleCalendarOwner'];
        $clientScopes = ['https://www.googleapis.com/auth/calendar'];
        $developerKey = Yii::$app->params['googleServerApiKey'];
        $calendarId = $gcalToken->calendar_id; //Yii::$app->params['googleCalendarId'];
        $firstAccessToken = $gcalToken->firstAccessToken;
        $accessToken = $gcalToken->accessToken;

        if (strpos($clientId, "googleusercontent") == false
            || !strlen($clientEmail)
            || !strlen($calendarPrivateKey)) {
          echo missingServiceAccountDetailsWarning();
          exit;
        }

        $client = new \Google_Client();
        $client->setApprovalPrompt ("auto");
        $client->setApprovalPrompt ("force");
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($clientRedirectUri);
        $client->setScopes($clientScopes);
        $client->setAccessType('offline');
        $session = new \yii\web\Session;
        if(Yii::$app->request->get('code') !== null){
            $client->authenticate(Yii::$app->request->get('code'));
            $accessToken = $client->getAccessToken();
            $gcalToken->first_access_token_json = $accessToken;
            $gcalToken->access_token_json = $accessToken;
            $gcalToken->save();
            return $this->redirect([yii\helpers\Url::to(['gcal-token/view']), 'id' => $id]);
        } 

        $credentials = new \Google_Auth_AssertionCredentials(
            $clientEmail,
            $clientScopes,
            $calendarPrivateKey,
            $gcalToken->calendar_private_key_password,
            'http://oauth.net/grant_type/jwt/1.0/bearer', // Default grant type
            $calendarOwner

        );

        if($client->getAccessToken() === null){
            $client->setAssertionCredentials($credentials);
            $client->setScopes($clientScopes);
            $client->setClientId($clientId);
            $authUrl = $client->createAuthUrl();
            return $this->redirect($authUrl);
        }

        return $this->redirect([yii\helpers\Url::to(['gcal-token/view']), 'id' => $id]);
    }

    function actionRefreshToken()
    {
        $id = Yii::$app->request->get('id');
        if($id == null){
            if(Yii::$app->session->getFlash('id') == null){
                // show error page
                die();
            }
            $id = Yii::$app->session->getFlash('id');
        }

        $gcalToken = \frontend\models\GcalToken::find()
            ->where(['id' => $id])
            ->one();

        $oauthClient = $gcalToken->oauthClient; 
        $clientId = $oauthClient->web->client_id;// Yii::$app->params['googleClientId'];
        $clientEmail = $gcalToken->client_email;// Yii::$app->params['googleClientEmail'];
        $clientSecret = $oauthClient->web->client_secret;// Yii::$app->params['googleClientSecret'];
        $clientRedirectUri = 'http://'. $_SERVER['SERVER_NAME'] . \yii\helpers\Url::to(['gcal-token/first-token']);
        $calendarPrivateKey = $gcalToken->calendar_private_key;// Yii::$app->params['googleCalendarPrivateKey'];
        $calendarOwner = $gcalToken->calendar_owner_email; // Yii::$app->params['googleCalendarOwner'];
        $clientScopes = ['https://www.googleapis.com/auth/calendar'];
        $developerKey = Yii::$app->params['googleServerApiKey'];
        $calendarId = $gcalToken->calendar_id; //Yii::$app->params['googleCalendarId'];
        $firstAccessToken = $gcalToken->firstAccessToken;
        $accessToken = $gcalToken->accessToken;

        if (strpos($clientId, "googleusercontent") == false
            || !strlen($clientEmail)
            || !strlen($calendarPrivateKey)) {
          echo missingServiceAccountDetailsWarning();
          exit;
        }

        $client = new \Google_Client();
        $client->setApprovalPrompt ("auto");
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($clientRedirectUri);
        $client->setScopes($clientScopes);
        $client->setAccessType('offline');
        if(empty($gcalToken->first_access_token)){
          if(Yii::$app->request->get('code') !== null){
            $client->authenticate(Yii::$app->request->get('code'));
            $accessToken = $client->getAccessToken();
            $gcalToken->first_access_token_json = $accessToken;
            $gcalToken->access_token_json = $accessToken;
            $gcalToken->save();
            return $this->redirect([yii\helpers\Url::to(['gcal-token/view']), 'id' => $id]);
          } 
        }

        $credentials = new \Google_Auth_AssertionCredentials(
            $clientEmail,
            $clientScopes,
            $calendarPrivateKey,
            $gcalToken->calendar_private_key_password,
            'http://oauth.net/grant_type/jwt/1.0/bearer', // Default grant type
            $calendarOwner

        );

        if($client->getAccessToken() === null){
            $client->setAssertionCredentials($credentials);
            $client->setScopes($clientScopes);
            $client->setClientId($clientId);
            $authUrl = $client->createAuthUrl();
            return $this->redirect($authUrl);
        }

        $client->setDeveloperKey($developerKey);
        $service = new \Google_Service_Calendar($client);
        $client->setApplicationName("PAPI");
        if(strpos($developerKey, "<") !== false) {
            echo missingApiKeyWarning();
            exit;
        }

        $client->setAssertionCredentials($credentials);

        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshToken($firstAccessToken->refresh_token);
            $gcalToken->access_token = $client->getAccessToken();
            $gcalToken->save();
        }

        return $this->redirect([yii\helpers\Url::to(['gcal-token/view']), 'id' => $id]);
    }
}
