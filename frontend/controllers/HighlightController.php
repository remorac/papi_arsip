<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\PengadaanSearch;
use frontend\models\PengadaanSearchInbox;
use frontend\models\PengadaanSearchInProgress;
use frontend\models\PengadaanSearchPoDone;
use frontend\models\MetodaActivity;
use frontend\models\DurasiStandar;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\PengadaanKorinSearch;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanPoSearch;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanPrItemSearch;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDokumenNotrequired;
use frontend\models\PengadaanDokumenNotrequiredSearch;
use frontend\models\PengadaanIssue;
use frontend\models\PengadaanIssueSearch;
use frontend\models\PengadaanItem;
use frontend\models\PengadaanItemSearch;
use frontend\models\Event;
use frontend\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengadaanController implements the CRUD actions for Pengadaan model.
 */
class HighlightController extends Controller
{
   public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengadaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $alerts = Yii::$app->session->getFlash('alerts');
        
        $searchModelPengadaanKorin      = new PengadaanKorinSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanKorinSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanKorin     = $searchModelPengadaanKorin->search(Yii::$app->request->queryParams);
        
        $searchModelPengadaanPo         = new PengadaanPoSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanPoSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanPo        = $searchModelPengadaanPo->search(Yii::$app->request->queryParams);
        
        $searchModelPengadaanPrItem     = new PengadaanPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanPrItemSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanPrItem    = $searchModelPengadaanPrItem->search(Yii::$app->request->queryParams);
        
        $searchModelPengadaanDokumenNotrequired    = new PengadaanDokumenNotrequiredSearch();
        Yii::$app->request->queryParams            = array_merge(Yii::$app->request->queryParams, ['PengadaanDokumenNotrequiredSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanDokumenNotrequired   = $searchModelPengadaanDokumenNotrequired->search(Yii::$app->request->queryParams);
        
        $searchModelPengadaanIssue      = new PengadaanIssueSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanIssueSearch' => ['pengadaanActivity.pengadaan_id' => $id]]);
        $dataProviderPengadaanIssue     = $searchModelPengadaanIssue->search(Yii::$app->request->queryParams);
        
        $searchModelPengadaanItem       = new PengadaanItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanItemSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanItem      = $searchModelPengadaanItem->search(Yii::$app->request->queryParams);
        
        $searchModelEvent               = new EventSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['EventSearch' => ['pengadaan_id' => $id]]);
        $dataProviderEvent              = $searchModelEvent->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'alerts' => $alerts,

            'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
            'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,

            'searchModelPengadaanPo' => $searchModelPengadaanPo,
            'dataProviderPengadaanPo' => $dataProviderPengadaanPo,

            'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
            'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,
            
            'searchModelPengadaanDokumenNotrequired' => $searchModelPengadaanDokumenNotrequired,
            'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,
            
            'searchModelPengadaanIssue' => $searchModelPengadaanIssue,
            'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,
            
            'searchModelPengadaanItem' => $searchModelPengadaanItem,
            'dataProviderPengadaanItem' => $dataProviderPengadaanItem,
            
            'searchModelEvent' => $searchModelEvent,
            'dataProviderEvent' => $dataProviderEvent,
        ]);
    }

    /**
     * Displays a list Pengadaan model with parameter.
     * @param integer $id
     * @return mixed
     */
    public function actionList()
    {
        $get = Yii::$app->request->get();
        
        $searchModel = new PengadaanSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanSearch'])) $queryParams['PengadaanSearch'] = array();
        
        if(isset($get['barang_jasa_id'])){
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['barang_jasa_id' => $get['barang_jasa_id']]);
        }
        if(isset($get['activity_group_id'])){
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['activity.activity_group_id' => $get['activity_group_id']]);
        }
        if(isset($get['purchaser_personel_procurement_id'])){
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['purchaser_personel_procurement_id' => $get['purchaser_personel_procurement_id']]);
        }
        if(!isset($get['grsa_mode']) || isset($get['grsa_mode']) && $get['grsa_mode']!=1) {
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['pengadaan_activity.start_date' => '!null']);
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['pengadaan_activity.end_date' => 'null']);
        }
        if(isset($get['grsa_mode']) && $get['grsa_mode']==1){
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['pengadaan_activity.start_date' => '!null']);
            $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['pengadaan_activity.end_date' => '!null']);
        }
        $queryParams['PengadaanSearch'] = array_merge($queryParams['PengadaanSearch'], ['pengadaan_durasi_adjusted.durasi' => '!null']);
        
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
