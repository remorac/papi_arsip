<?php

namespace frontend\controllers;

use Yii;
use yii\base\DynamicModel;
use frontend\models\SapPr;
use frontend\models\SapPo;
use frontend\models\Pengadaan;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanPo;

class ArsipController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	$model = new DynamicModel([
	        'keyword',
	    ]);

	    if (Yii::$app->request->post()) {
	        $keyword 	= Yii::$app->request->post("keyword");
	        $model_pr 	= PengadaanPrItem::find()
	        		->select("distinct(purchase_requisition) as pr, pengadaan_id")
	        		->where("purchase_requisition like '%".$keyword."%'")
	        		->groupBy("pengadaan_id")
	        		->all();
	        $model_po 	= PengadaanPo::find()
	        		->select("distinct(purchasing_document) as po, pengadaan_id")
	        		->where("purchasing_document like '%".$keyword."%'")
	        		->groupBy("pengadaan_id")
	        		->all();
	        foreach ($model_pr as $row) {
	        	$model_pengadaan[] = Pengadaan::findOne($row->pengadaan_id);
	        }
	        foreach ($model_po as $row) {
	        	if (isset($model_pengadaan)) {
		        	foreach ($model_pengadaan as $row2) {
		        		$pengadaan = Pengadaan::findOne($row->pengadaan_id);
			        	if ($row2->id != $pengadaan->id) $model_pengadaan[] = $pengadaan;
			        }
		        } else {
		        	$model_pengadaan[] = Pengadaan::findOne($row->pengadaan_id);
		        }
	        }
	        
	        if (isset($model_pengadaan)) {
	        	return $this->render('result', [
		        	'model_pengadaan' => $model_pengadaan,
		        ]);
	    	} else {
	    		Yii::$app->session->setFlash('error', 'No results found.');
	    		return $this->render('index', ['model' => $model]);
	    	}
	    }
	    return $this->render('index', ['model' => $model]);
    }

	public function actionPr() {
		$arr = SapPr::find()->select("distinct(`Purchase Requisition`) as pr")->asArray()->all();
		return (json_encode($arr));
	}

	public function actionPo() {
		$arr = SapPo::find()->select("distinct(`Purchasing Document`) as po")->asArray()->all();
		return (json_encode($arr));
	}

	public function actionFile() {
		$pr = Yii::$app->request->get("pr");
		$po = Yii::$app->request->get("po");

		/*$arrs = PengadaanPrItem::find()
				->select("pengadaan_id")
				->where("purchase_requisition like '%$pr%'")
				->groupBy("pengadaan_id")
				->asArray()
				->all();*/

		$arrs = PengadaanPo::find()
				->select("pengadaan_id")
				->where("purchasing_document like '%$po%'")
				->groupBy("pengadaan_id")
				->asArray()
				->all();

		$response = [];
		foreach ($arrs as $arr) {
			$connection = Yii::$app->getDb();
			$command = $connection->createCommand("
					select
						pengadaan.id as pengadaan_id,
						pengadaan.nama as pengadaan_nama,
						activity.id as activity_id,
						activity.nama as activity_nama,
						dokumen_upload.id as dokumen_upload_id,
						dokumen_upload.pdf_filename,
						dokumen_upload.no_dokumen,
						dokumen_upload.tgl_dokumen,
						dokumen_jenis.id as dokumen_jenis_id,
						dokumen_jenis.nama as dokumen_jenis_nama
					from
						pengadaan,
						pengadaan_activity,
						activity,
						pengadaan_activity_dokumen,
						dokumen_upload,
						dokumen_jenis
					where
						pengadaan.id = pengadaan_activity.pengadaan_id
						and pengadaan_activity.id = pengadaan_activity_dokumen.pengadaan_activity_id
						and pengadaan_activity.activity_id = activity.id
						and pengadaan_activity_dokumen.dokumen_upload_id = dokumen_upload.id
						and dokumen_upload.dokumen_jenis_id = dokumen_jenis.id
					");
			$response[] = $command->queryAll();
		}
		print "<pre>";
		print_r($response);
		// print_r(json_encode($response));
	}

}
