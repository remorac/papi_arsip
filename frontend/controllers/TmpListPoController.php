<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\AuditFolder;
use frontend\models\TmpListPo;
use frontend\models\TmpListPoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * TmpListPoController implements the CRUD actions for TmpListPo model.
 */
class TmpListPoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TmpListPo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TmpListPoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImport()
    {
        if ($post = Yii::$app->request->post()) {

            \frontend\models\TmpListPo::deleteAll();

            $packageFile    = \yii\web\UploadedFile::getInstanceByName('package-file');
            $reader         = ReaderFactory::create(Type::XLSX);
            $reader->open($packageFile->tempName);

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {                
                    foreach ($row as $index) {
                        if (\frontend\models\TmpListPo::findOne((string)$index) === null) {
                            $model = new \frontend\models\TmpListPo();
                            $model->po = (string)$index;
                            $model->save();
                        }
                    }                   
                }
            }
            $reader->close();
            // Yii::$app->getSession()->setFlash('success', 'Data Imported.');
            return $this->redirect(['index']);
        } else {
            return $this->render('import');
        }
    }

    /**
     * Displays a single TmpListPo model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TmpListPo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TmpListPo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->po]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TmpListPo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->po]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TmpListPo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TmpListPo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TmpListPo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TmpListPo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreateZipAll($overwrite = 0) 
    {
        $flag = 1;
        $tmpListPos = TmpListPo::find()->all();
        foreach ($tmpListPos as $tmpListPo) {
            if ($tmpListPo->pengadaan) {
                if (!$overwrite) {
                    $filepath = Yii::getAlias('@webroot') . '/bundled/' . $tmpListPo->pengadaan->nama . '.zip';
                    if (file_exists($filepath)) $flag = 0;
                }
                if ($flag && ($model = Pengadaan::findOne($tmpListPo->pengadaan->id)) !== null) {
                    $model->createZip();
                }
            }
        }
        return $this->redirect(['index']);
    }
    
	public function actionCrazzip($overwrite = 0) 
    {
        $flag = 1;
        $pengadaans = Pengadaan::find()->all();
        foreach ($pengadaans as $pengadaan) {
                if (!$overwrite) {
                    $filepath = Yii::getAlias('@webroot') . '/bundled/' . $pengadaan->nama . '.zip';
                    if (file_exists($filepath)) $flag = 0;
                }
                if ($flag && ($model = Pengadaan::findOne($pengadaan->id)) !== null) {
                    $model->createZip();
                }
        }
        return $this->redirect(['index']);
    }

    public function actionCreateZip($id, $page = '')
    {
        $model = Pengadaan::findOne($id);
        $model->createZip();
        return $this->redirect(['index', 'page' => $page]);
    }
    
    

	public function actionCrazfold($overwrite = 0) 
    {
        $flag = 1;
        $pengadaans = Pengadaan::find()->all();
        foreach ($pengadaans as $pengadaan) {
            if ($flag && ($model = Pengadaan::findOne($pengadaan->id)) !== null) {
                $model->folderize();
            }
        }
        return $this->redirect(['index']);
    }

    public function actionFolderize($id, $page = '')
    {
        $model = Pengadaan::findOne($id);
        $model->folderize();
        return $this->redirect(['index', 'page' => $page]);
    }



    public function actionDownloadZip($filename) 
    {
        $filepath = Yii::getAlias('@webroot').'/bundled/'.$filename;
        Yii::$app->response->sendFile($filepath);
    }
	
	public function actionFileNotExist() {
		$models = \frontend\models\DokumenUpload::find()->all();
		
		echo '<table border="1">';
		echo '<tr>
			<th>exist</th>
			<th>id</th>
			<th>filename</th>
			<th>pengadaan</th>
			<th>act</th>
		<tr>';

		foreach ($models as $model) {
			if (!$model->fileExists) {
				echo '<tr>';
				echo '<td>'.$model->id.'</td>';
				echo '<td>'.$model->fileExists.'</td>';
				echo '<td>'.$model->pdf_filename.'</td>';
				echo '<td>';
				foreach ($model->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
				 echo $pengadaanActivityDokumen->pengadaanActivity->pengadaan->nama.'<br>';
				}
				echo '</td>';
				echo '<td>';
				foreach ($model->pengadaanActivityDokumens as $pengadaanActivityDokumen) {
				 echo $pengadaanActivityDokumen->pengadaanActivity->activity->nama.'<br>';
				}
				echo '</td>';
				echo '</tr>';
				
				$model->delete();
			}
		}
		echo '</table>';
	}
}
