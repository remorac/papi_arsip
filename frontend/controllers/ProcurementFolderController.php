<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ProcurementFolderController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($folder = 'procurement')
    {
    	if ($post = Yii::$app->request->post()) {
    		$path = Yii::getAlias('@uploads/'.$folder.'/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }
            $pdf_files = UploadedFile::getInstancesByName('pdf_files');
            // print_r($pdf_files); die;
            
            foreach ($pdf_files as $pdf_file) {
                $filename = $path.$pdf_file->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = $path.$pdf_file->name.' ('.$i.').'.$ext;
                }
                $pdf_file->saveAs($filename);
            }
    	}
        return $this->render('index', [
        	'folder' => $folder,
        ]);
    }

    public function actionCreate($folder = 'procurement')
    {
    	if ($post = Yii::$app->request->post()) {
    		if ($post['newpath']) {
    			$newpath = Yii::getAlias('@uploads/'.$folder.'/'.$post['newpath']);
    			if (!is_dir($newpath)) mkdir($newpath, 0777, true);
    			return $this->redirect(['index', 'folder' => $folder]);
    		}
    	}
        return $this->render('create', [
        	'folder' => $folder,
        ]);
    }

    //AJAX handler
    public function actionRename() {
        $get = Yii::$app->request->get();

        if (Yii::$app->request->isAjax) {
            $folder  = $get['folder'];
            $is_file  = $get['is_file'];
            $old_name = $get['old_name'];
            $new_name = $get['new_name'];
            if ($old_name == $new_name) return;
            if (!file_exists(Yii::getAlias('@uploads/'.$folder.'/'.$new_name))) {
                rename(Yii::getAlias('@uploads/'.$folder.'/'.$old_name), Yii::getAlias('@uploads/'.$folder.'/'.$new_name));
                return $this->render('index', [
                    'folder' => $folder,
                ]);
            } else {
                return 'file_exists';
            }
        }
    }

    public function actionDownload($filename) 
    {
        $filepath = Yii::getAlias('@uploads/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }

    public function actionRemove($filename, $folder) 
    {
        $filepath = Yii::getAlias('@uploads/'.$filename);
        if (file_exists($filepath)) {
        	is_dir($filepath) ? $this->rrmdir($filepath) : unlink($filepath);
        }
        return $this->redirect(['index', 'folder' => $folder]);
    }

    function rrmdir($dir) { 
		if (is_dir($dir)) { 
			$objects = scandir($dir); 
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (is_dir($dir."/".$object)) rrmdir($dir."/".$object);
					else unlink($dir."/".$object); 
				} 
			}
			rmdir($dir); 
		} 
	}
}
