<?php

namespace frontend\controllers;

use Yii;
use frontend\models\KpiBiayaReport;
use frontend\models\KpiBiayaReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpiBiayaReportController implements the CRUD actions for KpiBiayaReport model.
 */
class KpiBiayaReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpiBiayaReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $kpiBiayaReport = new KpiBiayaReport();
        $kpiBiayaReport->refreshKpiBiaya();

        $searchModel = new KpiBiayaReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpiBiayaReport model.
     * @param string $tahun_bulan
     * @param integer $jenis
     * @return mixed
     */
    public function actionView($tahun_bulan, $jenis)
    {
        return $this->render('view', [
            'model' => $this->findModel($tahun_bulan, $jenis),
        ]);
    }

    /**
     * Finds the KpiBiayaReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $tahun_bulan
     * @param integer $jenis
     * @return KpiBiayaReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tahun_bulan, $jenis)
    {
        if (($model = KpiBiayaReport::findOne(['tahun_bulan' => $tahun_bulan, 'jenis' => $jenis])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
