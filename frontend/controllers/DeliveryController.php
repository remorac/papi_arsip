<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ImportForm;

class DeliveryController extends \yii\web\Controller
{
    public function actionImport()
    {
        $model = new ImportForm();
        /*if ($model->load(Yii::$app->request->post())) {
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            
            if (is_object($model->file)) {
                $filename    = "uploads/".$model->file->name;
                $model->file->saveAs($filename);
                
                $objReader      = \PHPExcel_IOFactory::createReader('CSV');

                //set template
                $template       = Yii::getAlias('@app/web/uploads/ME2M.csv');
                $objPHPExcel    = $objReader->load($template);
                //$activeSheet    = $objPHPExcel->getActiveSheet();
                var_dump($objPHPExcel); die;   
            }
        } */
        return $this->render('import', ['model' => $model]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionWorksheet()
    {
        return $this->render('worksheet');
    }

}
