<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pengadaan;
use frontend\models\PengadaanSearch;
use frontend\models\PengadaanSearchInbox;
use frontend\models\PengadaanSearchInProgress;
use frontend\models\PengadaanSearchPoDone;
use frontend\models\MetodaActivity;
use frontend\models\DurasiStandar;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\PengadaanKorinSearch;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanPoSearch;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanPrItemSearch;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDokumenNotrequired;
use frontend\models\PengadaanDokumenNotrequiredSearch;
use frontend\models\PengadaanIssue;
use frontend\models\PengadaanIssueSearch;
use frontend\models\PengadaanItem;
use frontend\models\PengadaanItemSearch;
use frontend\models\PengadaanFileUncategorized;
use frontend\models\Event;
use frontend\models\EventSearch;
use frontend\models\SapPo;
use frontend\models\HargaSebelumDiskon;
use frontend\models\DokumenUpload;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use frontend\models\PengadaanAccessGroup;
use frontend\models\AccessGroup;

/**
 * PengadaanController implements the CRUD actions for Pengadaan model.
 */
class WorksheetPurchaserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionNormalize() {
        $activities = \frontend\models\Activity::find()->all();
        $pengadaans = Pengadaan::find()->all();
        foreach ($pengadaans as $pengadaan) {
            foreach ($activities as $activity) {
                if (PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."' and activity_id = '".$activity->id."'")->count() >= 2) {
                    PengadaanActivity::deleteAll("pengadaan_id = '".$pengadaan->id."' and activity_id = '".$activity->id."' and end_date is null");
                }
                if (PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$pengadaan->id."' and activity_id = '".$activity->id."' and durasi = '0'")->count() >= 1) {
                    PengadaanActivity::deleteAll("pengadaan_id = '".$pengadaan->id."' and activity_id = '".$activity->id."'");
                }
            }
            if ($pengadaan->start_plan == "" or is_null($pengadaan->start_plan) or empty($pengadaan->start_plan)) {
                if (($pengadaan->pengadaanActivities)) {
                    $pengadaan->start_plan = PengadaanActivity::find()->where("pengadaan_id = '".$pengadaan->id."'")->orderBy("start_date asc")->one()->start_date;
                    $pengadaan->save();
                }
            }
        }

        $pengadaanPriceLists = Pengadaan::find()->where("metoda_id = '3' or metoda_id = '12'")->all();
        foreach ($pengadaanPriceLists as $pengadaanPriceList) {
          // if (is_object(PengadaanActivity::find()->where("pengadaan_id = '".$pengadaanPriceList->id."' and activity_id = '2'")->all())) {
            PengadaanActivity::deleteAll("pengadaan_id = '".$pengadaanPriceList->id."' and activity_id = '2'");
          // }
        }

        return $this->redirect(['index']);

        /*
         * query for normalize #2

            select
            pengadaan_id as idp,
            activity_id as ida,
            (select count(activity_id) from pengadaan_activity  where pengadaan_id = idp and activity_id = ida) as jml

            from
            pengadaan_activity
        */
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengadaanSearchInbox();
        $queryParams = Yii::$app->request->queryParams;

        if(Yii::$app->user->identity->authAssignment->item_name == 'purchaser_barang' || Yii::$app->user->identity->authAssignment->item_name == 'purchaser_jasa'){
            if (!isset($queryParams['PengadaanSearchInbox'])) $queryParams['PengadaanSearchInbox'] = array();
            $queryParams['PengadaanSearchInbox'] = array_merge($queryParams['PengadaanSearchInbox'],
                ['purchaser_personel_procurement_id' => Yii::$app->user->identity->personel->personelProcurement->id,]
            );
        }
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInprogress()
    {
        $searchModel = new PengadaanSearchInProgress();
        $queryParams = Yii::$app->request->queryParams;

        if(Yii::$app->user->identity->authAssignment->item_name == 'purchaser_barang' || Yii::$app->user->identity->authAssignment->item_name == 'purchaser_jasa'){
            if (!isset($queryParams['PengadaanSearchInProgress'])) $queryParams['PengadaanSearchInProgress'] = array();
            $queryParams['PengadaanSearchInProgress'] = array_merge($queryParams['PengadaanSearchInProgress'],
                ['purchaser_personel_procurement_id' => Yii::$app->user->identity->personel->personelProcurement->id,]
            );
        }
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('in_progress', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPodone()
    {
        $searchModel = new PengadaanSearchPoDone();
        $queryParams = Yii::$app->request->queryParams;

        if(Yii::$app->user->identity->authAssignment->item_name == 'purchaser_barang' || Yii::$app->user->identity->authAssignment->item_name == 'purchaser_jasa'){
            if (!isset($queryParams['PengadaanSearchPoDone'])) $queryParams['PengadaanSearchPoDone'] = array();
            $queryParams['PengadaanSearchPoDone'] = array_merge($queryParams['PengadaanSearchPoDone'],
                ['purchaser_personel_procurement_id' => Yii::$app->user->identity->personel->personelProcurement->id,]
            );
        }
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('po_done', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model  = $this->findModel($id);
        $post   = Yii::$app->request->post();

        if ($model->load($post)) {
            $path = Yii::getAlias('@uploads/uncategorized/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }
            $model->pdf_files = UploadedFile::getInstances($model, 'pdf_files');
            
            foreach ($model->pdf_files as $pdf_file) {
                $pengadaanFileUncategorized = new \frontend\models\PengadaanFileUncategorized();
                $pengadaanFileUncategorized->pengadaan_id = $id;

                $pengadaanFileUncategorized->filename    = $pdf_file->name;
                $filename               = Yii::getAlias('@uploads/uncategorized/'.$pdf_file->name);
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $pengadaanFileUncategorized->filename    = $pdf_file->name.' ('.$i.').'.$ext;
                    $filename               = Yii::getAlias('@uploads/uncategorized/'.$pengadaanFileUncategorized->filename);
                }
                $pdf_file->saveAs($filename);
                if ($pengadaanFileUncategorized->save()) return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if ($post) {
            $accessGroups = AccessGroup::find()->all();
            foreach ($accessGroups as $accessGroup) {
                if (isset($post[$accessGroup->id])) {
                    if (PengadaanAccessGroup::findOne(['pengadaan_id' => $model->id, 'access_group_id' => $accessGroup->id]) === null) {
                        $modelPengadaanAccessGroup = new PengadaanAccessGroup();
                        $modelPengadaanAccessGroup->pengadaan_id = $model->id;
                        $modelPengadaanAccessGroup->access_group_id = $accessGroup->id;
                        $modelPengadaanAccessGroup->save();
                    }
                } else {
                    PengadaanAccessGroup::deleteAll(['pengadaan_id' => $model->id, 'access_group_id' => $accessGroup->id]);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $alerts = Yii::$app->session->getFlash('alerts');

        /*$query = PengadaanPo::find()->join("left join","sap_po","pengadaan_po.purchasing_document = sap_po.`purchasing document`");
        $dataProviderPengadaanPo = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);*/

        $searchModelPengadaanKorin      = new PengadaanKorinSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanKorinSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanKorin     = $searchModelPengadaanKorin->search(Yii::$app->request->queryParams);



        $searchModelPengadaanPo         = new PengadaanPoSearch();
        $queryParams = Yii::$app->request->queryParams;

        if (!isset($queryParams['PengadaanPoSearch'])) $queryParams['PengadaanPoSearch'] = array();
        $queryParams['PengadaanPoSearch'] = array_merge($queryParams['PengadaanPoSearch'],
            ['pengadaan_id' => $id]
        );
        $dataProviderPengadaanPo = $searchModelPengadaanPo->search($queryParams);
        $dataProviderPengadaanPo->pagination->defaultPageSize = 5;



        $searchModelPengadaanPrItem     = new PengadaanPrItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanPrItemSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanPrItem    = $searchModelPengadaanPrItem->search(Yii::$app->request->queryParams);

        $searchModelPengadaanDokumenNotrequired    = new PengadaanDokumenNotrequiredSearch();
        Yii::$app->request->queryParams            = array_merge(Yii::$app->request->queryParams, ['PengadaanDokumenNotrequiredSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanDokumenNotrequired   = $searchModelPengadaanDokumenNotrequired->search(Yii::$app->request->queryParams);

        $searchModelPengadaanIssue      = new PengadaanIssueSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanIssueSearch' => ['pengadaanActivity.pengadaan_id' => $id]]);
        $dataProviderPengadaanIssue     = $searchModelPengadaanIssue->search(Yii::$app->request->queryParams);

        $searchModelPengadaanItem       = new PengadaanItemSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanItemSearch' => ['pengadaan_id' => $id]]);
        $dataProviderPengadaanItem      = $searchModelPengadaanItem->search(Yii::$app->request->queryParams);

        $searchModelEvent               = new EventSearch();
        Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['EventSearch' => ['pengadaan_id' => $id]]);
        $dataProviderEvent              = $searchModelEvent->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'alerts' => $alerts,

            'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
            'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,

            'searchModelPengadaanPo' => $searchModelPengadaanPo,
            'dataProviderPengadaanPo' => $dataProviderPengadaanPo,

            'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
            'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,

            'searchModelPengadaanDokumenNotrequired' => $searchModelPengadaanDokumenNotrequired,
            'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,

            'searchModelPengadaanIssue' => $searchModelPengadaanIssue,
            'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,

            'searchModelPengadaanItem' => $searchModelPengadaanItem,
            'dataProviderPengadaanItem' => $dataProviderPengadaanItem,

            'searchModelEvent' => $searchModelEvent,
            'dataProviderEvent' => $dataProviderEvent,
        ]);
    }

    /**
     * Creates a new Pengadaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pengadaan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pengadaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pengadaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }




    public function actionAdjust($id)
    {
        $model = Pengadaan::findOne($id);
        $modelMetodaActivity = MetodaActivity::find()->where("metoda_id='".$model->metoda_id."'")->orderBy("urutan ASC")->all();
        if (!$model->level_approval_id) $model->level_approval_id = 1;

        if(Yii::$app->request->post())
        {
            $durasi         = Yii::$app->request->post('durasi');
            $activity_id    = Yii::$app->request->post('activity_id');
            PengadaanDurasiAdjusted::deleteAll("pengadaan_id = '".$model->id."'");
            for ($i=0; $i<count($durasi); $i++) {
                $modelPengadaanDurasiAdjusted = new PengadaanDurasiAdjusted();
                $modelPengadaanDurasiAdjusted->pengadaan_id = $model->id;
                $modelPengadaanDurasiAdjusted->activity_id  = $activity_id[$i];
                $modelPengadaanDurasiAdjusted->durasi       = $durasi[$i];
                $modelPengadaanDurasiAdjusted->save();
            }
            if (!PengadaanActivity::find()->where("pengadaan_id = '".$model->id."' and activity_id = '2'")->one()) {
                $modelPengadaanActivity = new PengadaanActivity();
                $modelPengadaanActivity->pengadaan_id    = $model->id;
                $modelPengadaanActivity->activity_id     = '2';
                $modelPengadaanActivity->start_date      = date('Y-m-d');
                $modelPengadaanActivity->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('adjust', [
                'model' => $model,
                'modelMetodaActivity' => $modelMetodaActivity,
            ]);
        }
    }








    //AJAX handler
    public function actionKorin() {        
        $get = Yii::$app->request->get();

        $model = new PengadaanKorin();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_id = $get['pengadaan_id'];
                $model->korin_id = $get['korin_id'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                PengadaanKorin::findOne($get['korin_id'])->delete();
            }

            $searchModelPengadaanKorin        = new PengadaanKorinSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanKorinSearch' => ['pengadaan_id' => $get['pengadaan_id']]]);
            $dataProviderPengadaanKorin       = $searchModelPengadaanKorin->search(Yii::$app->request->queryParams);

            return $this->renderAjax('korin', [
                'pengadaan_id' => $get['pengadaan_id'],
                'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
                'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,
            ]);
        }
    }

    //AJAX handler
    public function actionPurchaseorder() {
        $get = Yii::$app->request->get();

        $model = new PengadaanPo();

        //if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_id        = $get['pengadaan_id'];
                $model->purchasing_document = $get['po-purchasing_document'];
                $model->contract_reference  = $get['po-contract_reference'];
                $model->save();

                //simpan default harga sebelum diskon (=harga po)
                /*if ((SapPo::find()->where("`Purchasing Document`= '".$model->purchasing_document."'"))) {
                    $sapPo = SapPo::find()->where("`Purchasing Document`= '".$model->purchasing_document."'")->all();
                    $no = 0;
                    foreach ($sapPo as $row) {
                        $modelHSD = new HargaSebelumDiskon();
                        $modelHSD->purchasing_document = $row->{"Purchasing Document"};
                        $modelHSD->item_po = $row->{"Item"};
                        $modelHSD->harga = $row->{"Net price"} / $row->{"Price Unit"};
                        $modelHSD->save();
                    }
                }*/
            }

            if ($get['op'] == "delete")
            {
                $modelPengadaanPo = PengadaanPo::findOne($get['po_id']);
                HargaSebelumDiskon::deleteAll("purchasing_document = '".$modelPengadaanPo->purchasing_document."'");
                $modelPengadaanPo->delete();
            }

            $searchModelPengadaanPo        = new PengadaanPoSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanPoSearch' => ['pengadaan_id' => $get['pengadaan_id']]]);
            $dataProviderPengadaanPo       = $searchModelPengadaanPo->search(Yii::$app->request->queryParams);

            $model = Pengadaan::findOne($get['pengadaan_id']);
            return $this->renderAjax('purchase_order', [
                'pengadaan_id' => $get['pengadaan_id'],
                'searchModelPengadaanPo' => $searchModelPengadaanPo,
                'dataProviderPengadaanPo' => $dataProviderPengadaanPo,
                'model' => $model,
            ]);
        //}
    }

    function actionUpdateContractref() {
        $get = Yii::$app->request->get();

        if ($get['op'] == "updateContract")
        {
            $modelPengadaanPo = PengadaanPo::findOne($get['contract-reference_id']);
            $modelPengadaanPo->contract_reference = $get['contract-reference_contract_reference'];
            $modelPengadaanPo->save();
        }
    }

    //AJAX handler
    public function actionItempr() {
        $get = Yii::$app->request->get();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                if (isset($get['itemPr-select_all_item']) && $get['itemPr-select_all_item']=="on") {
                    $rows = (new \yii\db\Query())
                        ->from('sap_pr')
                        ->where(['Purchase Requisition' => $get['itemPr-purchase_requisition']])
                        ->all();

                    foreach ($rows as $row) {
                        $model = new PengadaanPrItem();
                        $model->pengadaan_id            = $get['pengadaan_id'];
                        $model->purchase_requisition    = $row['Purchase Requisition'];
                        $model->item_of_requisition     = $row['Item of Requisition'];
                        $model->oe_adjusted             = $row['Valuation Price']/$row['Price Unit'];
                        $model->save();
                    }
                } else {
                    if (isset($get['itemPr-item_of_requisition']) && !empty($get['itemPr-item_of_requisition'])) {
                        $model = new PengadaanPrItem();
                        $model->pengadaan_id            = $get['pengadaan_id'];
                        $model->purchase_requisition    = $get['itemPr-purchase_requisition'];
                        $model->item_of_requisition     = $get['itemPr-item_of_requisition'];
                        $model->save();
                    }
                }
            }

            if ($get['op'] == "delete")
            {
                $model = new PengadaanPrItem();
                PengadaanPrItem::findOne($get['pengadaan-pr-item_id'])->delete();
            }
            if ($get['op'] == "close")
            {
                $model = new PengadaanPrItem();
                $model = PengadaanPrItem::findOne($get['pengadaan-pr-item_id']);
                $model->closed = 1;
                $model->save();
            }
            if ($get['op'] == "continue")
            {
                $model = new PengadaanPrItem();
                $model = PengadaanPrItem::findOne($get['pengadaan-pr-item_id']);
                $model->closed = 0;
                $model->save();
            }

            $searchModelPengadaanPrItem     = new PengadaanPrItemSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanPrItemSearch' => ['pengadaan_id' => $get['pengadaan_id']]]);
            $dataProviderPengadaanPrItem    = $searchModelPengadaanPrItem->search(Yii::$app->request->queryParams);

            return $this->renderAjax('item_pr', [
                'pengadaan_id' => $get['pengadaan_id'],
                'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
                'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,
            ]);
        }
    }

    /*select2 ajax handler for no PR*/
    public function actionListpr($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select([
                    new \yii\db\Expression("`Purchase Requisition` as id"),
                    new \yii\db\Expression("`Purchase Requisition` as text")
                    ])
                ->from("sap_pr")
                ->where("`Purchase Requisition` LIKE '%".$q."%'")
                ->groupBy("`Purchase Requisition`")
                ->orderBy("`Purchase Requisition`")
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => $id];
        }
        return $out;
    }

    //AJAX handler
    public function actionOeadjusted() {
        $get = Yii::$app->request->get();

        //if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                if (isset($get['pengadaan-pr-item_id']) && !empty($get['pengadaan-pr-item_id'])) {
                    $model = PengadaanPrItem::findOne($get['pengadaan-pr-item_id']);
                    $model->oe_adjusted = $get['pengadaan-pr-item_oe_adjusted'];
                    $model->save();
                }
            }
            $alerts = Yii::$app->session->getFlash('alerts');
            return $alerts;
        //}
    }

    //AJAX handler
    public function actionHsdadjusted() {
        $get = Yii::$app->request->get();

        //if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                if (isset($get['harga-sebelum-diskon_id']) && !empty($get['harga-sebelum-diskon_id'])) {
                    $model = HargaSebelumDiskon::findOne($get['harga-sebelum-diskon_id']);
                    $model->harga = $get['harga-sebelum-diskon_harga'];
                    $model->save();
                } else {
                    $modelHSD = new HargaSebelumDiskon();
                    $modelHSD->purchasing_document = $get['harga-sebelum-diskon_po'];
                    $modelHSD->item_po             = $get['harga-sebelum-diskon_item_po'];
                    $modelHSD->harga               = $get['harga-sebelum-diskon_harga'];
                    $modelHSD->save();
                }
            }
            $alerts = Yii::$app->session->getFlash('alerts');
            return $alerts;
        //}
    }



    //AJAX handler
    public function actionPengadaanstartplan() {
        $get = Yii::$app->request->get();

        //if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                if (isset($get['pengadaan-start_plan']) && !empty($get['pengadaan-start_plan'])) {
                    $model = $this->findModel($get['pengadaan-startplan_pengadaan_id']);
                }
                $model->start_plan = $get['pengadaan-start_plan'];
                $model->save();
            }
            $alerts = Yii::$app->session->getFlash('alerts');
            return $this->renderAjax('activity_jadwal', [
                'model' => $this->findModel($get['pengadaan-startplan_pengadaan_id']),
                'alerts' => $alerts,
            ]);
        //}
    }

    //AJAX handler
    public function actionPengadaanactivity() {
        $get = Yii::$app->request->get();

        $model = new PengadaanActivity();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                if (isset($get['pengadaan-activity_id']) && !empty($get['pengadaan-activity_id'])) {
                    $model = PengadaanActivity::findOne($get['pengadaan-activity_id']);
                }
                // PengadaanActivity::find()->where("pengadaan_id = '".$get['pengadaan-activity_pengadaan_id']."' and activity_id = '".$get['pengadaan-activity_activity_id']."'")->delete();
                $model->pengadaan_id    = $get['pengadaan-activity_pengadaan_id'];
                $model->activity_id     = $get['pengadaan-activity_activity_id'];
                $model->start_date      = $get['pengadaan-activity_start_date'];
                $model->end_date        = $get['pengadaan-activity_end_date'];
                $model->save();

                /*$arr = array();
                $modelPengadaan = Pengadaan::findOne($model->pengadaan_id);
                foreach ($modelPengadaan->metoda->metodaActivities as $row) {
                    $durasiCurrent = PengadaanDurasiAdjusted::find()->where("pengadaan_id = '".$modelPengadaan->id."' and activity_id = '".$row->activity->id."'")->one()->durasi;

                    if ($row->activity->id <= $model->activity_id) next($arr);
                    if ($row->activity->id > $model->activity_id && $durasiCurrent == 0) next($arr);

                    $arr[] = $row->activity->id;
                }
                //next($arr);

                $dateNext = new \DateTime($model->end_date);
                $dateNext->add(new \DateInterval('P1D'));

                $modelNext = new PengadaanActivity();
                if (PengadaanActivity::find()->where("pengadaan_id='".$model->pengadaan_id."' and activity_id='".current($arr)."'")->one()) {
                    $modelNext = PengadaanActivity::find()->where("pengadaan_id='".$model->pengadaan_id."' and activity_id='".current($arr)."'")->one();
                }
                $modelNext->pengadaan_id    = $model->pengadaan_id;
                $modelNext->activity_id     = current($arr);
                $modelNext->start_date      = $dateNext->format('Y-m-d');
                $modelNext->end_date        = "";
                $modelNext->save();*/

            }
            if ($get['op'] == "delete")
            {
                $model = PengadaanActivity::findOne($get['pengadaan-activity_id']);
                //PengadaanActivity::deleteAll("activity_id > '".$model->activity_id."'");
                $connection = new \yii\db\Query();
                $connection->createCommand()
                    ->delete("pengadaan_activity", "activity_id > '".$model->activity_id."' and pengadaan_id = '".$model->pengadaan_id."'")
                    ->execute();
                $model->end_date = null;
                $model->save();
            }
            $alerts = Yii::$app->session->getFlash('alerts');
            return $this->renderAjax('activity_jadwal', [
                'model' => $this->findModel($get['pengadaan-activity_pengadaan_id']),
                'alerts' => $alerts,
            ]);
        }
    }

    //AJAX handler
    public function actionPengadaanactivitydokumen() {
        $get = Yii::$app->request->get();

        $model = new PengadaanActivityDokumen();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                $model->pengadaan_activity_id   = $get['pengadaan-activity-dokumen_pengadaan_activity_id'];
                $model->dokumen_upload_id       = $get['pengadaan-activity-dokumen_dokumen_upload_id'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                PengadaanActivityDokumen::findOne($get['pengadaan-activity-dokumen_id'])->delete();
            }

            return $this->renderAjax('activity_dokumen', [
                'pengadaanActivity_id' => $get['pengadaan-activity-dokumen_pengadaan_activity_id'],
            ]);
        }
    }

    //AJAX handler
    public function actionRenameFile() {
        $get = Yii::$app->request->get();

        $model      = DokumenUpload::findOne($get['pengadaan-activity-dokumen_dokumen_upload_id']);
        $oldname    = $model->pdf_filename;

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                $model->pdf_filename    = $get['pengadaan-activity-dokumen_pdf_filename'];
                $arr                    = explode("_", $model->pdf_filename);
                $model->no_dokumen      = end($arr);

                if (substr($model->no_dokumen, -4) == ".pdf") {
                    $arr                = explode(".", $model->no_dokumen);
                    $filetype           = array_pop($arr);
                    $model->no_dokumen  = implode(".", $arr);
                }
                if ($model->save()) {
                    rename(Yii::getAlias('@uploads/'.$oldname), Yii::getAlias('@uploads/'.$model->pdf_filename));
                }
            }
            return $this->renderAjax('activity_dokumen', [
                'pengadaanActivity_id' => $get['pengadaan-activity-dokumen_pengadaan_activity_id'],
            ]);
        }
    }

    //AJAX handler
    public function actionRenameFileUncategorized() {
        $get = Yii::$app->request->get();

        $model      = PengadaanFileUncategorized::findOne($get['pengadaan_file_uncategorized_id']);
        $oldname    = $model->filename;
        $no_dokumen = '';

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "save")
            {
                $model->filename    = $get['pengadaan_file_uncategorized_filename'];
                $arr                = explode("_", $model->filename);
                $no_dokumen         = end($arr);

                if (substr($no_dokumen, -4) == ".pdf") {
                    $arr         = explode(".", $no_dokumen);
                    $filetype    = array_pop($arr);
                    $no_dokumen  = implode(".", $arr);
                }
                if ($model->save()) {
                    rename(Yii::getAlias('@uploads/uncategorized/'.$oldname), Yii::getAlias('@uploads/uncategorized/'.$model->filename));
                }
            }
            return $this->redirect(['view',
                'id' => $model->pengadaan_id,
            ]);
        }
    }


    //AJAX handler
    public function actionPengadaandokumennotrequired() {
        $get = Yii::$app->request->get();

        $model = new PengadaanDokumenNotrequired();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_id        = $get['pengadaan_id'];
                $model->dokumen_jenis_id    = $get['dokumen_jenis_id'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                PengadaanDokumenNotrequired::findOne($get['id'])->delete();
            }

            $searchModelPengadaanDokumenNotrequired     = new PengadaanDokumenNotrequiredSearch();
            Yii::$app->request->queryParams             = array_merge(Yii::$app->request->queryParams, ['PengadaanDokumenNotrequiredSearch' => ['pengadaan_id' => $get['pengadaan_id']]]);
            $dataProviderPengadaanDokumenNotrequired    = $searchModelPengadaanDokumenNotrequired->search(Yii::$app->request->queryParams);

            return $this->renderAjax('pengaturan_dokumen', [
                'pengadaan_id' => $get['pengadaan_id'],
                'searchModelPengadaanDokumenNotrequired' => $searchModelPengadaanDokumenNotrequired,
                'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,
            ]);
        }
    }

    //AJAX handler
    public function actionPengadaanissue() {
        $get = Yii::$app->request->get();

        $model = new PengadaanIssue();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_activity_id   = $get['pengadaan-issue_pengadaan_activity_id'];
                $model->tanggal                 = $get['pengadaan-issue_tanggal'];
                $model->keterangan              = $get['pengadaan-issue_keterangan'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                PengadaanIssue::findOne($get['pengadaan-issue_id'])->delete();
            }

            $searchModelPengadaanIssue     = new PengadaanIssueSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanIssueSearch' => ['pengadaanActivity.pengadaan_id' => $get['pengadaan-issue_pengadaan_id']]]);
            $dataProviderPengadaanIssue    = $searchModelPengadaanIssue->search(Yii::$app->request->queryParams);

            return $this->renderAjax('today_status', [
                'pengadaan_id' => $get['pengadaan-issue_pengadaan_id'],
                'searchModelPengadaanIssue' => $searchModelPengadaanIssue,
                'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,
            ]);
        }
    }

    //AJAX handler
    public function actionEvent() {
        $get = Yii::$app->request->get();

        $model = new Event();

        //if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_id    = $get['event_pengadaan_id'];
                $model->activity_id     = $get['event_activity_id'];
                $model->tanggal         = $get['event_tanggal'];
                $model->jam             = $get['event_jam'];
                $model->tempat_id       = $get['event_tempat_id'];
                //$model->keterangan    = $get['event_keterangan'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                Event::findOne($get['event_id'])->delete();
            }

            $searchModelEvent               = new EventSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['EventSearch' => ['pengadaan_id' => $get['event_pengadaan_id']]]);
            $dataProviderEvent              = $searchModelEvent->search(Yii::$app->request->queryParams);

            return $this->renderAjax('event', [
                'pengadaan_id' => $get['event_pengadaan_id'],
                'searchModelEvent' => $searchModelEvent,
                'dataProviderEvent' => $dataProviderEvent,
            ]);
        //}
    }

    //AJAX handler
    public function actionPengadaanitem() {
        $get = Yii::$app->request->get();

        $model = new PengadaanItem();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create")
            {
                $model->pengadaan_id    = $get['pengadaan-item_pengadaan_id'];
                $model->nama            = $get['pengadaan-item_nama'];
                $model->quantity        = $get['pengadaan-item_quantity'];
                $model->satuan          = $get['pengadaan-item_satuan'];
                $model->keterangan      = $get['pengadaan-item_keterangan'];
                $model->save();
            }
            if ($get['op'] == "delete")
            {
                PengadaanItem::findOne($get['pengadaan-item_id'])->delete();
            }

            $searchModelPengadaanItem     = new PengadaanItemSearch();
            Yii::$app->request->queryParams = array_merge(Yii::$app->request->queryParams, ['PengadaanItemSearch' => ['pengadaan_id' => $get['pengadaan-item_pengadaan_id']]]);
            $dataProviderPengadaanItem    = $searchModelPengadaanItem->search(Yii::$app->request->queryParams);

            return $this->renderAjax('pengadaan-item', [
                'pengadaan_id' => $get['pengadaan-item_pengadaan_id'],
                'searchModelPengadaanItem' => $searchModelPengadaanItem,
                'dataProviderPengadaanItem' => $dataProviderPengadaanItem,
            ]);
        }
    }

    //AJAX handler
    public function actionDocumentsverification() {
        $get = Yii::$app->request->get();

        $model = Pengadaan::findOne($get['pengadaan_id']);

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "verify")
            {
                $model->id                      = $get['pengadaan_id'];
                $model->documents_verification  = 1;
                $model->save();
                return $this->renderAjax('documents_verification', [
                    'dv' => 1
                ]);
            }
            if ($get['op'] == "unverify")
            {
                $model->id                      = $get['pengadaan_id'];
                $model->documents_verification  = 0;
                $model->save();
                return $this->renderAjax('documents_verification', [
                    'dv' => 0
                ]);
            }
        }
    }

    public function actionVerifiedByArsiparis($id)
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_arsiparis = $model->verified_by_arsiparis == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionVerifiedByPurchaser($id)
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_purchaser = $model->verified_by_purchaser == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionVerifiedByManager($id)
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_manager = $model->verified_by_manager == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionCreateZip($id) 
    {
        $model = Pengadaan::findOne($id);
        $model->createZip();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDeleteUncategorized($id) 
    {
        $model = PengadaanFileUncategorized::findOne($id);
        $model->delete();
        $path = Yii::getAlias('@uploads/uncategorized/'.$model->filename);
        if (file_exists($path)) unlink ($path);
        return $this->redirect(['view', 'id' => $model->pengadaan_id]);
    }
}
