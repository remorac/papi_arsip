<?php

namespace frontend\controllers;

use Yii;
use frontend\models\LegalPo;
use frontend\models\LegalPoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * LegalPoController implements the CRUD actions for LegalPo model.
 */
class LegalPoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LegalPo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LegalPoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LegalPo model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LegalPo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ($post = Yii::$app->request->post()) {
            if (($packageFile = UploadedFile::getInstanceByName('package-file')) !== null) {
                LegalPo::deleteAll();
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($packageFile->tempName);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    $i = 0;
                    foreach ($sheet->getRowIterator() as $row) {
                        if ($i > 0) {
                            if (($model = LegalPo::findOne(['Purchase_Order' => strval($row[2]), 'No_Kontrak' => strval($row[3])])) === null) {
                                $model = new LegalPo();
                            }
                            $model->Kode_Klasifikasi        = strval($row[1]);
                            $model->Purchase_Order          = strval($row[2]);
                            $model->No_Kontrak              = str_replace(' ', '', strval($row[3]));
                            $model->Isi_Berkas              = strval($row[4]);
                            $model->Tahun                   = strval($row[5]);
                            $model->Vendor                  = strval($row[6]);
                            $model->Box                     = strval($row[7]);
                            $model->Lokasi                  = strval($row[8]);
                            $model->filename                = str_replace('/', '-', $model->No_Kontrak).'.pdf';
                            
                            if (!$model->save()) {
                                print '<pre>'; print_r($model->getErrors()); die();
                            }
                        }
                        $i++;
                    }
                }
                $reader->close();
                return $this->redirect(['index']);
            }
        }            
        return $this->render('create');
    }

    /**
     * Updates an existing LegalPo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->No_Kontrak]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LegalPo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the LegalPo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LegalPo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LegalPo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
