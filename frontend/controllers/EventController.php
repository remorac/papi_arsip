<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Event;
use frontend\models\EventSearch;
use frontend\models\HariLibur;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post())){
            if($model->save()) {

                $origin = Yii::$app->request->get('orig');

                if(!empty($origin)){
                    Yii::$app->session->addFlash('alerts', ['class' => 'alert-success', 'body' => 'Event "'.$model->activity->nama . ': ' . $model->pengadaan->nama . ' (' . $model->pengadaan->barangJasa->nama .') : ' . $model->tanggal.'" telah berhasil dibuat']);
                    Yii::$app->session->setFlash('goto', 'c6');
                    return $this->redirect($origin);
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
            $alerts = Yii::$app->session->getFlash('alerts');
            $alerts[] = ['class' => 'alert-danger', 'body' => 'Event gagal disimpan'];

            $pengadaanModel = new \frontend\models\Pengadaan();
            $pengadaanSelect2Items = $pengadaanModel->getSelect2Items();

            $tempatModel = new \frontend\models\Tempat();
            $tempatSelect2Items = $tempatModel->getSelect2Items();

            return $this->render('create', [
                'model' => $model,
                'pengadaanSelect2Items' => $pengadaanSelect2Items,
                'tempatSelect2Items' => $tempatSelect2Items,
                'alerts' => $alerts,
            ]);

        } else {

            if(Yii::$app->request->get('pengadaan') != null){
                $model->pengadaan_id = Yii::$app->request->get('pengadaan');
            }
            if(Yii::$app->request->get('activity') != null){
                $model->activity_id = Yii::$app->request->get('activity');
            }
            $pengadaanModel = new \frontend\models\Pengadaan();
            $pengadaanSelect2Items = $pengadaanModel->getSelect2Items();
            
            $tempatModel = new \frontend\models\Tempat();
            $tempatSelect2Items = $tempatModel->getSelect2Items();

            $alerts = Yii::$app->session->getFlash('alerts');

            return $this->render('create', [
                'model' => $model,
                'pengadaanSelect2Items' => $pengadaanSelect2Items,
                'tempatSelect2Items' => $tempatSelect2Items,
                'alerts' => $alerts,
            ]);
        }
    }

    function insertGoogleCalendarEvent(\DateTime $startTime, \DateInterval $interval, $summary, $location)
    {
        $gcalToken = \frontend\models\GcalToken::findOne(1);
            //->where(['selected' => 1])
            //->one();
        //var_dump($gcalToken);
        //die();
        $oauthClient = $gcalToken->oauthClient; 
        $clientId = $oauthClient->web->client_id;// Yii::$app->params['googleClientId'];
        $clientEmail = $gcalToken->client_email;// Yii::$app->params['googleClientEmail'];
        $clientSecret = $oauthClient->web->client_secret;// Yii::$app->params['googleClientSecret'];
        $clientRedirectUri = 'http://'. $_SERVER['SERVER_NAME'] . \yii\helpers\Url::to([Yii::$app->params['googleClientRedirectUri']]);
        $calendarPrivateKey = $gcalToken->calendar_private_key;// Yii::$app->params['googleCalendarPrivateKey'];
        $calendarOwner = $gcalToken->calendar_owner_email; // Yii::$app->params['googleCalendarOwner'];
        $clientScopes = ['https://www.googleapis.com/auth/calendar'];
        $developerKey = Yii::$app->params['googleServerApiKey'];
        $calendarId = $gcalToken->calendar_id; //Yii::$app->params['googleCalendarId'];
        $firstAccessToken = $gcalToken->firstAccessToken;
        $accessToken = $gcalToken->accessToken;

        if (strpos($clientId, "googleusercontent") == false
            || !strlen($clientEmail)
            || !strlen($calendarPrivateKey)) {
          echo missingServiceAccountDetailsWarning();
          exit;
        }

        $client = new \Google_Client();
        //$client->setApprovalPrompt ("auto");
        $client->setApprovalPrompt ("force");
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($clientRedirectUri);
        $client->setScopes($clientScopes);
        $client->setAccessType('offline');
        $session = new \yii\web\Session;

        $credentials = new \Google_Auth_AssertionCredentials(
            $clientEmail,
            $clientScopes,
            $calendarPrivateKey,
            $gcalToken->calendar_private_key_password,
            //'notasecret',                                 // Default P12 password
            'http://oauth.net/grant_type/jwt/1.0/bearer', // Default grant type
            //'http://oauth.net/grant_type/device/1.0/bearer', // Default grant type
            $calendarOwner

        );

        $client->setDeveloperKey($developerKey);

        $service = new \Google_Service_Calendar($client);
        $client->setApplicationName("PAPI");
        if(strpos($developerKey, "<") !== false) {
            echo missingApiKeyWarning();
            exit;
        }


        $client->setAssertionCredentials($credentials);

        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshToken($firstAccessToken->refresh_token);
            $gcalToken->access_token_json = $client->getAccessToken();
            $gcalToken->save();
        }

        $endTime = new \DateTime($startTime->format(\DateTime::ISO8601));
        $endTime->add($interval);

        $event = new \Google_Service_Calendar_Event(array(
            'summary' => $summary,
            'location' => $location,
            //'description' => 'A chance to hear more about Google\'s developer products.',
            'start' => array(
                'dateTime' => $startTime->format(\DateTime::ISO8601),
                'timeZone' => 'Asia/Jakarta',
                ),
            'end' => array(
                'dateTime' => date('Y-m-d') . 'T17:00:00-07:00',
                //'dateTime' => $endTime,
                'timeZone' => 'Asia/Jakarta',
                ),
            'recurrence' => array(
                //'RRULE:FREQ=DAILY;COUNT=2'
                ),
            'attendees' => array(
                //array('email' => 'wandriputra@gmail.com'),
                array('email' => 'habiburrahman.1430@gmail.com'),
                array('email' => 'ice.harisandy.pind6@gmail.com'),
                array('email' => 'piery.togap@gmail.com'),
                array('email' => 'indra.ginanjar@gmail.com'),
                //.array('email' => 'sbrin@example.com'),
                ),
            'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 24 * 60),
                    array('method' => 'popup', 'minutes' => 10),
                    ),
                ),
        ));

        $event = $service->events->insert($calendarId, $event);

    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $pengadaanModel = new \frontend\models\Pengadaan();
            $pengadaanSelect2Items = $pengadaanModel->getSelect2Items();

            $tempatModel = new \frontend\models\Tempat();
            $tempatSelect2Items = $tempatModel->getSelect2Items();
            $alerts = Yii::$app->session->getFlash('alerts');
            
            return $this->render('update', [
                'model' => $model,
                'pengadaanSelect2Items' => $pengadaanSelect2Items,
                'tempatSelect2Items' => $tempatSelect2Items,
                'alerts' => $alerts,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteSelected()
    {
        if(!Yii::$app->request->isDelete && isset(Yii::$app->request->bodyParams['selection'])){
            return;
        }
        $selection = Yii::$app->request->bodyParams['selection'];
        $activeRecords = Event::findAll(["id" => $selection]);
        foreach($activeRecords as $record){
            $record->delete();
        }

        //return $this->redirect(['index']);
    }
}
