<?php

namespace frontend\controllers;

use Yii;
use frontend\models\KorinKeluar;
use frontend\models\KorinKeluarSearch;
use frontend\models\KorinKeluarDokumen;
use frontend\models\KorinKeluarDokumenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * KorinKeluarController implements the CRUD actions for KorinKeluar model.
 */
class KorinKeluarController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KorinKeluar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KorinKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KorinKeluar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelKorinKeluarDokumen      = new KorinKeluarDokumenSearch();
        Yii::$app->request->queryParams     = array_merge(Yii::$app->request->queryParams, ['KorinKeluarDokumenSearch' => ['korin_keluar_id' => $id]]);
        $dataProviderKorinKeluarDokumen     = $searchModelKorinKeluarDokumen->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelKorinKeluarDokumen' => $searchModelKorinKeluarDokumen,
            'dataProviderKorinKeluarDokumen' => $dataProviderKorinKeluarDokumen,
        ]);
    }

    /**
     * Creates a new KorinKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KorinKeluar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KorinKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KorinKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        KorinKeluarDokumen::deleteAll(['korin_keluar_id' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KorinKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KorinKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KorinKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //AJAX handler
    public function actionDokumen() {
        $get = Yii::$app->request->get();
        
        $model = new KorinKeluarDokumen();

        if (Yii::$app->request->isAjax) {
            if ($get['op'] == "create") 
            {
                // $model->isNewRecord = true;
                $model->korin_keluar_id = $get['korin_keluar_id'];
                $model->dokumen_upload_id = $get['dokumen_upload_id'];
                $model->save();
            }
            if ($get['op'] == "delete") 
            {
                KorinKeluarDokumen::findOne($get['korin_keluar_dokumen_id'])->delete();
            }

            $searchModelKorinKeluarDokumen      = new KorinKeluarDokumenSearch();
            Yii::$app->request->queryParams     = array_merge(Yii::$app->request->queryParams, ['KorinKeluarDokumenSearch' => ['korin_keluar_id' => $get['korin_keluar_id']]]);
            $dataProviderKorinKeluarDokumen     = $searchModelKorinKeluarDokumen->search(Yii::$app->request->queryParams);
            return $this->renderAjax('dokumen', [
                'korin_keluar_id' => $get['korin_keluar_id'],
                'searchModelKorinKeluarDokumen' => $searchModelKorinKeluarDokumen,
                'dataProviderKorinKeluarDokumen' => $dataProviderKorinKeluarDokumen,
            ]);
        }
    }



    /*select2 ajax handler*/
    public function actionList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(["id as id", 
                    new \yii\db\Expression("CONCAT(
                        no_surat 
                        ,' : ', perihal) 
                        as text")])
                ->from("korin_keluar")
                ->where("no_surat LIKE '%".$q."%' or 
                        tanggal_surat LIKE '%".$q."%' or 
                        perihal LIKE '%".$q."%'
                        order by no_surat DESC")
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => KorinKeluar::find($id)->no_surat];
        }
        return $out;
    }

}
