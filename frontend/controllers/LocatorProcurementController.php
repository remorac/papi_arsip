<?php

namespace frontend\controllers;

use Yii;
use frontend\models\LocatorProcurement;
use frontend\models\LocatorProcurementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * LocatorProcurementController implements the CRUD actions for LocatorProcurement model.
 */
class LocatorProcurementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LocatorProcurement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocatorProcurementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LocatorProcurement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LocatorProcurement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if ($post = Yii::$app->request->post()) {
            if (($packageFile = UploadedFile::getInstanceByName('package-file')) !== null) {
                LocatorProcurement::deleteAll();
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($packageFile->tempName);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    $i = 0;
                    foreach ($sheet->getRowIterator() as $row) {
                        if ($i > 0) {
                            // if (($model = LocatorProcurement::findOne(['purchase_order' => strval($row[2]), 'nama_pengadaan' => strval($row[3])])) === null) {
                                $model = new LocatorProcurement();
                            // }
                            $model->kode_klasifikasi        = strval($row[1]);
                            $model->purchase_order          = strval($row[2]);
                            $model->nama_pengadaan          = strval($row[3]);
                            $model->tahun                   = strval($row[4]);
                            $model->box                     = strval($row[5]);
                            $model->locator                 = strval($row[6]);
                            
                            if (!$model->save()) {
                                print '<pre>'; print_r($model->getErrors()); die();
                            }
                        }
                        $i++;
                    }
                }
                $reader->close();
                return $this->redirect(['index']);
            }
        }            
        return $this->render('create');
    }

    /**
     * Updates an existing LocatorProcurement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LocatorProcurement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the LocatorProcurement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LocatorProcurement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LocatorProcurement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
