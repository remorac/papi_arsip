<?php

namespace frontend\controllers;

use Yii;
use frontend\models\KpiDurasiKuartalReport;
use frontend\models\KpiDurasiKuartalReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KpiDurasiKuartalReportController implements the CRUD actions for KpiDurasiKuartalReport model.
 */
class KpiDurasiKuartalReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KpiDurasiKuartalReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $kpiDurasiKuartalReport = new KpiDurasiKuartalReport();
        $kpiDurasiKuartalReport->refreshKpiDurasiKuartal();
        
        $searchModel = new KpiDurasiKuartalReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpiDurasiKuartalReport model.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return mixed
     */
    public function actionView($tahun_kuartal, $jenis)
    {
        return $this->render('view', [
            'model' => $this->findModel($tahun_kuartal, $jenis),
        ]);
    }

    /**
     * Creates a new KpiDurasiKuartalReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KpiDurasiKuartalReport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tahun_kuartal' => $model->tahun_kuartal, 'jenis' => $model->jenis]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KpiDurasiKuartalReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return mixed
     */
    public function actionUpdate($tahun_kuartal, $jenis)
    {
        $model = $this->findModel($tahun_kuartal, $jenis);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tahun_kuartal' => $model->tahun_kuartal, 'jenis' => $model->jenis]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KpiDurasiKuartalReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return mixed
     */
    public function actionDelete($tahun_kuartal, $jenis)
    {
        $this->findModel($tahun_kuartal, $jenis)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KpiDurasiKuartalReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $tahun_kuartal
     * @param integer $jenis
     * @return KpiDurasiKuartalReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tahun_kuartal, $jenis)
    {
        if (($model = KpiDurasiKuartalReport::findOne(['tahun_kuartal' => $tahun_kuartal, 'jenis' => $jenis])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
