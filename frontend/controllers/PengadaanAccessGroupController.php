<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PengadaanAccessGroup;
use frontend\models\PengadaanAccessGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;
use frontend\models\Pengadaan;

/**
 * PengadaanAccessGroupController implements the CRUD actions for PengadaanAccessGroup model.
 */
class PengadaanAccessGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengadaanAccessGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengadaanAccessGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PengadaanAccessGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PengadaanAccessGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PengadaanAccessGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PengadaanAccessGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PengadaanAccessGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the PengadaanAccessGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PengadaanAccessGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PengadaanAccessGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGenerate()
    {
        $pengadaans = Pengadaan::find()->all();
        foreach ($pengadaans as $pengadaan) {
            foreach ($pengadaan->zcps1013s as $zcps1013) {
                if ($zcps1013->WBS_Element) {
                    if (substr($zcps1013->WBS_Element, -2, 1) == '-') { 
                        $area_code = substr($zcps1013->WBS_Element, -1);
                        $access_group_id = '';
                        if ($area_code == 'C') $access_group_id = 1;
                        if ($area_code == 'E') $access_group_id = 2;
                        if ($area_code == 'M') $access_group_id = 3;
                        if ($access_group_id) {
                            if (($pengadaan_access_group = PengadaanAccessGroup::findOne([
                                'pengadaan_id' => $pengadaan->id,
                                'access_group_id' => $access_group_id,
                            ])) === null) {
                                $pengadaan_access_group = new PengadaanAccessGroup();
                            }
                            $pengadaan_access_group->pengadaan_id = $pengadaan->id;
                            $pengadaan_access_group->access_group_id = $access_group_id;
                            $pengadaan_access_group->save();
                        }
                    }
                }
            }
        }
        return $this->redirect(['index']);
    }
}
