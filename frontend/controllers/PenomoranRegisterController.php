<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PenomoranRegister;
use frontend\models\PenomoranRegisterSearch;
use frontend\models\PenomoranJenisSub;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PenomoranRegisterController implements the CRUD actions for PenomoranRegister model.
 */
class PenomoranRegisterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PenomoranRegister models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PenomoranRegisterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PenomoranRegister model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PenomoranRegister model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();

        $model = new PenomoranRegister();

        if ($model->load(Yii::$app->request->post())) {
            $nomor_old = 0;
            $nomor_new = 0;
            $nomor_sub = "";
            $ext_nomor = false;

            if (!empty($post['num_setter']) && is_numeric($post['num_setter'])) {
                $nomor_new = $post['num_setter'];
            } else {
                if (is_object(PenomoranRegister::find()
                    ->where("penomoran_jenis_id = '".$model->penomoran_jenis_id."' and right(suffix, 2)='".date('y')."'")
                    ->orderBy("nomor_dokumen DESC")
                    ->one()))
                {
                    $nomor_old = PenomoranRegister::find()
                        ->where("penomoran_jenis_id = '".$model->penomoran_jenis_id."' and right(suffix, 2)='".date('y')."'")
                        ->orderBy("nomor_dokumen DESC")
                        ->one()
                        ->nomor_dokumen;
                }
            }

            if ($model->tanggal_dokumen < date('Y-m-d')) {
                //getOldMaxNum
                if (is_object(PenomoranRegister::find()
                    ->where("tanggal_dokumen <= '".$model->tanggal_dokumen."' and penomoran_jenis_id = '".$model->penomoran_jenis_id."'")
                    ->andWhere("year(tanggal_dokumen) = year('".$model->tanggal_dokumen."')")
                    ->orderBy("nomor_dokumen DESC")
                    ->one()))
                {
                    $pre_nomor_old = PenomoranRegister::find()
                        ->where("tanggal_dokumen <= '".$model->tanggal_dokumen."' and penomoran_jenis_id = '".$model->penomoran_jenis_id."'")
                        ->andWhere("year(tanggal_dokumen) = year('".$model->tanggal_dokumen."')")
                        ->orderBy("nomor_dokumen DESC")
                        ->one()->nomor_dokumen;
                    if ($pre_nomor_old != $nomor_old) {
                        $ext_nomor = true;
                        $nomor_old = $pre_nomor_old;

                        $nomor_sub = "";
                        if (is_object(PenomoranRegister::find()
                            ->where("tanggal_dokumen = '".$model->tanggal_dokumen."' and penomoran_jenis_id = '".$model->penomoran_jenis_id."'")
                            ->andWhere("year(tanggal_dokumen) = year('".$model->tanggal_dokumen."')")
                            ->orderBy("sub_nomor DESC")
                            ->one())) {
                            $nomor_sub = PenomoranRegister::find()
                                ->where("tanggal_dokumen = '".$model->tanggal_dokumen."' and penomoran_jenis_id = '".$model->penomoran_jenis_id."'")
                                ->andWhere("year(tanggal_dokumen) = year('".$model->tanggal_dokumen."')")
                                ->orderBy("sub_nomor DESC")
                                ->one()->sub_nomor;
                        }
                        if ($nomor_sub == "") $nomor_sub = "a";
                        else $nomor_sub++;
                    }
                }
            }

            //setNewNums
            if ($ext_nomor) $nomor_new = $nomor_old;
            else $nomor_new = $nomor_old+1;

            $model->nomor_dokumen   = $nomor_new;
            $model->sub_nomor       = $nomor_sub;
            $model->suffix          = "/".$model->penomoranJenis->kode."/PIND6/PRO10/".date("m.y", strtotime($model->tanggal_dokumen));
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PenomoranRegister model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PenomoranRegister model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PenomoranRegister model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PenomoranRegister the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PenomoranRegister::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    //AJAX handler
    public function actionChexist() {   
        $get = Yii::$app->request->get();

        if (Yii::$app->request->isAjax) {
            $penomoran_jenis_id = $get['penomoran_jenis_id'];
            $count = PenomoranRegister::find()
                ->where("penomoran_jenis_id = '".$penomoran_jenis_id."' and right(suffix, 2)='".date('y')."'")
                ->count();
            return $count;
        }
    }

    //AJAX handler
    public function actionSubjenis() {
        $get = Yii::$app->request->get();

        if (Yii::$app->request->isAjax) {
            $opts = "<option value=''>-</option>";
            $penomoran_jenis_id = $get['penomoran_jenis_id'];

            if ($subjenis = PenomoranJenisSub::find()
                    ->where("penomoran_jenis_id = '".$penomoran_jenis_id."'")
                    ->all())
            {
                foreach ($subjenis as $row) {
                    $opts.= "<option value=".$row->id.">".$row->nama."</option>";
                }
            }
            echo $opts;
        }
    }

}
