<?php

namespace frontend\controllers;

use Yii;

use frontend\models\Pengadaan;
use frontend\models\PengadaanSearchArsip;
use frontend\models\MetodaActivity;
use frontend\models\DurasiStandar;
use frontend\models\PengadaanDurasiAdjusted;
use frontend\models\PengadaanKorin;
use frontend\models\PengadaanKorinSearch;
use frontend\models\PengadaanPo;
use frontend\models\PengadaanPoSearch;
use frontend\models\PengadaanPrItem;
use frontend\models\PengadaanPrItemSearch;
use frontend\models\PengadaanActivity;
use frontend\models\PengadaanActivityDokumen;
use frontend\models\PengadaanDokumenNotrequired;
use frontend\models\PengadaanDokumenNotrequiredSearch;
use frontend\models\PengadaanIssue;
use frontend\models\PengadaanIssueSearch;
use frontend\models\PengadaanItem;
use frontend\models\PengadaanItemSearch;
use frontend\models\Event;
use frontend\models\EventSearch;
use frontend\models\SapPo;
use frontend\models\HargaSebelumDiskon;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SemuaPengadaanController implements the CRUD actions for Pengadaan model.
 */
class SemuaPengadaanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengadaanSearchArsip();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all doubled Pengadaan models.
     * @return mixed
     */
    public function actionUndoubler()
    {
        return $this->render('undoubles');
    }

    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $alerts = Yii::$app->session->getFlash('alerts');

        $query = PengadaanPo::find()->join("left join","sap_po","pengadaan_po.purchasing_document = sap_po.`purchasing document`");
        $dataProviderPengadaanPo = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        
        $searchModelPengadaanKorin      = new PengadaanKorinSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanKorinSearch'])) $queryParams['PengadaanKorinSearch'] = array();
        $queryParams['PengadaanKorinSearch'] = array_merge($queryParams['PengadaanKorinSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanKorin     = $searchModelPengadaanKorin->search($queryParams);
        
        $searchModelPengadaanPo = new PengadaanPoSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanPoSearch'])) $queryParams['PengadaanPoSearch'] = array();
        $queryParams['PengadaanPoSearch'] = array_merge($queryParams['PengadaanPoSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanPo = $searchModelPengadaanPo->search($queryParams);
        
        $searchModelPengadaanPrItem     = new PengadaanPrItemSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanPrItemSearch'])) $queryParams['PengadaanPrItemSearch'] = array();
        $queryParams['PengadaanPrItemSearch'] = array_merge($queryParams['PengadaanPrItemSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanPrItem    = $searchModelPengadaanPrItem->search($queryParams);
        
        $searchModelPengadaanDokumenNotrequired    = new PengadaanDokumenNotrequiredSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanDokumenNotrequiredSearch'])) $queryParams['PengadaanDokumenNotrequiredSearch'] = array();
        $queryParams['PengadaanDokumenNotrequiredSearch'] = array_merge($queryParams['PengadaanDokumenNotrequiredSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanDokumenNotrequired   = $searchModelPengadaanDokumenNotrequired->search($queryParams);
        
        $searchModelPengadaanIssue      = new PengadaanIssueSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanIssueSearch'])) $queryParams['PengadaanIssueSearch'] = array();
        $queryParams['PengadaanIssueSearch'] = array_merge($queryParams['PengadaanIssueSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanIssue     = $searchModelPengadaanIssue->search($queryParams);
        
        $searchModelPengadaanItem       = new PengadaanItemSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['PengadaanItemSearch'])) $queryParams['PengadaanItemSearch'] = array();
        $queryParams['PengadaanItemSearch'] = array_merge($queryParams['PengadaanItemSearch'], ['pengadaan_id' => $id,]);
        $dataProviderPengadaanItem      = $searchModelPengadaanItem->search($queryParams);
        
        $searchModelEvent               = new EventSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['EventSearch'])) $queryParams['EventSearch'] = array();
        $queryParams['EventSearch'] = array_merge($queryParams['EventSearch'], ['pengadaan_id' => $id,]);
        $dataProviderEvent              = $searchModelEvent->search($queryParams);

        return $this->render('view', [
            'alerts' => $alerts,
            'model' => $this->findModel($id),

            'searchModelPengadaanKorin' => $searchModelPengadaanKorin,
            'dataProviderPengadaanKorin' => $dataProviderPengadaanKorin,

            'searchModelPengadaanPo' => $searchModelPengadaanPo,
            'dataProviderPengadaanPo' => $dataProviderPengadaanPo,

            'searchModelPengadaanPrItem' => $searchModelPengadaanPrItem,
            'dataProviderPengadaanPrItem' => $dataProviderPengadaanPrItem,
            
            'searchModelPengadaanDokumenNotrequired' => $searchModelPengadaanDokumenNotrequired,
            'dataProviderPengadaanDokumenNotrequired' => $dataProviderPengadaanDokumenNotrequired,
            
            'searchModelPengadaanIssue' => $searchModelPengadaanIssue,
            'dataProviderPengadaanIssue' => $dataProviderPengadaanIssue,
            
            'searchModelPengadaanItem' => $searchModelPengadaanItem,
            'dataProviderPengadaanItem' => $dataProviderPengadaanItem,
            
            'searchModelEvent' => $searchModelEvent,
            'dataProviderEvent' => $dataProviderEvent,
        ]);
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionVerifiedByArsiparis($id) 
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_arsiparis = $model->verified_by_arsiparis == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionVerifiedByPurchaser($id) 
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_purchaser = $model->verified_by_purchaser == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionVerifiedByManager($id) 
    {
        $model = Pengadaan::findOne($id);
        $model->verified_by_manager = $model->verified_by_manager == 0 ? Yii::$app->user->id : 0;
        $model->save();
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionCreateZip($id) 
    {
        $model = Pengadaan::findOne($id);
        $model->createZip();
        return $this->redirect(['index', 'PengadaanSearchArsip[id]' => $id]);
    }

    public function actionDownloadZip($filename) 
    {
        $filepath = Yii::getAlias('@bundles/'.$filename);
        Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }

    public function actionDownloadSpreadsheet($id) 
    {
        $file = $id.".xls";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='.$file);

        return $this->renderPartial('resume', [
            'model' => Pengadaan::findOne($id),
        ]);
    }

    public function actionDownloadSpreadsheetAll() 
    {
        $file = "List_of_All_File.xls";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='.$file);

        return $this->renderPartial('resume-all');
    }
}
