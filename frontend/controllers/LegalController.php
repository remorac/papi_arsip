<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Legal;
use frontend\models\LegalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

/**
 * LegalController implements the CRUD actions for Legal model.
 */
class LegalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Legal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LegalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Legal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Legal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Legal();

        if ($model->load(Yii::$app->request->post())) {
            $model->pdfFiles = UploadedFile::getInstances($model, 'pdfFiles');
            // print '<pre>'; print_r($model->pdfFiles); die();
            
            if ($model->upload()) {
                $this->getAllFiles();
                $alert = [
                    'type' => 'success',
                    'message' => 'Files uploaded successfully.',
                ];
            } else {
                // print_r($model->getErrors()); die();
                $alert = [
                    'type' => 'error',
                    'message' => 'Uploading files failed.',
                ];
            }
            Yii::$app->session->setFlash($alert['type'], $alert['message']);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Legal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Legal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {            
            $model = $this->findModel($id);
            $model->delete();
            $path = Yii::getAlias('@uploads/legal/'.$model->filename);
            if (file_exists($path)) unlink ($path);
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Legal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Legal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Legal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }




    /**
     * Populate all Legal models.
     * @return mixed
     */
    public function actionPopulate()
    {
        Yii::$app->db->createCommand('truncate legal')->execute();
        $this->getAllFiles();
        
        return $this->redirect(['index']);
    }

    /**
     * Populate all Afis models.
     * @return mixed
     */
    public function actionPatch()
    {
        $this->getAllFiles();
        return $this->redirect(['index']);
    }

    /**
     * Populate all Afis models.
     * @return mixed
     */
    public function getAllFiles()
    {
        $files = new \DirectoryIterator(Yii::getAlias('@uploads/legal'));
        foreach ($files as $file) {
            if (!$file->isDot()) {
                // var_dump($file->getFilename());
                $model = new Legal();
                $model->filename    = $file->getFilename();

                if (Legal::findOne(['filename' => $model->filename]) === null) {
                    $model->save();
                }

                // print '<pre>'; print_r($model); print '</pre><br>';
            }
        }        
        return;
    }

    /**
     * Check orphaned files.
     * @return mixed
     */
    public function actionOrphaned()
    {
        $orphans = [];
        
        $files = new \DirectoryIterator(Yii::getAlias('@uploads/legal'));
        foreach ($files as $file) {
            if (!$file->isDot()) {
                $filename    = $file->getFilename();

                if (Legal::findOne(['filename' => $filename]) === null) {
                    $orphans[] = [
                        'filename' => $filename,
                    ];
                }

                // print '<pre>'; print_r($model); print '</pre><br>';
            }
        }
        
        return $this->render('orphan', [
            'orphans' => $orphans,
        ]);
    }

    public function actionDownload($filename) 
    {
        $filepath = Yii::getAlias('@uploads/legal/'.$filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
    }


    public function actionRename($filename, $id = "") 
    {
        $post = Yii::$app->request->post();
        $oldfile = Yii::getAlias('@uploads/legal/'.$filename);

        if ($post && $post['filename'] != $filename) {
            $filepath = Yii::getAlias('@uploads/legal/'.$post['filename']);
            if (file_exists($filepath)) {
                Yii::$app->session->setFlash('error', 'There is a file with same name. Try another name.');
            } else {
                if (file_exists($oldfile)) rename($oldfile, $filepath);
                if ($post['id']) {
                    $this->findModel($post['id'])->delete();
                }
                $this->getAllFiles();
                return $this->redirect(['index']);
            }
        }
        return $this->render('rename', [
            'filename' => $filename,
            'id' => $id,
        ]);        
    }


    public function actionRemove($filename, $id = "") 
    {
        $filepath = Yii::getAlias('@uploads/legal/'.$filename);
        if (file_exists($filepath)) unlink($filepath);
        if ($id) {
            try {
                $this->findModel($id)->delete();
                $this->getAllFiles();
            } catch (IntegrityException $e) {
                throw new \yii\web\HttpException(500,"Failed. This data can not be deleted.", 405);
            }
        }
        return $this->redirect(['orphaned']);
    }
}
