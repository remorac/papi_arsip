<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\PasswordForm;
use frontend\models\User;
use yii\base\InvalidParamException;
use yii\base\DynamicModel;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'changepassword', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'changepassword', 'index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->identity->username == 'deloitte') {
            return $this->redirect(['/auditor/index']);
        } else {
            $model = new \yii\base\DynamicModel([
                'keyword', 
                'flag_description',
                'flag_purchase_order',
                'flag_afis',
                'flag_legal',
                'flag_warehouse',
                'flag_logistic',
                'flag_material',
                'flag_hrga',
                'flag_utility',
                'flag_technical_adm',
            ]);
            $model->addRule(['keyword'], 'required')
                ->addRule([
                    'flag_description',
                    'flag_purchase_order',
                    'flag_afis',
                    'flag_legal',
                    'flag_warehouse',
                    'flag_logistic',
                    'flag_material',
                    'flag_hrga',
                    'flag_utility',
                    'flag_technical_adm',
                ], 'safe');

            if ($model->load(Yii::$app->request->get()) && $model->validate()) {
                return $this->render('search', [
                    'post' => $model,
                ]);
            } else {            
                $model->flag_description = true;
                $model->flag_purchase_order = false;
                $model->flag_afis = false;
                $model->flag_legal = false;
                $model->flag_warehouse = false;
                $model->flag_logistic = false;
                $model->flag_material = false;
                $model->flag_hrga = false;
                $model->flag_utility = false;
                $model->flag_technical_adm = false;

                return $this->render('index', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionOverview()
    {
        if (Yii::$app->user->identity->username == 'deloitte') {
            return $this->redirect(['/auditor/index']);
        } else {
            return $this->render('overview');
        }
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->can('deloitte')) {
                return $this->redirect(['/auditor/index']);
            } else {
                return $this->goBack();
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    /*public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }*/

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

     public function actionChangepassword(){

        $post = Yii::$app->request->post();

        $model = new PasswordForm;
        $modeluser = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
     
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password = $post['PasswordForm']['newpass'];
                    $modeluser->setPassword($modeluser->password);
                    $modeluser->generateAuthKey();
                    //print "<pre>"; print_r($modeluser);
                    //die();
                    if($modeluser->save(false)){
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed.'
                        );
                        return $this->redirect(['index']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed.'
                        );
                        return $this->render('changepassword',['model'=>$model]);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',['model'=>$model]);
                }
            }else{
                return $this->render('changepassword',['model'=>$model]);
            }
        }else{
            return $this->render('changepassword',['model'=>$model]);
        }
    }
}
