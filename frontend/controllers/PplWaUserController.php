<?php

namespace frontend\controllers;

use Yii;
use frontend\models\PplWaUser;
use frontend\models\PplWaUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PplWaUserController implements the CRUD actions for PplWaUser model.
 */
class PplWaUserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PplWaUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PplWaUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PplWaUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PplWaUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PplWaUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->uploadedFile = UploadedFile::getInstance($model, 'uploadedFile');

            if (is_object($model->uploadedFile)) {
                $model->file            = $model->uploadedFile->name;
                $filename               = "ppl/".$model->uploadedFile->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile->name." (".$i.").".$ext;
                    $model->file            = $model->uploadedFile->name." (".$i.").".$ext;
                }
                $model->uploadedFile->saveAs($filename);
            }
            $model->uploadedFile = null;
            if (!$model->save()) {print '<pre>'; print_r($model->getErrors()); die();}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PplWaUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {            
            $model->uploadedFile = UploadedFile::getInstance($model, 'uploadedFile');

            if (is_object($model->uploadedFile)) {
                $model->file            = $model->uploadedFile->name;
                $filename               = "ppl/".$model->uploadedFile->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "ppl/".$model->uploadedFile->name." (".$i.").".$ext;
                    $model->file            = $model->uploadedFile->name." (".$i.").".$ext;
                }
                $model->uploadedFile->saveAs($filename);
            }
            $model->uploadedFile = null;
            if (!$model->save()) {print '<pre>'; print_r($model->getErrors()); die();}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PplWaUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $file = Yii::getAlias('@webroot').'/ppl/'.$model->file;
        if (file_exists($file)) unlink($file);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PplWaUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PplWaUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PplWaUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
