<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SapPr;


/**
* SapPrController .
 */
class SapPrController extends \yii\web\Controller
{

    public function actionIndex()
    {
        //$model = new SapPr();
        $model = SapPr::find()->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
