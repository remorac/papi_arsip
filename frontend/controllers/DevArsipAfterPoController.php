<?php

namespace frontend\controllers;

use Yii;
use frontend\models\DevArsipAfterPo;
use frontend\models\DevArsipAfterPoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DevArsipAfterPoController implements the CRUD actions for DevArsipAfterPo model.
 */
class DevArsipAfterPoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DevArsipAfterPo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DevArsipAfterPoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DevArsipAfterPo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DevArsipAfterPo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DevArsipAfterPo();

        if ($model->load(Yii::$app->request->post())) {
            $model->dokumen = \yii\web\UploadedFile::getInstance($model, 'dokumen');
            
            if (is_object($model->dokumen)) {
                $model->pdf_filename    = $model->dokumen->name;
                $filename               = "document/".$model->dokumen->name;
                $i = 0;
            
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $filename               = "document/".$model->dokumen->name." (".$i.").".$ext;
                    $model->pdf_filename    = $model->dokumen->name." (".$i.").".$ext;
                }
                $model->dokumen->saveAs($filename);
            }
            
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DevArsipAfterPo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->dokumen = \yii\web\UploadedFile::getInstance($model, 'dokumen');
            $old_filename   = "document/".$model->pdf_filename;
        
            if (is_object($model->dokumen)) {
                $model->pdf_filename    = $model->dokumen->name;
                $filename               = "document/".$model->pdf_filename;
                $i                      = 0;
        
                while (file_exists($filename)) {
                    $i++;
                    $filename_sliced        = explode('.',$filename);
                    $ext                    = end($filename_sliced);
                    $model->pdf_filename    = $model->dokumen->name." (".$i.").".$ext;
                    $filename               = "document/".$model->pdf_filename;
                }
        
                $model->dokumen->saveAs($filename);
                if (is_file($old_filename)) unlink($old_filename);
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DevArsipAfterPo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $old_filename = "document/".$model->pdf_filename;
        if (is_file($old_filename)) unlink($old_filename);
        return $this->redirect(['index']);
    }

    /**
     * Finds the DevArsipAfterPo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DevArsipAfterPo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DevArsipAfterPo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
