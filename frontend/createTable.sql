-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `pengadaan_file_uncategorized`;
CREATE TABLE `pengadaan_file_uncategorized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengadaan_id` int(11) NOT NULL,
  `filename` text NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pengadaan_id` (`pengadaan_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `pengadaan_file_uncategorized_ibfk_1` FOREIGN KEY (`pengadaan_id`) REFERENCES `pengadaan` (`id`),
  CONSTRAINT `pengadaan_file_uncategorized_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `pengadaan_file_uncategorized_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-01-07 23:17:47

