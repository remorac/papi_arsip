<?php 

namespace frontend\rbac;

use yii\rbac\Rule;
use yii\web\NotFoundHttpException;
use frontend\models\PengadaanAccessGroup;
use frontend\models\UserAccessGroup;

/**
 * Checks if authorID matches user passed via params
 */
class ExternalProcurementRule extends Rule
{
    public $name = 'isExternalProcurement';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $get = \Yii::$app->request->get();
        if (isset($get['id'])) {
            $model = \frontend\models\Pengadaan::findOne($get['id']);
            if (is_object($model)) {
                $pengadaanAccessGroups = PengadaanAccessGroup::findAll(['pengadaan_id' => $get['id']]);
                foreach ($pengadaanAccessGroups as $pengadaanAccessGroup) {
                    if ((UserAccessGroup::findOne(['access_group_id' => $pengadaanAccessGroup->access_group_id])) !== null) {
                        return true;
                    }
                }
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return false;
    }
}