<?php 

namespace frontend\rbac;

use yii\rbac\Rule;
use yii\web\NotFoundHttpException;

/**
 * Checks if authorID matches user passed via params
 */
class PurchaserRule extends Rule
{
    public $name = 'isPurchaser';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $get = \Yii::$app->request->get();
        if (isset($get['id'])) {
            $model = \frontend\models\Pengadaan::findOne($get['id']);
            if (is_object($model)) {
                if ($model->created_by == $user || $model->updated_by == $user || $model->purchaserPersonelProcurement->personel_id == \Yii::$app->user->identity->personel_id) {
                    return true;
                }
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return false;
    }
}