<?php 

namespace frontend\rbac;

use yii\rbac\Rule;
use yii\web\NotFoundHttpException;
use frontend\models\PengadaanAccessGroup;

/**
 * Checks if authorID matches user passed via params
 */
class ProcurementCivilRule extends Rule
{
    public $name = 'isProcurementCivil';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $get = \Yii::$app->request->get();
        if (isset($get['id'])) {
            $model = \frontend\models\Pengadaan::findOne($get['id']);
            if (is_object($model)) {
                if (PengadaanAccessGroup::findOne(['pengadaan_id' => $get['id'], 'access_group_code' => 'C'])) {
                    return true;
                }
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return false;
    }
}