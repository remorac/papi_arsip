<?php

namespace backend\controllers;

use Yii;
use backend\models\Incoming;
use backend\models\IncomingPurchaseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

use backend\models\Config;
use backend\models\IncomingItem;

/**
 * IncomingPurchaseController implements the CRUD actions for Incoming model.
 */
class IncomingPurchaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-item' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Incoming models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IncomingPurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Incoming model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Incoming model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Incoming();
        $model->date = date('Y-m-d');

        if (Config::findOne(['key' => 'GlobalPaymentLimit'])->value != '0') {
            $model->due_date = date('Y-m-d', strtotime($model->date . ' +'.Config::findOne(['key' => 'GlobalPaymentLimit'])->value.' day'));
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $model->incoming_type_id = 1;
            if (Config::findOne(['key' => 'AutomaticPurchaseSerial'])->value == '1') {
                $model->serial = dechex(microtime(true)*10000);
            }
            if ($model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                print_r($model->getErrors());
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Incoming model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelItem = new IncomingItem();
        $modelItem->incoming_id = $id;

        if (($incoming_item_id = Yii::$app->request->get('incoming_item_id')) !== null) {
            $modelItem = IncomingItem::findOne($incoming_item_id);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } elseif ($modelItem->load(Yii::$app->request->post()) && $modelItem->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelItem' => $modelItem,
            ]);
        }
    }

    /**
     * Deletes an existing Incoming model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Deletes an existing Incoming Item model.
     * If deletion is successful, the browser will be redirected to the 'update' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteItem($id)
    {
        try {
            $model = IncomingItem::findOne($id);
            $model->delete();
            return $this->redirect(['update', 'id' => $model->incoming_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Incoming model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Incoming the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Incoming::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSimple() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
     
     
            $res = array(
                'body'    => date('Y-m-d H:i:s'),
                'success' => true,
            );
     
            return $res;
        }
    }

    public function actionGetPrice() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
     
            $response = [
                'price'   => (($price = \backend\models\Item::findOne(Yii::$app->request->post('item_id'))->default_price_buy) !== null) ? $price : 0,
                'success' => true,
            ];
     
            return $response;
        }
    }
}
