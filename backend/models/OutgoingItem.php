<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "outgoing_item".
 *
 * @property integer $id
 * @property integer $outgoing_id
 * @property integer $item_id
 * @property double $quantity
 * @property double $price
 * @property integer $discount_id
 * @property integer $is_taxable
 * @property integer $is_deleted
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Incoming[] $incomings
 * @property Outgoing $outgoing
 * @property Item $item
 * @property Discount $discount
 * @property User $createdBy
 * @property User $updatedBy
 * @property OutgoingPaymentBreakdown[] $outgoingPaymentBreakdowns
 * @property OutgoingPayment[] $outgoingPayments
 */
class OutgoingItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outgoing_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['outgoing_id', 'item_id', 'quantity', 'price'], 'required'],
            [['outgoing_id', 'item_id', 'discount_id', 'is_taxable', 'is_deleted', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['quantity', 'price'], 'number'],
            [['outgoing_id', 'item_id'], 'unique', 'targetAttribute' => ['outgoing_id', 'item_id'], 'message' => 'The combination of Outgoing and Item has already been taken.'],
            [['outgoing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Outgoing::className(), 'targetAttribute' => ['outgoing_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discount::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'outgoing_id' => Yii::t('app', 'Outgoing'),
            'item_id' => Yii::t('app', 'Item'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'discount_id' => Yii::t('app', 'Discount'),
            'is_taxable' => Yii::t('app', 'Is Taxable'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings()
    {
        return $this->hasMany(Incoming::className(), ['outgoing_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoing()
    {
        return $this->hasOne(Outgoing::className(), ['id' => 'outgoing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPaymentBreakdowns()
    {
        return $this->hasMany(OutgoingPaymentBreakdown::className(), ['outgoing_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPayments()
    {
        return $this->hasMany(OutgoingPayment::className(), ['id' => 'outgoing_payment_id'])->viaTable('outgoing_payment_breakdown', ['outgoing_item_id' => 'id']);
    }
}
