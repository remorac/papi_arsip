<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Category[] $categories
 * @property Category[] $categories0
 * @property Classification[] $classifications
 * @property Classification[] $classifications0
 * @property Config[] $configs
 * @property Config[] $configs0
 * @property Customer[] $customers
 * @property Customer[] $customers0
 * @property CustomerCategory[] $customerCategories
 * @property CustomerCategory[] $customerCategories0
 * @property DestinationTable[] $destinationTables
 * @property DestinationTable[] $destinationTables0
 * @property Discount[] $discounts
 * @property Discount[] $discounts0
 * @property Incoming[] $incomings
 * @property Incoming[] $incomings0
 * @property IncomingItem[] $incomingItems
 * @property IncomingItem[] $incomingItems0
 * @property IncomingPayment[] $incomingPayments
 * @property IncomingPayment[] $incomingPayments0
 * @property IncomingPaymentBreakdown[] $incomingPaymentBreakdowns
 * @property IncomingPaymentBreakdown[] $incomingPaymentBreakdowns0
 * @property IncomingType[] $incomingTypes
 * @property IncomingType[] $incomingTypes0
 * @property Item[] $items
 * @property Item[] $items0
 * @property ItemCategory[] $itemCategories
 * @property ItemCategory[] $itemCategories0
 * @property ItemLocation[] $itemLocations
 * @property ItemLocation[] $itemLocations0
 * @property Location[] $locations
 * @property Location[] $locations0
 * @property Outgoing[] $outgoings
 * @property Outgoing[] $outgoings0
 * @property OutgoingItem[] $outgoingItems
 * @property OutgoingItem[] $outgoingItems0
 * @property OutgoingPayment[] $outgoingPayments
 * @property OutgoingPayment[] $outgoingPayments0
 * @property OutgoingPaymentBreakdown[] $outgoingPaymentBreakdowns
 * @property OutgoingPaymentBreakdown[] $outgoingPaymentBreakdowns0
 * @property OutgoingType[] $outgoingTypes
 * @property OutgoingType[] $outgoingTypes0
 * @property PaymentType[] $paymentTypes
 * @property PaymentType[] $paymentTypes0
 * @property ReturnPlan[] $returnPlans
 * @property ReturnPlan[] $returnPlans0
 * @property Salesman[] $salesmen
 * @property Salesman[] $salesmen0
 * @property SalesmanCategory[] $salesmanCategories
 * @property SalesmanCategory[] $salesmanCategories0
 * @property Storage[] $storages
 * @property Storage[] $storages0
 * @property StorageCategory[] $storageCategories
 * @property StorageCategory[] $storageCategories0
 * @property Supplier[] $suppliers
 * @property Supplier[] $suppliers0
 * @property SupplierCategory[] $supplierCategories
 * @property SupplierCategory[] $supplierCategories0
 * @property UnitOfMeasurement[] $unitOfMeasurements
 * @property UnitOfMeasurement[] $unitOfMeasurements0
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories0()
    {
        return $this->hasMany(Category::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifications()
    {
        return $this->hasMany(Classification::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassifications0()
    {
        return $this->hasMany(Classification::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(Config::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs0()
    {
        return $this->hasMany(Config::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers0()
    {
        return $this->hasMany(Customer::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCategories()
    {
        return $this->hasMany(CustomerCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCategories0()
    {
        return $this->hasMany(CustomerCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationTables()
    {
        return $this->hasMany(DestinationTable::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationTables0()
    {
        return $this->hasMany(DestinationTable::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts0()
    {
        return $this->hasMany(Discount::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings()
    {
        return $this->hasMany(Incoming::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings0()
    {
        return $this->hasMany(Incoming::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingItems()
    {
        return $this->hasMany(IncomingItem::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingItems0()
    {
        return $this->hasMany(IncomingItem::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingPayments()
    {
        return $this->hasMany(IncomingPayment::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingPayments0()
    {
        return $this->hasMany(IncomingPayment::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingPaymentBreakdowns()
    {
        return $this->hasMany(IncomingPaymentBreakdown::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingPaymentBreakdowns0()
    {
        return $this->hasMany(IncomingPaymentBreakdown::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingTypes()
    {
        return $this->hasMany(IncomingType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingTypes0()
    {
        return $this->hasMany(IncomingType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems0()
    {
        return $this->hasMany(Item::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategories()
    {
        return $this->hasMany(ItemCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategories0()
    {
        return $this->hasMany(ItemCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemLocations()
    {
        return $this->hasMany(ItemLocation::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemLocations0()
    {
        return $this->hasMany(ItemLocation::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations0()
    {
        return $this->hasMany(Location::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoings()
    {
        return $this->hasMany(Outgoing::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoings0()
    {
        return $this->hasMany(Outgoing::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingItems()
    {
        return $this->hasMany(OutgoingItem::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingItems0()
    {
        return $this->hasMany(OutgoingItem::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPayments()
    {
        return $this->hasMany(OutgoingPayment::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPayments0()
    {
        return $this->hasMany(OutgoingPayment::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPaymentBreakdowns()
    {
        return $this->hasMany(OutgoingPaymentBreakdown::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPaymentBreakdowns0()
    {
        return $this->hasMany(OutgoingPaymentBreakdown::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingTypes()
    {
        return $this->hasMany(OutgoingType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingTypes0()
    {
        return $this->hasMany(OutgoingType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypes()
    {
        return $this->hasMany(PaymentType::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypes0()
    {
        return $this->hasMany(PaymentType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnPlans()
    {
        return $this->hasMany(ReturnPlan::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnPlans0()
    {
        return $this->hasMany(ReturnPlan::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesmen()
    {
        return $this->hasMany(Salesman::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesmen0()
    {
        return $this->hasMany(Salesman::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesmanCategories()
    {
        return $this->hasMany(SalesmanCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesmanCategories0()
    {
        return $this->hasMany(SalesmanCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages0()
    {
        return $this->hasMany(Storage::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageCategories()
    {
        return $this->hasMany(StorageCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageCategories0()
    {
        return $this->hasMany(StorageCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Supplier::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers0()
    {
        return $this->hasMany(Supplier::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierCategories()
    {
        return $this->hasMany(SupplierCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplierCategories0()
    {
        return $this->hasMany(SupplierCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitOfMeasurements()
    {
        return $this->hasMany(UnitOfMeasurement::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitOfMeasurements0()
    {
        return $this->hasMany(UnitOfMeasurement::className(), ['updated_by' => 'id']);
    }
}
