<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $unit_of_measurement_id
 * @property double $current_quantity
 * @property double $minimum_quantity
 * @property double $default_price_buy
 * @property double $default_price_sell
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IncomingItem[] $incomingItems
 * @property Incoming[] $incomings
 * @property User $createdBy
 * @property User $updatedBy
 * @property UnitOfMeasurement $unitOfMeasurement
 * @property ItemCategory[] $itemCategories
 * @property Category[] $categories
 * @property ItemLocation[] $itemLocations
 * @property OutgoingItem[] $outgoingItems
 * @property Outgoing[] $outgoings
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['unit_of_measurement_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['current_quantity', 'minimum_quantity', 'default_price_buy', 'default_price_sell'], 'number'],
            [['name'], 'string', 'max' => 191],
            [['name'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['unit_of_measurement_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitOfMeasurement::className(), 'targetAttribute' => ['unit_of_measurement_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'unit_of_measurement_id' => Yii::t('app', 'Unit Of Measurement'),
            'current_quantity' => Yii::t('app', 'Current Quantity'),
            'minimum_quantity' => Yii::t('app', 'Minimum Quantity'),
            'default_price_buy' => Yii::t('app', 'Default Price Buy'),
            'default_price_sell' => Yii::t('app', 'Default Price Sell'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingItems()
    {
        return $this->hasMany(IncomingItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings()
    {
        return $this->hasMany(Incoming::className(), ['id' => 'incoming_id'])->viaTable('incoming_item', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitOfMeasurement()
    {
        return $this->hasOne(UnitOfMeasurement::className(), ['id' => 'unit_of_measurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategories()
    {
        return $this->hasMany(ItemCategory::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('item_category', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemLocations()
    {
        return $this->hasMany(ItemLocation::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingItems()
    {
        return $this->hasMany(OutgoingItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoings()
    {
        return $this->hasMany(Outgoing::className(), ['id' => 'outgoing_id'])->viaTable('outgoing_item', ['item_id' => 'id']);
    }
}
