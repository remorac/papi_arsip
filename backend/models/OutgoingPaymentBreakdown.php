<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "outgoing_payment_breakdown".
 *
 * @property integer $id
 * @property integer $outgoing_payment_id
 * @property integer $outgoing_item_id
 * @property double $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property OutgoingPayment $outgoingPayment
 * @property OutgoingItem $outgoingItem
 * @property User $createdBy
 * @property User $updatedBy
 */
class OutgoingPaymentBreakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outgoing_payment_breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['outgoing_payment_id', 'outgoing_item_id', 'amount'], 'required'],
            [['outgoing_payment_id', 'outgoing_item_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['amount'], 'number'],
            [['outgoing_payment_id', 'outgoing_item_id'], 'unique', 'targetAttribute' => ['outgoing_payment_id', 'outgoing_item_id'], 'message' => 'The combination of Outgoing Payment and Outgoing Item has already been taken.'],
            [['outgoing_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => OutgoingPayment::className(), 'targetAttribute' => ['outgoing_payment_id' => 'id']],
            [['outgoing_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => OutgoingItem::className(), 'targetAttribute' => ['outgoing_item_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'outgoing_payment_id' => Yii::t('app', 'Outgoing Payment'),
            'outgoing_item_id' => Yii::t('app', 'Outgoing Item'),
            'amount' => Yii::t('app', 'Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPayment()
    {
        return $this->hasOne(OutgoingPayment::className(), ['id' => 'outgoing_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingItem()
    {
        return $this->hasOne(OutgoingItem::className(), ['id' => 'outgoing_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
