<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Incoming;

/**
 * IncomingSearch represents the model behind the search form about `backend\models\Incoming`.
 */
class IncomingSearch extends Incoming
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'incoming_type_id', 'supplier_id', 'storage_id', 'customer_id', 'return_plan_id', 'outgoing_item_id', 'salesman_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['serial', 'date', 'due_date', 'image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Incoming::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'incoming_type_id' => $this->incoming_type_id,
            'supplier_id' => $this->supplier_id,
            'storage_id' => $this->storage_id,
            'customer_id' => $this->customer_id,
            'return_plan_id' => $this->return_plan_id,
            'outgoing_item_id' => $this->outgoing_item_id,
            'salesman_id' => $this->salesman_id,
            'due_date' => $this->due_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
