<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "outgoing".
 *
 * @property integer $id
 * @property string $serial
 * @property string $date
 * @property string $due_date
 * @property integer $outgoing_type_id
 * @property integer $customer_id
 * @property integer $storage_id
 * @property integer $supplier_id
 * @property integer $return_plan_id
 * @property integer $incoming_item_id
 * @property integer $salesman_id
 * @property string $remark
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property OutgoingType $outgoingType
 * @property Customer $customer
 * @property Storage $storage
 * @property Supplier $supplier
 * @property ReturnPlan $returnPlan
 * @property IncomingItem $incomingItem
 * @property User $createdBy
 * @property User $updatedBy
 * @property Salesman $salesman
 * @property OutgoingItem[] $outgoingItems
 * @property Item[] $items
 * @property OutgoingPayment[] $outgoingPayments
 */
class Outgoing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outgoing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial', 'date', 'outgoing_type_id'], 'required'],
            [['date', 'due_date'], 'safe'],
            [['outgoing_type_id', 'customer_id', 'storage_id', 'supplier_id', 'return_plan_id', 'incoming_item_id', 'salesman_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['remark'], 'string'],
            [['serial'], 'string', 'max' => 191],
            [['serial'], 'unique'],
            [['outgoing_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OutgoingType::className(), 'targetAttribute' => ['outgoing_type_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['return_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReturnPlan::className(), 'targetAttribute' => ['return_plan_id' => 'id']],
            [['incoming_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => IncomingItem::className(), 'targetAttribute' => ['incoming_item_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['salesman_id'], 'exist', 'skipOnError' => true, 'targetClass' => Salesman::className(), 'targetAttribute' => ['salesman_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'serial' => Yii::t('app', 'Serial'),
            'date' => Yii::t('app', 'Date'),
            'due_date' => Yii::t('app', 'Due Date'),
            'outgoing_type_id' => Yii::t('app', 'Outgoing Type'),
            'customer_id' => Yii::t('app', 'Customer'),
            'storage_id' => Yii::t('app', 'Storage'),
            'supplier_id' => Yii::t('app', 'Supplier'),
            'return_plan_id' => Yii::t('app', 'Return Plan'),
            'incoming_item_id' => Yii::t('app', 'Incoming Item'),
            'salesman_id' => Yii::t('app', 'Salesman'),
            'remark' => Yii::t('app', 'Remark'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingType()
    {
        return $this->hasOne(OutgoingType::className(), ['id' => 'outgoing_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnPlan()
    {
        return $this->hasOne(ReturnPlan::className(), ['id' => 'return_plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingItem()
    {
        return $this->hasOne(IncomingItem::className(), ['id' => 'incoming_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesman()
    {
        return $this->hasOne(Salesman::className(), ['id' => 'salesman_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingItems()
    {
        return $this->hasMany(OutgoingItem::className(), ['outgoing_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('outgoing_item', ['outgoing_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingPayments()
    {
        return $this->hasMany(OutgoingPayment::className(), ['outgoing_id' => 'id']);
    }
}
