<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "incoming_payment_breakdown".
 *
 * @property integer $id
 * @property integer $incoming_payment_id
 * @property integer $incoming_item_id
 * @property double $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IncomingPayment $incomingPayment
 * @property IncomingItem $incomingItem
 * @property User $createdBy
 * @property User $updatedBy
 */
class IncomingPaymentBreakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incoming_payment_breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['incoming_payment_id', 'incoming_item_id', 'amount'], 'required'],
            [['incoming_payment_id', 'incoming_item_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['amount'], 'number'],
            [['incoming_payment_id', 'incoming_item_id'], 'unique', 'targetAttribute' => ['incoming_payment_id', 'incoming_item_id'], 'message' => 'The combination of Incoming Payment and Incoming Item has already been taken.'],
            [['incoming_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => IncomingPayment::className(), 'targetAttribute' => ['incoming_payment_id' => 'id']],
            [['incoming_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => IncomingItem::className(), 'targetAttribute' => ['incoming_item_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'incoming_payment_id' => Yii::t('app', 'Incoming Payment'),
            'incoming_item_id' => Yii::t('app', 'Incoming Item'),
            'amount' => Yii::t('app', 'Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingPayment()
    {
        return $this->hasOne(IncomingPayment::className(), ['id' => 'incoming_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingItem()
    {
        return $this->hasOne(IncomingItem::className(), ['id' => 'incoming_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
