var ajaxCallbacks = {
    "simpleDone" : function (response) {
        // This is called by the link attribute "data-on-done" => "simpleDone"
        console.dir(response);
        $("#ajax_result_01").html(response.body);
    },
    "deleteItemDone" : function (response) {
        console.dir(response);
        $("tr[data-key='"+response.id+"'").fadeOut();
    }
}

$(document).ready(function() {
	calculateSubtotal();

	$('.delete-item').click(toAjax);

	$('#incomingitem-item_id').on("change", function(e) { 
		console.log('value: '+this.value);
		$.post("?r=incoming-purchase/get-price", {item_id : this.value}, function(response) {
			$('#incomingitem-price').val(response.price);
			$('#incomingitem-price-disp').maskMoney('mask', response.price);
			$('#incomingitem-quantity').val('');
			calculateSubtotal();
			$('#incomingitem-quantity').focus();
		}, "json");
	});

	$('#incomingitem-item_id').on("select2:unselecting", function(e) { 
		$('#incomingitem-quantity').val(0).change();
		$('#incomingitem-price').val(0);
		$('#incomingitem-price-disp').maskMoney('mask', 0);
	});

	$('#incomingitem-quantity').on("change", function(e) {
		calculateSubtotal();
	});

	$('#incomingitem-quantity').on("keyup", function(e) {
		calculateSubtotal();
	});

	$('input[name="IncomingItem[is_taxable]"]').on("switchChange.bootstrapSwitch", function(e) {
		calculateSubtotal();
	});

	function calculateSubtotal() {
		price 		= $('#incomingitem-price').val();
		quantity 	= $('#incomingitem-quantity').val();
		//discount
		tax 		= $('#incomingitem-is_taxable').is(':checked');

		subtotal = (price * quantity) + (tax * (0.1 * (price * quantity)));
		$('#incomingitem-subtotal').val(subtotal);
		$('#incomingitem-subtotal-disp').maskMoney('mask', subtotal);
	}
});