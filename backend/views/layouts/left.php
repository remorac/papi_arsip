<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/luffy.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <div style="margin-bottom:10px"><?= Yii::$app->user->identity->username ?></div>

                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
                <a href="#"><i class="fa fa-envelope text-success"></i> <?= Yii::$app->user->identity->email ?></a>
            </div>
        </div>

        <div class="text-center" style="
            background: #eee; 
            border-radius: 4px; 
            border: 1px solid #eee; 
            margin:5px 10px 10px 10px;
            padding: 2px; 
            font-size: 5px;
        ">
            <!-- <i class="fa fa-ellipsis-h text-primary"></i> -->
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Users', 'icon' => 'user', 'url' => ['/user']],
                    ['label' => 'Items', 'icon' => 'th-list', 'url' => ['/item']],
                    ['label' => 'Unit of Measurements', 'icon' => 'th-list', 'url' => ['/unit-of-measurement']],
                    ['label' => 'Categories', 'icon' => 'th-list', 'url' => ['/category']],
                    ['label' => 'Classification', 'icon' => 'th-list', 'url' => ['/classification']],
                    ['label' => 'Incoming Type', 'icon' => 'th-list', 'url' => ['/incoming-type']],
                    [
                        'label' => 'Incoming',
                        'icon' => 'arrow-right',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Purchase', 'url' => ['/incoming-purchase/index']],
                            ['label' => 'Consignation', 'url' => ['/incoming-consignation/index']],
                            ['label' => 'Transfer', 'url' => ['/incoming-transfer/index']],
                            ['label' => 'Return', 'url' => ['/incoming-return/index']],
                            ['label' => 'Replace', 'url' => ['/incoming-replace/index']],
                            ['label' => 'Payment', 'url' => ['/incoming-payment/index']],
                        ],
                    ],
                    ['label' => 'Config', 'icon' => 'gear', 'url' => ['/config']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Same tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
