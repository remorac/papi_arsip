<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Incoming',
]) . $model->serial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incomings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serial, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="incoming-update box box-warning">

    <!-- <div class="box-header"></div> -->

    <div class="box-body">
	    <?= $this->render('_form-update', [
	        'model' => $model,
	        'modelItem' => $modelItem,
	    ]) ?>
    </div>

</div>
