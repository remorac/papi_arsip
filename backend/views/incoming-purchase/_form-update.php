<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\money\MaskMoney;
use kartik\grid\GridView;
use kartik\datecontrol\DateControl;
use backend\models\IncomingType;
use backend\models\Supplier;
use backend\models\Storage;
use backend\models\Customer;
use backend\models\ReturnPlan;
use backend\models\OutgoingItem;
use backend\models\Salesman;
use backend\models\IncomingItem;
use backend\models\Item;
use backend\models\Discount;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="incoming-form">

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" style="border-bottom: none">
            <h4 class="panel-title">
                <?= $model->serial . ' : <b>' . $model->supplier->name .'</b>, <small>'. $model->date .' - '. $model->due_date.'</small>' ?>
                <a class="pull-right" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <small><i class="glyphicon glyphicon-pencil"></i> Update</small>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body" style="background: #fdfdfd">
                
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

                <?php 
                    if (\backend\models\Config::findOne(['key' => 'AutomaticPurchaseSerial'])->value == '0') {
                        echo $form->field($model, 'serial')->textInput(['maxlength' => true]);
                    }
                ?>

                <?= $form->field($model, 'supplier_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Supplier::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ]); ?>

                <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATE,
                    'readonly' => true,
                    'ajaxConversion' => false,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ]); ?>

                <?= $form->field($model, 'due_date')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATE,
                    'readonly' => true,
                    'ajaxConversion' => false,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ]); ?>

                <?= $form->field($model, 'remark')->textarea(['rows' => '6']); ?>
                
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
    
    <div class="panel panel-default panel-body">
        <div class="row">
            <div class="col-sm-12">

                <?php $form = ActiveForm::begin(); ?>

                <table width="100%" class="table-form">
                    <tr>
                        <td width="">
                            <?= $form->field($modelItem, 'item_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Item::find()->all(), 'id', 'name'),
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]); ?>
                        </td>
                        <td width="10%">
                            <?= $form->field($modelItem, 'quantity')->textInput() ?>
                        </td>
                        <td width="15%">
                            <?= $form->field($modelItem, 'price')->widget(MaskMoney::classname()) ?>
                        </td>
                        <td width="10%">
                            <?= $form->field($modelItem, 'discount_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Discount::find()->all(), 'id', 'name'),
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]); ?>
                        </td>
                        <td width="92px">
                            <?= $form->field($modelItem, 'is_taxable')->widget(SwitchInput::classname(), [
                                'pluginOptions' => [
                                    'onText' => 'Yes',
                                    'offText' => 'No',
                                ]
                            ]); ?>
                        </td>
                        <td width="15%">
                            <label class="control-label">Subtotal</label>
                            <?= MaskMoney::widget([
                                'id' => 'incomingitem-subtotal',
                                'name' => 'incomingitem-subtotal',
                                'readonly' => true,
                            ]); ?>
                            <!-- <input type="text" readonly="" class="form-control" id="incomingitem-subtotal" /> -->
                        </td>
                        <td width="184px">
                            <label class="control-label">&nbsp;</label>
                            <?php 
                                if ($modelItem->isNewRecord) {
                                    echo Html::submitButton('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Add'), ['class' => 'btn btn-success btn-block']);      
                                } else {
                                    echo '<div class="input-group">';
                                    echo Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Update'), ['class' => 'btn btn-primary'])
                                    .' '. Html::a('<i class="glyphicon glyphicon-remove"></i> '. Yii::t('app', 'Cancel'), ['update', 'id' => $model->id], [
                                        'class' => 'btn btn-default',
                                    ]);
                                    echo "<div>";
                                }
                            ?>
                        </td>
                    </tr>
                </table>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php             
                    $dataProvider = new \yii\data\ActiveDataProvider([
                        'query' => IncomingItem::find()->where(['incoming_id' => $model->id])->orderBy('is_deleted'),
                        'pagination' => false,
                        'sort' => [
                            'defaultOrder' => [
                                'id' => SORT_DESC,
                            ]
                        ],
                    ]);
                
                    echo \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'pjax' => true,
                        'hover' => true,
                        'striped' => false,
                        'bordered' => false,
                        'toolbar'=> [
                            Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                            Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
                            '{toggleData}',
                            // $exportMenu,
                        ],
                        'panel' => false,
                        'pjaxSettings' => ['options' => ['id' => 'grid']],
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'headerOptions' => ['class' => 'text-right'],
                                'contentOptions' => ['class' => 'text-right'],
                            ],
                            // 'id',
                            [
                                'attribute' => 'item_id',
                                'value' => 'item.name',
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => ArrayHelper::map(Item::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                                'filterInputOptions'=>['placeholder'=>''],
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear'=>true],
                                ],
                            ],
                            [
                                'attribute' => 'quantity',
                                'format' => ['decimal', 2],
                                'headerOptions' => ['class' => 'text-right'],
                                'contentOptions' => ['class' => 'text-right'],
                            ],
                            [
                                'attribute' => 'price',
                                'format' => ['decimal', 2],
                                'headerOptions' => ['class' => 'text-right'],
                                'contentOptions' => ['class' => 'text-right'],
                            ],
                            [
                                'attribute' => 'discount_id',
                                'value' => 'discount.name',
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => ArrayHelper::map(Discount::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                                'filterInputOptions'=>['placeholder'=>''],
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear'=>true],
                                ],
                            ],
                            [
                                'attribute' => 'is_taxable',
                                'format' => 'raw',
                                'value' => function($data) {
                                    return $data->is_taxable == 1 ? '<i class="glyphicon glyphicon-ok text-success"></i>' : null;
                                }
                            ],
                            [
                                'attribute' => '',
                                'label' => 'Subtotal',
                                'format' => ['decimal', 2],
                                'headerOptions' => ['class' => 'text-right'],
                                'contentOptions' => ['class' => 'text-right'],
                                'value' => function($data) {
                                    return ($data->quantity * $data->price) + ($data->is_taxable * (0.1 * ($data->quantity * $data->price)));
                                }
                            ],
                            [
                                'contentOptions' => ['class' => 'action-column nowrap text-right'],
                                'attribute' => '',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return 
                                    Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $data->incoming_id, 'incoming_item_id' => $data->id], [
                                        'class' => 'btn btn-xs btn-default btn-text-warning',
                                    ])
                                    .' '.
                                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-item', 'id' => $data->id], [
                                        'class' => 'btn btn-xs btn-default btn-text-danger',
                                        'data-confirm' => 'Are you sure you want to delete this item?',
                                        'data-method' => 'post',
                                    ]);
                                }
                            ], 
                            // 'is_deleted:integer',
                            // 'created_at:integer',
                            // 'updated_at:integer',
                            // 'created_by:integer',
                            // 'updated_by:integer',
                        ],
                    ]);
                ?>
            </div>
        </div>
    </div>

    <div class="form-panel col-sm-12">
            <?= Html::a('<i class="glyphicon glyphicon-stop"></i> '. Yii::t('app', 'Done'), ['view', 'id' => $model->id], [
                'class' => 'btn btn-default btn-text-danger',
            ]) ?>
    </div>

</div>

<?php 
    $this->registerJsFile(
        '@web/js/incoming-purchase.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>