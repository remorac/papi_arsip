<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use backend\models\Item;
use backend\models\IncomingItem;
use backend\models\Discount;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */

$this->title = $model->serial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incomings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="incoming-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-print"></i> '. Yii::t('app', 'Print'), ['print', 'id' => $model->id], [
            'class' => 'btn btn-default',
        ]) ?>
        <i class="small pull-right text-muted">
            Created at <?= Yii::$app->formatter->asDatetime($model->created_at) ?> by <?= $model->createdBy->username ?><?php if ($model->created_at != $model->updated_at) { ?>, Updated at <?= Yii::$app->formatter->asDatetime($model->updated_at) ?> by <?= $model->updatedBy->username ?><?php } ?>.
        </i>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'serial',
                [
                    'attribute' => 'supplier_id',
                    'value' => '<big>' . $model->supplier->name . '</big><br><small>' . $model->supplier->address . '</small>',
                    'format' => 'raw',
                ],
                'date:date',
                'due_date:date',
                'remark:ntext',
                /*[
                    'label' => 'Created',
                    'value' => 'at ' . Yii::$app->formatter->asDatetime($model->created_at) . ' by ' . $model->createdBy->username,
                ],
                [
                    'label' => 'Updated',
                    'value' => 'at ' . Yii::$app->formatter->asDatetime($model->updated_at) . ' by ' . $model->updatedBy->username,
                ],*/
            ],
        ]) ?>
    </div>

    <div class="box-body">    
        <?php             
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => IncomingItem::find()->where(['incoming_id' => $model->id])->orderBy('is_deleted'),
                'pagination' => false,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        
            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                // 'pjax' => true,
                'hover' => true,
                'striped' => false,
                'bordered' => false,
                'panel' => false,
                'pjaxSettings' => ['options' => ['id' => 'grid']],
                // 'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    // 'id',
                    [
                        'attribute' => 'item_id',
                        'value' => 'item.name',
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Item::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                        'filterInputOptions'=>['placeholder'=>''],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear'=>true],
                        ],
                    ],
                    [
                        'attribute' => 'quantity',
                        'format' => ['decimal', 2],
                        'headerOptions' => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'attribute' => 'price',
                        'format' => ['decimal', 2],
                        'headerOptions' => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'attribute' => 'discount_id',
                        'value' => 'discount.name',
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(Discount::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                        'filterInputOptions'=>['placeholder'=>''],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear'=>true],
                        ],
                    ],
                    [
                        'attribute' => 'is_taxable',
                        'format' => 'raw',
                        'value' => function($data) {
                            return $data->is_taxable == 1 ? '<i class="glyphicon glyphicon-ok text-success"></i>' : null;
                        }
                    ],
                    [
                        'attribute' => '',
                        'label' => 'Subtotal',
                        'format' => ['decimal', 2],
                        'headerOptions' => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'value' => function($data) {
                            return ($data->quantity * $data->price) + ($data->is_taxable * (0.1 * ($data->quantity * $data->price)));
                        }
                    ],
                    // 'is_deleted:integer',
                    // 'created_at:integer',
                    // 'updated_at:integer',
                    // 'created_by:integer',
                    // 'updated_by:integer',
                ],
            ]);
        ?>
    </div>
</div>