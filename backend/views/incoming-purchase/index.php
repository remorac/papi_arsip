<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\IncomingType;
use backend\models\Supplier;
use backend\models\Storage;
use backend\models\Customer;
use backend\models\ReturnPlan;
use backend\models\OutgoingItem;
use backend\models\Salesman;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IncomingPurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Purchases');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="incoming-index box box-primary">
    <div class="box-body">

        <?php 
            $exportColumns = [
                [
                    'class' => 'yii\grid\SerialColumn',
                ],
                'id',
                'serial',
                'date:date',
                'incomingType.name:text:Incoming type',
                'supplier.name:text:Supplier',
                'due_date:date',
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ];

            $exportMenu = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $exportColumns,
                'filename' => 'Incomings',
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Export',
                    'class' => 'btn btn-default'
                ],
                'target' => ExportMenu::TARGET_SELF,
                'exportConfig' => [
                    ExportMenu::FORMAT_CSV => false,
                    ExportMenu::FORMAT_EXCEL => false,
                    ExportMenu::FORMAT_HTML => false,
                ],
                'styleOptions' => [
                    ExportMenu::FORMAT_EXCEL_X => [
                        'font' => [
                            'color' => ['argb' => '00000000'],
                        ],
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_NONE,
                            'color' => ['argb' => 'DDDDDDDD'],
                        ],
                    ],
                ],
                'pjaxContainerId' => 'grid',
            ]);

            $gridColumns = [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'contentOptions' => ['class' => 'action-column nowrap text-left'],
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url) {
                            return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
                        },
                        'update' => function ($url) {
                            return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
                        },
                        'delete' => function ($url) {
                            return Html::a('', $url, [
                                'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                                'data-method' => 'post', 
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')]);
                        },
                    ],
                ],
                // 'id',
                'serial',
                [
                    'attribute' => 'date',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterInputOptions' => ['placeholder' => ''],
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ],
                ],
                [
                    'attribute' => 'supplier_id',
                    'value' => 'supplier.name',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(Supplier::find()->orderBy('name')->asArray()->all(), 'id', 'name'), 
                    'filterInputOptions'=>['placeholder'=>''],
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear'=>true],
                    ],
                ],
                [
                    'attribute' => 'due_date',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterInputOptions' => ['placeholder' => ''],
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ],
                ],
                // 'created_at:integer',
                // 'updated_at:integer',
                // 'created_by:integer',
                // 'updated_by:integer',
            ];
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'pjax' => true,
            'hover' => true,
            'striped' => false,
            'bordered' => false,
            'toolbar'=> [
                Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
                '{toggleData}',
                // $exportMenu,
            ],
            'panel' => [
                'type' => 'no-border',
                'heading' => false,
                'before' => '{summary}',
                'after' => false,
            ],
            'panelBeforeTemplate' => '
                <div class="row">
                    <div class="col-sm-6">
                        <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                            {toolbar}
                        </div> 
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-right">
                            {before}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            ',
            'pjaxSettings' => ['options' => ['id' => 'grid']],
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
        ]); ?>

    </div>
</div>

<?php 
    
    echo Html::a('Click me', ['simple'], [
        'id' => 'ajax_link_01',
        'data-on-done' => 'simpleDone',
    ]);
    
    echo Html::tag('div', '...', ['id' => 'ajax_result_01']);
     
    $this->registerJs("$('#ajax_link_01').click(toAjax);", \yii\web\View::POS_END);


    $this->registerJs('
    var ajaxCallbacks = {
        "simpleDone" : function (response) {
            // This is called by the link attribute "data-on-done" => "simpleDone"
            console.dir(response);
            $("#ajax_result_01").html(response.body);
        }
    }
    ', \yii\web\View::POS_END);

?>