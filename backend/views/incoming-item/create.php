<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\IncomingItem */

$this->title = Yii::t('app', 'Create Incoming Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incoming Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incoming-item-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
