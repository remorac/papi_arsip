<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\IncomingItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incoming Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="incoming-item-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'incoming.name:text:Incoming',
                'item.name:text:Item',
                [
                    'attribute' => 'quantity',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'price',
                    'format' => ['decimal', 2],
                ],
                'discount.name:text:Discount',
                'is_taxable:integer',
                'is_deleted:integer',
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
    </div>
</div>
