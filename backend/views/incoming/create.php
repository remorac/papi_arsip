<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */

$this->title = Yii::t('app', 'Create Incoming');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incomings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incoming-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
