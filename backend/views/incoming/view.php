<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */

$this->title = $model->serial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incomings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="incoming-view box box-info">

    <div class="box-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'serial',
                'date',
                'incomingType.name:text:Incoming Type',
                'supplier.name:text:Supplier',
                'storage.name:text:Storage',
                'customer.name:text:Customer',
                'returnPlan.name:text:Return Plan',
                'outgoingItem.name:text:Outgoing Item',
                'salesman.name:text:Salesman',
                'due_date',
                'image:ntext',
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
    </div>
</div>
