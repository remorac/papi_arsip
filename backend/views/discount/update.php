<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Discount */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Discount',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="discount-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
