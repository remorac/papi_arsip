<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Discount */

$this->title = Yii::t('app', 'Create Discount');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
